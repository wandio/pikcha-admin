﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class OrdersByFilialsItem
    {
        public string Branch { get; set; }
        public int AbonentOrders { get; set; }
        public int PaidOrders { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }
    }
}
