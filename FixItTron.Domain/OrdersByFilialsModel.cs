﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class OrdersByFilialsModel
    {
        public List<OrdersByFilialsItem> Items { get; set; }

        public OrdersByFilialsModel()
        {
            Items = new List<OrdersByFilialsItem>();
        }
    }
}
