﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public bool IsFirst { get; set; }
        public string Description { get; set; }
    }
}
