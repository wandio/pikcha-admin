﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class StatisticsDataModel
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public decimal Value { get; set; }
        public IEnumerable<StatisticsDataSubModel> SubItems { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }
    }
}
