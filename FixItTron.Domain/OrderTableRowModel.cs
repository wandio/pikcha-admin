﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class OrderTableRowModel
    {
        public string OrderNumber { get; set; }

        public string OrderType { get; set; }

        public DateTime? RegistrationDate { get; set; }

        public DateTime? PerformanceDate { get; set; }

        public string Address { get; set; }

        public string Category { get; set; }

        public decimal Amount { get; set; }

        public decimal ServiceAmount { get; set; }

        public decimal MaterialAmount { get; set; }

        public decimal OtherAmount { get; set; }

        public string Status { get; set; }

        public string AccountingStatus { get; set; }
    }
}
