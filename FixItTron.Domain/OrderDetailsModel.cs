﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class OrderDetailsModel
    {
        public IEnumerable<ServiceListItem> Services { get; set; }

        public IEnumerable<MaterialListItem> Materials { get; set; }

        public string OrderNumber { get; set; }

        public DateTime? PerformanceDate { get; set; }

        public string Address { get; set; }

        public string OrderType { get; set; }

        public string OrderDescription { get; set; }

        public decimal SumServicesAndMaterials()
        {
            return Math.Round(Services.Sum(x => x.Amount) + Materials.Sum(x => x.Amount), 2);
        }

        public OrderDetailsModel()
        {
            Services = new List<ServiceListItem>();
            Materials = new List<MaterialListItem>();
        }
    }

    public class ServiceListItem
    {
        public string Description { get; set; }

        public string Dimension { get; set; }

        public decimal Count { get; set; }

        public decimal Cost { get; set; }

        public string Time { get; set; }

        public decimal Amount { get; set; }

        public string Garanty { get; set; }
    }

    public class MaterialListItem
    {
        public string Garanty { get; set; }

        public decimal Amount { get; set; }

        public decimal Cost { get; set; }

        public decimal Count { get; set; }

        public string Dimension { get; set; }

        public string Description { get; set; }

        public string Code { get; set; }
    }
}
