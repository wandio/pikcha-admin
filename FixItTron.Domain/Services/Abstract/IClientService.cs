﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Services.Abstract
{
    public interface IClientService
    {
        TermsOfContractModel GetTermsOfContract(int clientId);

        OrdersSummaryModel GetOrdersSummary(int clientId, DateTime? dateFrom, DateTime? dateTo);

        IEnumerable<OrderTableRowModel> GetOrdersList(int clientId, string orderNumber, IEnumerable<int> orderTypeIds, DateTime? registrationStart, DateTime? registrationEnd, DateTime? performanceStart, DateTime? performanceEnd, decimal? amountFrom, decimal? amountTo, decimal? serviceAmountFrom, decimal? serviceAmountTo, decimal? materialAmountFrom, decimal? materialAmountTo, decimal? otherAmountFrom, decimal? otherAmountTo, IEnumerable<string> address, IEnumerable<int> categoryIds, IEnumerable<int> statusIds, int page, int pageSize, string sortBy, string sort, out int count);

        OrderDetailsModel GetOrderDetails(int clientId, string orderNumber);

        IDictionary<int, string> GetOrderStatuses(int clientId);

        IDictionary<int, string> GetOrderTypes();

        IDictionary<int, string> GetCategories(int clientId);

        IDictionary<int, string> GetMonths();

        IEnumerable<KeyValuePair<int, string>> GetServiceAndMaterialsDescriptions(int clientId);

        bool Authenticate(string userName, string password, out UserModel user);

        IEnumerable<ServicesAndMaterialsTableRowModel> GetServicesAndMaterialsList(int clientId, string orderNumber, int? typeId, DateTime? performanceStart, DateTime? performanceEnd, IEnumerable<string> address, IEnumerable<int> categoryIds, decimal? amountFrom, decimal? amountTo, IEnumerable<int> description, int? countFrom, int? countTo, int page, int pageSize, string sortBy, string sort, out int count);

        IEnumerable<FinanceTableRowModel> GetClientFinances(int clientId, int? year);

        bool FirstAuthorization(int clientId, string userName, string password, out string errorText);

        bool CheckUserExist(string userName);

        bool ResetPassword(string userName, string newPassword, out string errorText);

        bool ChangePassword(int clientId, string currentPassword, string newPassword, out string errorText);

        void SendEmail(string receiveEmail, string subject, string link);

        OrdersByFilialsModel GetOrdersByFilials(int clientId, DateTime? dateFrom, DateTime? dateTo);
        IEnumerable<BranchItem> GetBranches(int clientId);

        IEnumerable<StatisticsDataModel> GetMaterialsByFilials(int clientId, DateTime? dateFrom, DateTime? dateTo);

        IEnumerable<StatisticsDataModel> GetMaterialsByCategories(int clientId, DateTime? dateFrom, DateTime? dateTo);

        IEnumerable<StatisticsDataModel> GetMaterialsByDescriptions(int clientId, DateTime? dateFrom, DateTime? dateTo, IEnumerable<int> categoryIds);

        IEnumerable<StatisticsDataModel> GetMaterialsCountByDescriptions(int clientId, DateTime? dateFrom, DateTime? dateTo, IEnumerable<int> categoryIds);

        IEnumerable<StatisticsDataModel> GetServiceCategoryCountByFilials(int clientId, DateTime? dateFrom, DateTime? dateTo, int? branchId);

        IEnumerable<StatisticsDataModel> GetServiceDescriptionCountByFilials(int clientId, DateTime? dateFrom, DateTime? dateTo, int? branchId, bool showFull = false);

        IEnumerable<StatisticsDataModel> GetServiceCountByCategory(int clientId, DateTime? dateFrom, DateTime? dateTo);

        bool SaveClientBranch(int clientId, string branch, int clientObjectID, out string errorText);
    }
}
