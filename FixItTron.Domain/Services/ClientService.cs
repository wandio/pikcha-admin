﻿using FixItTron.Domain.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Mandrill;
using Mandrill.Models;
using System.Configuration;
using System.Web.Hosting;
using FixItTron.Domain.Helpers;
using Mandrill.Requests.Messages;
using System.Collections.Specialized;

namespace FixItTron.Domain.Services
{
    public class ClientService : IClientService
    {
        #region clientdata

        public TermsOfContractModel GetTermsOfContract(int clientId)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var contractsRaw = client.ClientContractsGet(clientId);

            var model = new TermsOfContractModel();
            model.CallNumberMonthlyLimit = contractsRaw.MonthOrdersLimit;
            model.CallHoursMonthlyLimit = contractsRaw.MonthOrderTimeLimit;
            model.CallNumberYearLimit = contractsRaw.YearOrdersLimit;
            model.CallHoursYearLimit = contractsRaw.YearOrderTimeLimit;
            model.ContractDate = contractsRaw.ContractDate;
            model.Currency = contractsRaw.Iso;
            model.MonthlyCost = contractsRaw.Amount;
            model.RateLimitExceedByHour = contractsRaw.OutLimitTimeTarifDescription;
            model.RemainingNumberOfCallsPerMonth = contractsRaw.CurrentMonthOrdersLimit;
            model.RemainingHoursOfCallsPerMonth = contractsRaw.CurrentMonthTimeLimit;
            model.RemainingNumberOfCallsPerYear = contractsRaw.CurrentYearOrdersLimit;
            model.RemainingHoursOfCallsPerYear = contractsRaw.CurrentYearTimeLimit;
            model.UsedNumberOfCallsPerMonth = contractsRaw.UsedMonthOrdersLimit;
            model.UsedHoursOfCallsPerMonth = contractsRaw.UsedMonthTimeLimit;
            model.UsedNumberOfCallsPerYear = contractsRaw.UsedYearOrdersLimit;
            model.UsedHoursOfCallsPerYear = contractsRaw.UsedYearTimeLimit;
            model.ExistingDebt = contractsRaw.Balance;
            model.ContractStartDate = contractsRaw.ContractStartDate;
            model.ContractEndDate = contractsRaw.ContractEndDate;

            return model;
        }

        public OrdersSummaryModel GetOrdersSummary(int clientId, DateTime? dateFrom, DateTime? dateTo)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var ordersRaw = client.OrdersCountByCategoryGet(clientId, DateFrom: dateFrom, DateTo: dateTo);

            var model = new OrdersSummaryModel();
            model.Items.Add(new OrdersSummaryItem()
            {
                Title = DomainResources.Subscription,
                Items = new List<OrdersItemModel>() {
                    AddOrdersItemModel(DomainResources.Normal, ordersRaw[0], OrdersSummaryType.Completed) ,
                    AddOrdersItemModel(DomainResources.Guarantee, ordersRaw[1], OrdersSummaryType.Completed) ,
                    AddOrdersItemModel(DomainResources.Consultation, ordersRaw[2], OrdersSummaryType.Completed) ,
                    AddOrdersItemModel(DomainResources.Repeated, ordersRaw[3], OrdersSummaryType.Completed) ,

                    AddOrdersItemModel(DomainResources.Normal, ordersRaw[4], OrdersSummaryType.Rejected) ,
                    AddOrdersItemModel(DomainResources.Guarantee, ordersRaw[5], OrdersSummaryType.Rejected) ,
                    AddOrdersItemModel(DomainResources.Consultation, ordersRaw[6], OrdersSummaryType.Rejected) ,
                    AddOrdersItemModel(DomainResources.Repeated, ordersRaw[7], OrdersSummaryType.Rejected)
                }
            });

            model.Items.Add(new OrdersSummaryItem()
            {
                Title = DomainResources.Paid,
                Items = new List<OrdersItemModel>() {
                    AddOrdersItemModel(DomainResources.Normal, ordersRaw[8], OrdersSummaryType.Completed) ,
                    AddOrdersItemModel(DomainResources.Guarantee, ordersRaw[9], OrdersSummaryType.Completed) ,
                    AddOrdersItemModel(DomainResources.Consultation, ordersRaw[10], OrdersSummaryType.Completed) ,
                    AddOrdersItemModel(DomainResources.Repeated, ordersRaw[11], OrdersSummaryType.Completed) ,

                    AddOrdersItemModel(DomainResources.Normal, ordersRaw[12], OrdersSummaryType.Rejected) ,
                    AddOrdersItemModel(DomainResources.Guarantee, ordersRaw[13], OrdersSummaryType.Rejected) ,
                    AddOrdersItemModel(DomainResources.Consultation, ordersRaw[14], OrdersSummaryType.Rejected) ,
                    AddOrdersItemModel(DomainResources.Repeated, ordersRaw[15], OrdersSummaryType.Rejected)
                }
            });

            return model;
        }

        private static OrdersItemModel AddOrdersItemModel(string title, FixItTron.Domain.MrMasterServiceReference.StatussInfo statussInfo, OrdersSummaryType type)
        {
            return new OrdersItemModel()
            {
                Title = title,
                Quantity = statussInfo.Count,
                Service = statussInfo.ServiceAmount,
                Material = statussInfo.MaterialAmount,
                Other = statussInfo.OtherAmount,
                Type = type
            };
        }

        public OrderDetailsModel GetOrderDetails(int clientId, string orderNumber)
        {
            var result = new OrderDetailsModel();

            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var orderRaw = client.ClientOrdersGet(clientId, orderNumber, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, 1, SortField: "OrderNo", SortOrder: "asc", WhithDetails: true).SingleOrDefault();
            if (orderRaw != null)
            {
                result.OrderNumber = orderRaw.OrderNo;
                result.PerformanceDate = orderRaw.ExecDate;
                result.Address = orderRaw.Address;
                result.OrderType = orderRaw.Type;
                result.OrderDescription = orderRaw.Description;
                result.Services = orderRaw.ServiceList.Select(x => new ServiceListItem()
                {
                    Description = x.Description,
                    Dimension = x.Dimension,
                    Count = x.Count,
                    Cost = x.Cost,
                    Time = x.Time,
                    Amount = x.Amount,
                    Garanty = x.Garanty
                });
                result.Materials = orderRaw.MaterialList.Select(x => new MaterialListItem()
                {
                    Description = x.Description,
                    Dimension = x.Dimension,
                    Count = x.Count,
                    Cost = x.Cost,
                    Code = x.Code,
                    Amount = x.Amount,
                    Garanty = x.Garanty
                });
            }
            return result;
        }

        public IEnumerable<OrderTableRowModel> GetOrdersList(int clientId, string orderNumber, IEnumerable<int> orderTypeIds, DateTime? registrationStart, DateTime? registrationEnd, DateTime? performanceStart, DateTime? performanceEnd, decimal? amountFrom, decimal? amountTo, decimal? serviceAmountFrom, decimal? serviceAmountTo, decimal? materialAmountFrom, decimal? materialAmountTo, decimal? otherAmountFrom, decimal? otherAmountTo, IEnumerable<string> address, IEnumerable<int> categoryIds, IEnumerable<int> statusIds, int page, int pageSize, string sortBy, string sort, out int count)
        {
            var result = new List<OrderTableRowModel>();

            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var ordersRaw = client.ClientOrdersGet(clientId, orderNumber, orderTypeIds.Combine(), statusIds.Combine(), categoryIds.Combine(), address.Combine(), registrationStart, registrationEnd, performanceStart, performanceEnd, amountFrom, amountTo, serviceAmountFrom, serviceAmountTo, materialAmountFrom, materialAmountTo, otherAmountFrom, otherAmountTo, AccountingStatus: null, Index: page, PageSize: pageSize, SortField: sortBy, SortOrder: sort, WhithDetails: false);

            count = 0;

            if (ordersRaw.Any())
            {
                count = ordersRaw[0].RowCount;

                result = ordersRaw.Select(x => new OrderTableRowModel()
                {
                    OrderNumber = x.OrderNo,
                    OrderType = x.Type,
                    RegistrationDate = x.RegDate,
                    PerformanceDate = x.ExecDate,
                    Address = x.Address,
                    Category = x.Category,
                    Amount = x.Amount,
                    ServiceAmount = x.ServiceAmount,
                    MaterialAmount = x.MaterialAmount,
                    OtherAmount = x.OtherAmount,
                    Status = x.Status,
                    AccountingStatus = x.AccountingStatus
                }).ToList();
            }

            return result;
        }

        public IEnumerable<ServicesAndMaterialsTableRowModel> GetServicesAndMaterialsList(int clientId, string orderNumber, int? typeId, DateTime? performanceStart, DateTime? performanceEnd, IEnumerable<string> address, IEnumerable<int> categoryIds, decimal? amountFrom, decimal? amountTo, IEnumerable<int> description, int? countFrom, int? countTo, int page, int pageSize, string sortBy, string sort, out int count)
        {
            var result = new List<ServicesAndMaterialsTableRowModel>();
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var servicesAndMaterialsRaw = client.ClientServicesAndMaterialsGet(clientId, typeId.Combine(), performanceStart, performanceEnd, address.Combine(), categoryIds.Combine(), amountFrom, amountTo, description.Combine(), countFrom, countTo, orderNumber, page, pageSize, sortBy, sort);

            count = 0;

            if (servicesAndMaterialsRaw.Any())
            {
                count = servicesAndMaterialsRaw[0].RowCount;

                result = servicesAndMaterialsRaw.Select(x => new ServicesAndMaterialsTableRowModel()
                {
                    PerformanceDate = x.ExecDate,
                    OrderNumber = x.OrderNo,
                    Type = x.Type,
                    Address = x.Address,
                    Category = x.Category,
                    Name = x.Description,
                    Count = x.Count,
                    Amount = x.Amount
                }).ToList();
            }

            return result;
        }

        public IEnumerable<FinanceTableRowModel> GetClientFinances(int clientId, int? year)
        {
            var result = new List<FinanceTableRowModel>();
            var months = GetMonths();

            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var financesRaw = client.ClientFinancesGet(clientId, year);
            result = financesRaw.Select((x, ind) => new FinanceTableRowModel
            {
                AbonentAmount = x.AbonentAmount,
                AccrualAmount = x.AccrualAmount,
                EndBalance = x.EndBalance,
                MaterialAmount = x.MaterialAmount,
                OtherAmount = x.OtherAmount,
                PaymentAmount = x.PaymentAmount,
                ServiceAmount = x.ServiceAmount,
                StartBalance = x.StartBalance,
                MonthIndex = (ind + 1),
                MonthName = months[ind + 1]
            }).ToList();

            return result;
        }

        public OrdersByFilialsModel GetOrdersByFilials(int clientId, DateTime? dateFrom, DateTime? dateTo)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var ordersRaw = client.OrdersByFilialsGet(clientId, DateFrom: dateFrom, DateTo: dateTo);
            var model = new OrdersByFilialsModel();
            model.Items = ordersRaw.OrderByDescending(x => x.AbonentOrders).ThenByDescending(x => x.PaidOrders).Select(x => new OrdersByFilialsItem()
            {
                Branch = x.Branch,
                AbonentOrders = x.AbonentOrders,
                PaidOrders = x.PaidOrders,
                Color1 = "#0070a8",
                Color2 = "#cc2121"
            }).ToList();

            return model;
        }

        public IEnumerable<StatisticsDataModel> GetMaterialsByFilials(int clientId, DateTime? dateFrom, DateTime? dateTo)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var rawData = client.MaterialsByFilialsGet(clientId, DateFrom: dateFrom, DateTo: dateTo);

            return rawData.Select(x => new StatisticsDataModel()
            {
                Title = x.Branch,
                Value = x.Amount,
                Color1 = "#0070a8",
                Color2 = "#cc2121"
            });
        }

        public IEnumerable<StatisticsDataModel> GetMaterialsByCategories(int clientId, DateTime? dateFrom, DateTime? dateTo)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var rawData = client.MaterialsByCategoriesGet(clientId, DateFrom: dateFrom, DateTo: dateTo);

            return rawData.Select(x => new StatisticsDataModel()
            {
                Title = x.Category,
                Value = x.Amount
            });
        }

        public IEnumerable<StatisticsDataModel> GetMaterialsByDescriptions(int clientId, DateTime? dateFrom, DateTime? dateTo, IEnumerable<int> categoryIds)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var rawData = client.MaterialsByDescriptionsGet(clientId, DateFrom: dateFrom, DateTo: dateTo, CategoryID: categoryIds.Combine());

            return rawData.Select(x => new StatisticsDataModel()
            {
                Title = x.Description,
                Value = x.Amount,
                Color1 = "#0070a8"
            });
        }

        public IEnumerable<StatisticsDataModel> GetMaterialsCountByDescriptions(int clientId, DateTime? dateFrom, DateTime? dateTo, IEnumerable<int> categoryIds)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var rawData = client.MaterialsCountByDescriptionsGet(clientId, DateFrom: dateFrom, DateTo: dateTo, CategoryID: categoryIds.Combine());

            return rawData.Select(x => new StatisticsDataModel()
            {
                Title = x.Description,
                Value = x.Count,
                Color1 = "#0070a8"
            });
        }

        public IEnumerable<StatisticsDataModel> GetServiceCategoryCountByFilials(int clientId, DateTime? dateFrom, DateTime? dateTo, int? branchId)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var rawData = client.ServiceCategoryCountByFilialsGet(clientId, DateFrom: dateFrom, DateTo: dateTo, Branch: branchId.Combine());

            return rawData.Select(x => new StatisticsDataModel()
            {
                Title = x.Category,
                Value = x.Count,
                Color1 = "#0070a8"
            });
            //return rawData.GroupBy(x => x.Branch).Select(x => new StatisticsDataModel()
            //{
            //    Title = x.Key,
            //    SubItems = x.Select(s => new StatisticsDataSubModel()
            //    {
            //        Title = s.Category,
            //        Value = s.Count
            //    })
            //});
        }

        public IEnumerable<StatisticsDataModel> GetServiceDescriptionCountByFilials(int clientId, DateTime? dateFrom, DateTime? dateTo, int? branchId, bool showFull = false)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var rawData = client.ServiceDescriptionCountByFilialsGet(clientId, DateFrom: dateFrom, DateTo: dateTo, Branch: branchId.Combine());

            //if (showFull)
            {
                var rawServices = rawData.SelectMany(x => x.ServicesCount).Select(x => new { x.Service, x.Count });
                return rawServices.GroupBy(x => x.Service).Select(x => new StatisticsDataModel
                {
                    Title = x.Key,
                    Value = x.Sum(s => s.Count),
                    Color1 = "#0070a8"
                });
            }
            //return rawData.Select(x => new StatisticsDataModel()
            //{
            //    Title = x.BranchDescription,
            //    SubItems = x.ServicesCount.Select(s => new StatisticsDataSubModel()
            //    {
            //        Title = s.Service,
            //        Value = s.Count
            //    })
            //});
        }

        public IEnumerable<StatisticsDataModel> GetServiceCountByCategory(int clientId, DateTime? dateFrom, DateTime? dateTo)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var rawData = client.ServiceCountByCategoryGet(clientId, DateFrom: dateFrom, DateTo: dateTo);

            return rawData.Select(x => new StatisticsDataModel()
            {
                Title = x.Category,
                Value = x.Count,
                Color1 = "#0070a8"
            });
        }

        public bool SaveClientBranch(int clientId, string branch, int clientObjectID, out string errorText)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            return client.ClientBranchEdit(branch, clientObjectID, clientId, out errorText);
        }

        #endregion

        #region lookups

        public IDictionary<int, string> GetOrderStatuses(int clientId)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var statuses = client.OrderStatussesGet(clientId);
            return statuses.ToDictionary(x => x.ID, y => y.OrderStatussDescription);
        }

        public IDictionary<int, string> GetOrderTypes()
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var types = client.OrderTypesGet();
            return types.ToDictionary(x => x.ID, y => y.OrderTypeDescription);
        }

        public IDictionary<int, string> GetCategories(int clientId)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var categories = client.OrderCategoriesGet(clientId);
            return categories.ToDictionary(x => x.ID, y => y.OrderCategoryDescription);
        }

        public IDictionary<int, string> GetMonths()
        {
            var currentCulture = System.Globalization.CultureInfo.CurrentCulture;
            System.Globalization.DateTimeFormatInfo format = currentCulture.DateTimeFormat;
            return format.MonthNames.Take(12).Select((month, ind) => new { MonthName = month, Index = ind + 1 }).ToDictionary(x => x.Index, y => y.MonthName);
        }

        public IEnumerable<BranchItem> GetBranches(int clientId)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var branches = client.BranchesGet(clientId);
            return branches.Select(x => new BranchItem()
            {
                BranchId = x.ID,
                BranchName = x.Branch,
                Address = x.Address
            });
        }

        public IEnumerable<KeyValuePair<int, string>> GetServiceAndMaterialsDescriptions(int clientId)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var descriptions = client.ServiceAndMaterialsDescriptionsGet(clientId);
            return descriptions.Select(x => new KeyValuePair<int, string>(x.ID, x.Description));
        }

        #endregion

        #region authentication

        public bool Authenticate(string userName, string password, out UserModel user)
        {
            bool isSuccessful = false;
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            var userRaw = client.AuthorizeClient(userName, password, out isSuccessful);

            user = new UserModel();
            if (isSuccessful)
            {
                user.UserId = userRaw.ID;
                user.IsFirst = userRaw.IsFirst;
                user.Description = userRaw.Description;
                user.UserName = userName;
            }

            return isSuccessful;
        }

        public bool FirstAuthorization(int clientId, string userName, string password, out string errorText)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            bool isSuccessful = client.RegisterUser(clientId, userName, password, out errorText);
            return isSuccessful;
        }

        public bool CheckUserExist(string userName)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            return client.CheckUserExist(userName);
        }

        public bool ResetPassword(string userName, string newPassword, out string errorText)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            return client.ResetPassword(userName, newPassword, out errorText);
        }

        public bool ChangePassword(int clientId, string currentPassword, string newPassword, out string errorText)
        {
            MrMasterServiceReference.MrMasterClientInfo client = new MrMasterServiceReference.MrMasterClientInfo();
            bool isSuccessful = client.ChangePassword(clientId, currentPassword, newPassword, out errorText);
            return isSuccessful;
        }

        public void SendEmail(string receiveEmail, string subject, string link)
        {
            EmailMessage message = new EmailMessage();
            message.To = new List<EmailAddress>() { new EmailAddress(receiveEmail) };
            message.Subject = subject;
            message.FromEmail = ConfigurationManager.AppSettings["Mail_From_Email"];
            message.FromName = ConfigurationManager.AppSettings["Mail_From_DisplayName"];
            message.Html = SetEmailTemplate(link);

            MandrillApi api = new MandrillApi(ConfigurationManager.AppSettings["MandrillApiKey"]);
            api.SendMessage(new SendMessageRequest(message));

            //var result = await api.SendMessage(new SendMessageRequest(message));

            //foreach (var item in result)
            //{
            //    if (item.Status == EmailResultStatus.Rejected)
            //    {
            //        throw new Exception(item.RejectReason);
            //    }
            //}

            //using (var message = new MailMessage())
            //{
            //    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();

            //    message.From = new MailAddress(ConfigurationManager.AppSettings["Mail_From_Email"], ConfigurationManager.AppSettings["Mail_From_DisplayName"]);
            //    message.To.Add(new MailAddress(receiveEmail));
            //    message.Subject = subject;
            //    message.IsBodyHtml = true;

            //    body = SetEmailTemplate(subject, body);

            //    message.Body = body;
            //    smtp.Send(message);
            //}
        }

        private string SetEmailTemplate(string link)
        {
            string styledEmailTemplate = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/App_Data/template.html"));
            styledEmailTemplate = styledEmailTemplate.Replace(Constants.EmailTemplateKeyWords.Link, link);
            return styledEmailTemplate;
        }

        #endregion
    }
}
