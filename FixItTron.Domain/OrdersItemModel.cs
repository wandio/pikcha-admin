﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class OrdersItemModel
    {
        public string Title { get; set; }
        public decimal Quantity { get; set; }
        public decimal Service { get; set; }
        public decimal Material { get; set; }
        public decimal Other { get; set; }
        public OrdersSummaryType Type { get; set; }
    }
}
