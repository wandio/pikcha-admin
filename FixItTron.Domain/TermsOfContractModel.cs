﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class TermsOfContractModel
    {
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public string ContractDate { get; set; }
        public string Currency { get; set; }
        public decimal MonthlyCost { get; set; }
        public int CallNumberMonthlyLimit { get; set; }
        public int CallHoursMonthlyLimit { get; set; }
        public int CallNumberYearLimit { get; set; }
        public int CallHoursYearLimit { get; set; }
        public string RateLimitExceedByHour { get; set; }
        public int RemainingNumberOfCallsPerMonth { get; set; }
        public int RemainingHoursOfCallsPerMonth { get; set; }
        public int RemainingNumberOfCallsPerYear { get; set; }
        public int RemainingHoursOfCallsPerYear { get; set; }
        public int UsedNumberOfCallsPerMonth { get; set; }
        public int UsedHoursOfCallsPerMonth { get; set; }
        public int UsedNumberOfCallsPerYear { get; set; }
        public int UsedHoursOfCallsPerYear { get; set; }
        public decimal ExistingDebt { get; set; }

    }
}
