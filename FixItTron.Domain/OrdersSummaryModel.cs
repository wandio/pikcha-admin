﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class OrdersSummaryModel
    {
        public List<OrdersSummaryItem> Items { get; set; }

        public decimal TotalQuantity()
        {
            return Items.Sum(x => x.Items.Sum(s => s.Quantity));
        }

        public decimal TotalService()
        {
            return Items.Sum(x => x.Items.Sum(s => s.Service));
        }

        public decimal TotalMaterial()
        {
            return Items.Sum(x => x.Items.Sum(s => s.Material));
        }

        public decimal TotalOther()
        {
            return Items.Sum(x => x.Items.Sum(s => s.Other));
        }

        public OrdersSummaryModel()
        {
            Items = new List<OrdersSummaryItem>();
        }
    }
}
