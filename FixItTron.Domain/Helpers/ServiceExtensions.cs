﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Helpers
{
    public static class ServiceExtensions
    {
        public static string Combine(this IEnumerable<int> values)
        {
            if (values != null && values.Any())
            {
                return String.Join(";", values) + ";";
            }
            return null;
        }

        public static string Combine(this IEnumerable<string> values)
        {
            if (values != null && values.Any())
            {
                return String.Join(";", values) + ";";
            }
            return null;
        }

        public static string Combine(this int? value)
        {
            if (value.HasValue)
            {
                return value.Value.ToString() + ";";
            }
            return null;
        }
    }
}
