﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Helpers
{
    public static class Constants
    {
        public static class Formats
        {
            /// <summary>
            /// "#,##0.##"
            /// </summary>
            public static string DecimalFormat = "#,##0.00";
            /// <summary>
            /// "0.##"
            /// </summary>
            public static string PercentFormat = "0.##";
        }

        public static string SessionClientId = "SessionClientId";

        public static class EmailTemplateKeyWords
        {
            public const string Link = "##Link##";
        }
    }
}
