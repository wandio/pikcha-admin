﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class FinanceTableRowModel
    {
        public decimal StartBalance { get; set; }

        public decimal AccrualAmount { get; set; }

        public decimal MaterialAmount { get; set; }

        public decimal OtherAmount { get; set; }

        public decimal AbonentAmount { get; set; }

        public decimal ServiceAmount { get; set; }

        public decimal PaymentAmount { get; set; }

        public decimal EndBalance { get; set; }

        public int MonthIndex { get; set; }

        public string MonthName { get; set; }
    }
}
