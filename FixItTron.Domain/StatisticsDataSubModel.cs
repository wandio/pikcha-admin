﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Domain
{
    public class StatisticsDataSubModel
    {
        public string Title { get; set; }
        public decimal Value { get; set; }
    }
}
