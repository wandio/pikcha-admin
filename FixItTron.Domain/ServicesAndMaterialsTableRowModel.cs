﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class ServicesAndMaterialsTableRowModel
    {
        public DateTime? PerformanceDate { get; set; }

        public string OrderNumber { get; set; }

        public string Type { get; set; }

        public string Address { get; set; }

        public string Category { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public decimal Amount { get; set; }
    }
}
