﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain
{
    public class OrdersSummaryItem
    {
        public string Title { get; set; }
        public List<OrdersItemModel> Items { get; set; }
    }
}
