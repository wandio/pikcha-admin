﻿using Common.Enums;
using FixItTron.Domain.Db;
using FixItTron.Domain.Model;
using FixItTron.Services.Helpers;
using FixItTron.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services
{
    public class ApiService : IApiService
    {
        PikchaDbContext Db;

        public ApiService()
        {
            ServiceMapperConfiguration.Configure(AutoMapper.Mapper.Configuration);
            Db = new PikchaDbContext();
        }

        public Models.Api.GetFacebookShareLogoResponse GetFacebookShareLogo()
        {
            Models.Api.GetFacebookShareLogoResponse result = new Models.Api.GetFacebookShareLogoResponse();
            var e = Db.Set<Event>().FirstOrDefault(x => DateTime.Now >= x.StartDate && DateTime.Now <= x.EndDate && x.ShowLogoOnShare && x.IsActive);
            if (e != null)
            {
                result.LogoUrl = e.FacebookLogoFileName;
            }
            return result;
        }

        public Models.Api.GetInstagramShareLogoResponse GetInstagramShareLogo()
        {
            Models.Api.GetInstagramShareLogoResponse result = new Models.Api.GetInstagramShareLogoResponse();
            var e = Db.Set<Event>().FirstOrDefault(x => DateTime.Now >= x.StartDate && DateTime.Now <= x.EndDate && x.ShowLogoOnShare && x.IsActive);
            if (e != null)
            {
                result.LogoUrl = e.InstagramLogoFileName;
            }
            return result;
        }

        public Models.Api.GetPreviewLogoResponse GetPreviewLogo()
        {
            Models.Api.GetPreviewLogoResponse result = new Models.Api.GetPreviewLogoResponse();
            var e = Db.Set<Event>().FirstOrDefault(x => DateTime.Now >= x.StartDate && DateTime.Now <= x.EndDate && x.ShowLogoOnPreview && x.IsActive);
            if (e != null)
            {
                result.LogoUrl = e.MobileLogoFileName;
            }
            return result;
        }

        public Models.Api.GetFacebookShareLogoResponse GetFacebookShareLogo2(int eventId)
        {
            Models.Api.GetFacebookShareLogoResponse result = new Models.Api.GetFacebookShareLogoResponse();
            var e = Db.Set<Event>().FirstOrDefault(x => x.EventId == eventId && DateTime.Now >= x.StartDate &&
                DateTime.Now <= x.EndDate && x.ShowLogoOnShare && x.IsActive);
            if (e != null)
            {
                result.LogoUrl = e.FacebookLogoFileName;
            }
            return result;
        }

        public Models.Api.GetInstagramShareLogoResponse GetInstagramShareLogo2(int eventId)
        {
            Models.Api.GetInstagramShareLogoResponse result = new Models.Api.GetInstagramShareLogoResponse();
            var e = Db.Set<Event>().FirstOrDefault(x => x.EventId == eventId && DateTime.Now >= x.StartDate &&
                DateTime.Now <= x.EndDate && x.ShowLogoOnShare && x.IsActive);
            if (e != null)
            {
                result.LogoUrl = e.InstagramLogoFileName;
            }
            return result;
        }

        public Models.Api.GetPreviewLogoResponse GetPreviewLogo2(int eventId)
        {
            Models.Api.GetPreviewLogoResponse result = new Models.Api.GetPreviewLogoResponse();
            var e = Db.Set<Event>().FirstOrDefault(x => x.EventId == eventId && DateTime.Now >= x.StartDate && DateTime.Now <= x.EndDate && x.ShowLogoOnPreview && x.IsActive);
            if (e != null)
            {
                result.LogoUrl = e.MobileLogoFileName;
            }
            return result;
        }

        public Models.Api.ActivatePromoCodeResponse ActivatePromoCode(Models.Api.ActivatePromoCodeRequest request)
        {
            UserType userType = (UserType)Enum.Parse(typeof(UserType), request.UserType.ToString(), true);
            var promoCode = Db.Set<PromoCode>().FirstOrDefault(x => x.Code == request.PromoCode);
            if (promoCode == null)
            {
                return new Models.Api.ActivatePromoCodeResponse
                {
                    Success = false,
                    PromoCodeExists = false,
                    PromoCodeIsAlreadyUsed = false
                };
            }

            var user = Db.Set<User>().FirstOrDefault(x => x.Username == request.UserName && x.UserType == userType);

            if (!promoCode.MultiUse)
            {
                var usedPromo = Db.Set<UserPromoCode>().FirstOrDefault(x => x.PromoCode.Code == request.PromoCode);
                if (usedPromo != null)
                {
                    return new Models.Api.ActivatePromoCodeResponse
                    {
                        Success = false,
                        PromoCodeExists = true,
                        PromoCodeIsAlreadyUsed = true
                    };
                }
            }
            else
            {
                var usersUsedPromo = Db.Set<UserPromoCode>().FirstOrDefault(x => x.PromoCode.Code == request.PromoCode && x.User.Username == request.UserName);
                if (usersUsedPromo != null)
                {
                    return new Models.Api.ActivatePromoCodeResponse
                    {
                        Success = false,
                        PromoCodeExists = true,
                        PromoCodeIsAlreadyUsed = true
                    };
                }
            }


            Db.Set<UserPromoCode>().Add(new UserPromoCode()
            {
                CreateDate = DateTime.Now,
                PromoCode = promoCode,
                User = user
            });
            user.AvailablePrints = user.AvailablePrints + promoCode.PrintCount;
            Db.Entry(user).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
            return new Models.Api.ActivatePromoCodeResponse
            {
                Success = true,
                PromoCodeExists = true,
                PromoCodeIsAlreadyUsed = false
            };
        }

        public Models.Api.CreateSurveyResponse CreateSurvey(Models.Api.CreateSurveyRequest request)
        {
            try
            {
                var user = Db.Set<User>().Single(x => x.UserId == request.UserId);
                var evnt = Db.Set<Event>().FirstOrDefault(x => x.EventId == request.EventId);
                var survey = new Survey
                {
                    CreateDate = DateTime.Now,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    Gender = request.Gender,
                    User = user,
                    Event = evnt,
                    SourceType = SourceType.Messenger
                };
                Db.Set<Survey>().Add(survey);
                Db.SaveChanges();
            }
            catch (Exception ex)
            {
                return new Models.Api.CreateSurveyResponse
                {
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
            

            return new Models.Api.CreateSurveyResponse
            {
                Success = true
            };
        }

        public Models.Api.AnswerSurveyQuestion1Response AnswerSurveyQuestion1(Models.Api.AnswerSurveyQuestion1Request request)
        {
            try
            {
                var survey = Db.Set<Survey>().SingleOrDefault(x => x.UserId == request.UserId);
                if (survey != null)
                {
                    survey.Question1 = request.Question1Answer;
                    Db.SaveChanges();
                }
                else
                {
                    return new Models.Api.AnswerSurveyQuestion1Response
                    {
                        Success = false,
                        ErrorMessage = ServiceResources.CouldNotFindSurveyEntry
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Api.AnswerSurveyQuestion1Response
                {
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }

            return new Models.Api.AnswerSurveyQuestion1Response
            {
                Success = true
            };
        }

        public Models.Api.AnswerSurveyQuestion2Response AnswerSurveyQuestion2(Models.Api.AnswerSurveyQuestion2Request request)
        {
            try
            {
                var survey = Db.Set<Survey>().SingleOrDefault(x => x.UserId == request.UserId);
                if (survey != null)
                {
                    survey.Question2 = request.Question2Answer;
                    Db.SaveChanges();
                }
                else
                {
                    return new Models.Api.AnswerSurveyQuestion2Response
                    {
                        Success = false,
                        ErrorMessage = ServiceResources.CouldNotFindSurveyEntry
                    };
                }
            }
            catch (Exception ex)
            {
                return new Models.Api.AnswerSurveyQuestion2Response
                {
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }

            return new Models.Api.AnswerSurveyQuestion2Response
            {
                Success = true
            };
        }

        public Models.Api.CreateUserResponse CreateUser(Models.Api.CreateUserRequest request)
        {
            var existingUser = Db.Set<User>().FirstOrDefault(x => x.Username == request.UserName);
            if (existingUser != null)
            {
                return new Models.Api.CreateUserResponse() { Success = false, UserExists = true };
            }
            else
            {
                int freePrints = 0;
                var config = Db.Set<Configuration>().FirstOrDefault(x => x.Name == "FREE_PRINTS");
                if (config != null)
                {
                    try
                    {
                        freePrints = int.Parse(config.Value);
                    }
                    catch
                    { }
                }

                var user = new User()
                {
                    Username = request.UserName,
                    AvailablePrints = freePrints,
                    CreateDate = DateTime.Now,
                    UsedPrints = 0,
                    Email = request.Email,
                    FullName = request.FullName,
                    UserType = (UserType)Enum.Parse(typeof(UserType), request.UserType.ToString(), true)
                };

                Db.Set<User>().Add(user);
                Db.SaveChanges();
                return new Models.Api.CreateUserResponse()
                {
                    Success = true,
                    UserExists = false,
                    UserId = user.UserId
                };
            }
        }

        public Models.Api.GetUserInfoResponse GetUserInfo(Models.Api.GetUserInfoRequest request)
        {
            UserType userType = (UserType)Enum.Parse(typeof(UserType), request.UserType.ToString(), true);
            var user = Db.Set<User>().FirstOrDefault(x => x.Username == request.UserName && x.UserType == userType);
            if (user == null)
            {
                return new Models.Api.GetUserInfoResponse() { AvailablePrints = 0, UsedPrints = 0 };
            }
            else
            {
                return new Models.Api.GetUserInfoResponse() { AvailablePrints = user.AvailablePrints, UsedPrints = user.UsedPrints };
            }
        }

        public Models.Api.UploadImageResponse UploadImage(Models.Api.UploadImageRequest request)
        {
            try
            {
                //TODO: change to graphicsforprint
                string name = AmazonImageUploaderService.UploadImage(Convert.FromBase64String(request.ImageBase64), "graphicsforprint");

                Photo photo = new Photo()
                {
                    CreateDate = DateTime.Now,
                    FileName = name,
                    SourceType = request.Source
                };

                Db.Configuration.ValidateOnSaveEnabled = false;
                Db.Configuration.AutoDetectChangesEnabled = false;
                Db.Configuration.EnsureTransactionsForFunctionsAndCommands = false;
                Db.Configuration.LazyLoadingEnabled = false;
                Db.Configuration.ProxyCreationEnabled = false;

                Db.Set<Photo>().Add(photo);
                Db.SaveChanges();

                return new Models.Api.UploadImageResponse() { PhotoId = photo.PhotoId, Success = true };
            }
            catch
            {
                return new Models.Api.UploadImageResponse() { PhotoId = 0, Success = false };
            }
        }

        public Models.Api.UploadImage2Response UploadImage2(Models.Api.UploadImage2Request request)
        {
            UserType userType = (UserType)Enum.Parse(typeof(UserType), request.UserType.ToString(), true);
            var user = Db.Set<User>().FirstOrDefault(x => x.Username == request.UserName && x.UserType == userType);

            if (user.AvailablePrints <= user.UsedPrints)
            {
                return new Models.Api.UploadImage2Response() { PhotoId = 0, Success = false };
            }

            string name = AmazonImageUploaderService.UploadImage(Convert.FromBase64String(request.ImageBase64));

            Photo photo = new Photo()
            {
                CreateDate = DateTime.Now,
                FileName = name,
                SourceType = request.Source,
                CreateUser = user
            };

            Db.Configuration.ValidateOnSaveEnabled = false;
            Db.Configuration.AutoDetectChangesEnabled = false;
            Db.Configuration.EnsureTransactionsForFunctionsAndCommands = false;
            Db.Configuration.LazyLoadingEnabled = false;
            Db.Configuration.ProxyCreationEnabled = false;

            Db.Set<Photo>().Add(photo);

            user.UsedPrints = user.UsedPrints + 1;
            Db.Entry(user).State = System.Data.Entity.EntityState.Modified;

            Db.SaveChanges();
            return new Models.Api.UploadImage2Response() { Success = true, PhotoId = photo.PhotoId };
        }

        public Models.Api.UploadIsAvailableResponse UploadIsAvailable(Models.Api.UploadIsAvailableRequest request)
        {
            UserType userType = (UserType)Enum.Parse(typeof(UserType), request.UserType.ToString(), true);
            var user = Db.Set<User>().FirstOrDefault(x => x.Username == request.UserName && x.UserType == userType);
            return new Models.Api.UploadIsAvailableResponse()
            {
                Available = (user == null ? false : (user.AvailablePrints > user.UsedPrints))
            };
        }

        public Models.Api.UserExistsResponse UserExists(Models.Api.UserExistsRequest request)
        {
            UserType userType = (UserType)Enum.Parse(typeof(UserType), request.UserType.ToString(), true);
            var user = Db.Set<User>().FirstOrDefault(x => x.Username == request.UserName && x.UserType == userType);
            if (user == null)
            {
                return new Models.Api.UserExistsResponse() { Exists = false };
            }
            else
            {
                return new Models.Api.UserExistsResponse() { Exists = true, UserId = user.UserId };
            }
        }

        public Models.Api.CheckHealthResponse CheckHealth()
        {
            var test = Db.Set<Event>().Any();
            return new FixItTron.Services.Models.Api.CheckHealthResponse
            {
                IsHealty = true
            };
        }

        public Models.Api.GetEventSurveyResponse GetEventSurvey(Models.Api.GetEventSurveyRequest request)
        {
            var evnt = Db.Set<Event>().FirstOrDefault(x => x.FacebookPageId == request.PageId && x.IsActive && DateTime.Now >= x.StartDate && DateTime.Now <= x.EndDate);

            if (evnt != null)
            {
                Survey survey = null;
                if (evnt.ShowSurvey)
                {
                    survey = Db.Set<Survey>().FirstOrDefault(x => x.EventId == evnt.EventId && x.User.Username == request.UserId);
                }

                return new Models.Api.GetEventSurveyResponse
                {
                    EventId = evnt.EventId,
                    FacebookAppSecret = evnt.FacebookAppSecret,
                    FacebookPageAccessToken = evnt.FacebookPageAccessToken,
                    FacebookPageId = evnt.FacebookPageId,
                    ShowSurvey = evnt.ShowSurvey,
                    SurveyExists = survey != null,
                    SurveyFilled = survey != null && survey.Question1 != SurveyQuestion1Answer.NoAnswer && survey.Question2 != SurveyQuestion2Answer.NoAnswer,
                    PartiallyFilled = survey != null && (survey.Question1 == SurveyQuestion1Answer.NoAnswer || survey.Question2 == SurveyQuestion2Answer.NoAnswer),
                };
            }

            return new Models.Api.GetEventSurveyResponse
            {
                ShowSurvey = false
            };
        }

        public Models.Api.ValidateEventCodeResponse ValidateEventCode(Models.Api.ValidateEventCodeRequest request)
        {
            var evnt = Db.Set<Event>().FirstOrDefault(x => x.EventCode == request.EventCode);

            if (evnt == null)
            {
                return new Models.Api.ValidateEventCodeResponse
                {
                    Success = false,
                    IsValid = false
                };
            }

            bool surveySubmitted = Db.Set<Survey>().Any(x => x.EventId == evnt.EventId &&
                x.User.Username == request.UserName && x.User.UserType == (Common.Enums.UserType)request.UserType);

            return new Models.Api.ValidateEventCodeResponse
            {
                IsValid = true,
                Success = true,
                HasSurvey = evnt.ShowSurvey,
                IsActive = evnt.IsActive,
                EventId = evnt.EventId,
                StartDate = evnt.StartDate,
                EndDate = evnt.EndDate,
                SurveySubmitted = surveySubmitted
            };
        }

        public Models.Api.SubmitSurveyResponse SubmitSurvey(Models.Api.SubmitSurveyRequest request)
        {
            try
            {
                var survey = Db.Set<Survey>().FirstOrDefault(x => x.EventId == request.EventId &&
                    x.User.Username == request.UserName && x.User.UserType == (Common.Enums.UserType)request.UserType);

                if (survey != null)
                {
                    survey.FirstName = request.FirstName;
                    survey.LastName = request.LastName;
                    survey.Email = request.Email;
                    survey.Question1 = request.Question1Answer;
                    survey.Question2 = request.Question2Answer;
                }
                else
                {
                    var user = Db.Set<User>().SingleOrDefault(x => x.Username == request.UserName && x.UserType == (Common.Enums.UserType)request.UserType);
                    if (user == null)
                    {
                        return new Models.Api.SubmitSurveyResponse
                        {
                            Success = false,
                            ErrorMessage = ServiceResources.UserDoesNotExist
                        };
                    }

                    var evnt = Db.Set<Event>().FirstOrDefault(x => x.EventId == request.EventId);
                    if (evnt == null)
                    {
                        return new Models.Api.SubmitSurveyResponse
                        {
                            Success = false,
                            ErrorMessage = ServiceResources.EventDoesNotExist
                        };
                    }

                    survey = new Survey
                    {
                        CreateDate = DateTime.Now,
                        FirstName = request.FirstName,
                        LastName = request.LastName,
                        Email = request.Email,
                        Question1 = request.Question1Answer,
                        Question2 = request.Question2Answer,
                        User = user,
                        Event = evnt,
                        SourceType = request.SourceType
                    };
                    Db.Set<Survey>().Add(survey);
                }
                Db.SaveChanges();

                return new Models.Api.SubmitSurveyResponse
                {
                    Success = true,
                    SurveyId = survey.SurveyId
                };
            }
            catch (Exception ex)
            {
                return new Models.Api.SubmitSurveyResponse
                {
                    Success = false,
                    ErrorMessage = ex.Message
                };
            }
        }
    }
}
