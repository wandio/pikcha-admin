﻿using Common.Enums;
using FixItTron.Services.Helpers;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Api;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ImageResizer;
using Newtonsoft.Json;
using System.Net.Http.Formatting;

namespace FixItTron.Services
{
    public class ChatbotService : IChatbotService
    {
        IApiService _apiService;

        public static string AppSecret { get; set; }
        public static string PageAccessToken { get; set; }

        private const string sendApiUrl = "https://graph.facebook.com/v2.8/me/messages?access_token=";
        private const string userProfileApiUrl = "https://graph.facebook.com/v2.8/{0}?fields=first_name,last_name,gender&access_token={1}";

        public ChatbotService(IApiService apiService)
        {
            _apiService = apiService;
        }

        public bool VerifyRequestSignature(string signature, byte[] body)
        {
            string expected;
            using (var hmac = new HMACSHA1(Encoding.ASCII.GetBytes(AppSecret)))
            {
                expected = string.Join("", hmac.ComputeHash(body).Select(x => x.ToString("X2")).ToArray());
            }

            return signature.Equals(expected);
        }

        private async Task<UploadImage2Response> UploadImage(string url, string userId)
        {
            Stream stream;
            Image image;
            byte[] bytes;
            using (var client = new HttpClient())
            {
                using (stream = await client.GetStreamAsync(url))
                {
                    using (image = Image.FromStream(stream))
                    {
                        var size = image.Width > image.Height ? image.Height : image.Width;
                        var x = image.Width > image.Height ? (image.Width - image.Height) / 2 : 0;
                        var y = image.Width < image.Height ? (image.Height - image.Width) / 2 : 0;

                        using (var dest = new MemoryStream())
                        {
                            var job = new ImageJob(image, dest, new Instructions(string.Format("speed=0&crop={0},{1},-{0},-{1}", x, y)));
                            job.Build();

                            bytes = dest.ToArray();
                        }
                    }
                }
            }

            string imageBase64 = Convert.ToBase64String(bytes);

            var uploadRequest = new Models.Api.UploadImage2Request
            {
                ImageBase64 = imageBase64,
                Source = Common.Enums.SourceType.Messenger,
                UserName = userId,
                UserType = Models.Api.UserType.Facebook
            };

            return _apiService.UploadImage2(uploadRequest);
        }

        private async Task SendTextMessageAsync(string userId, string messageText, string pageAccessToken)
        {
            var payload = new Models.Api.SendApiPayload
            {
                recipient = new Models.Api.SendApiRecipient { id = userId },
                message = new Models.Api.SendApiMessage
                {
                    text = messageText
                }
            };

            using (var client = new HttpClient())
            {
                await client.PostAsJsonAsync(
                    sendApiUrl + pageAccessToken, payload);
            }
        }

        private void SendTextMessage(string userId, string messageText, string pageAccessToken)
        {
            Task.Run(async () =>
            {
                await SendTextMessageAsync(userId, messageText, pageAccessToken);
            });
        }

        private void SendIntroduction(string userId, string pageAccessToken)
        {
            var payload = new Models.Api.SendApiPayload
            {
                recipient = new Models.Api.SendApiRecipient
                {
                    id = userId
                },
                message = new Models.Api.SendApiMessage
                {
                    attachment = new SendApiAttachment
                    {
                        type = "template",
                        payload = new SendApiAttachmentPayload
                        {
                            template_type = "button",
                            text = "Hope you are enjoying your trip! \u2708 Before you print your first picture, " +
                                   "help us by answering 2 short questions and you'll be on your way!",
                            buttons = new SendApiAttachmentButton[]
                            {
                                new SendApiAttachmentButton
                                {
                                    type = "postback",
                                    title = "Okay, Let’s do this",
                                    payload = "OKAY_LETS_DO_THIS"
                                }
                            }
                        }
                    }
                }
            };

            Task.Run(async () =>
            {
                JsonMediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter
                {
                    SerializerSettings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    }
                };

                using (var client = new HttpClient())
                {
                    await client.PostAsync(sendApiUrl + pageAccessToken, payload, jsonFormatter);
                };
            });
        }

        private void SendButtonTemplate(string userId, string text, string buttonTitle, string buttonPayload, string linkButtonTitle, string linkButtonUrl, string pageAccessToken)
        {
            var payload = new Models.Api.SendApiPayload
            {
                recipient = new Models.Api.SendApiRecipient
                {
                    id = userId
                },
                message = new Models.Api.SendApiMessage
                {
                    attachment = new SendApiAttachment
                    {
                        type = "template",
                        payload = new SendApiAttachmentPayload
                        {
                            template_type = "button",
                            text = text,
                            buttons = new SendApiAttachmentButton[]
                            {
                                new SendApiAttachmentButton
                                {
                                    type = "web_url",
                                    title = linkButtonTitle,
                                    url = linkButtonUrl
                                },
                                new SendApiAttachmentButton
                                {
                                    type = "postback",
                                    title = buttonTitle,
                                    payload = buttonPayload
                                }
                            }
                        }
                    }
                }
            };

            Task.Run(async () =>
            {
                var payloadJson = JsonConvert.SerializeObject(payload, Formatting.None, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                StringContent stringContent = new StringContent(payloadJson, UnicodeEncoding.UTF8, "application/json");

                using (var client = new HttpClient())
                {
                    await client.PostAsync(sendApiUrl + pageAccessToken, stringContent);
                };
            });
        }

        private void SendQuestion1(string userId, string pageAccessToken)
        {
            var payload = new Models.Api.SendApiPayload
            {
                recipient = new Models.Api.SendApiRecipient
                {
                    id = userId
                },
                message = new Models.Api.SendApiMessage
                {
                    text = "Question No.1 (out of 2) \r\n" +
                        "When do you see yourself coming back on a Masa Program?",
                    quick_replies = new SendApiQuickReply[]
                    {
                        new SendApiQuickReply
                        {
                            content_type = "text",
                            title = "As soon as possible",
                            payload = "QUESTION1_ANSWER1"
                        },
                        new SendApiQuickReply
                        {
                            content_type = "text",
                            title = "Next Winter",
                            payload = "QUESTION1_ANSWER2"
                        },
                        new SendApiQuickReply
                        {
                            content_type = "text",
                            title = "This Coming Summer",
                            payload = "QUESTION1_ANSWER3"
                        },
                        new SendApiQuickReply
                        {
                            content_type = "text",
                            title = "I don’t know",
                            payload = "QUESTION1_ANSWER4"
                        }
                    }
                }
            };

            Task.Run(async () =>
            {
                JsonMediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter
                {
                    SerializerSettings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    }
                };

                using (var client = new HttpClient())
                {
                    await client.PostAsync(sendApiUrl + pageAccessToken, payload, jsonFormatter);
                };
            });
        }

        private void SendQuestion2(string userId, string pageAccessToken)
        {
            var payload = new Models.Api.SendApiPayload
            {
                recipient = new Models.Api.SendApiRecipient
                {
                    id = userId
                },
                message = new Models.Api.SendApiMessage
                {
                    text = "What kind of Masa program would interest you?",
                    quick_replies = new SendApiQuickReply[]
                    {
                        new SendApiQuickReply
                        {
                            content_type = "text",
                            title = "Gap Year (18-21 year old)",
                            payload = "QUESTION2_ANSWER1"
                        },
                        new SendApiQuickReply
                        {
                            content_type = "text",
                            title = "Volunteer",
                            payload = "QUESTION2_ANSWER2"
                        },
                        new SendApiQuickReply
                        {
                            content_type = "text",
                            title = "Internship (21+)",
                            payload = "QUESTION2_ANSWER3"
                        },
                        new SendApiQuickReply
                        {
                            content_type = "text",
                            title = "Study Abroad",
                            payload = "QUESTION2_ANSWER4"
                        },
                        new SendApiQuickReply
                        {
                            content_type = "text",
                            title = "Summer",
                            payload = "QUESTION2_ANSWER5"
                        }
                    }
                }
            };

            Task.Run(async () =>
            {
                JsonMediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter
                {
                    SerializerSettings = new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    }
                };

                using (var client = new HttpClient())
                {
                    await client.PostAsync(sendApiUrl + pageAccessToken, payload, jsonFormatter);
                };
            });
        }

        private async Task SendQrImageMessage(int photoId, string userId, string pageAccessToken)
        {
            var payload = new Models.Api.SendApiPayload
            {
                recipient = new Models.Api.SendApiRecipient { id = userId },
                message = new Models.Api.SendApiMessage
                {
                    attachment = new Models.Api.SendApiAttachment
                    {
                        type = "image",
                        payload = new Models.Api.SendApiAttachmentPayload
                        {
                            url = "http://chart.apis.google.com/chart?cht=qr&chs=400&chl=" + photoId
                        }
                    }
                }
            };

            JsonMediaTypeFormatter jsonFormatter = new JsonMediaTypeFormatter
            {
                SerializerSettings = new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                }
            };

            using (var client = new HttpClient())
            {
                await client.PostAsync(
                    sendApiUrl + pageAccessToken, payload, jsonFormatter);
            }
        }

        private async Task<FacebookUserProfile> GetUserProfile(string userId, string pageAccessToken)
        {
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(string.Format(userProfileApiUrl, userId, pageAccessToken));
                var userProfileJson = await response.Content.ReadAsStringAsync();
                var userProfile = Newtonsoft.Json.JsonConvert.DeserializeObject<FacebookUserProfile>(userProfileJson);
                return userProfile;
            }
        }

        public async Task<bool> HandleWebhookPost(WebhookData request)
        {
            bool result = false;

            if (request.@object == "page")
            {
                // Iterate over each entry
                // There may be multiple if batched
                foreach (var pageEntry in request.entry)
                {
                    foreach (var messagingEvent in pageEntry.messaging)
                    {
                        var resp = _apiService.GetEventSurvey(new GetEventSurveyRequest
                        {
                            PageId = pageEntry.id,
                            UserId = messagingEvent.sender.id
                        });
                        //pageEntry.id == "1606997879596528"
                        string pageAccessToken = PageAccessToken;
                        if (!string.IsNullOrEmpty(resp.FacebookPageAccessToken))
                        {
                            pageAccessToken = resp.FacebookPageAccessToken;
                        }

                        if (resp.ShowSurvey && !resp.SurveyFilled)
                        {
                            //survey bot

                            if (messagingEvent.message == null && messagingEvent.postback == null)
                            {
                                continue;
                            }

                            result = true;

                            var existsResponse = _apiService.UserExists(new UserExistsRequest
                            {
                                UserName = messagingEvent.sender.id,
                                UserType = Models.Api.UserType.Facebook
                            });
                            int userId = existsResponse.UserId;

                            if (!existsResponse.Exists)
                            {
                                var response = _apiService.CreateUser(new CreateUserRequest
                                {
                                    UserName = messagingEvent.sender.id,
                                    UserType = Models.Api.UserType.Facebook
                                });
                                userId = response.UserId;
                            }

                            if (!resp.SurveyExists)
                            {
                                var userProfile = await GetUserProfile(messagingEvent.sender.id, pageAccessToken);
                                _apiService.CreateSurvey(new CreateSurveyRequest
                                {
                                    FirstName = userProfile.first_name,
                                    LastName = userProfile.last_name,
                                    Gender = userProfile.gender == "male" ? Gender.Male : Gender.Female,
                                    UserId = userId,
                                    EventId = resp.EventId
                                });

                                await SendTextMessageAsync(messagingEvent.sender.id, "Hi there " + userProfile.first_name +
                                    ", I am the Pikchat Printing Bot and I’ll be giving you some free prints!", pageAccessToken);
                                SendIntroduction(messagingEvent.sender.id, pageAccessToken);
                            }
                            else if (messagingEvent.postback != null)
                            {
                                if (messagingEvent.postback.payload == "OKAY_LETS_DO_THIS")
                                {
                                    await SendTextMessageAsync(messagingEvent.sender.id, "Ok, great", pageAccessToken);

                                    SendQuestion1(messagingEvent.sender.id, pageAccessToken);
                                }
                            }
                            else if (messagingEvent.message != null && messagingEvent.message.quick_reply != null)
                            {
                                var payload = messagingEvent.message.quick_reply.payload;
                                if (payload.StartsWith("QUESTION1_ANSWER"))
                                {
                                    var answer = (SurveyQuestion1Answer)int.Parse(payload.Last().ToString());
                                    _apiService.AnswerSurveyQuestion1(new AnswerSurveyQuestion1Request
                                    {
                                        UserId = userId,
                                        Question1Answer = answer
                                    });

                                    await SendTextMessageAsync(messagingEvent.sender.id, "Thanks, One more to go!", pageAccessToken);

                                    SendQuestion2(messagingEvent.sender.id, pageAccessToken);
                                }
                                else if (payload.StartsWith("QUESTION2_ANSWER"))
                                {
                                    var answer = (SurveyQuestion2Answer)int.Parse(payload.Last().ToString());
                                    _apiService.AnswerSurveyQuestion2(new AnswerSurveyQuestion2Request
                                    {
                                        UserId = userId,
                                        Question2Answer = answer
                                    });

                                    switch (answer)
                                    {
                                        case SurveyQuestion2Answer.NoAnswer:
                                            break;
                                        case SurveyQuestion2Answer.GapYear:
                                            SendButtonTemplate(messagingEvent.sender.id,
                                                "Great! You should know that there are many benefits to taking a break from academics before you immerse yourself in college life.\r\n" +
                                                "Check out the amazing Gap year programs Masa Israel has to offer.\r\n" +
                                                "Feel free to email me with any questions:\r\n" +
                                                "masainfo@masaisrael.org",
                                                "Got It - Thanks", "GOT_IT_THANKS",
                                                "Visit Site", "http://www.masaisrael.org/gap-year", pageAccessToken);
                                            break;
                                        case SurveyQuestion2Answer.Volunteer:
                                            SendButtonTemplate(messagingEvent.sender.id,
                                                "Giving is receiving!" +
                                                "Make a difference in a different place.\r\n" +
                                                "Check out the dozens of opportunities to make a real impact within the challenging social tapestry of Israel society.\r\n" +
                                                "Feel free to email me for more information or with any questions masainfo@masaisrael.org,",
                                                "Got It - Thanks", "GOT_IT_THANKS",
                                                "Visit Site", "http://postcollege.masaisrael.org/volunteer/volunteer-programs/", pageAccessToken);
                                            break;
                                        case SurveyQuestion2Answer.Internship:
                                            SendButtonTemplate(messagingEvent.sender.id,
                                                "Great choice! By interning in Israel, you will have the opportunity to work in some of the best companies in the world and to do real work! Your cv will thank you.\r\n" +
                                                "Check out the incredible internships we have to offer.\r\n" +
                                                "Feel free to email me for more information or with any questions masainfo@masaisrael.org",
                                                "Got It - Thanks", "GOT_IT_THANKS",
                                                "Visit Site", "http://postcollege.masaisrael.org/", pageAccessToken);
                                            break;
                                        case SurveyQuestion2Answer.StudyAbroad:
                                            await SendTextMessageAsync(messagingEvent.sender.id,
                                                "You’ve probably heard the statistics before: Approximately 70% of college students study abroad.\r\n" +
                                                "But you probably also have a lot of questions.\r\n", pageAccessToken);
                                            SendButtonTemplate(messagingEvent.sender.id,
                                                "Get some answers and check out your amazing options to study abroad at some of the top universities in Israel.\r\n" +
                                                "Feel free to email me for more information or with any questions masainfo@masaisrael.org",
                                                "Got It - Thanks", "GOT_IT_THANKS",
                                                "Visit Site", "http://studyabroad.masaisrael.org/?utm_source=website&utm_medium=menu&utm_campaign=mainsite", pageAccessToken);
                                            break;
                                        case SurveyQuestion2Answer.Summer:
                                            SendButtonTemplate(messagingEvent.sender.id,
                                                "Masa Israel has teamed up with Onward Israel to offer you 6 - 8 week programs!\r\n" +
                                                "For more information check out their website.",
                                                "Got It - Thanks", "GOT_IT_THANKS",
                                                "Visit Site", "http://join.masaisrael.org/OnwardIsrael", pageAccessToken);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                SendTextMessage(messagingEvent.sender.id, "Hey I’m not that smart. try sticking with the buttons or type in a promo code for more print credits", pageAccessToken);
                            }
                        }
                        else
                        {
                            if (messagingEvent.message == null && messagingEvent.postback == null)
                            {
                                continue;
                            }

                            result = true;

                            bool exists = _apiService.UserExists(new UserExistsRequest
                            {
                                UserName = messagingEvent.sender.id,
                                UserType = Models.Api.UserType.Facebook
                            }).Exists;

                            if (!exists)
                            {
                                _apiService.CreateUser(new CreateUserRequest
                                {
                                    UserName = messagingEvent.sender.id,
                                    UserType = Models.Api.UserType.Facebook
                                });
                            }

                            if (messagingEvent.postback != null)
                            {
                                if (resp.ShowSurvey && messagingEvent.postback.payload == "GOT_IT_THANKS")
                                {
                                    SendTextMessage(messagingEvent.sender.id, "Thanks a lot!\r\nNow go ahead and send me any picture!", pageAccessToken);
                                }
                                else
                                {
                                    SendTextMessage(messagingEvent.sender.id, "Hi there I’m Pikchat!\r\nSend me any picture you want and " +
                                        "I’ll send you a special code just for you :)\r\nMeanwhile Iv’e added some credits to your balance " +
                                        "so you could start right away.\r\nGo ahead, try me!", pageAccessToken);
                                }
                            }
                            else if (messagingEvent.message != null)
                            {
                                var message = messagingEvent.message;

                                if (!string.IsNullOrEmpty(message.text))
                                {
                                    var response = _apiService.ActivatePromoCode(new ActivatePromoCodeRequest
                                    {
                                        UserName = messagingEvent.sender.id,
                                        UserType = Models.Api.UserType.Facebook,
                                        PromoCode = message.text
                                    });

                                    if (!response.Success)
                                    {
                                        if (!response.PromoCodeExists)
                                        {
                                            SendTextMessage(messagingEvent.sender.id, "We couldn't find promotion code you entered", pageAccessToken);
                                        }
                                        else if (response.PromoCodeIsAlreadyUsed)
                                        {
                                            SendTextMessage(messagingEvent.sender.id, "That promotion code is already used", pageAccessToken);
                                        }
                                    }
                                    else
                                    {
                                        SendTextMessage(messagingEvent.sender.id, "Hooray! Credits were added to your balance", pageAccessToken);
                                    }
                                }
                                else if (message.attachments != null)
                                {
                                    foreach (var attachment in message.attachments)
                                    {
                                        if (attachment.type == "image" && attachment.payload != null && !string.IsNullOrEmpty(attachment.payload.url))
                                        {
                                            var response = _apiService.UploadIsAvailable(new UploadIsAvailableRequest
                                            {
                                                UserName = messagingEvent.sender.id,
                                                UserType = Models.Api.UserType.Facebook
                                            });

                                            if (!response.Available)
                                            {
                                                SendTextMessage(messagingEvent.sender.id, "Awww Snap! Not enough credits. Type in a new code", pageAccessToken);
                                            }
                                            else
                                            {
                                                var uploadResponse = await UploadImage(attachment.payload.url, messagingEvent.sender.id);
                                                if (uploadResponse.Success)
                                                {
                                                    await SendQrImageMessage(uploadResponse.PhotoId, messagingEvent.sender.id, pageAccessToken);
                                                    if (resp.ShowSurvey)
                                                    {
                                                        await SendTextMessageAsync(messagingEvent.sender.id, "Was great to see you! Have fun!", pageAccessToken);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }
    }
}
