﻿using FixItTron.Domain.Db;
using FixItTron.Domain.Model;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Domain.Services;
using Wandio.Domain;
using FixItTron.Services.Models.Lookup;
using FixItTron.Services.Models.Lookup.Districts;
using FixItTron.Services.Models.Lookup.Cities;
using FixItTron.Services.Models.Lookup.Countries;
using FixItTron.Services.Models.Lookup.ProductClass;
using FixItTron.Services.Models.Lookup.UnitOfMeasure;
using FixItTron.Services.Models.Lookup.TaxGroups;
using FixItTron.Services.Models.Lookup.ProductType;
using Common.Enums;
using FixItTron.Services.Models.Lookup.Reason;
using FixItTron.Services.Models.Lookup.WarehouseTypes;
using FixItTron.Services.Models.Lookup.ScheduleAdditionalStatuses;
using AutoMapper;
using FixItTron.Services.Helpers;
using FixItTron.Services.Models.Document;
using Wandio.Domain.Services;
using FixItTron.Services.Models.Lookup.Position;
using FixItTron.Services.Models.Lookup.Currencies;
using FixItTron.Services.Models.Lookup.Banks;
using FixItTron.Services.Models.Lookup.PaymentClassifier;
using FixItTron.Services.Models.Lookup.BoxOffices;
using FixItTron.Services.Models.Lookup.AbsenseEmployee;
using FixItTron.Services.Models.Lookup.AbsenseTypes;
using FixItTron.Services.Models.Lookup.ObjectAdditionals;
using FixItTron.Services.Models.Lookup.ApplicationUsers;

namespace FixItTron.Services
{
    public class LookupService : ILookupService
    {
        PikchaDbContext Db;

        public LookupService(PikchaDbContext db)
        {
            Db = db;
            ServiceMapperConfiguration.Configure(AutoMapper.Mapper.Configuration);
        }

        #region selectLists
        public IEnumerable<LookupModel> GetEvents()
        {
            return Db.Set<Event>().Select(x => new LookupModel
            {
                Id = x.EventId,
                Name = x.Name
            });
        }

        public IEnumerable<LookupModel> GetCompanies()
        {
            return Db.Set<Company>().Select(x => new LookupModel
            {
                Id = x.CompanyId,
                Name = x.Name
            });
        }
        #endregion

        #region applicationUsers

        public GetApplicationUserListResponse GetApplicationUserList(GetApplicationUserListRequest request)
        {
            var baseQuery = Db.Set<User>()
                .Filter(request.Username, x => x.Username)
                .Filter(request.FullName, x => x.FullName)
                .Filter(request.Email, x => x.Email)
                .Filter(request.AvailablePrints, x => x.AvailablePrints)
                .Filter(request.UsedPrints, x => x.UsedPrints)
                .Filter(request.CreateDate, x => x.CreateDate)
                .And(request.UserType, x => x.UserType == request.UserType);
            
            var totalCount = baseQuery.Count();

            var items = baseQuery
                .SortAndPage(request)
                .Select(x => new ApplicationUserListItem
                {
                    UserId = x.UserId,
                    FullName = x.FullName,
                    Username = x.Username,
                    Email = x.Email,
                    AvailablePrints = x.AvailablePrints,
                    UsedPrints = x.UsedPrints,
                    CreateDate = x.CreateDate,
                    UserType = x.UserType
                });

            return new GetApplicationUserListResponse
            {
                TotalCount = totalCount,
                Users = items
            };
        }

        public DeleteApplicationUserResponse DeleteApplicationUser(DeleteApplicationUserRequest request)
        {
            Db.Set<Photo>().RemoveRange(Db.Set<Photo>().Where(x => x.CreateUserId == request.ApplicationUserId));

            Db.Set<UserPromoCode>().RemoveRange(Db.Set<UserPromoCode>().Where(x => x.UserId == request.ApplicationUserId));

            Db.SaveChanges();

            User appuser = Db.Set<User>().Find(request.ApplicationUserId);
            Db.Set<User>().Remove(appuser);

            Db.SaveChanges();

            return new DeleteApplicationUserResponse
            {
                Success = true
            };
        }

        public DeleteApplicationUsersResponse DeleteApplicationUsers(DeleteApplicationUsersRequest request)
        {
            Db.Set<Photo>().RemoveRange(Db.Set<Photo>().Where(x => x.CreateUserId.HasValue && request.Ids.Contains(x.CreateUserId.Value)));

            Db.Set<UserPromoCode>().RemoveRange(Db.Set<UserPromoCode>().Where(x => x.UserId.HasValue && request.Ids.Contains(x.UserId.Value)));

            Db.SaveChanges();

            foreach (var id in request.Ids)
            {
                var user = Db.Set<User>().Find(id);
                Db.Entry(user).State = System.Data.Entity.EntityState.Deleted;
            }

            Db.SaveChanges();

            return new DeleteApplicationUsersResponse
            {
                Success = true
            };
        }

        #endregion

        #region survey

        public GetSurveyListResponse GetSurveyList(GetSurveyListRequest request)
        {
            var baseQuery = Db.Set<Survey>()
                .And(request.EventName, x => x.Event.Name.Contains(request.EventName))
                .And(request.Username, x => x.User.Username.Contains(request.Username))
                .And(request.UserType, x => x.User.UserType == request.UserType)
                .And(request.SourceType, x => x.SourceType == request.SourceType)
                .Filter(request.FirstName, x => x.FirstName)
                .Filter(request.LastName, x => x.LastName)
                .Filter(request.CreateDate, x => x.CreateDate)
                .And(request.Gender, x => x.Gender == request.Gender)
                .And(request.Question1, x => x.Question1 == request.Question1)
                .And(request.Question2, x => x.Question2 == request.Question2);

            var totalCount = baseQuery.Count();

            var items = baseQuery
                .SortAndPage(request)
                .Select(x => new SurveyListItem
                {
                    EventName = x.Event.Name,
                    Username = x.User.Username,
                    UserType = x.User.UserType,
                    SourceType = x.SourceType,
                    Email = x.Email,
                    CreateDate = x.CreateDate,
                    SurveyId = x.SurveyId,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Gender = x.Gender,
                    Question1 = x.Question1,
                    Question2 = x.Question2
                });

            return new GetSurveyListResponse
            {
                TotalCount = totalCount,
                Surveys = items
            };
        }

        #endregion
    }
}
