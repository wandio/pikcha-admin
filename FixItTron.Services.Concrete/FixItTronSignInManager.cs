﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin;
using FixItTron.Services.Models.User;

namespace FixItTron.Services
{
    public class FixItTronSignInManager : SignInManager<AdminUser, int>
    {
        public FixItTronSignInManager(FixItTronUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {

        }

        public static FixItTronSignInManager Create(IdentityFactoryOptions<FixItTronSignInManager> options, IOwinContext context)
        {
            return new FixItTronSignInManager(context.GetUserManager<FixItTronUserManager>(), context.Authentication);
        }

        public AuthenticateResponse Authenticate(AuthenticateRequest request)
        {
            var result = this.PasswordSignIn(request.UserName, request.Password, request.RememberMe, false);

            if (result == SignInStatus.Success)
            {
                return new AuthenticateResponse()
                {
                    Success = true
                };
            }

            return new AuthenticateResponse()
            {
                Success = false,
                ErrorMessage = result.ToString()
            };
        }
    }
}
