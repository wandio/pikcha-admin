﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Services.Interfaces;
using FixItTron.Domain.Db;
using FixItTron.Services.Helpers;
using FixItTron.Services.Models.Configurations;
using FixItTron.Domain.Model;
using AutoMapper;
using Wandio.Domain;

namespace FixItTron.Services
{
    public class ConfigurationsService : IConfigurationsService
    {
        PikchaDbContext Db;

        public ConfigurationsService(PikchaDbContext db)
        {
            Db = db;
            ServiceMapperConfiguration.Configure(AutoMapper.Mapper.Configuration);
        }

        public GetConfigurationListResponse GetConfigurationList(GetConfigurationListRequest request)
        {
            var baseQuery = Db.Set<Configuration>()
                .Filter(request.Name, x => x.Name)
                .Filter(request.Value, x => x.Value);

            var totalCount = baseQuery.Count();

            var items = baseQuery
                .SortAndPage(request)
                .Select(x => new ConfigurationItem
                {
                    ConfigurationId = x.ConfigurationId,
                    Name = x.Name,
                    Value = x.Value
                });

            return new GetConfigurationListResponse
            {
                Configurations = items,
                TotalCount = totalCount
            };
        }

        public GetConfigurationForUpdateResponse GetConfigurationForUpdate(GetConfigurationForUpdateRequest request)
        {
            Configuration Configuration = Db.Set<Configuration>().Find(request.ConfigurationId);

            var result = Mapper.Map<ConfigurationItem>(Configuration);

            return new GetConfigurationForUpdateResponse
            {
                Configuration = result
            };
        }

        public UpdateConfigurationResponse UpdateConfiguration(UpdateConfigurationRequest request)
        {
            var configuration = Db.Set<Configuration>().Find(request.ConfigurationId);
            configuration.Value = request.Value;
            //configuration = Mapper.Map<AddEditConfigurationItem, Configuration>(request.Configuration, configuration);

            configuration.LastChangeDate = DateTime.Now;

            Db.Entry(configuration).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();

            return new UpdateConfigurationResponse()
            {
                Success = true
            };
        }
    }
}
