﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Services.Interfaces;
using FixItTron.Domain.Db;
using FixItTron.Services.Helpers;
using FixItTron.Domain.Model;
using AutoMapper;
using FixItTron.Services.Models.PromoCode;
using Wandio.Domain;

namespace FixItTron.Services
{
    public class PromoCodeService : IPromoCodeService
    {
        PikchaDbContext Db;

        public PromoCodeService(PikchaDbContext db)
        {
            Db = db;
            ServiceMapperConfiguration.Configure(AutoMapper.Mapper.Configuration);
        }

        public GetPromoCodeListResponse GetPromoCodeList(GetPromoCodeListRequest request)
        {
            var baseQuery = Db.Set<PromoCode>()
                .Filter(request.Code, x => x.Code)
                .Filter(request.MultiUse, x => x.MultiUse)
                .Filter(request.PrintCount, x => x.PrintCount)
                .Filter(request.Description, x => x.Description)
                .And(request.EventId, x => x.EventId == request.EventId);

            var totalCount = baseQuery.Count();

            var items = baseQuery
                .SortAndPage(request)
                .Select(x => new PromoCodeListItem
                {
                    PromoCodeId = x.PromoCodeId,
                    Code = x.Code,
                    Event = x.Event != null ? x.Event.Name : string.Empty,
                    MultiUse = x.MultiUse,
                    PrintCount = x.PrintCount,
                    Description = x.Description
                });

            return new GetPromoCodeListResponse
            {
                PromoCodes = items,
                TotalCount = totalCount
            };
        }

        public DeletePromoCodeResponse DeletePromoCode(DeletePromoCodeRequest request)
        {
            var any = Db.Set<UserPromoCode>().Any(x => x.PromoCodeId == request.PromoCodeId);

            if (any)
            {
                return new DeletePromoCodeResponse
                {
                    Success = false,
                    ErrorMessage = ServiceResources.CannotDeletePromoCode
                };

            }

            PromoCode promoCode = Db.Set<PromoCode>().Find(request.PromoCodeId);
            Db.Set<PromoCode>().Remove(promoCode);
            Db.SaveChanges();

            return new DeletePromoCodeResponse
            {
                Success = true
            };
        }

        public DeletePromoCodesResponse DeletePromoCodes(DeletePromoCodesRequest request)
        {
            var any = Db.Set<UserPromoCode>().Any(x => request.Ids.Contains(x.PromoCodeId.Value));

            if (any)
            {
                return new DeletePromoCodesResponse
                {
                    Success = false,
                    ErrorMessage = ServiceResources.CannotDeletePromoCode
                };
            }

            foreach (var id in request.Ids)
            {
                PromoCode promoCode = Db.Set<PromoCode>().Find(id);
                Db.Entry(promoCode).State = System.Data.Entity.EntityState.Deleted;
            }

            Db.SaveChanges();

            return new DeletePromoCodesResponse
            {
                Success = true
            };
        }

        public CreatePromoCodeResponse CreatePromoCode(CreatePromoCodeRequest request)
        {
            if (Db.Set<PromoCode>().Where(p => p.Code == request.PromoCode.Code).FirstOrDefault() != null)
            {
                return new CreatePromoCodeResponse()
                {
                    Success = false,
                    ErrorMessage = ServiceResources.CodeIsNotUnique
                };
            }

            PromoCode promoCode = Mapper.Map<PromoCode>(request.PromoCode);
            promoCode.CreateDate = DateTime.Now;

            var userId = Db.Set<AdminUser>().Where(u => u.UserName == request.PromoCode.UserName).Select(u => u.Id).Single();
            promoCode.CreateAdminUserId = userId;

            Db.Set<PromoCode>().Add(promoCode);
            Db.SaveChanges();

            return new CreatePromoCodeResponse()
            {
                Success = true,
                PromoCodeId = promoCode.PromoCodeId
            };
        }

        public GetPromoCodeForUpdateResponse GetPromoCodeForUpdate(GetPromoCodeForUpdateRequest request)
        {
            PromoCode promoCode = Db.Set<PromoCode>().Find(request.PromoCodeId);

            var result = Mapper.Map<AddEditPromoCodeItem>(promoCode);

            return new GetPromoCodeForUpdateResponse
            {
                PromoCode = result
            };
        }

        public UpdatePromoCodeResponse UpdatePromoCode(UpdatePromoCodeRequest request)
        {
            if (Db.Set<PromoCode>().Where(p => p.Code == request.PromoCode.Code && p.PromoCodeId != request.PromoCode.PromoCodeId).FirstOrDefault() != null)
            {
                return new UpdatePromoCodeResponse()
                {
                    Success = false,
                    ErrorMessage = ServiceResources.CodeIsNotUnique
                };
            }

            var promoCode = Db.Set<PromoCode>().Find(request.PromoCode.PromoCodeId);
            promoCode = Mapper.Map<AddEditPromoCodeItem, PromoCode>(request.PromoCode, promoCode);

            Db.Entry(promoCode).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();

            return new UpdatePromoCodeResponse()
            {
                Success = true
            };
        }

        public AddPromoCodesResponse AddPromoCodes(AddPromoCodesRequest request)
        {
            foreach (var item in request.PromoCodes)
            {
                if (Db.Set<PromoCode>().Where(p => p.Code == item.Code).FirstOrDefault() != null)
                {
                    return new AddPromoCodesResponse()
                    {
                        Success = false,
                        ErrorMessage = ServiceResources.CodeIsNotUnique
                    };
                }

                PromoCode promoCode = Mapper.Map<PromoCode>(item);

                var events = Db.Set<Event>().Where(e => e.Name == item.EventName).FirstOrDefault();
                if (events != null)
                {
                    promoCode.EventId = events.EventId;
                }

                promoCode.CreateDate = DateTime.Now;

                var userId = Db.Set<AdminUser>().Where(u => u.UserName == item.UserName).Select(u => u.Id).Single();
                promoCode.CreateAdminUserId = userId;

                Db.Set<PromoCode>().Add(promoCode);
            }


            Db.SaveChanges();

            return new AddPromoCodesResponse()
            {
                Success = true
            };
        }
    }
}
