﻿using FixItTron.Domain.Db;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Web.Common;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using FixItTron.Services.Interfaces;
using System.Configuration;

namespace FixItTron.Services
{
    public class DependencyInjector
    {
        public static void RegisterNinjectServices(IKernel kernel)
        {
            kernel.Bind<PikchaDbContext>().ToSelf().InRequestScope();
            kernel.Bind<FixItTronUserManager>().ToMethod(c => HttpContext.Current.GetOwinContext().GetUserManager<FixItTronUserManager>());
            kernel.Bind<FixItTronSignInManager>().ToMethod(c => HttpContext.Current.GetOwinContext().GetUserManager<FixItTronSignInManager>());
            kernel.Bind<FixItTronRoleManager>().ToMethod(c => HttpContext.Current.GetOwinContext().GetUserManager<FixItTronRoleManager>());
            kernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication);
            kernel.Bind<IRoleService>().To<RoleService>().InRequestScope();
            kernel.Bind<ILookupService>().To<LookupService>().InRequestScope();

            //USERMANAGER
            kernel.Bind<IPromoCodeService>().To<PromoCodeService>().InRequestScope();
            kernel.Bind<IEventService>().To<EventService>().InRequestScope();
            kernel.Bind<ICompanyService>().To<CompanyService>().InRequestScope();
            kernel.Bind<IConfigurationsService>().To<ConfigurationsService>().InRequestScope();
            kernel.Bind<IApiService>().To<ApiService>().InRequestScope();
            kernel.Bind<IImageUploadService>().To<ImageUploadService>().InRequestScope();
        }

        public static void RegisterIdentityServices(IAppBuilder app)
        {
            app.CreatePerOwinContext(PikchaDbContext.Create);
            app.CreatePerOwinContext<FixItTronUserManager>(FixItTronUserManager.Create);
            app.CreatePerOwinContext<FixItTronSignInManager>(FixItTronSignInManager.Create);
            app.CreatePerOwinContext<FixItTronRoleManager>(FixItTronRoleManager.Create);
        }
    }
}
