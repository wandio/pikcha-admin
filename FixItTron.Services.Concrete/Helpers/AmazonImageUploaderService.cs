﻿using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Helpers
{
    public static class AmazonImageUploaderService
    {
        private static string _awsAccessKeyId;
        public static string AwsAccessKeyId
        {
            get
            {
                return _awsAccessKeyId;
            }
            set
            {
                _awsAccessKeyId = value;
            }
        }

        private static string _awsSecretAccessKey;
        public static string AwsSecretAccessKey
        {
            get
            {
                return _awsSecretAccessKey;
            }
            set
            {
                _awsSecretAccessKey = value;
            }
        }

        private static string _awsImagePathPrefix;
        public static string AwsImagePathPrefix
        {
            get
            {
                return _awsImagePathPrefix;
            }
            set
            {
                _awsImagePathPrefix = value;
            }
        }

        private static string _awsAdminImagePathPrefix;
        public static string AwsAdminImagePathPrefix
        {
            get
            {
                return _awsAdminImagePathPrefix;
            }
            set
            {
                _awsAdminImagePathPrefix = value;
            }
        }

        public static string UploadImage(byte[] data, string bucketName = "pikchaimg")
        {
            var client = new AmazonS3Client(AwsAccessKeyId, AwsSecretAccessKey, Amazon.RegionEndpoint.EUWest1);
            string name = Path.ChangeExtension(Guid.NewGuid().ToString(), ".png");
            MemoryStream ms = new MemoryStream(data);

            ms.Position = 0;

            PutObjectRequest p = new PutObjectRequest();
            p.InputStream = ms;
            p.Key = name;
            p.BucketName = bucketName;
           
            p.CannedACL = S3CannedACL.PublicRead;

            client.PutObject(p);

            return name;
        }
    }
}
