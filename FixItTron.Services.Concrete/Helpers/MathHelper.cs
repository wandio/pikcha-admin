﻿using Common.Enums;
using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Helpers
{
    public static class MathHelper
    {
        /// <summary>
        /// Used specifically for product cost. Formats as 4 decimal points 
        /// </summary>
        /// <param name="cost"></param>
        /// <returns></returns>
        public static decimal? ToCost(this decimal? cost)
        {
            if (!cost.HasValue)
            {
                return cost;
            }
            return cost.Value.ToCost();
        }

        /// <summary>
        /// Used specifically for product cost. Formats as 4 decimal points 
        /// </summary>
        /// <param name="cost"></param>
        /// <returns></returns>
        public static decimal ToCost(this decimal cost, bool highPrecision = false)
        {
            return Math.Round(cost, 4);
        }

        public static decimal? ToAmount(this decimal? cost, bool highPrecision = false)
        {
            if (!cost.HasValue)
            {
                return cost;
            }
            return cost.Value.ToAmount(highPrecision);
        }

        public static decimal ToAmount(this decimal cost, bool highPrecision = false)
        {
            if (highPrecision)
            {
                return Math.Round(cost, 5);
            }
            return Math.Round(cost, 2);
        }

        public static decimal? ToQuantity(this decimal? quantity, bool highPrecision = false)
        {
            if (!quantity.HasValue)
            {
                return quantity;
            }
            return !highPrecision ? Math.Round(quantity.Value, 3) : Math.Round(quantity.Value, 5);
        }

        public static decimal ToQuantity(this decimal quantity)
        {
            return Math.Round(quantity, 2);
        }

        public static void GetDiscountPrice(decimal price, DiscountType dicountType, decimal discountVal, out decimal discountPrice)
        {
            switch (dicountType)
            {
                case DiscountType.DiscountPercent:
                    discountPrice = price - (price * (discountVal / 100));
                    break;
                case DiscountType.DiscountAmount:
                    discountPrice = price - discountVal;
                    break;
                default:
                    discountPrice = discountVal;
                    break;
            }
        }

        public static decimal GetQuantityByUnit(decimal originalQuantity, decimal? productUnit)
        {
            var unit = productUnit ?? 1;

            if (originalQuantity < unit)
            {
                return 1;
            }

            var quantityByUnit = Math.Ceiling((originalQuantity / unit));
            return quantityByUnit;
        }
    }
}
