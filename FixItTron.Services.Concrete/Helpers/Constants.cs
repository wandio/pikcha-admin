﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Helpers
{
    public static class Constants
    {
        public static class SystemServiceCodes
        {
            public const string AccrualCode = "Accrual";
            public const string SubscriptionCode = "Subscription";
        }

        public class SystemPaymentClassifierCodes
        {
            public const string BoxExpense = "BOXEXP";
            public const string BoxIncome = "BOXINC";
            public const string BankExpense = "BANKEXP";
            public const string BankIncome = "BANKINC";

            public const string IncomeFromContractor = "CONTRINC";
            public const string PayToContractor = "CONTREXP";

        }

        public class EmployeePositionConstants
        {
            public const string Operator = "Operator";
        }

        public class SystemCancelReasonCodes
        {
            public const string ScheduleReassigned = "ScheduleReassigned";
        }
    }
}
