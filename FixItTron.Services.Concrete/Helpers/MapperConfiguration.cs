﻿using FixItTron.Domain.Db;
using FixItTron.Domain.Model;
using FixItTron.Services.Models.Role;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Domain.Services;
using Common.Enums;
using FixItTron.Services.Models.Document;
using FixItTron.Services.Models.Lookup.ProductType;
using FixItTron.Services.Models.Configuration;
using FixItTron.Services.Models.User;
using FixItTron.Services.Models.Lookup.Position;
using FixItTron.Services.Models.Lookup.Currencies;
using FixItTron.Services.Models.Company;
using FixItTron.Services.Models.Configurations;
using FixItTron.Services.Models.Event;
using FixItTron.Services.Models.PromoCode;

namespace FixItTron.Services.Helpers
{
    public static class ServiceMapperConfiguration
    {
        public static void Configure(AutoMapper.IConfiguration configuration)
        {
            #region pikcha

            #region company
            configuration.CreateMap<AddEditCompanyItem, Company>();
            configuration.CreateMap<Company, AddEditCompanyItem>();
            #endregion

            #region promoCode
            configuration.CreateMap<AddEditPromoCodeItem, PromoCode>();
            configuration.CreateMap<PromoCode, AddEditPromoCodeItem>();
            #endregion

            #region configuration
            configuration.CreateMap<Configuration, ConfigurationItem>();
            #endregion

            #region event
            configuration.CreateMap<AddEditEventItem, Event>()
                .ForMember(dest => dest.LogoFileName, options => options.Ignore())
                .ForMember(dest => dest.MobileLogoFileName, options => options.Ignore())
                .ForMember(dest => dest.InstagramLogoFileName, options => options.Ignore())
                .ForMember(dest => dest.FacebookLogoFileName, options => options.Ignore())
                .ForMember(dest => dest.BackgroundLogoFileName, options => options.Ignore());
            configuration.CreateMap<Event, AddEditEventItem>();
            configuration.CreateMap<Document, FixItTron.Services.Models.Event.DocumentListItem>();//////////////////
            configuration.CreateMap<FixItTron.Services.Models.Event.UploadedFileListItem, UploadedFile>();///////////
            configuration.CreateMap<UploadedFile, FixItTron.Services.Models.Event.UploadedFileListItem>();////////////
            configuration.CreateMap<FixItTron.Services.Models.Event.DocumentListItem, Document>();//////////
            #endregion

            #endregion



            configuration.CreateMap<CreateRoleRequest, Role>()
               .ForMember(dest => dest.Permissions, options => options.ResolveUsing(src => MapPermissions(src.Permissions)));

            configuration.CreateMap<Role, GetRoleResponse>()
                .ForMember(dest => dest.Permissions, options => options.ResolveUsing(src => ResolvePermissionsFromDomain(src.Permissions)));

            configuration.CreateMap<UpdateRoleRequest, Role>()
               .ForMember(dest => dest.Permissions, options => options.ResolveUsing(src => MapPermissions(src.Permissions)));

        }

        private static IEnumerable<EditRolePermissionItem> ResolvePermissionsFromDomain(ICollection<RoleClaim> collection)
        {
            return collection.Select(roleModel => new EditRolePermissionItem()
            {
                Securable = roleModel.ClaimType.ExtractEnum<Securable>(),
                Create = roleModel.ClaimValue.ExtractEnum<Permissions>().HasFlag(Permissions.Create),
                Read = roleModel.ClaimValue.ExtractEnum<Permissions>().HasFlag(Permissions.Read),
                Update = roleModel.ClaimValue.ExtractEnum<Permissions>().HasFlag(Permissions.Update),
                Delete = roleModel.ClaimValue.ExtractEnum<Permissions>().HasFlag(Permissions.Delete),
                Book = roleModel.ClaimValue.ExtractEnum<Permissions>().HasFlag(Permissions.Book),
                Approve = roleModel.ClaimValue.ExtractEnum<Permissions>().HasFlag(Permissions.Approve),
                Post = roleModel.ClaimValue.ExtractEnum<Permissions>().HasFlag(Permissions.Post),
                PowerPermission = roleModel.ClaimValue.ExtractEnum<Permissions>().HasFlag(Permissions.PowerPermission),
                Cancel = roleModel.ClaimValue.ExtractEnum<Permissions>().HasFlag(Permissions.Cancel)
            }).ToArray();
        }

        private static ICollection<RoleClaim> MapPermissions(IList<EditRolePermissionItem> list)
        {
            Func<EditRolePermissionItem, Permissions> extractor = roleModel =>
            {
                Permissions result = Permissions.None;
                Action<bool, Permissions> setPermission = (condition, permission) =>
                {
                    if (condition)
                    {
                        result = result | permission;
                    }
                };

                setPermission(roleModel.Create, Permissions.Create);
                setPermission(roleModel.Read, Permissions.Read);
                setPermission(roleModel.Update, Permissions.Update);
                setPermission(roleModel.Delete, Permissions.Delete);
                setPermission(roleModel.Book, Permissions.Book);
                setPermission(roleModel.Approve, Permissions.Approve);
                setPermission(roleModel.Post, Permissions.Post);
                setPermission(roleModel.Cancel, Permissions.Cancel);
                setPermission(roleModel.PowerPermission, Permissions.PowerPermission);

                return result;
            };

            return list.Select(roleModel => new RoleClaim()
            {
                ClaimType = roleModel.Securable.ExtractString(),
                ClaimValue = extractor(roleModel).ExtractString()
            }).Where(permission => permission.ClaimValue != Permissions.None.ExtractString()).ToArray();
        }
    }
}