﻿using FixItTron.Domain.Db;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Domain.Services;
using FixItTron.Domain.Model;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Helpers;
using System.Linq.Expressions;
using FixItTron.Services.Models.Configuration;
using Common.Enums;
using System.Text.RegularExpressions;

namespace FixItTron.Services
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Sums but checks before any
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="source"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static decimal SumIfAny<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector)
        {
            if (source.Any())
            {
                return source.Sum(selector);
            }

            return default(decimal);
        }

        public static decimal? SumIfAny<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector)
        {
            if (source.Any())
            {
                return source.Sum(selector);
            }

            return null;
        }

        public static TEnum ExtractEnum<TEnum>(this string value) where TEnum : struct, IConvertible
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("T must be of enum type");
            }

            TEnum result;
            if (Enum.TryParse<TEnum>(value, out result))
            {
                return result;
            }

            return default(TEnum);
        }

        public static TEnum ConvertEnum<TEnum>(this Enum source)
        {
            if (source != null)
            {
                return (TEnum)Enum.Parse(typeof(TEnum), source.ToString(), true);
            }
            return default(TEnum);
        }

        /// <summary>
        /// converts securable to string value
        /// </summary>
        public static string ExtractString(this Securable securable)
        {
            return ((int)securable).ToString();
        }

        /// <summary>
        /// converts permission to string value
        /// </summary>
        public static string ExtractString(this Permissions permission)
        {
            return ((int)permission).ToString();
        }

        public static string GetPasswordHash(this string password)
        {
            var sha1 = new SHA1CryptoServiceProvider();
            var textToShash = Encoding.Default.GetBytes(password);
            var result = sha1.ComputeHash(textToShash);
            var resultText = Convert.ToBase64String(result);
            resultText = resultText.Replace("+", "_");
            return resultText;
        }

        public static string ResolveCode(this string code)
        {
            return code == ServiceResources.NewCodeIndicator ? string.Empty : code;
        }

        public static float ExtractHour(this DateTime date)
        {
            float result = date.Hour;
            if (date.Minute > 0)
            {
                result += 0.3f;//30 minutes representation
            }
            return result;
        }

        public static string GetSubstring(this string str, int lengthToCut = 30)
        {
            if (str.Length > lengthToCut)
                return str.Substring(0, lengthToCut);
            else
                return str;
        }
        public static string GetWithoutHtml(this string str)
        {
            return Regex.Replace(str, "<(.|\\n)*?>", string.Empty);
        }
    }
}
