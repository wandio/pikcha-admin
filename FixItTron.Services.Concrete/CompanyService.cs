﻿using FixItTron.Domain.Db;
using FixItTron.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Company;
using FixItTron.Domain.Model;
using AutoMapper;
using Wandio.Domain;

namespace FixItTron.Services
{
    public class CompanyService : ICompanyService
    {
        PikchaDbContext Db;

        public CompanyService(PikchaDbContext db)
        {
            ServiceMapperConfiguration.Configure(AutoMapper.Mapper.Configuration);
            Db = db;
        }

        public GetCompanyListResponse GetCompanyList(GetCompanyListRequest request)
        {
            var baseQuery = Db.Set<Company>()
                .Filter(request.Name, x => x.Name);

            var totalCount = baseQuery.Count();

            var items = baseQuery
                .SortAndPage(request)
                .Select(x => new CompanyListItem
                {
                    CompanyId = x.CompanyId,
                    Name = x.Name
                });

            return new GetCompanyListResponse
            {
                Companies = items,
                TotalCount = totalCount
            };
        }

        public DeleteCompanyResponse DeleteCompany(DeleteCompanyRequest request)
        {
            if (Db.Set<Event>().Any(c => c.CompanyId == request.CompanyId))
            {
                return new DeleteCompanyResponse
                {
                    Success = false,
                    ErrorMessage = ServiceResources.CannotDeleteCompanyHasRelationships
                };
            }

            Company company = Db.Set<Company>().Find(request.CompanyId);

            Db.Set<Company>().Remove(company);
            Db.SaveChanges();

            return new DeleteCompanyResponse
            {
                Success = true
            };
        }

        public CreateCompanyResponse CreateCompany(CreateCompanyRequest request)
        {
            Company company = Mapper.Map<Company>(request.Company);
            company.CreateDate = DateTime.Now;

            var userId = Db.Set<AdminUser>().Where(u => u.UserName == request.Company.UserName).Select(u => u.Id).Single();
            company.CreateAdminUserId = userId;

            Db.Set<Company>().Add(company);
            Db.SaveChanges();

            return new CreateCompanyResponse()
            {
                Success = true,
                CompanyId = company.CompanyId
            };
        }

        public GetCompanyForUpdateResponse GetCompanyForUpdate(GetCompanyForUpdateRequest request)
        {
            Company company = Db.Set<Company>().Find(request.CompanyId);

            var result = Mapper.Map<AddEditCompanyItem>(company);

            return new GetCompanyForUpdateResponse
            {
                Company = result
            };
        }

        public UpdateCompanyResponse UpdateCompany(UpdateCompanyRequest request)
        {
            var company = Db.Set<Company>().Find(request.Company.CompanyId);
            company = Mapper.Map<AddEditCompanyItem, Company>(request.Company, company);

            company.LastChangeDate = DateTime.Now;

            var userId = Db.Set<AdminUser>().Where(u => u.UserName == request.Company.UserName).Select(u => u.Id).Single();
            company.ChangeAdminUserId = userId;

            Db.Entry(company).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();

            return new UpdateCompanyResponse()
            {
                Success = true
            };
        }
    }
}
