﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using FixItTron.Domain.Model;
using FixItTron.Services.Models.User;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using FixItTron.Domain.Db;
using Microsoft.AspNet.Identity.EntityFramework;
using FixItTron.Services.Helpers;
using AutoMapper;
using FixItTron.Services.Models.Document;

namespace FixItTron.Services
{
    public class FixItTronUserManager : UserManager<AdminUser, int>
    {
        PikchaDbContext Db;

        public FixItTronUserManager(IUserStore<AdminUser, int> store, PikchaDbContext db)
            : base(store)
        {
            ServiceMapperConfiguration.Configure(AutoMapper.Mapper.Configuration);
            Db = db;
            //this.PasswordHasher = new CryptoPasswordHasher();
        }

        public GetUserRolesResponse GetUserRoles(GetUserRolesRequest request)
        {
            var roles = Db.Set<UserRole>().Where(u => u.UserId == request.UserId);
            List<int> ids = new List<int>();

            foreach (var role in roles)
            {
                ids.Add(role.RoleId);
            }

            return new GetUserRolesResponse
            {
                RoleIds = ids
            };
        }

        public CreateUserResponse CreateUser(CreateUserRequest request)
        {
            AdminUser user = new AdminUser()
            {
                UserName = request.UserName,
                Email = request.Email
            };

            var result = this.Create(user, request.Password);

            if (result.Succeeded)
            {
                return new CreateUserResponse()
                {
                    Success = true
                };
            }

            return new CreateUserResponse()
            {
                Success = false,
                ErrorMessage = string.Join(", ", result.Errors)
            };
        }

        public void RefreshUserClaims(AdminUser user)
        {
            var roles = user.Roles;
            user.Claims.ToList().ForEach(x => Db.Entry(x).State = System.Data.Entity.EntityState.Deleted);

            foreach (var role in roles)
            {
                var roleClaims = Db.Set<RoleClaim>().Where(r => r.RoleId == role.RoleId);
                foreach (var claim in roleClaims)
                {
                    var userClaim = new UserClaim();
                    userClaim.ClaimType = claim.ClaimType;
                    userClaim.ClaimValue = claim.ClaimValue;
                    userClaim.UserId = user.Id;

                    Db.Set<UserClaim>().Add(userClaim);
                    user.Claims.Add(userClaim);
                }
            }

            Db.SaveChanges();
        }

        public UpdateUserRolesResponse UpdateUserRoles(UpdateUserRolesRequest request)
        {
            var oldRoles = Db.Set<UserRole>().Where(u => u.UserId == request.UserId);
            Db.Set<UserRole>().RemoveRange(oldRoles);
            Db.SaveChanges();

            this.AddToRoles(request.UserId, request.Roles);
            RefreshUserClaims(Db.Set<AdminUser>().Where(u => u.Id == request.UserId).Single());

            return new UpdateUserRolesResponse
            {
                Success = true
            };
        }

        public GetEmployeeProfileResponse GetEmployeeProfile(GetEmployeeProfileRequest request)
        {
            //var employee = Db.Set<Employee>().Where(u => u.User != null && u.User.UserName == request.Name).FirstOrDefault();
            //var result = Mapper.Map<EmployeeProfileItem>(employee);
            //result.HasImage = employee.Image != null;

            return new GetEmployeeProfileResponse
            {
                //Employee = result
            };
        }

        public ChangePasswordResponse ChangePassword(ChangePasswordRequest request)
        {
            var user = Db.Set<AdminUser>().Where(u => u.UserName == request.Name).FirstOrDefault();

            var result = this.ChangePassword(user.Id, request.CurrentPassword, request.NewPassword);

            if (result.Succeeded)
            {
                return new ChangePasswordResponse()
                {
                    Success = true
                };
            }

            return new ChangePasswordResponse()
            {
                Success = false,
                ErrorMessage = ServiceResources.IncorrectPassword//string.Join(", ", result.Errors)
            };
        }

        public override bool SupportsUserPhoneNumber
        {
            get
            {
                return false;
            }
        }

        public override bool SupportsUserLockout
        {
            get
            {
                return false;
            }
        }

        public override bool SupportsUserTwoFactor
        {
            get
            {
                return false;
            }
        }

        public override bool SupportsUserClaim
        {
            get
            {
                return false;
            }
        }

        public override bool SupportsUserRole
        {
            get
            {
                return false;
            }
        }

        public static FixItTronUserManager Create(IdentityFactoryOptions<FixItTronUserManager> options, IOwinContext context)
        {
            var db = context.Get<PikchaDbContext>();
            var manager = new FixItTronUserManager(
                new UserStore<AdminUser, Role, int, UserLogin, UserRole, UserClaim>(db), db);

            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<AdminUser, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 3,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = false;

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<AdminUser, int>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            return manager;
        }

        public GetProfileImageResponse GetProfileImage(GetProfileImageRequest request)
        {
            var query = Db.Set<AdminUser>()
                .Where(u => u.UserName == request.Name)
                .Select(x => new { Image = x.ImageFileName })
                .SingleOrDefault();

            return new GetProfileImageResponse
            {
                Image = query.Image
            };
        }

        public ChangeProfileImageResponse ChangeProfileImage(ChangeProfileImageRequest request)
        {
            AdminUser employee = Db.Set<AdminUser>().Where(u => u.UserName == request.Name).FirstOrDefault();

            //TODO: change to graphicsforprint
            employee.ImageFileName = AmazonImageUploaderService.UploadImage(request.Image.File.Content, "graphicsforprint");

            Db.Entry(employee).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();

            return new ChangeProfileImageResponse()
            {
                Success = true
            };
        }
    }
}
