﻿using AutoMapper;
using FixItTron.Domain.Db;
using FixItTron.Domain.Model;
using FixItTron.Services.Helpers;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Role;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain;

namespace FixItTron.Services
{
    public class RoleService : IRoleService
    {
        PikchaDbContext Db;

        public RoleService(PikchaDbContext db)
        {
            Db = db;
            ServiceMapperConfiguration.Configure(AutoMapper.Mapper.Configuration);
        }

        public Dictionary<Securable, Permissions> GetConfiguration()
        {
            Dictionary<Securable, Permissions> result = new Dictionary<Securable, Permissions>();

            #region Sales

            #region Operations

            result.Add(Securable.SalesQuotations, Permissions.FullCrud | Permissions.PostCrud); //???
            result.Add(Securable.SalesOrders, Permissions.FullCrud | Permissions.PostCrud);//???
            result.Add(Securable.Schedules, Permissions.Read | Permissions.Update);//???
            result.Add(Securable.Invoices, Permissions.FullCrud | Permissions.PostCrud | Permissions.PowerPermission);//???
            result.Add(Securable.InboundPayments, Permissions.FullCrud | Permissions.PostCrud);//???

            #endregion

            #region Information

            result.Add(Securable.CustomerFinancials, Permissions.FullCrud | Permissions.PostCrud);

            #endregion

            #region Configuration

            result.Add(Securable.Customers, Permissions.FullCrud | Permissions.PostCrud);//=========
            result.Add(Securable.ObjectAdditionals, Permissions.FullCrud);
            result.Add(Securable.ContractorCategories, Permissions.FullCrud);
            result.Add(Securable.PriceLists, Permissions.FullCrud | Permissions.PostCrud);////
            result.Add(Securable.PriceRules, Permissions.FullCrud | Permissions.PostCrud);///////
            result.Add(Securable.Jobs, Permissions.FullCrud);
            result.Add(Securable.TaxGroups, Permissions.FullCrud);
            result.Add(Securable.Countries, Permissions.FullCrud);
            result.Add(Securable.Cities, Permissions.FullCrud);
            result.Add(Securable.Streets, Permissions.FullCrud);
            result.Add(Securable.Districts, Permissions.FullCrud);
            result.Add(Securable.ScheduleAdditionalStatuses, Permissions.FullCrud);

            #endregion

            #endregion

            #region Products

            #region Configuration

            result.Add(Securable.Services, Permissions.FullCrud);////
            result.Add(Securable.ServiceCategories, Permissions.FullCrud);
            result.Add(Securable.Materials, Permissions.FullCrud);/////
            result.Add(Securable.MaterialCategories, Permissions.FullCrud);
            result.Add(Securable.Warehouses, Permissions.FullCrud);////
            result.Add(Securable.WarehouseTypes, Permissions.FullCrud);
            result.Add(Securable.ServiceClasses, Permissions.FullCrud);
            result.Add(Securable.MaterialTypes, Permissions.FullCrud);
            result.Add(Securable.UnitOfMeasures, Permissions.FullCrud);

            #endregion

            #endregion

            #region FinancialMovements

            #region Operations

            result.Add(Securable.BankOperations, Permissions.FullCrud | Permissions.PostCrud);///
            result.Add(Securable.BoxOfficeOperations, Permissions.FullCrud | Permissions.PostCrud);/////

            #endregion

            #region Configuration

            result.Add(Securable.BankAccounts, Permissions.FullCrud | Permissions.PostCrud);//////
            result.Add(Securable.BoxOffices, Permissions.FullCrud);
            result.Add(Securable.PaymentClassifiers, Permissions.FullCrud);
            result.Add(Securable.Banks, Permissions.FullCrud);
            result.Add(Securable.Currencies, Permissions.FullCrud);

            #endregion

            #endregion

            #region Administration

            #region Configuration

            result.Add(Securable.SystemConfiguration, Permissions.Read | Permissions.Create | Permissions.Update);
            result.Add(Securable.Roles, Permissions.FullCrud);
            result.Add(Securable.Employees, Permissions.FullCrud);
            result.Add(Securable.Positions, Permissions.FullCrud);
            result.Add(Securable.Reasons, Permissions.FullCrud);
            result.Add(Securable.ScheduleGeneralConfiguration, Permissions.Read | Permissions.Create | Permissions.Update);
            result.Add(Securable.EmployeeSchedule, Permissions.Read | Permissions.Update);
            result.Add(Securable.WorkingSchedules, Permissions.FullCrud);
            result.Add(Securable.EmployeeAbsences, Permissions.FullCrud | Permissions.Approve | Permissions.Cancel);
            result.Add(Securable.AbsenceTypes, Permissions.FullCrud);
            result.Add(Securable.CodeGenerations, Permissions.Read | Permissions.Update);
            result.Add(Securable.Questionnaires, Permissions.FullCrud);
            result.Add(Securable.NotificationTemplates, Permissions.FullCrud);

            #endregion

            #endregion

            return result;
        }

        public GetRoleListResponse GetRoleList(GetRoleListRequest request)
        {
            var baseQuery = Db.Set<Role>()
                .Filter(request.Name, x => x.Name);

            var totalCount = baseQuery.Count();

            var items = baseQuery
                .SortAndPage(request)
                .Select(x => new RoleListItem
                {
                    RoleId = x.Id,
                    Name = x.Name,
                    UserCount = x.Users.Count()
                });

            return new GetRoleListResponse
            {
                Roles = items,
                TotalCount = totalCount
            };
        }

        public CreateRoleResponse CreateRole(CreateRoleRequest request)
        {
            try
            {
                var role = Mapper.Map<Role>(request);

                Db.Set<Role>().Add(role);
                Db.SaveChanges();

                return new CreateRoleResponse()
                {
                    Success = true,
                    RoleId = role.Id
                };
            }
            catch
            {
                return new CreateRoleResponse()
                {
                    Success = false,
                    ErrorMessage = ServiceResources.CouldNotCreateRole
                };
            }
        }

        public GetRoleResponse GetRole(GetRoleRequest request)
        {
            var role = Db.Set<Role>().Find(request.RoleId);
            var response = Mapper.Map<Role, GetRoleResponse>(role);
            return response;
        }

        public UpdateRoleResponse UpdateRole(UpdateRoleRequest request)
        {
            try
            {
                var role = Db.Set<Role>().Find(request.RoleId);
                role = Mapper.Map<UpdateRoleRequest, Role>(request, role);

                //var userRoles = Db.Set<UserRole>().Where(x => x.RoleId == role.Id);
                //var permissions = role.Permissions.Select(x => new { x.RoleId, x.ClaimType, x.ClaimValue });

                //foreach (var permission in permissions)
                //{
                //    foreach (var userRole in userRoles.Where(x => x.RoleId == permission.RoleId))
                //    {
                //        var claim = Db.Set<UserClaim>().Where(x => x.UserId == userRole.UserId && x.ClaimType == permission.ClaimType).SingleOrDefault();
                //        if (claim != null)
                //        {
                //            claim.ClaimValue = permission.ClaimValue;
                //        }
                //        else
                //        {

                //        }
                //    }
                //}

                Update(role);

                var users = Db.Set<AdminUser>().Where(x => x.Roles.Any(r => r.RoleId == role.Id));

                foreach (var user in users)
                {
                    RefreshUserClaims(user);
                }

                Db.SaveChanges();

                return new UpdateRoleResponse()
                {
                    Success = true
                };
            }
            catch (Exception)
            {
                return new UpdateRoleResponse()
                {
                    Success = false,
                    ErrorMessage = ServiceResources.CouldNotUpdateRole
                };
            }
        }

        private void RefreshUserClaims(AdminUser user)
        {
            var roles = user.Roles;
            user.Claims.ToList().ForEach(x => Db.Entry(x).State = System.Data.Entity.EntityState.Deleted);

            foreach (var role in roles)
            {
                var roleClaims = Db.Set<RoleClaim>().Where(r => r.RoleId == role.RoleId);
                foreach (var claim in roleClaims)
                {
                    var userClaim = new UserClaim();
                    userClaim.ClaimType = claim.ClaimType;
                    userClaim.ClaimValue = claim.ClaimValue;
                    userClaim.UserId = user.Id;

                    Db.Set<UserClaim>().Add(userClaim);
                    user.Claims.Add(userClaim);
                }
            }
        }

        private void Update(Role role)
        {
            Db.Entry(role).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();
        }
    }
}
