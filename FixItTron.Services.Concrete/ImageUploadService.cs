﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using FixItTron.Domain.Db;
using FixItTron.Domain.Model;
using FixItTron.Services.Helpers;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.ImageUpload;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services
{
    public class ImageUploadService : IImageUploadService
    {
        PikchaDbContext Db;

        public ImageUploadService(PikchaDbContext db)
        {
            ServiceMapperConfiguration.Configure(AutoMapper.Mapper.Configuration);
            Db = db;
        }

        public UploadImageResponse UploadImage(UploadImageRequest request)
        {
            var photo = new Photo();

            string name = AmazonImageUploaderService.UploadImage(request.Image.Data);

            photo.FileName = name;
            photo.CreateDate = DateTime.Now;
            photo.SourceType = Common.Enums.SourceType.Web;

            Db.Set<Photo>().Add(photo);
            Db.SaveChanges();

            return new UploadImageResponse()
            {
                Success = true,
                PhotoId = photo.PhotoId
            };
        }


        public GetImageByIdResponse GetImageById(GetImageByIdRequest request)
        {
            var response = new GetImageByIdResponse();

            var img = Db.Set<Photo>().FirstOrDefault(x => x.PhotoId == request.Id);

            if (img == null)
            {
                response.Success = false;
                response.ErrorMessage = "Image Not Found";
                return response;
            }

            bool validatePrint = Db.Set<Configuration>().Where(config => config.Name == "PRINT_VALIDATION").Select(config => config.Value).SingleOrDefault() == "1";
            if (validatePrint)
            {
                if (img.IsPrinted)
                {
                    response.Success = false;
                    response.ErrorMessage = "Sorry, this image is already printed";
                    return response;
                }

                img.IsPrinted = true;

                Db.SaveChanges();
            }

            var events = Db.Set<Event>().Find(request.EventId);
            events.PrintCount = events.PrintCount.HasValue ? events.PrintCount.Value + 1 : 1;
            Db.Entry(events).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();

            response.Success = true;
            response.FileName = img.FileName;

            return response;
        }
    }
}
