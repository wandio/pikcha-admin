﻿using FixItTron.Domain.Db;
using FixItTron.Services.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Event;
using FixItTron.Domain.Model;
using Wandio.Domain;
using AutoMapper;

namespace FixItTron.Services
{
    public class EventService : IEventService
    {
        PikchaDbContext Db;

        public EventService(PikchaDbContext db)
        {
            ServiceMapperConfiguration.Configure(AutoMapper.Mapper.Configuration);
            Db = db;
        }

        public GetEventListResponse GetEventList(GetEventListRequest request)
        {
            var baseQuery = Db.Set<Event>()
                .Filter(request.Name, x => x.Name)
                .Filter(request.EventCode, x => x.EventCode)
                .Filter(request.IsActive, x => x.IsActive)
                .Filter(request.IsActive, x => x.ShowLogoOnPreview)
                .Filter(request.IsActive, x => x.ShowLogoOnShare)
                .Filter(request.Username, x => x.Username)
                .Filter(request.StartDate, x => x.StartDate)
                .Filter(request.EndDate, x => x.EndDate)
                .Filter(request.PrintCount, x => x.PrintCount)
                .And(request.CompanyId, x => x.CompanyId == request.CompanyId);

            var totalCount = baseQuery.Count();

            var items = baseQuery
                .SortAndPage(request)
                .Select(x => new EventListItem
                {
                    EventId = x.EventId,
                    Name = x.Name,
                    EventCode = x.EventCode,
                    Company = x.Company != null ? x.Company.Name : string.Empty,
                    IsActive = x.IsActive,
                    Username = x.Username,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    PrintCount = x.PrintCount,
                    ShowLogoOnShare = x.ShowLogoOnShare,
                    ShowLogoOnPreview = x.ShowLogoOnPreview
                });

            return new GetEventListResponse
            {
                Events = items,
                TotalCount = totalCount
            };
        }

        public DeleteEventResponse DeleteEvent(DeleteEventRequest request)
        {
            if (Db.Set<PromoCode>().Any(c => c.EventId == request.EventId))
            {
                return new DeleteEventResponse
                {
                    Success = false,
                    ErrorMessage = ServiceResources.CannotDeleteEventHasRelationships
                };
            }

            Event Event = Db.Set<Event>().Find(request.EventId);

            Db.Set<Event>().Remove(Event);
            Db.SaveChanges();

            return new DeleteEventResponse
            {
                Success = true
            };
        }

        public CreateEventResponse CreateEvent(CreateEventRequest request)
        {
            Event Event = Mapper.Map<Event>(request.Event);
            Event.CreateDate = DateTime.Now;

            var userId = Db.Set<AdminUser>().Where(u => u.UserName == request.Event.UserName).Select(u => u.Id).Single();
            Event.CreateAdminUserId = userId;

            Db.Set<Event>().Add(Event);
            Db.SaveChanges();

            return new CreateEventResponse()
            {
                Success = true,
                EventId = Event.EventId
            };
        }

        public GetEventForUpdateResponse GetEventForUpdate(GetEventForUpdateRequest request)
        {
            Event Event = Db.Set<Event>().Find(request.EventId);

            var result = Mapper.Map<AddEditEventItem>(Event);
            result.HasImage = !string.IsNullOrWhiteSpace(Event.LogoFileName);
            result.LogoFileName = Event.LogoFileName;

            return new GetEventForUpdateResponse
            {
                Event = result
            };
        }

        public UpdateEventResponse UpdateEvent(UpdateEventRequest request)
        {
            var Event = Db.Set<Event>().Find(request.Event.EventId);
            Event = Mapper.Map<AddEditEventItem, Event>(request.Event, Event);
            Event.LastChangeDate = DateTime.Now;

            var userId = Db.Set<AdminUser>().Where(u => u.UserName == request.Event.UserName).Select(u => u.Id).Single();
            Event.ChangeAdminUserId = userId;

            Db.Entry(Event).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();

            return new UpdateEventResponse()
            {
                Success = true
            };
        }

        public GetEventLogoResponse GetEventLogo(GetEventLogoRequest request)
        {
            var query = Db.Set<Event>()
                .Where(u => u.EventId == request.EventId)
                .Select(x => new
                {
                    Logo = x.LogoFileName,
                    BackgroundColor = x.BackgroundColor,
                    BackgroundLogo = x.BackgroundLogoFileName,
                    ShowBackgroundLogo = x.ShowBackgroundLogo
                })
                .SingleOrDefault();

            return new GetEventLogoResponse
            {
                LogoFileName = query.Logo,
                BackgroundColor = query.BackgroundColor,
                BackgroundLogoFileName = query.ShowBackgroundLogo ? query.BackgroundLogo : ""
            };
        }

        public ChangeEventLogoResponse ChangeEventLogo(ChangeEventLogoRequest request)
        {
            Event Event = Db.Set<Event>().Find(request.EventId);

            //TODO: change to graphicsforprint
            string name = AmazonImageUploaderService.UploadImage(request.Logo.File.Content, "graphicsforprint");

            switch (request.LogoType)
            {
                case Common.Enums.LogoType.MainLogo:
                    Event.LogoFileName = name;
                    break;
                case Common.Enums.LogoType.MobileLogo:
                    Event.MobileLogoFileName = name;
                    break;
                case Common.Enums.LogoType.InstagramLogo:
                    Event.InstagramLogoFileName = name;
                    break;
                case Common.Enums.LogoType.FacebookLogo:
                    Event.FacebookLogoFileName = name;
                    break;
                case Common.Enums.LogoType.BackgroundLogo:
                    Event.BackgroundLogoFileName = name;
                    break;
                default:
                    break;
            }

            Db.Entry(Event).State = System.Data.Entity.EntityState.Modified;
            Db.SaveChanges();

            return new ChangeEventLogoResponse()
            {
                Success = true
            };
        }

        public AuthorizePrintResponse AuthorizePrint(AuthorizePrintRequest request)
        {
            var events = Db.Set<Event>().Where(e => e.Username == request.Username && e.Password == request.Password).FirstOrDefault();

            if (events == null)
            {
                return new AuthorizePrintResponse
                {
                    Success = false,
                    ErrorMessage = "Invalid username or password"
                };
            }

            var now = DateTime.Now;
            if (!events.IsActive || events.StartDate > now || events.EndDate < now)
            {
                return new AuthorizePrintResponse
                {
                    Success = false,
                    ErrorMessage = "Event is not active"
                };
            }

            return new AuthorizePrintResponse
            {
                Success = true,
                EventId = events.EventId
            };
        }
    }
}
