﻿using FixItTron.Domain.Db;
using FixItTron.Domain.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services
{
    public class FixItTronRoleManager : RoleManager<Role, int>
    {
        public FixItTronRoleManager(IRoleStore<Role, int> store)
            : base(store)
        {
        }

        public static FixItTronRoleManager Create(IdentityFactoryOptions<FixItTronRoleManager> options, IOwinContext context)
        {
            var manager = new FixItTronRoleManager(new RoleStore<Role, int, UserRole>(context.Get<PikchaDbContext>()));
            return manager;
        }
    }
}
