﻿var App = function () {
    var handlePickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            language: 'ka',
            rtl: Metronic.isRTL(),
            autoclose: true,
            format: 'dd.mm.yyyy'
        });
    };

    var handleGridFilter = function () {
        $(document).on('keyup', 'tr.filter :input', function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                Metronic.blockUI({ animate: true });
                var $this = $(this);
                var $row = $this.parents('tr.filter').find(':submit.filter-submit').click();
            }
        });

        $(document).on('click', 'tr.filter :submit.filter-submit', function (event) {
            event.preventDefault();
            Metronic.blockUI({ animate: true });
            var $this = $(this);
            var $row = $('tr.filter');
            var q = $row.find(':input').serialize();
            window.location.href = $row.data('filter-url') + '?' + q;
        });
        $(document).on('click', 'tr.filter :submit.filter-cancel', function (event) {
            event.preventDefault();
            Metronic.blockUI({ animate: true });
            var $this = $(this);
            var $row = $this.parents('tr.filter');
            window.location.href = $row.data('clear-url');
        });
    };

    var handleLoading = function () {
        $(document).on('click', '.gread-link, .dropup .dropdown-menu li a:not(".js-prevent-loading"), .pagination li a:not(".js-prevent-loading"), .nav.navbar-nav a:not(".js-prevent-loading")', function (event) {
            Metronic.blockUI({ animate: true });
        });
    };

    var handleTableRowExpander = function () {
        $('tr.parent').click(function () {
            var $this = $(this);
            var btn = $this.find('.row-details');
            if (btn.hasClass('row-details-close')) {
                btn.removeClass('row-details-close');
                btn.addClass('row-details-open');
            } else {
                btn.addClass('row-details-close');
                btn.removeClass('row-details-open');
            }

            $this.nextUntil('tr.parent', '.sub-item').fadeToggle('fast');
        });
    };

    var handleModalAction = function () {
        $(document).on('click', '.js-modal-action', function (e) {
            e.preventDefault();
            var $this = $(this);

            Metronic.blockUI({ animate: true });

            $.get($this.attr('href'), function (data) {
                var modal = $(data).filter('.modal').modal();
                modal.show();

                Metronic.unblockUI();

                modal.on('hidden.bs.modal', function () {
                    $(this).data('bs.modal', null);
                    $(this).remove();
                });
            });
        });
    };

    var handleSelect2 = function () {
        if ($().select2) {
            $('.select2me').select2('destroy');
            $('.select2me').select2({
                locale: 'ka',
                placeholder: "",
                allowClear: true
            });
        }
    };

    return {
        init: function () {
            handlePickers();
            handleGridFilter(); //handles submission of grid filter
            handleLoading();
        },

        initTableRowExpander: function () {
            handleTableRowExpander();
        },

        initModalActions: function () {
            handleModalAction();
        },

        initSelect2: function () {
            handleSelect2();
        }
    };
}();