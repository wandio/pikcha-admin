﻿var Dashboard = function () {
    var handleWidgetFiler = function () {
        $(document).on('change', '.js-widget-handler', function (event) {
            event.preventDefault();
            var $this = $(this);
            var $form = $this.parents('.form-inline');

            Metronic.blockUI({
                target: '.js-widget-container',
                animate: true
            });

            var url = $form.data('widget-url') + "?" + $form.serialize();

            if (url) {
                $.get(url, function (data) {
                    var container = $('.js-widget-container');
                    container.empty().html(data);
                }).done(function () {
                    Metronic.unblockUI('.js-widget-container');
                    App.initTableRowExpander();
                });
            }
            else {
                Metronic.unblockUI('.js-widget-container');
                bootbox.alert('Oops! Something went wrong');
            }
        });

        $(document).on('change', '.js-chart-handler', function (event) {
            event.preventDefault();
            var $this = $(this);
            var $form = $this.parents('.form-inline');

            Metronic.blockUI({
                target: '.js-chart-container',
                animate: true
            });

            var url = $form.data('chart-url') + "?" + $form.serialize();

            if (url) {
                $.get(url, function (data) {
                    var container = $('.js-chart-container');
                    container.empty().html(data);
                    handleChart();
                }).done(function () {
                    Metronic.unblockUI('.js-chart-container');
                });
            }
            else {
                Metronic.unblockUI('.js-chart-container');
                bootbox.alert('Oops! Something went wrong');
            }
        });
    };

    var handleChart = function () {
        var chartData = $('#chart-ordersbyfilials').data('set');
        var chart = AmCharts.makeChart("chart-ordersbyfilials", {
            "mouseWheelZoomEnabled": false,
            "zoomOutText": '',
            "chartScrollbar": {
                "autoGridCount": true,
                "graph": "AmGraph-1",
                "scrollbarHeight": 60
            },
            "pathToImages": "/Assets/plugins/_amcharts/images/",
            "chartCursor": {
                "cursorPosition": "mouse"
            },
            "type": "serial",
            "categoryField": "Branch",
            "rotate": true,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left",
                "labelRotation": 0
            },
            "data_labels_always_on": true,
            "graphs": [
                {
                    "balloonText": "რაოდენობა სააბონენტო:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "სააბონენტო",
                    "type": "column",
                    "valueField": "AbonentOrders",
                    colorField: "Color1",
                    "labelText": "სააბონენტო:[[value]]",
                },
                {
                    "balloonText": "რაოდენობა ფასიანი:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0.2,
                    "title": "ფასიანი",
                    "type": "column",
                    "valueField": "PaidOrders",
                    colorField: "Color2",
                    "labelText": "ფასიანი:[[value]]",
                }
            ],
            "guides": [],
            "valueAxes": [
                {
                    "id": "ValueAxis-1",
                    "position": "top",
                    "axisAlpha": 0
                }
            ],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": chartData,
            "export": AmCharts.exportCFG
        });

        setTimeout(function () { chart.addListener("dataUpdated", handleZoomChart(chart, chartData)) }, 500);
        handleZoomChart(chart, chartData);
        AmCharts.checkEmptyData(chart);
    };

    AmCharts.checkEmptyData = function (chart) {
        if (0 == chart.dataProvider.length) {
            // set min/max on the value axis
            chart.valueAxes[0].minimum = 0;
            chart.valueAxes[0].maximum = 100;

            // add dummy data point
            var dataPoint = {
                dummyValue: 0
            };
            dataPoint[chart.categoryField] = '';
            chart.dataProvider = [dataPoint];

            // add label
            chart.addLabel(0, '30%', 'ჩანაწერი ვერ მოიძებნა', 'center', 30);

            // set opacity of the chart div
            chart.chartDiv.style.opacity = 0.5;

            // redraw it
            chart.validateNow();
        }
    };

    function setZoomOptions(options) {
        options.mouseWheelZoomEnabled = false;
        options.zoomOutText = '';
        options.chartScrollbar = {
            "autoGridCount": true,
            "graph": "AmGraph-1",
            "scrollbarHeight": 40
        };
        options.pathToImages = "/Assets/plugins/_amcharts/images/";
        options.chartCursor = {
            "cursorPosition": "mouse"
        }
    }

    function handleZoomChart(chart, chartData) {
        //different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
        if (chartData.length > 20) {
            chart.zoomToIndexes(0, 20);
        }
        else {
            chart.zoomToIndexes(0, chartData.length);
        }
    }

    return {
        init: function () {
            handleWidgetFiler();
            handleChart();
            App.initTableRowExpander();
        }
    };
}();