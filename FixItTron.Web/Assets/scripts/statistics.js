﻿var Statistics = function () {

    var MaterialsByFilials = 'MaterialsByFilials';
    var MaterialsByCategories = 'MaterialsByCategories';
    var MaterialsByDescriptions = 'MaterialsByDescriptions';
    var MaterialsCountByDescriptions = 'MaterialsCountByDescriptions';

    var ServiceCategoryCountByFilials = 'ServiceCategoryCountByFilials';
    var ServiceDescriptionCountByFilials = 'ServiceDescriptionCountByFilials';
    var ServiceDescriptionCount = 'ServiceDescriptionCount';
    var ServiceCountByCategory = 'ServiceCountByCategory';

    var FinancesMaterials = 'FinancesMaterials';
    var FinancesSumAccrual = 'FinancesSumAccrual';

    AmCharts.checkEmptyData = function (chart) {
        if (0 == chart.dataProvider.length) {
            // set min/max on the value axis
            if (chart.valueAxes) {
                chart.valueAxes[0].minimum = 0;
                chart.valueAxes[0].maximum = 100;
            }

            // add dummy data point
            var dataPoint = {
                dummyValue: 0
            };
            dataPoint[chart.categoryField] = '';
            chart.dataProvider = [dataPoint];

            // add label
            chart.addLabel(0, '30%', 'ჩანაწერი ვერ მოიძებნა', 'center', 30);

            // set opacity of the chart div
            chart.chartDiv.style.opacity = 0.5;

            // redraw it
            chart.validateNow();
        }
    };

    var handleStatisticsLoad = function (url, targetContainer, target) {
        if (url) {
            $.get(url, function (data) {
                var container = $(targetContainer);
                container.replaceWith(data);
                if (target == MaterialsByFilials) {
                    handleMaterialsByFilials();
                }
                else if (target == MaterialsByCategories) {
                    handleMaterialsByCategories();
                }
                else if (target == MaterialsByDescriptions) {
                    handleMaterialsByDescriptions();
                }
                else if (target == MaterialsCountByDescriptions) {
                    handleMaterialsCountByDescriptions();
                }
                else if (target == ServiceCategoryCountByFilials) {
                    handleServiceCategoryCountByFilials();
                }
                else if (target == ServiceDescriptionCountByFilials) {
                    handleServiceDescriptionCountByFilials();
                }
                else if (target == ServiceDescriptionCount) {
                    handleServiceDescriptionCount();
                }
                else if (target == ServiceCountByCategory) {
                    handleServiceCountByCategory();
                }
                else if (target == FinancesMaterials) {
                    handleFinancesMaterials();
                }
                else if (target == FinancesSumAccrual) {
                    handleFinancesSumAccrual();
                }
            }).done(function () {
                Metronic.unblockUI(targetContainer);
                App.initSelect2();
            });
        }
        else {
            Metronic.unblockUI(targetContainer);
            bootbox.alert('Oops! Something went wrong');
        }
    };

    var handleStatisticsFilters = function () {

        $(document).on('change', '.js-form-filter-handler', function (event) {
            event.preventDefault();
            var $this = $(this);
            var target = $this.data('target');
            var targetContainer = '.target-' + $this.data('target');

            var formSerializedData = $this.parents('.form-inline').serialize();

            //total selected
            if ($this.hasClass('js-branch-handler')) {
                targetContainer = '.target-' + $this.data('target-current');

                Metronic.blockUI({
                    target: targetContainer,
                    animate: true
                });

                if ($this.val() == '') {
                    var urlTotal = $this.data('chart-total-url') + "&" + formSerializedData;
                    handleStatisticsLoad(urlTotal, targetContainer, target);
                }
                else {
                    var url = $this.data('chart-url') + "&" + formSerializedData;
                    handleStatisticsLoad(url, targetContainer, target);
                }
            }
            else {

                Metronic.blockUI({
                    target: targetContainer,
                    animate: true
                });

                var url = $this.data('chart-url') + "&" + formSerializedData;
                handleStatisticsLoad(url, targetContainer, target);
            }
        });

        $(document).on('click', '.js-chart-filter', function (e) {
            e.preventDefault();
            var $this = $(this);
            var target = $this.data('target');
            var targetContainer = '.target-' + $this.data('target-current');

            Metronic.blockUI({
                target: targetContainer,
                animate: true
            });

            var formSerializedData = $this.parents('.form-inline').serialize();
            var url = $this.attr('href') + "&" + formSerializedData;
            handleStatisticsLoad(url, targetContainer, target);
        });
    };

    var barChartOptions = function (rotate) {
        if (!rotate) {
            rotate = false;
        }
        return {
            "type": "serial",
            "categoryField": "",
            "rotate": rotate,
            "startDuration": 1,
            "categoryAxis": {
                "gridPosition": "start",
                "position": "left",
                "labelRotation": 80
            },
            "data_labels_always_on": true,
            "graphs": [],
            "guides": [],
            //"valueAxes": [
            //    {
            //        "id": "ValueAxis-1",
            //        "position": "top",
            //        "axisAlpha": 0
            //    }
            //],
            "allLabels": [],
            "balloon": {},
            "titles": [],
            "dataProvider": [],
            "export": AmCharts.exportCFG
        }
    };

    var pieChartOptions = function () {
        return {
            "type": "pie",
            "theme": "light",
            //labelRadius: -30,
            labelText: "[[title]]: [[value]]",
            "legend": {
                "markerType": "circle",
                "position": "right",
                "marginRight": 150,
                "autoMargins": false,
                labelText: "[[title]]",
                valueText: ""
            },
            "valueAxis": [
            { inside: true }
            ],
            "dataProvider": [],
            "valueField": "Value",
            "titleField": "Title",
            "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
            "export": AmCharts.exportCFG
        };
    };

    var stackedColumnChart = function () {
        return {
            "type": "serial",
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "right",
                "useGraphSettings": true,
                "markerSize": 10
            },
            "startDuration": 1,
            "categoryField": "",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "position": "left"
            },
            "valueAxes": [{
                "stackType": "regular",
                //"axisAlpha": 0.3,
                //"gridAlpha": 0
            }],
            "graphs": [],
            "dataProvider": [],
            "export": AmCharts.exportCFG
        }
    };

    var handleMaterialsByFilials = function () {
        var chartData = $('#chart-MaterialsByFilials').data('set');
        if (chartData) {
            var options = barChartOptions(true);
            setZoomOptions(options);

            options.categoryField = "Title";
            options.graphs = [{
                "balloonText": "ღირებულება:[[value]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "title": "ღირებულება",
                "type": "column",
                "valueField": "Value",
                colorField: "Color1",
                "labelText": "[[value]]",
                columnWidth: 0.1
            }];
            options.dataProvider = chartData;
            var chart = AmCharts.makeChart('chart-MaterialsByFilials', options);

            chart.addListener("dataUpdated", handleZoomChart(chart, chartData));
            handleZoomChart(chart, chartData);
            AmCharts.checkEmptyData(chart);
        }
    };

    var handleMaterialsByCategories = function () {
        var chartData = $('#chart-MaterialsByCategories').data('set');
        if (chartData) {
            var options = pieChartOptions();
            options.dataProvider = chartData;
            var chart = AmCharts.makeChart('chart-MaterialsByCategories', options);
            AmCharts.checkEmptyData(chart);
        }
    };

    var handleMaterialsByDescriptions = function () {
        var chartData1 = $('#chart-MaterialsByDescriptions').data('set');
        if (chartData1) {
            var chartData = [];
            chartData = chartData1;
            var options = barChartOptions(true);
            setZoomOptions(options);

            options.categoryField = "Title";
            options.graphs = [{
                "balloonText": "ღირებულება:[[value]] ლარი",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "Value",
                "colorField": "Color1",
                "labelText": "[[value]] ლარი"
            }];
            options.dataProvider = chartData;

            var chart = AmCharts.makeChart('chart-MaterialsByDescriptions', options);

            setTimeout(function () { chart.addListener("dataUpdated", handleZoomChart(chart, chartData)) }, 1000);
            handleZoomChart(chart, chartData);
            AmCharts.checkEmptyData(chart);
        }
    };

    var handleMaterialsCountByDescriptions = function () {
        var chartData1 = $('#chart-MaterialsCountByDescriptions').data('set');
        if (chartData1) {
            var chartData = [];
            chartData = chartData1;
            var options = barChartOptions(true);
            setZoomOptions(options);

            options.categoryField = "Title";
            options.graphs = [{
                "balloonText": "რაოდენობა:[[value]] ცალი",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "Value",
                colorField: "Color1",
                "labelText": "[[value]] ცალი"
            }];
            options.dataProvider = chartData;

            var chart = AmCharts.makeChart('chart-MaterialsCountByDescriptions', options);

            chart.addListener("dataUpdated", handleZoomChart(chart, chartData));
            handleZoomChart(chart, chartData);
            AmCharts.checkEmptyData(chart);
        }
    };

    var handleServiceCategoryCountByFilials = function () {
        var chartData = $('#chart-ServiceCategoryCountByFilials').data('set');
        if (chartData) {
            var options = barChartOptions(true);
            options.categoryField = "Title";
            /*
            var mappedData = [];
            for (var i = 0; i < chartData.length; i++) {
                var item = chartData[i];
                var jsonObj = {};
                jsonObj.Title = item.Title;
                for (var j = 0; j < item.SubItems.length; j++) {
                    var subItem = item.SubItems[j];
                    var propName = subItem.Title;
                    jsonObj[propName] = subItem.Value;

                    options.graphs.push({
                        "balloonText": propName + ": [[value]]",
                        "fillAlphas": 0.8,
                        "id": "AmGraph-" + j,
                        "lineAlpha": 0.2,
                        "title": propName,
                        "type": "column",
                        "valueField": propName,
                        "labelText": "[[value]]"
                    });
                }

                mappedData.push(jsonObj);
            }

            options.dataProvider = mappedData;
            */
            options.dataProvider = chartData;
            options.graphs = [{
                "balloonText": "[[category]]:[[value]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "title": "[[category]]",
                "type": "column",
                "valueField": "Value",
                colorField: "Color1",
                "labelText": "[[value]]"
            }];

            var chart = AmCharts.makeChart('chart-ServiceCategoryCountByFilials', options);
            AmCharts.checkEmptyData(chart);
        }
    };

    var handleServiceCountByCategory = function () {
        var chartData = $('#chart-ServiceCountByCategory').data('set');
        if (chartData) {
            var options = barChartOptions(true);
            //setZoomOptions(options);

            options.categoryField = "Title";
            options.graphs = [{
                "balloonText": "რაოდენობა:[[value]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "title": "რაოდენობა",
                "type": "column",
                "valueField": "Value",
                colorField: "Color1",
                "labelText": "[[value]]"
            }];
            options.dataProvider = chartData;
            var chart = AmCharts.makeChart('chart-ServiceCountByCategory', options);

            //chart.addListener("dataUpdated", handleZoomChart(chart, chartData));
            //handleZoomChart(chart, chartData);
            AmCharts.checkEmptyData(chart);
        }
    };

    var handleServiceDescriptionCountByFilials = function () {
        var chartData = $('#chart-ServiceDescriptionCountByFilials').data('set');
        if (chartData) {
            var options = barChartOptions(true);
            options.categoryField = "Title";
            setZoomOptions(options);
            /*
            var mappedData = [];
            for (var i = 0; i < chartData.length; i++) {
                var item = chartData[i];
                var jsonObj = {};
                jsonObj.Title = item.Title;
                for (var j = 0; j < item.SubItems.length; j++) {
                    var subItem = item.SubItems[j];
                    var propName = subItem.Title;
                    jsonObj[propName] = subItem.Value;

                    options.graphs.push({
                        "balloonText": propName + ": [[value]]",
                        "fillAlphas": 0.8,
                        "id": "AmGraph-" + j,
                        "lineAlpha": 0.2,
                        "title": propName,
                        "type": "column",
                        "valueField": propName,
                        "labelText": "[[value]]"
                    });
                }

                mappedData.push(jsonObj);
            }

            options.dataProvider = mappedData;
            */
            options.dataProvider = chartData;
            options.graphs = [{
                "balloonText": "რაოდენობა:[[value]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "Value",
                colorField: "Color1",
                "labelText": "[[value]]"
            }];

            var chart = AmCharts.makeChart('chart-ServiceDescriptionCountByFilials', options);

            chart.addListener("dataUpdated", handleZoomChart(chart, chartData));
            handleZoomChart(chart, chartData);
            AmCharts.checkEmptyData(chart);
        }
    };

    var handleServiceDescriptionCount = function () {
        var chartData1 = $('#chart-ServiceDescriptionCount').data('set');
        if (chartData1) {
            var chartData = [];
            chartData = chartData1;
            var options = barChartOptions(true);
            options.categoryField = "Title";
            setZoomOptions(options);

            options.dataProvider = chartData;
            options.graphs = [{
                "balloonText": "რაოდენობა:[[value]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "type": "column",
                "valueField": "Value",
                colorField: "Color1",
                "labelText": "[[value]]"
            }];

            var chart = AmCharts.makeChart('chart-ServiceDescriptionCount', options);

            // listen for "dataUpdated" event (fired when chart is rendered) and call zoomChart method when it happens
            setTimeout(function () { chart.addListener("dataUpdated", handleZoomChart(chart, chartData)) }, 1000);
            handleZoomChart(chart, chartData);
            AmCharts.checkEmptyData(chart);
        }
    };

    function setZoomOptions(options) {
        options.mouseWheelZoomEnabled = false;
        options.zoomOutText = '';
        options.chartScrollbar = {
            "autoGridCount": true,
            "graph": "AmGraph-1",
            "scrollbarHeight": 60
        };
        options.pathToImages = "/Assets/plugins/_amcharts/images/";
        options.chartCursor = {
            "cursorPosition": "mouse"
        }
    }

    function handleZoomChart(chart, chartData) {
        // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
        if (chartData.length > 30) {
            chart.zoomToIndexes(0, 30);
        }
        else {
            chart.zoomToIndexes(0, chartData.length);
        }
    }

    var handleFinancesMaterials = function () {
        var chartData = $('#chart-FinancesMaterials').data('set');
        if (chartData) {
            var options = barChartOptions();
            options.categoryAxis.labelRotation = 0;
            options.categoryField = "Title";
            options.graphs = [{
                "balloonText": "[[value]]",
                "fillAlphas": 0.8,
                "id": "AmGraph-1",
                "lineAlpha": 0.2,
                "title": "თანხა",
                "type": "column",
                "valueField": "Value",
                colorField: "Color1",
                "labelText": "[[value]]"
            }];
            options.dataProvider = chartData;
            var chart = AmCharts.makeChart('chart-FinancesMaterials', options);
            AmCharts.checkEmptyData(chart);
        }
    };

    var handleFinancesSumAccrual = function () {
        var chartData = $('#chart-FinancesSumAccrual').data('set');
        if (chartData) {
            var options = stackedColumnChart();
            options.categoryField = "Title";

            var graghKeyValues = [{ title: 'სააბონენტო', value: 'AbonentAmount' }, { title: 'მასალები', value: 'MaterialAmount' }, { title: 'სხვა', value: 'OtherAmount' }, { title: 'ლიმიტგადაჭარბება', value: 'ServiceAmount' }];

            for (var i = 0; i < graghKeyValues.length; i++) {
                var item = graghKeyValues[i];
                options.graphs.push({
                    "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                    "fillAlphas": 0.8,
                    "labelText": "[[value]]",
                    "lineAlpha": 0.3,
                    "title": item.title,
                    "type": "column",
                    "color": "#000000",
                    "valueField": item.value
                });
            }
            options.dataProvider = chartData;
            var chart = AmCharts.makeChart('chart-FinancesSumAccrual', options);
            AmCharts.checkEmptyData(chart);
        }
    };

    var handleFinancesFilter = function () {
        $(document).on('click', '.js-chart-filter', function (e) {
            e.preventDefault();
            var $this = $(this);
            var target = $this.data('target');
            var targetContainer = '.target-' + $this.data('target-current');

            Metronic.blockUI({
                target: targetContainer,
                animate: true
            });

            var year = $('.js-filter-handler').val();
            var url = $this.attr('href') + "&year=" + (year ? year : '');

            handleStatisticsLoad(url, targetContainer, target);
        });
    };

    return {
        init: function () {
            handleStatisticsFilters();
            handleMaterialsByFilials();
            handleMaterialsByCategories();
            handleMaterialsByDescriptions();
            handleServiceCountByCategory();
            handleServiceDescriptionCount();
        },

        initHandleStatisticsLoad: function (url, targetContainer, target) {
            handleStatisticsLoad(url, targetContainer, target);
        },

        initFinances: function () {
            handleFinancesFilter();
            handleFinancesSumAccrual();
        }
    };
}();