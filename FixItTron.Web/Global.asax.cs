﻿using FixItTron.Web.App_Start;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FixItTron.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            FluentValidation.Mvc.FluentValidationModelValidatorProvider.Configure();
            ClientDataTypeModelValidatorProvider.ResourceClassKey = "ValidationMessages";
            DefaultModelBinder.ResourceClassKey = "ValidationMessages";
            ValidatorOptions.ResourceProviderType = typeof(ValidationMessages);
            MapperConfig.Configure(AutoMapper.Mapper.Configuration);
            ModelMetadataProviders.Current = new FixItTronModelMetadataProvider();
            ModelBindingConfig.Configure();
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ka-GE");
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("ka-GE");

            Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator = ",";
            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";

            Thread.CurrentThread.CurrentUICulture.NumberFormat.NumberDecimalSeparator = ".";
            Thread.CurrentThread.CurrentUICulture.NumberFormat.NumberGroupSeparator = ",";
            Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        }
    }
}