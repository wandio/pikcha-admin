﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString DateTo<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> property)
        {
            string[] suffixes = new string[] { "არი", "ალი", "ერი" };

            var modelMetadata = ModelMetadata.FromLambdaExpression<TModel, TProperty>(property, html.ViewData);
            DateTime? model = (DateTime?)modelMetadata.Model;
            if (model.HasValue)
            {
                var date = model.Value;
                string month = date.ToString("MMMM");
                string result = date.ToString("yyyy dd MMMM");

                if (suffixes.Any(x => month.EndsWith(x)))
                {
                    var suffix = suffixes.SingleOrDefault(x => month.EndsWith(x));
                    month = month.Substring(0, month.Length - 3) + suffix[1] + "ამდე";
                    result = string.Format("{0} {1}", date.ToString("yyyy dd"), month);
                }
                else if (month.EndsWith("ი"))
                {
                    month = month.Substring(0, month.Length - 1) + "ამდე";
                    result = string.Format("{0} {1}", date.ToString("yyyy dd"), month);
                }
                else if (month.EndsWith("ო"))
                {
                    month = month + "მდე";
                    result = string.Format("{0} {1}", date.ToString("yyyy dd"), month);
                }

                return MvcHtmlString.Create(result);
            }
            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString DateFrom<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> property)
        {
            string[] suffixes = new string[] { "არი", "ალი", "ერი" };

            var modelMetadata = ModelMetadata.FromLambdaExpression<TModel, TProperty>(property, html.ViewData);
            DateTime? model = (DateTime?)modelMetadata.Model;
            if (model.HasValue)
            {
                var date = model.Value;
                string month = date.ToString("MMMM");
                string result = date.ToString("yyyy dd MMMM");

                if (suffixes.Any(x => month.EndsWith(x)))
                {
                    var suffix = suffixes.SingleOrDefault(x => month.EndsWith(x));
                    suffix = suffix.Substring(1, suffix.Length - 1);
                    month = month.Substring(0, month.Length - 3) + suffix + "დან";
                    result = string.Format("{0} {1}", date.ToString("yyyy dd"), month);
                }
                else
                {
                    month = month + "დან";
                    result = string.Format("{0} {1}", date.ToString("yyyy dd"), month);
                }
                return MvcHtmlString.Create(result);
            }
            return MvcHtmlString.Empty;
        }

        public static MvcHtmlString Validation(this HtmlHelper helper)
        {
            if (!helper.ViewData.ModelState.IsValid)
            {
                TagBuilder container = new TagBuilder("div");
                container.AddCssClass("alert alert-danger display-hide1");

                TagBuilder button = new TagBuilder("button");
                button.AddCssClass("close");
                button.MergeAttribute("data-close", "alert");

                TagBuilder summary = new TagBuilder("div");
                summary.AddCssClass("validation-summary-errors");

                var errorList = "<ul>";

                foreach (var key in helper.ViewData.ModelState.Keys)
                {
                    foreach (var err in helper.ViewData.ModelState[key].Errors)
                    {
                        errorList += "<li>" + helper.Encode(err.ErrorMessage) + "<li/>";
                    }
                }

                errorList += "</ul>";

                summary.InnerHtml = errorList;
                container.InnerHtml = button.ToString() + summary.ToString();
                return MvcHtmlString.Create(container.ToString());
            }
            return MvcHtmlString.Empty;
        }

        private static MvcHtmlString CreateFlash(TempDataDictionary tempData, string key, string className)
        {
            TagBuilder tagBuilder = new TagBuilder("div");
            tagBuilder.AddCssClass(className);

            TagBuilder tagBuilder2 = new TagBuilder("button");

            tagBuilder2.AddCssClass("close");
            tagBuilder2.MergeAttribute("type", "button");
            tagBuilder2.MergeAttribute("data-dismiss", "alert");
            tagBuilder2.MergeAttribute("aria-hidden", "true");

            tagBuilder.InnerHtml = tagBuilder2.ToString(TagRenderMode.Normal);
            tagBuilder.InnerHtml += tempData[key].ToString();

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString RenderFlash(this TempDataDictionary tempData)
        {
            MvcHtmlString result = MvcHtmlString.Empty;
            if (tempData != null)
            {
                if (tempData.ContainsKey("Message"))
                {
                    result = CreateFlash(tempData, "Message", "Metronic-alerts alert alert-success fade in");
                }
                if (tempData.ContainsKey("Error"))
                {
                    result = CreateFlash(tempData, "Error", "Metronic-alerts alert alert-danger fade in");
                }
            }
            return result;
        }

        public static void AddError(this TempDataDictionary tempData, string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                throw new System.ArgumentNullException("message");
            }
            if (tempData != null)
            {
                tempData["Error"] = message;
            }
        }

        public static void AddFlash(this TempDataDictionary tempData, string message)
        {
            if (string.IsNullOrEmpty(message))
            {
                throw new System.ArgumentNullException("message");
            }
            if (tempData != null)
            {
                tempData["Message"] = message;
            }
        }
    }
}