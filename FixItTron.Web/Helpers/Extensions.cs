﻿using FixItTron.Domain.Helpers;
using FixItTron.Domain.Services.Abstract;
using FixItTron.Web.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FixItTron.Web.Helpers
{
    public static class Extensions
    {
        public static IEnumerable<SelectListItem> PrependNull(this System.Collections.Generic.IEnumerable<SelectListItem> source, string nullText = "")
        {
            return new SelectListItem[]
			{
				new SelectListItem
				{
					Text = nullText,
					Value = string.Empty
				}
			}.Concat(source);
        }

        public static IEnumerable<SelectListItem> YearsSelectList(bool currentDefault = false)
        {
            var curYear = DateTime.Today.Year;

            var yearRange = Enumerable.Range(2010, DateTime.Now.Year - (DateTime.Now.Year - 4) + 2).OrderByDescending(x => x);
            return yearRange.Select(x => new SelectListItem()
            {
                Text = x.ToString(),
                Value = x.ToString(),
                Selected = (currentDefault && x == curYear)
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> OrderTypesSelectList(this IClientService service)
        {
            return service.GetOrderTypes().Select(x => new SelectListItem()
            {
                Text = x.Value,
                Value = x.Key.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> ServicesAndMaterialsTypesSelectList(this IClientService service)
        {
            return new List<SelectListItem>() 
            { 
                new SelectListItem() { Text = ApplicationStrings.Service, Value = "1" },
                new SelectListItem() { Text = ApplicationStrings.Material, Value = "2" }
            }.PrependNull();
        }

        public static IEnumerable<SelectListItem> CategoriesSelectList(this IClientService service, int clientId)
        {
            return service.GetCategories(clientId).Select(x => new SelectListItem()
            {
                Text = x.Value,
                Value = x.Key.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> StatusesSelectList(this IClientService service, int clientId)
        {
            return service.GetOrderStatuses(clientId).Select(x => new SelectListItem()
            {
                Text = x.Value,
                Value = x.Key.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> AddressesSelectList(this IClientService service, int clientId)
        {
            return service.GetBranches(clientId).Select(x => new SelectListItem()
            {
                Text = x.Address,
                Value = x.BranchId.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> ServiceAndMaterialsDescriptionsSelectList(this IClientService service, int clientId)
        {
            return service.GetServiceAndMaterialsDescriptions(clientId).Select(x => new SelectListItem()
            {
                Text = x.Value,
                Value = x.Key.ToString()
            }).PrependNull();
        }

        public static string ToShortDate(this DateTime? date)
        {
            if (date.HasValue)
            {
                return date.Value.ToShortDateString();
            }
            return string.Empty;
        }

        public static int GetClientId(this HttpSessionStateBase session)
        {
            if (session[Constants.SessionClientId] == null)
            {
                FormsAuthentication.SignOut();
            }
            return Convert.ToInt32(session[Constants.SessionClientId]);
        }

        public static void SetClientIdToSession(this HttpSessionStateBase session, int clientId)
        {
            session[Constants.SessionClientId] = clientId;
        }

        public static void ClearClientIdCache(this HttpSessionStateBase session)
        {
            session[Constants.SessionClientId] = null;
        }

        public static void RefreshClientIdCache(this HttpSessionStateBase session, int clientId)
        {
            session.ClearClientIdCache();
            session.SetClientIdToSession(clientId);
        }

        public static string GetClientName(this IPrincipal principal)
        {
            if (principal.Identity.IsAuthenticated)
            {
                string clientName = ((FormsIdentity)principal.Identity).Ticket.UserData;
                return clientName;
            }
            return string.Empty;
        }

        public static IEnumerable<SelectListItem> MonthsSelectList(this IClientService service)
        {
            return service.GetMonths().Select(x => new SelectListItem()
            {
                Text = x.Value,
                Value = x.Key.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> BranchsSelectList(this IClientService service, int clientId)
        {
            return service.GetBranches(clientId).Select(x => new SelectListItem()
            {
                Text = x.BranchName,
                Value = x.BranchId.ToString()
            });
        }
    }
}