﻿using FixItTron.Domain;
using FixItTron.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.App_Start
{
    public static class MapperConfig
    {
        public static void Configure(AutoMapper.IConfiguration configuration)
        {
            configuration.CreateMap<BranchItem, EditBranchViewModel>()
                .ForMember(dest => dest.OriginalBranchName, options => options.MapFrom(src => src.BranchName));
        }
    }
}