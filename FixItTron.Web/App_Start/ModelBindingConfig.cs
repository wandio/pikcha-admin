﻿using FixItTron.Web.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.App_Start
{
    public static class ModelBindingConfig
    {
        public static void Configure()
        {
            ModelBinders.Binders.Add(typeof(CurrentPageInformation), new CurrentPageInformationModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime?), new DateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime), new DateTimeModelBinder());
        }

        public class CurrentPageInformationModelBinder : DefaultModelBinder
        {
            protected override object CreateModel(ControllerContext controllerContext, ModelBindingContext bindingContext, Type modelType)
            {
                return new CurrentPageInformation(25, 1);
            }

            public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                var original = this.CreateModel(controllerContext, bindingContext, typeof(CurrentPageInformation));
                var m = base.BindModel(controllerContext, bindingContext);
                return m;
            }
        }

        public class DateTimeModelBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                Debug.Assert(bindingContext.ModelType == typeof(DateTime) || bindingContext.ModelType == typeof(DateTime?), "this ModelBinder should be used for DateTimes!");

                ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProviderResult);

                DateTime result;
                if (valueProviderResult != null && !string.IsNullOrWhiteSpace(valueProviderResult.AttemptedValue))
                {
                    try
                    {
                        if (DateTime.TryParse(valueProviderResult.AttemptedValue, out result) || DateTime.TryParse(valueProviderResult.AttemptedValue, CultureInfo.CurrentCulture, DateTimeStyles.None, out result))
                        {
                            return result;
                        }
                        else
                        {
                            throw new FormatException();
                        }
                    }
                    catch (FormatException ex)
                    {
                        bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex);
                    }
                }

                return null;
            }
        }
    }
}