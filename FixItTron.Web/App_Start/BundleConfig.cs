﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace FixItTron.Web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Assets/plugins/jquery/jquery-{version}.js",
            "~/Assets/plugins/jquery-ui/jquery-ui.min.js",
            "~/Assets/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.js",
            "~/Assets/plugins/jquery-slimscroll/jquery.slimscroll.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Assets/plugins/jquery-validation/jquery.validate.js",
                "~/Assets/plugins/jquery-validation/jquery.validate.unobtrusive.js",
                "~/Assets/plugins/jquery-validation/jquery.validate.unobtrusive.dynamic.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Assets/plugins/bootstrap/js/bootstrap.js",
                "~/Assets/plugins/uniform/jquery.uniform.js",
                "~/Assets/plugins/bootbox/bootbox.min.js",
                "~/Assets/plugins/jquery.blockui.min.js",
                "~/Assets/plugins/jquery-cookie/jquery.cokie.js",
                "~/Assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js",
                "~/Assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.en.js",
                "~/Assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.ka.js",
                "~/Assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js",
                "~/Assets/plugins/select2/select2.js",
                "~/Assets/plugins/select2/select2_locale_ka.js",
                "~/Assets/plugins/datatables/media/js/jquery.dataTables.js",
                "~/Assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js",
                "~/Assets/scripts/metronic.js",
                "~/Assets/scripts/layout.js",
                "~/Assets/scripts/app.js"));

            bundles.Add(new ScriptBundle("~/bundles/_charts").Include(
                "~/Assets/plugins/_amcharts/amcharts.js",
                "~/Assets/plugins/_amcharts/funnel.js",
                "~/Assets/plugins/_amcharts/gantt.js",
                "~/Assets/plugins/_amcharts/gauge.js",
                "~/Assets/plugins/_amcharts/pie.js",
                "~/Assets/plugins/_amcharts/radar.js",
                "~/Assets/plugins/_amcharts/serial.js",
                "~/Assets/plugins/_amcharts/xy.js",
                "~/Assets/plugins/_amcharts/themes/light.js",
                "~/Assets/plugins/_amcharts/plugins/export/export.js",
                "~/Assets/plugins/_amcharts/plugins/export/export.config.js"
                ));

            bundles.Add(new StyleBundle("~/style/theme")
                .Include("~/Assets/plugins/font-awesome/css/font-awesome.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/simple-line-icons/simple-line-icons.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/bootstrap/css/bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/uniform/css/uniform.default.css", new CssRewriteUrlTransform())
                .Include(
                "~/Assets/plugins/bootstrap-datepicker/css/datepicker.css",
                "~/Assets/plugins/bootstrap-datepicker/css/datepicker3.css")
                .Include("~/Assets/plugins/select2/select2.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/components-rounded.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/plugins.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/layout.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/themes/default.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/custom.css", new CssRewriteUrlTransform())
                );

            bundles.Add(new StyleBundle("~/style/login")
                .Include("~/Assets/plugins/font-awesome/css/font-awesome.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/simple-line-icons/simple-line-icons.css", new CssRewriteUrlTransform())
                .Include(
                "~/Assets/plugins/bootstrap/css/bootstrap.css",
                "~/Assets/plugins/uniform/css/uniform.default.css")
                .Include("~/Assets/css/login.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/components-rounded.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/plugins.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/layout.css", new CssRewriteUrlTransform())
                .Include(
                "~/Assets/css/themes/default.css",
                "~/Assets/css/custom.css"));
        }
    }
}