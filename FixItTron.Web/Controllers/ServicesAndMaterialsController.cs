﻿using FixItTron.Domain.Services.Abstract;
using FixItTron.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using FixItTron.Web.Helpers;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Core;
using FixItTron.Domain;
using FixItTron.Domain.Helpers;

namespace FixItTron.Web.Controllers
{
    public partial class ServicesAndMaterialsController : Controller
    {
        IClientService _clientService;

        public ServicesAndMaterialsController(IClientService clientService)
        {
            _clientService = clientService;
        }

        public SortSpecification DefaultSort
        {
            get { return new SortSpecification("OrderNo", "desc"); }
        }

        public virtual ActionResult Index(ServicesAndMaterialsFilterModel filter, CurrentPageInformation pagingInfo, SortSpecification sortSpecification)
        {
            int count = 0;
            if (sortSpecification == null || !sortSpecification.IsValidOrApplied())
            {
                sortSpecification = DefaultSort;
            }

            if (filter.DefaultFilter.HasValue && filter.DefaultFilter.Value == true && !filter.PerformanceDate.StartDate.HasValue && !filter.PerformanceDate.EndDate.HasValue)
            {
                var now = DateTime.Today;
                var firstDayOfMonth = new DateTime(now.Year, now.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                filter.PerformanceDate = new UnboundedPeriod(firstDayOfMonth, lastDayOfMonth);
            }

            filter.ServicesAndMaterialsTypesSelectList = _clientService.ServicesAndMaterialsTypesSelectList();
            filter.CategoriesSelectList = _clientService.CategoriesSelectList(this.Session.GetClientId());
            filter.AddressesSelectList = _clientService.AddressesSelectList(this.Session.GetClientId());
            filter.ServiceAndMaterialsDescriptionsSelectList = _clientService.ServiceAndMaterialsDescriptionsSelectList(this.Session.GetClientId());

            var model = new ServicesAndMaterialsListViewModel();
            model.Filter = filter;

            model.ServicesAndMaterials = _clientService.GetServicesAndMaterialsList(this.Session.GetClientId(), filter.OrderNumber, filter.TypeId, filter.PerformanceDate.StartDate, filter.PerformanceDate.EndDate, filter.Address, filter.CategoryIds, filter.Amount.From, filter.Amount.To, filter.Names, filter.Count.From, filter.Count.To, pagingInfo.Page, pagingInfo.PageSize, sortSpecification.SortBy, sortSpecification.Sort, out count);

            model.Headers = new[] {
                new GridHeaderModel(ApplicationStrings.PerformanceDateShort, "ExecDate", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Number, "OrderNo", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Type, "Type", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Address, "Address", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Category, "Category", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Name, "Description", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Count, "Count", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Amount, "Amount", sortSpecification),
                new GridHeaderModel(string.Empty, "", isSortable: false)
            };

            SetUpList(filter.GetRouteValues(), pagingInfo, sortSpecification, model, count, MVC.ServicesAndMaterials.Index());
            model.SortLink = (sort) => Url.Action(MVC.ServicesAndMaterials.Index().AddRouteValues(filter.GetRouteValues()).AddRouteValues(sort).AddRouteValues(pagingInfo));

            return View(model);
        }

        public void SetUpList(RouteValueDictionary routeValues, CurrentPageInformation pagingInfo, SortSpecification sortSpecification, ListPageViewModelBase model, int count, ActionResult action)
        {
            model.CurrentPageInformation = pagingInfo;
            model.SortSpecification = sortSpecification;
            model.Pager = new DataPagingInformation(pagingInfo.PageSize, pagingInfo.Page);
            model.Pager.ItemCount = count;
            model.Pager.PageSizes = new int[] { 25, 50, 75, 100 };
            model.Pager.CurrentPageLink = page => Url.Action(action.AddRouteValues(routeValues).AddRouteValue("page", page).AddRouteValue("pageSize", pagingInfo.PageSize).AddRouteValues(sortSpecification));
            model.Pager.PageSizeLink = pageSize => Url.Action(action.AddRouteValues(routeValues).AddRouteValue("pageSize", pageSize).AddRouteValues(sortSpecification));
        }

        public virtual ActionResult Export(ServicesAndMaterialsFilterModel filter, CurrentPageInformation pagingInfo, SortSpecification sortSpecification)
        {
            int count = 0;
            if (sortSpecification == null || !sortSpecification.IsValidOrApplied())
            {
                sortSpecification = DefaultSort;
            }

            var servicesAndMaterials = _clientService.GetServicesAndMaterialsList(this.Session.GetClientId(), filter.OrderNumber, filter.TypeId, filter.PerformanceDate.StartDate, filter.PerformanceDate.EndDate, filter.Address, filter.CategoryIds, filter.Amount.From, filter.Amount.To, filter.Names, filter.Count.From, filter.Count.To, 1, int.MaxValue, sortSpecification.SortBy, sortSpecification.Sort, out count);

            using (var stream = new MemoryStream())
            {
                XlsFile file = new XlsFile();
                file.NewFile(1);

                int row = 1;

                var headers = new[] { ApplicationStrings.PerformanceDateShort, ApplicationStrings.Number, ApplicationStrings.Type, ApplicationStrings.Address, ApplicationStrings.Category, ApplicationStrings.Name, ApplicationStrings.Count, ApplicationStrings.Amount };

                var fHeader = file.GetDefaultFormat;
                fHeader.Font.Style = TFlxFontStyles.Bold;
                fHeader.FillPattern.Pattern = TFlxPatternStyle.Automatic;
                fHeader.FillPattern.BgColor = TExcelColor.FromArgb(233, 242, 247, 0);

                int headerFormat = file.AddFormat(fHeader);
                for (int i = 0; i < headers.Length; i++)
                {
                    file.SetCellValue(row, i + 1, headers[i], headerFormat);
                }

                row++;

                WriteRows(file, ref row, servicesAndMaterials);

                file.AutofitCol(1, 10, false, 1.2f);
                file.Save(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "export.xls");
            }
        }

        protected void WriteRows(XlsFile file, ref int row, IEnumerable<ServicesAndMaterialsTableRowModel> data)
        {
            var fText = file.GetDefaultFormat;
            fText.Format = "@";
            int textFormat = file.AddFormat(fText);

            var fNumber = file.GetDefaultFormat;
            fNumber.Format = Constants.Formats.DecimalFormat;
            int numberFormat = file.AddFormat(fNumber);

            if (data != null && data.Count() > 0)
            {
                foreach (var dataRow in data)
                {
                    file.SetCellValue(1, row, 1, dataRow.PerformanceDate.ToShortDate(), textFormat);
                    file.SetCellValue(1, row, 2, dataRow.OrderNumber, textFormat);
                    file.SetCellValue(1, row, 3, dataRow.Type, textFormat);
                    file.SetCellValue(1, row, 4, dataRow.Address, textFormat);
                    file.SetCellValue(1, row, 5, dataRow.Category, textFormat);
                    file.SetCellValue(1, row, 6, dataRow.Name, textFormat);
                    file.SetCellValue(1, row, 7, dataRow.Count, textFormat);
                    file.SetCellValue(1, row, 8, dataRow.Amount, numberFormat);

                    row++;
                }
            }
        }

        [HttpGet]
        public virtual ActionResult GetStatistics()
        {
            var currentDate = DateTime.Now;
            var yearMonth = new UnboundedYearMonth()
            {
                YearStart = currentDate.Year,
                MonthStart = currentDate.Month,
                YearEnd = currentDate.Year,
                MonthEnd = currentDate.Month
            };

            var model = new List<StatisticsViewModel>();

            var materialsByFilials = SetUpChartModel(currentDate.Year, StatisticsChartType.MaterialsByFilials, yearMonth);
            var materialsByCategories = SetUpChartModel(currentDate.Year, StatisticsChartType.MaterialsByCategories, yearMonth);
            var materialsByDescriptions = SetUpChartModel(currentDate.Year, StatisticsChartType.MaterialsByDescriptions, yearMonth);
            var serviceCountByCategory = SetUpChartModel(currentDate.Year, StatisticsChartType.ServiceCountByCategory, yearMonth);
            var serviceDescriptionCount = SetUpChartModel(currentDate.Year, StatisticsChartType.ServiceDescriptionCount, yearMonth);

            model.Add(materialsByFilials);
            model.Add(materialsByCategories);
            model.Add(materialsByDescriptions);
            model.Add(serviceCountByCategory);
            model.Add(serviceDescriptionCount);

            return PartialView(model);
        }

        public virtual ActionResult StatisticsChart(int? year, StatisticsChartType type, UnboundedYearMonth yearMonth = null, int? branchId = null, IEnumerable<int> categoryIds = null)
        {
            var model = SetUpChartModel(year, type, yearMonth, branchId, categoryIds);
            return PartialView(model);
        }

        private StatisticsViewModel SetUpChartModel(int? year, StatisticsChartType type, UnboundedYearMonth yearMonth = null, int? branchId = null, IEnumerable<int> categoryIds = null)
        {
            var model = new StatisticsViewModel();
            switch (type)
            {
                case StatisticsChartType.MaterialsByFilials:
                    model.Title = ApplicationStrings.UseOfMaterials;
                    model.Description = ApplicationStrings.MaterialsByFilials;
                    model.Items = _clientService.GetMaterialsByFilials(this.Session.GetClientId(), yearMonth.GetStartDate(), yearMonth.GetEndDate()).OrderByDescending(x => x.Value);
                    break;
                case StatisticsChartType.MaterialsByCategories:
                    model.Title = ApplicationStrings.UseOfMaterials;
                    model.Description = ApplicationStrings.MaterialsByCategories;
                    model.Items = _clientService.GetMaterialsByCategories(this.Session.GetClientId(), yearMonth.GetStartDate(), yearMonth.GetEndDate());
                    break;
                case StatisticsChartType.MaterialsByDescriptions:
                    model.Title = ApplicationStrings.UseOfMaterials;
                    model.Description = ApplicationStrings.MaterialsByDescriptions;
                    model.Items = _clientService.GetMaterialsByDescriptions(this.Session.GetClientId(), yearMonth.GetStartDate(), yearMonth.GetEndDate(), categoryIds).OrderByDescending(x => x.Value);
                    break;
                case StatisticsChartType.MaterialsCountByDescriptions:
                    model.Title = ApplicationStrings.UseOfMaterials;
                    model.Description = ApplicationStrings.MaterialsCountByDescriptions;
                    model.Items = _clientService.GetMaterialsCountByDescriptions(this.Session.GetClientId(), yearMonth.GetStartDate(), yearMonth.GetEndDate(), categoryIds).OrderByDescending(x => x.Value);
                    break;
                case StatisticsChartType.ServiceCategoryCountByFilials:
                    model.Title = ApplicationStrings.UseOfServices;
                    model.Description = ApplicationStrings.ServiceCategoryCountByFilials;
                    model.Items = _clientService.GetServiceCategoryCountByFilials(this.Session.GetClientId(), yearMonth.GetStartDate(), yearMonth.GetEndDate(), branchId).OrderByDescending(x => x.Value);
                    break;
                case StatisticsChartType.ServiceDescriptionCountByFilials:
                    model.Title = ApplicationStrings.UseOfServices;
                    model.Description = ApplicationStrings.ServiceDescriptionCountByFilials;
                    model.Items = _clientService.GetServiceDescriptionCountByFilials(this.Session.GetClientId(), yearMonth.GetStartDate(), yearMonth.GetEndDate(), branchId).OrderByDescending(x => x.Value);
                    break;
                case StatisticsChartType.ServiceDescriptionCount:
                    model.Title = ApplicationStrings.UseOfServices;
                    model.Description = ApplicationStrings.ServiceDescriptionCount;
                    model.Items = _clientService.GetServiceDescriptionCountByFilials(this.Session.GetClientId(), yearMonth.GetStartDate(), yearMonth.GetEndDate(), branchId, true).OrderByDescending(x => x.Value);
                    break;
                case StatisticsChartType.ServiceCountByCategory:
                    model.Title = ApplicationStrings.UseOfServices;
                    model.Description = ApplicationStrings.ServiceCountByCategory;
                    model.Items = _clientService.GetServiceCountByCategory(this.Session.GetClientId(), yearMonth.GetStartDate(), yearMonth.GetEndDate()).OrderByDescending(x => x.Value);
                    break;
            }

            model.YearMonth = yearMonth;
            model.YearsSelectList = Extensions.YearsSelectList();
            model.MonthsSelectList = _clientService.MonthsSelectList();
            model.BranchsSelectList = _clientService.BranchsSelectList(this.Session.GetClientId()).PrependNull(ApplicationStrings.Total);
            model.CategoriesSelectList = _clientService.CategoriesSelectList(this.Session.GetClientId());
            model.CategoryIds = categoryIds;
            model.Year = year;
            model.Type = type;
            model.BranchId = branchId;

            return model;
        }
    }
}