﻿using FixItTron.Domain;
using FixItTron.Domain.Helpers;
using FixItTron.Domain.Services.Abstract;
using FixItTron.Web.Helpers;
using FixItTron.Web.Models;
using FlexCel.Core;
using FlexCel.XlsAdapter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FixItTron.Web.Controllers
{
    public partial class OrdersController : Controller
    {
        IClientService _clientService;
        public OrdersController(IClientService clientService)
        {
            _clientService = clientService;
        }

        public SortSpecification DefaultSort
        {
            get { return new SortSpecification("OrderNo", "desc"); }
        }

        public virtual ActionResult Index(OrdersFilterModel filter, CurrentPageInformation pagingInfo, SortSpecification sortSpecification)
        {
            int count = 0;
            if (sortSpecification == null || !sortSpecification.IsValidOrApplied())
            {
                sortSpecification = DefaultSort;
            }

            if (filter.DefaultFilter.HasValue && filter.DefaultFilter.Value == true && !filter.PerformanceDate.StartDate.HasValue && !filter.PerformanceDate.EndDate.HasValue)
            {
                var now = DateTime.Today;
                var firstDayOfMonth = new DateTime(now.Year, now.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                filter.PerformanceDate = new UnboundedPeriod(firstDayOfMonth, lastDayOfMonth);
            }

            filter.OrderTypesSelectList = _clientService.OrderTypesSelectList();
            filter.CategoriesSelectList = _clientService.CategoriesSelectList(this.Session.GetClientId());
            filter.StatusesSelectList = _clientService.StatusesSelectList(this.Session.GetClientId());
            filter.AddressesSelectList = _clientService.AddressesSelectList(this.Session.GetClientId());

            var model = new OrdersListViewModel();
            model.Filter = filter;

            model.Orders = _clientService.GetOrdersList(this.Session.GetClientId(), filter.OrderNumber, filter.OrderTypeIds, filter.RegistrationDate.StartDate, filter.RegistrationDate.EndDate, filter.PerformanceDate.StartDate, filter.PerformanceDate.EndDate, filter.Amount.From, filter.Amount.To, filter.ServiceAmount.From, filter.ServiceAmount.To, filter.MaterialAmount.From, filter.MaterialAmount.To, filter.OtherAmount.From, filter.OtherAmount.To, filter.Address, filter.CategoryIds, filter.StatusIds, pagingInfo.Page, pagingInfo.PageSize, sortSpecification.SortBy, sortSpecification.Sort, out count);

            model.Headers = new[] {
                new GridHeaderModel(ApplicationStrings.Number, "OrderNo", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Type, "Type", sortSpecification),
                new GridHeaderModel(ApplicationStrings.RegistrationDateShort, "RegDate", sortSpecification),
                new GridHeaderModel(ApplicationStrings.PerformanceDateShort, "ExecDate", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Address, "Address", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Category, "Category", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Amount, "Amount", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Service, "ServiceAmount", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Material, "MaterialAmount", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Other, "OtherAmount", sortSpecification),
                new GridHeaderModel(ApplicationStrings.Status, "Status", sortSpecification),
                new GridHeaderModel(ApplicationStrings.AccountingStatus, "AccountingStatus", sortSpecification, isSortable:false),
                new GridHeaderModel(string.Empty, "", isSortable: false)
            };

            SetUpList(filter.GetRouteValues(), pagingInfo, sortSpecification, model, count, MVC.Orders.Index());
            model.SortLink = (sort) => Url.Action(MVC.Orders.Index().AddRouteValues(filter.GetRouteValues()).AddRouteValues(sort).AddRouteValues(pagingInfo));

            return View(model);
        }

        public void SetUpList(RouteValueDictionary routeValues, CurrentPageInformation pagingInfo, SortSpecification sortSpecification, ListPageViewModelBase model, int count, ActionResult action)
        {
            model.CurrentPageInformation = pagingInfo;
            model.SortSpecification = sortSpecification;
            model.Pager = new DataPagingInformation(pagingInfo.PageSize, pagingInfo.Page);
            model.Pager.ItemCount = count;
            model.Pager.PageSizes = new int[] { 25, 50, 75, 100 };
            model.Pager.CurrentPageLink = page => Url.Action(action.AddRouteValues(routeValues).AddRouteValue("page", page).AddRouteValue("pageSize", pagingInfo.PageSize).AddRouteValues(sortSpecification));
            model.Pager.PageSizeLink = pageSize => Url.Action(action.AddRouteValues(routeValues).AddRouteValue("pageSize", pageSize).AddRouteValues(sortSpecification));
        }

        public virtual ActionResult Export(OrdersFilterModel filter, CurrentPageInformation pagingInfo, SortSpecification sortSpecification)
        {
            int count = 0;
            if (sortSpecification == null || !sortSpecification.IsValidOrApplied())
            {
                sortSpecification = DefaultSort;
            }

            var orders = _clientService.GetOrdersList(this.Session.GetClientId(), filter.OrderNumber, filter.OrderTypeIds, filter.RegistrationDate.StartDate, filter.RegistrationDate.EndDate, filter.PerformanceDate.StartDate, filter.PerformanceDate.EndDate, filter.Amount.From, filter.Amount.To, filter.ServiceAmount.From, filter.ServiceAmount.To, filter.MaterialAmount.From, filter.MaterialAmount.To, filter.OtherAmount.From, filter.OtherAmount.To, filter.Address, filter.CategoryIds, filter.StatusIds, 1, int.MaxValue, sortSpecification.SortBy, sortSpecification.Sort, out count);

            using (var stream = new MemoryStream())
            {
                XlsFile file = new XlsFile();
                file.NewFile(1);

                int row = 1;

                var headers = new[] { ApplicationStrings.Number, ApplicationStrings.Type, ApplicationStrings.RegistrationDateShort, ApplicationStrings.PerformanceDateShort, ApplicationStrings.Address, ApplicationStrings.Category, ApplicationStrings.Amount, ApplicationStrings.Service, ApplicationStrings.Material, ApplicationStrings.Other, ApplicationStrings.Status, ApplicationStrings.AccountingStatus };

                var fHeader = file.GetDefaultFormat;
                fHeader.Font.Style = TFlxFontStyles.Bold;
                fHeader.FillPattern.Pattern = TFlxPatternStyle.Automatic;
                fHeader.FillPattern.BgColor = TExcelColor.FromArgb(233, 242, 247, 0);

                int headerFormat = file.AddFormat(fHeader);
                for (int i = 0; i < headers.Length; i++)
                {
                    file.SetCellValue(row, i + 1, headers[i], headerFormat);
                }

                row++;

                WriteRows(file, ref row, orders);

                file.AutofitCol(1, 10, false, 1.2f);
                file.Save(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "export.xls");
            }
        }

        protected void WriteRows(XlsFile file, ref int row, IEnumerable<OrderTableRowModel> data)
        {
            var fText = file.GetDefaultFormat;
            fText.Format = "@";
            int textFormat = file.AddFormat(fText);

            var fNumber = file.GetDefaultFormat;
            fNumber.Format = Constants.Formats.DecimalFormat;
            int numberFormat = file.AddFormat(fNumber);

            if (data != null && data.Count() > 0)
            {
                foreach (var dataRow in data)
                {
                    file.SetCellValue(1, row, 1, dataRow.OrderNumber, textFormat);
                    file.SetCellValue(1, row, 2, dataRow.OrderType, textFormat);
                    file.SetCellValue(1, row, 3, dataRow.RegistrationDate.ToShortDate(), textFormat);
                    file.SetCellValue(1, row, 4, dataRow.PerformanceDate.ToShortDate(), textFormat);
                    file.SetCellValue(1, row, 5, dataRow.Address, textFormat);
                    file.SetCellValue(1, row, 6, dataRow.Category, textFormat);
                    file.SetCellValue(1, row, 7, dataRow.Amount, numberFormat);
                    file.SetCellValue(1, row, 8, dataRow.ServiceAmount, numberFormat);
                    file.SetCellValue(1, row, 9, dataRow.MaterialAmount, numberFormat);
                    file.SetCellValue(1, row, 10, dataRow.OtherAmount, numberFormat);
                    file.SetCellValue(1, row, 11, dataRow.Status, textFormat);
                    file.SetCellValue(1, row, 12, dataRow.AccountingStatus, textFormat);

                    row++;
                }
            }
        }

        [HttpGet]
        public virtual ActionResult Details(string orderNumber)
        {
            var model = _clientService.GetOrderDetails(this.Session.GetClientId(), orderNumber);
            return PartialView(model);
        }
    }
}