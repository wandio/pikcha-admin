﻿using FixItTron.Domain;
using FixItTron.Domain.Services.Abstract;
using FixItTron.Web.Helpers;
using FixItTron.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Controllers
{
    public partial class HomeController : Controller
    {
        IClientService _clientService;

        public HomeController(IClientService clientService)
        {
            _clientService = clientService;
        }

        public virtual ActionResult Index()
        {
            var currentDate = DateTime.Now;
            var model = new HomeViewModel();
            model.Year = currentDate.Year;
            model.YearsSelectList = Extensions.YearsSelectList();
            model.MonthsSelectList = _clientService.MonthsSelectList();

            model.ChartYearMonth.MonthEnd = currentDate.Month;
            model.ChartYearMonth.MonthStart = currentDate.Month;
            model.ChartYearMonth.YearEnd = currentDate.Year;
            model.ChartYearMonth.YearStart = currentDate.Year;

            model.WidgetYearMonth.MonthEnd = currentDate.Month;
            model.WidgetYearMonth.MonthStart = currentDate.Month;
            model.WidgetYearMonth.YearEnd = currentDate.Year;
            model.WidgetYearMonth.YearStart = currentDate.Year;

            model.TermsOfContract = _clientService.GetTermsOfContract(this.Session.GetClientId());
            model.OrdersSummary = _clientService.GetOrdersSummary(this.Session.GetClientId(), model.WidgetYearMonth.GetStartDate(), model.WidgetYearMonth.GetEndDate());

            return View(model);
        }

        [HttpGet]
        public virtual ActionResult WidgetOrdersSummary(UnboundedYearMonth widgetYearMonth)
        {
            var model = _clientService.GetOrdersSummary(this.Session.GetClientId(), widgetYearMonth.GetStartDate(), widgetYearMonth.GetEndDate());
            return PartialView(model);
        }

        [HttpGet]
        public virtual ActionResult GetOrdersByFilials(UnboundedYearMonth chartYearMonth)
        {
            var model = _clientService.GetOrdersByFilials(this.Session.GetClientId(), chartYearMonth.GetStartDate(), chartYearMonth.GetEndDate());
            return PartialView(model);
        }
    }
}