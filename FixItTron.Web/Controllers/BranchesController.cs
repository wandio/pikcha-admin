﻿using FixItTron.Domain.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FixItTron.Web.Helpers;
using FixItTron.Web.Models;

namespace FixItTron.Web.Controllers
{
    public partial class BranchesController : Controller
    {
        IClientService _clientService;

        public BranchesController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [HttpGet]
        public virtual ActionResult EditBranch()
        {
            var rawData = _clientService.GetBranches(this.Session.GetClientId());
            var model = AutoMapper.Mapper.Map<IEnumerable<EditBranchViewModel>>(rawData);
            return View(model);
        }

        [HttpPost]
        public virtual ActionResult EditBranch(IEnumerable<EditBranchViewModel> model)
        {
            if (ModelState.IsValid)
            {
                int clientId = this.Session.GetClientId();
                int index = 0;
                foreach (var item in model)
                {
                    string errorText = string.Empty;
                    bool success = _clientService.SaveClientBranch(clientId, item.BranchName, item.BranchId, out errorText);
                    if (!success)
                    {
                        ModelState.AddModelError(string.Format("[{0}].BranchName", index), errorText);
                    }
                    index++;
                }

                TempData.AddFlash(ApplicationStrings.AllBranchesUpdatedSuccessfully);
                return RedirectToAction(MVC.Branches.EditBranch());
            }
            return View(model);
        }
    }
}