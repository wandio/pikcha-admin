﻿using FixItTron.Domain;
using FixItTron.Domain.Helpers;
using FixItTron.Domain.Services.Abstract;
using FixItTron.Web.Helpers;
using FixItTron.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace FixItTron.Web.Controllers
{
    [AllowAnonymous]
    public partial class AuthController : Controller
    {
        IClientService _clientService;

        public AuthController(IClientService clientService)
        {
            _clientService = clientService;
        }

        [HttpGet]
        public virtual ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual ActionResult LogIn(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = new UserModel();
                if (_clientService.Authenticate(model.UserName, model.Password, out user))
                {
                    if (user.IsFirst)
                    {
                        this.Session.RefreshClientIdCache(user.UserId);

                        return RedirectToAction(MVC.Auth.FirstAuthorization().AddRouteValue("userName", user.UserName).AddRouteValue("description", user.Description));
                    }

                    HttpCookie authCookie = FormsAuthentication.GetAuthCookie(user.UserName, false);
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    FormsAuthenticationTicket newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration, model.RememberMe, user.Description, authCookie.Path);
                    authCookie.Value = FormsAuthentication.Encrypt(newTicket);
                    Response.Cookies.Add(authCookie);

                    this.Session.RefreshClientIdCache(user.UserId);

                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/") && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction(MVC.Home.Index());
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, ApplicationStrings.IncorrectUsernameOrPassword);
                }
            }

            return View(model);
        }

        [HttpGet]
        public virtual ActionResult FirstAuthorization(string userName, string description)
        {
            var viewModel = new FirstAuthorizationViewModel()
            {
                UserName = userName,
                Description = description
            };

            return View(viewModel);
        }

        [HttpPost]
        public virtual ActionResult FirstAuthorization(FirstAuthorizationViewModel model)
        {
            string errorText = string.Empty;
            var clientId = this.Session.GetClientId();

            if (ModelState.IsValid)
            {
                if (_clientService.FirstAuthorization(clientId, model.UserName, model.NewPassword, out errorText))
                {
                    HttpCookie authCookie = FormsAuthentication.GetAuthCookie(model.UserName, false);
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);
                    FormsAuthenticationTicket newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, ticket.Expiration, false, model.Description, authCookie.Path);
                    authCookie.Value = FormsAuthentication.Encrypt(newTicket);
                    Response.Cookies.Add(authCookie);

                    this.Session.RefreshClientIdCache(clientId);

                    return RedirectToAction(MVC.Home.Index());
                }
                else
                {
                    ModelState.AddModelError(string.Empty, errorText);
                }
            }
            return View(model);
        }

        [HttpGet]
        public virtual ActionResult ConfirmResetPassword()
        {
            var model = new ConfirmResetPasswordViewModel();
            return View();
        }

        [HttpPost]
        public virtual ActionResult ConfirmResetPassword(ConfirmResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var isSuccess = _clientService.CheckUserExist(model.Email);
                    if (isSuccess)
                    {
                        _clientService.SendEmail(model.Email, ApplicationStrings.ResetPassword, Url.ActionAbsolute(MVC.Auth.ResetPassword().AddRouteValue("key", MachineKeyHelper.Encrypt(model.Email, "passwordReset"))));

                        TempData.AddFlash(ApplicationStrings.PasswordResetLinkWasSentToEmail);
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, ApplicationStrings.CouldNotSendPasswordResetLink);
                    }
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, ApplicationStrings.CouldNotSendPasswordResetLink);
                }
            }
            return View(model);
        }

        [HttpGet]
        public virtual ActionResult ResetPassword(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                ModelState.AddModelError(string.Empty, ApplicationStrings.CouldNotResetPassword);
            }

            var model = new ResetPasswordViewModel();
            model.Key = key;

            return View(model);
        }

        [HttpPost]
        public virtual ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            string errorText = string.Empty;

            if (ModelState.IsValid)
            {
                try
                {
                    model.Email = MachineKeyHelper.Decrypt(model.Key, "passwordReset");
                    var isSuccess = _clientService.ResetPassword(model.Email, model.NewPassword, out errorText);
                    if (!isSuccess)
                    {
                        ModelState.AddModelError(string.Empty, errorText);
                    }
                    else
                    {
                        TempData.AddFlash(ApplicationStrings.PasswordReseted);
                        return RedirectToAction(MVC.Auth.LogIn());
                    }
                }
                catch
                {
                    ModelState.AddModelError(string.Empty, ApplicationStrings.CouldNotSendPasswordResetLink);
                }
            }
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public virtual ActionResult ChangePassword()
        {
            var model = new ChangePasswordViewModel();
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public virtual ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            string errorText = string.Empty;
            var clientId = this.Session.GetClientId();

            if (ModelState.IsValid)
            {
                if (_clientService.ChangePassword(clientId, model.CurrentPassword, model.NewPassword, out errorText))
                {
                    TempData.AddFlash(ApplicationStrings.PasswordChanged);
                    return RedirectToAction(MVC.Auth.LogOut());
                }
                else
                {
                    ModelState.AddModelError(string.Empty, errorText);
                }
            }
            return View(model);
        }

        [HttpGet]
        public virtual ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction(MVC.Auth.LogIn());
        }
    }
}