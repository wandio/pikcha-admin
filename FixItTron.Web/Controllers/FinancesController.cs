﻿using FixItTron.Domain.Services.Abstract;
using FixItTron.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FixItTron.Web.Helpers;
using System.IO;
using FlexCel.XlsAdapter;
using FlexCel.Core;
using FixItTron.Domain;
using FixItTron.Domain.Helpers;
using System.Web.Script.Serialization;

namespace FixItTron.Web.Controllers
{
    public partial class FinancesController : Controller
    {
        IClientService _clientService;

        public FinancesController(IClientService clientService)
        {
            _clientService = clientService;
        }

        public virtual ActionResult Index(int? year)
        {
            var model = new FinancesListViewModel();

            model.YearsSelectList = Extensions.YearsSelectList(currentDefault: false);
            model.Year = year;
            model.Finances = _clientService.GetClientFinances(this.Session.GetClientId(), year);
            model.Headers = _clientService.GetMonths().Select(x => new GridHeaderModel()
            {
                Title = x.Value.Substring(0, 3) + ".",
                Name = x.Value,
                IsSortable = false
            }).ToList();

            return View(model);
        }

        public virtual ActionResult Export(int? year)
        {
            var finances = _clientService.GetClientFinances(this.Session.GetClientId(), year);

            using (var stream = new MemoryStream())
            {
                XlsFile file = new XlsFile();
                file.NewFile(1);

                int row = 1;

                var headers = _clientService.GetMonths().Select(x => x.Value).ToList();

                var fHeader = file.GetDefaultFormat;
                fHeader.Font.Style = TFlxFontStyles.Bold;
                fHeader.FillPattern.Pattern = TFlxPatternStyle.Automatic;
                fHeader.FillPattern.BgColor = TExcelColor.FromArgb(233, 242, 247, 0);

                int headerFormat = file.AddFormat(fHeader);

                file.SetCellValue(row, 1, string.Empty, headerFormat);

                for (int i = 0; i < headers.Count(); i++)
                {
                    file.SetCellValue(row, i + 2, headers[i], headerFormat);
                }

                row++;

                WriteRows(file, ref row, finances, headers, headerFormat);

                file.AutofitCol(1, 10, false, 1.2f);
                file.Save(stream);
                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "export.xls");
            }
        }

        protected void WriteRows(XlsFile file, ref int row, IEnumerable<FinanceTableRowModel> data, List<string> headers, int headerFormat)
        {
            TFlxFormat fmt;

            var fText = file.GetDefaultFormat;
            fText.Format = "@";
            int textFormat = file.AddFormat(fText);

            var fNumber = file.GetDefaultFormat;
            fNumber.Format = Constants.Formats.DecimalFormat;
            int numberFormat = file.AddFormat(fNumber);

            if (data != null && data.Count() > 0)
            {
                file.SetCellValue(1, row, 1, ApplicationStrings.StartBalance, textFormat);
                for (int i = 0; i < headers.Count(); i++)
                {
                    var val = data.Where(x => x.MonthName == headers[i]).Sum(x => x.StartBalance);
                    file.SetCellValue(1, row, i + 2, val, numberFormat);
                }
                row++;

                file.SetRowOutlineLevel(row, row + 4, 1);

                #region collapsed

                file.SetRowHidden(row, true);
                fmt = BoldStyle(file, row, 1);
                file.SetCellValue(row, 1, ApplicationStrings.Accruals);
                for (int i = 0; i < headers.Count(); i++)
                {
                    fmt = BoldStyle(file, row, i + 2);
                    file.SetCellValue(row, i + 2, headers[i]);
                }
                row++;

                file.SetRowHidden(row, true);
                fmt = NormalStyle(file, row, 1);
                file.SetCellValue(row, 1, ApplicationStrings.AbonentAmount);
                for (int i = 0; i < headers.Count(); i++)
                {
                    var val = data.Where(x => x.MonthName == headers[i]).Sum(x => x.AbonentAmount);
                    file.SetCellValue(1, row, i + 2, val, numberFormat);
                }
                row++;

                file.SetRowHidden(row, true);
                fmt = NormalStyle(file, row, 1);
                file.SetCellValue(row, 1, ApplicationStrings.MaterialAmount);
                for (int i = 0; i < headers.Count(); i++)
                {
                    var val = data.Where(x => x.MonthName == headers[i]).Sum(x => x.MaterialAmount);
                    file.SetCellValue(1, row, i + 2, val, numberFormat);
                }
                row++;

                file.SetRowHidden(row, true);
                fmt = NormalStyle(file, row, 1);
                file.SetCellValue(row, 1, ApplicationStrings.OtherAmount);
                for (int i = 0; i < headers.Count(); i++)
                {
                    var val = data.Where(x => x.MonthName == headers[i]).Sum(x => x.OtherAmount);
                    file.SetCellValue(1, row, i + 2, val, numberFormat);
                }
                row++;

                file.SetRowHidden(row, true);
                fmt = NormalStyle(file, row, 1);
                file.SetCellValue(row, 1, ApplicationStrings.ServiceAmount);
                for (int i = 0; i < headers.Count(); i++)
                {
                    var val = data.Where(x => x.MonthName == headers[i]).Sum(x => x.ServiceAmount);
                    file.SetCellValue(1, row, i + 2, val, numberFormat);
                }
                row++;
                #endregion

                file.SetCellValue(1, row, 1, ApplicationStrings.SumAccrualAmount, textFormat);
                for (int i = 0; i < headers.Count(); i++)
                {
                    var val = data.Where(x => x.MonthName == headers[i]).Sum(x => x.AccrualAmount);
                    file.SetCellValue(1, row, i + 2, val, numberFormat);
                }
                row++;

                file.SetCellValue(1, row, 1, ApplicationStrings.SumPaymentAmount, textFormat);
                for (int i = 0; i < headers.Count(); i++)
                {
                    var val = data.Where(x => x.MonthName == headers[i]).Sum(x => x.PaymentAmount);
                    file.SetCellValue(1, row, i + 2, val, numberFormat);
                }
                row++;

                file.SetCellValue(1, row, 1, ApplicationStrings.EndBalance, textFormat);
                for (int i = 0; i < headers.Count(); i++)
                {
                    var val = data.Where(x => x.MonthName == headers[i]).Sum(x => x.EndBalance);
                    file.SetCellValue(1, row, i + 2, val, numberFormat);
                }
                row++;
            }
        }

        private static TFlxFormat BoldStyle(XlsFile file, int row, int col)
        {
            TFlxFormat fmt;
            fmt = file.GetCellVisibleFormatDef(row, col);
            fmt.Font.Color = TExcelColor.FromTheme(TThemeColor.Background2, -0.499984740745262);
            fmt.Font.Style = TFlxFontStyles.Bold;
            fmt.Font.Family = 2;
            file.SetCellFormat(row, col, file.AddFormat(fmt));
            return fmt;
        }

        private static TFlxFormat NormalStyle(XlsFile file, int row, int col)
        {
            TFlxFormat fmt;
            fmt = file.GetCellVisibleFormatDef(row, col);
            fmt.Font.Color = TExcelColor.FromTheme(TThemeColor.Background2, -0.499984740745262);
            fmt.Font.Family = 2;
            fmt.Format = "@";
            file.SetCellFormat(row, col, file.AddFormat(fmt));
            return fmt;
        }

        [HttpGet]
        public virtual ActionResult FilterFinances(int? year)
        {
            var model = new FinancesListViewModel();
            model.Finances = _clientService.GetClientFinances(this.Session.GetClientId(), year);
            model.Headers = _clientService.GetMonths().Select(x => new GridHeaderModel()
            {
                Title = x.Value.Substring(0, 3) + ".",
                Name = x.Value,
                IsSortable = false
            }).ToList();
            return PartialView(model);
        }

        public virtual ActionResult StatisticsChart(int? year, StatisticsChartType type)
        {
            var model = new StatisticsViewModel();
            var finances = _clientService.GetClientFinances(this.Session.GetClientId(), year);

            switch (type)
            {
                case StatisticsChartType.FinancesMaterials:
                    model.Title = ApplicationStrings.Material;
                    model.Items = finances.Select(x => new StatisticsDataModel() { Title = x.MonthName, Value = x.MaterialAmount, Color1 = "#0070a8" });
                    break;
                case StatisticsChartType.FinancesSumAccrual:
                    model.Title = ApplicationStrings.SumAccrualAmount;
                    //model.Items = finances.GroupBy(x => x.MonthName).Select(x => new StatisticsDataModel() { Title = x.Key, Value = x.Sum(s => s.AccrualAmount), Color1 = "#0070a8" });
                    model.JsonObject = new JavaScriptSerializer().Serialize(finances.GroupBy(x => x.MonthName).Select(x => new
                    {
                        Title = x.Key,
                        AbonentAmount = x.Sum(s => s.AbonentAmount),
                        MaterialAmount = x.Sum(s => s.MaterialAmount),
                        OtherAmount = x.Sum(s => s.MaterialAmount),
                        ServiceAmount = x.Sum(s => s.ServiceAmount)
                    }));
                    break;
            }

            model.YearsSelectList = Extensions.YearsSelectList();
            model.Year = year;
            model.Type = type;

            return PartialView(model);
        }
    }
}