﻿using FixItTron.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Controllers
{
    public partial class MenuController : Controller
    {
        [ChildActionOnly]
        public virtual ActionResult TopMenu()
        {
            string controllerName = ControllerContext.ParentActionViewContext.RouteData.Values["controller"].ToString();
            string actionName = ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();

            var model = new TopMenuModel();
            model.Items.Add(new MenuItem()
            {
                Title = ApplicationStrings.Home,
                Action = MVC.Home.Index(),
                Selected = controllerName.Equals(MVC.Home.Name)
            });
            model.Items.Add(new MenuItem()
            {
                Title = ApplicationStrings.Orders,
                Action = MVC.Orders.Index().AddRouteValue("DefaultFilter", true),
                Selected = controllerName.Equals(MVC.Orders.Name)
            });
            model.Items.Add(new MenuItem()
            {
                Title = ApplicationStrings.ServicesAndMaterials,
                Action = MVC.ServicesAndMaterials.Index().AddRouteValue("DefaultFilter", true),
                Selected = controllerName.Equals(MVC.ServicesAndMaterials.Name)
            });
            model.Items.Add(new MenuItem()
            {
                Title = ApplicationStrings.Finances,
                Action = MVC.Finances.Index(),
                Selected = controllerName.Equals(MVC.Finances.Name)
            });
            model.Items.Add(new MenuItem()
            {
                Title = ApplicationStrings.Branchs,
                Action = MVC.Branches.EditBranch(),
                Selected = controllerName.Equals(MVC.Branches.Name)
            });

            ControllerContext.ParentActionViewContext.ViewBag.Menu = model;
            return View(model);
        }

        [ChildActionOnly]
        public virtual ActionResult BreadCrumbs()
        {
            var menuModel = ControllerContext.ParentActionViewContext.ViewBag.Menu as TopMenuModel;
            var breadcrumbs = new Dictionary<string, string>();
            var currentMenuItem = menuModel.Items.SingleOrDefault(x => x.Selected == true);
            if (currentMenuItem != null)
            {
                breadcrumbs.Add(currentMenuItem.Title, Url.Action(currentMenuItem.Action));
            }
            return View(breadcrumbs);
        }
    }
}