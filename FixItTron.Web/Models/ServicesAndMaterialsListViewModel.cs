﻿using FixItTron.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    public class ServicesAndMaterialsListViewModel : ListPageViewModelBase
    {
        public IEnumerable<ServicesAndMaterialsTableRowModel> ServicesAndMaterials { get; set; }
        public IEnumerable<GridHeaderModel> Headers { get; set; }
        public ServicesAndMaterialsFilterModel Filter { get; set; }

        public ServicesAndMaterialsListViewModel()
        {
            ServicesAndMaterials = new List<ServicesAndMaterialsTableRowModel>();
        }
    }
}