﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    public class TopMenuModel
    {
        public List<MenuItem> Items { get; set; }

        public TopMenuModel()
        {
            Items = new List<MenuItem>();
        }
    }
}