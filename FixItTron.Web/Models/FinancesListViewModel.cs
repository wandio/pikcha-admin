﻿using FixItTron.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Models
{
    public class FinancesListViewModel
    {
        public IEnumerable<FinanceTableRowModel> Finances { get; set; }
        public List<GridHeaderModel> Headers { get; set; }

        public int? Year { get; set; }
        public IEnumerable<SelectListItem> YearsSelectList { get; set; }

        public decimal SumStartBalanceByMonth(string month)
        {
            return Finances.Where(x => x.MonthName == month).Sum(x => x.StartBalance);
        }

        public decimal SumEndBalanceByMonth(string month)
        {
            return Finances.Where(x => x.MonthName == month).Sum(x => x.EndBalance);
        }

        public decimal SumPaymentAmountByMonth(string month)
        {
            return Finances.Where(x => x.MonthName == month).Sum(x => x.PaymentAmount);
        }

        public decimal SumAccrualAmountByMonth(string month)
        {
            return Finances.Where(x => x.MonthName == month).Sum(x => x.AccrualAmount);
        }

        public FinancesListViewModel()
        {
            Finances = new List<FinanceTableRowModel>();
        }
    }
}