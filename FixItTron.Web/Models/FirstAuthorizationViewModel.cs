﻿using FixItTron.Domain;
using FixItTron.Web.Models.Validations;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    [Validator(typeof(FirstAuthorizationViewModelValidator))]
    public class FirstAuthorizationViewModel
    {
        public string UserName { get; set; }
        public string Description { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }

    }
}