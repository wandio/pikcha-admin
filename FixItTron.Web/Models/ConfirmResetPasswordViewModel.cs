﻿using FixItTron.Web.Models.Validations;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    [Validator(typeof(ConfirmResetPasswordViewModelValidator))]
    public class ConfirmResetPasswordViewModel
    {
        public string Email { get; set; }
    }
}