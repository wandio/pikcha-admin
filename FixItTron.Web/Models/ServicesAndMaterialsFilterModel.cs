﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FixItTron.Web.Models
{
    public class ServicesAndMaterialsFilterModel
    {
        public UnboundedPeriod PerformanceDate { get; set; }

        public int? TypeId { get; set; }
        public IEnumerable<SelectListItem> ServicesAndMaterialsTypesSelectList { get; set; }

        public List<string> Address { get; set; }
        public IEnumerable<SelectListItem> AddressesSelectList { get; set; }

        public List<int> CategoryIds { get; set; }
        public IEnumerable<SelectListItem> CategoriesSelectList { get; set; }

        public UnboundedIntRange Count { get; set; }

        public UnboundedDecimalRange Amount { get; set; }

        public string OrderNumber { get; set; }

        public bool? DefaultFilter { get; set; }

        public List<int> Names { get; set; }
        public IEnumerable<SelectListItem> ServiceAndMaterialsDescriptionsSelectList { get; set; }

        public ServicesAndMaterialsFilterModel()
        {
            Address = new List<string>();
            CategoryIds = new List<int>();
            Names = new List<int>();

            PerformanceDate = new UnboundedPeriod();
            Amount = new UnboundedDecimalRange();
            Count = new UnboundedIntRange();
        }

        public RouteValueDictionary GetRouteValues()
        {
            var result = new RouteValueDictionary();

            if (DefaultFilter.HasValue && DefaultFilter.Value == true)
            {
                result.Add("DefaultFilter", DefaultFilter);
            }
            if (TypeId.HasValue)
            {
                result.Add("TypeId", TypeId);
            }
            if (PerformanceDate != null)
            {
                if (PerformanceDate.StartDate.HasValue)
                {
                    result.Add("PerformanceDate.StartDate", PerformanceDate.StartDate.Value.ToShortDateString());
                }
                if (PerformanceDate.EndDate.HasValue)
                {
                    result.Add("PerformanceDate.EndDate", PerformanceDate.EndDate.Value.ToShortDateString());
                }
            }
            if (Amount != null)
            {
                if (Amount.From.HasValue)
                {
                    result.Add("Amount.From", Amount.From);
                }
                if (Amount.To.HasValue)
                {
                    result.Add("Amount.To", Amount.To);
                }
            }
            if (Count != null)
            {
                if (Count.From.HasValue)
                {
                    result.Add("Count.From", Count.From);
                }
                if (Amount.To.HasValue)
                {
                    result.Add("Count.To", Count.To);
                }
            }
            if (Address.Any())
            {
                for (int i = 0; i < Address.Count(); i++)
                {
                    result.Add(string.Format("Address[{0}]", i), Address[i]);
                }
            }
            if (Names.Any())
            {
                for (int i = 0; i < Names.Count(); i++)
                {
                    result.Add(string.Format("Names[{0}]", i), Names[i]);
                }
            }
            if (CategoryIds.Any())
            {
                for (int i = 0; i < CategoryIds.Count(); i++)
                {
                    result.Add(string.Format("CategoryIds[{0}]", i), CategoryIds[i]);
                }
            }
            if (!string.IsNullOrEmpty(OrderNumber))
            {
                result.Add("OrderNumber", OrderNumber);
            }
            return result;
        }
    }
}