﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    public class GridHeaderModel
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public bool IsSortable { get; set; }
        public bool IsSortedByThis { get; set; }
        public string CurrentOrder { get; set; }
        public int? Width { get; set; }

        public GridHeaderModel()
        {

        }

        public GridHeaderModel(string title, string name = null, SortSpecification currentSortInfo = null, bool isSortable = true, int? width = null)
        {
            this.Title = title;
            this.IsSortable = isSortable;
            this.Name = name;
            this.Width = width;
            if (currentSortInfo != null)
            {
                if (currentSortInfo.SortBy.Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    IsSortedByThis = true;
                    CurrentOrder = currentSortInfo.Sort;
                }
            }
        }
    }
}