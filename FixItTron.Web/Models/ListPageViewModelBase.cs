﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    public class ListPageViewModelBase
    {
        public CurrentPageInformation CurrentPageInformation { get; set; }

        public DataPagingInformation Pager { get; set; }

        public SortSpecification SortSpecification { get; set; }

        public Func<SortSpecification, string> SortLink { get; set; }
    }
}