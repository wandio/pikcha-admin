﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models.Validations
{
    public class ConfirmResetPasswordViewModelValidator : AbstractValidator<ConfirmResetPasswordViewModel>
    {
        public ConfirmResetPasswordViewModelValidator()
        {
            RuleFor(model => model.Email).NotEmpty();//.EmailAddress();
        }
    }
}