﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models.Validations
{
    public class EditBranchViewModelValidator : AbstractValidator<EditBranchViewModel>
    {
        public EditBranchViewModelValidator()
        {
            //RuleFor(model => model.BranchName).NotEmpty();
            RuleFor(model => model.BranchId).NotEmpty();
        }
    }
}