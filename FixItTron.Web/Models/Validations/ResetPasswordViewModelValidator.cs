﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models.Validations
{
    public class ResetPasswordViewModelValidator : AbstractValidator<ResetPasswordViewModel>
    {
        public ResetPasswordViewModelValidator()
        {
            RuleFor(model => model.Key).NotEmpty();
            RuleFor(model => model.NewPassword).NotEmpty();
            RuleFor(model => model.ConfirmNewPassword).NotEmpty().Equal(x => x.NewPassword).WithMessage(ApplicationStrings.PasswordsDoesNotMatch);
        }
    }
}