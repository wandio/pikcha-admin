﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models.Validations
{
    public class FirstAuthorizationViewModelValidator : AbstractValidator<FirstAuthorizationViewModel>
    {
        public FirstAuthorizationViewModelValidator()
        {
            RuleFor(model => model.Description).NotEmpty();
            RuleFor(model => model.UserName).NotEmpty();
            RuleFor(model => model.NewPassword).NotEmpty();
            RuleFor(model => model.ConfirmNewPassword).NotEmpty().Equal(x => x.NewPassword).WithMessage(ApplicationStrings.PasswordsDoesNotMatch);
        }
    }
}