﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Web.Models
{
    public class UnboundedPeriod
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public UnboundedPeriod(DateTime? start = null, DateTime? end = null)
        {
            StartDate = start;
            EndDate = end;
        }

        public UnboundedPeriod()
        {

        }
    }
}
