﻿using FixItTron.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Models
{
    public class HomeViewModel
    {
        public TermsOfContractModel TermsOfContract { get; set; }
        public OrdersSummaryModel OrdersSummary { get; set; }

        public int? Year { get; set; }
        public IEnumerable<SelectListItem> YearsSelectList { get; set; }
        public IEnumerable<SelectListItem> MonthsSelectList { get; set; }
        public UnboundedYearMonth ChartYearMonth { get; set; }
        public UnboundedYearMonth WidgetYearMonth { get; set; }

        public HomeViewModel()
        {
            ChartYearMonth = new UnboundedYearMonth();
            WidgetYearMonth = new UnboundedYearMonth();
        }
    }
}