﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    public class UnboundedRange<T> where T : struct
    {
        public Nullable<T> From { get; set; }
        public Nullable<T> To { get; set; }
    }
}