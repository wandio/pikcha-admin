﻿using FixItTron.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Models
{
    public class StatisticsViewModel
    {
        public StatisticsChartType Type { get; set; }
        public IEnumerable<StatisticsDataModel> Items { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public string JsonObject { get; set; }

        public int? Year { get; set; }
        public IEnumerable<SelectListItem> YearsSelectList { get; set; }
        public IEnumerable<SelectListItem> MonthsSelectList { get; set; }
        public UnboundedYearMonth YearMonth { get; set; }
        public int? BranchId { get; set; }
        public IEnumerable<SelectListItem> BranchsSelectList { get; set; }

        public IEnumerable<int> CategoryIds { get; set; }
        public IEnumerable<SelectListItem> CategoriesSelectList { get; set; }

        public StatisticsViewModel()
        {
            Items = new List<StatisticsDataModel>();
            YearMonth = new UnboundedYearMonth();
            CategoryIds = new List<int>();
        }
    }

    public class UnboundedYearMonth
    {
        public int? YearStart { get; set; }
        public int? YearEnd { get; set; }
        public int? MonthStart { get; set; }
        public int? MonthEnd { get; set; }

        public DateTime? GetStartDate()
        {
            if (YearStart.HasValue)
            {
                if (MonthStart.HasValue)
                {
                    return new DateTime(YearStart.Value, MonthStart.Value, 1);
                }
                return new DateTime(YearStart.Value, 1, 1);
            }
            return null;
        }

        public DateTime? GetEndDate()
        {
            if (YearEnd.HasValue)
            {
                if (MonthEnd.HasValue)
                {
                    var firstDay = new DateTime(YearEnd.Value, MonthEnd.Value, 1);
                    return firstDay.AddMonths(1).AddDays(-1);
                }
                return new DateTime(YearEnd.Value, 12, 31);
            }
            return null;
        }
    }
}