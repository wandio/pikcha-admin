﻿using FixItTron.Web.Models.Validations;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    [Validator(typeof(LoginViewModelValidator))]
    public class LoginViewModel
    {
        [Display(Name = "UserName", ResourceType = typeof(ApplicationStrings))]
        public string UserName { get; set; }
        [Display(Name = "Password", ResourceType = typeof(ApplicationStrings))]
        public string Password { get; set; }
        [Display(Name = "Remember", ResourceType = typeof(ApplicationStrings))]
        public bool RememberMe { get; set; }
    }
}