﻿using FixItTron.Web.Models.Validations;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    [Validator(typeof(ResetPasswordViewModelValidator))]
    public class ResetPasswordViewModel
    {
        public string Key { get; set; }
        public string Email { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmNewPassword { get; set; }
    }
}