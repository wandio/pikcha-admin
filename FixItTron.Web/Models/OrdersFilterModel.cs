﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FixItTron.Web.Models
{
    public class OrdersFilterModel
    {
        public string OrderNumber { get; set; }

        public List<int> OrderTypeIds { get; set; }
        public IEnumerable<SelectListItem> OrderTypesSelectList { get; set; }

        public UnboundedPeriod RegistrationDate { get; set; }
        public UnboundedPeriod PerformanceDate { get; set; }
        public UnboundedDecimalRange Amount { get; set; }
        public UnboundedDecimalRange ServiceAmount { get; set; }
        public UnboundedDecimalRange MaterialAmount { get; set; }
        public UnboundedDecimalRange OtherAmount { get; set; }

        public List<string> Address { get; set; }
        public IEnumerable<SelectListItem> AddressesSelectList { get; set; }

        public List<int> CategoryIds { get; set; }
        public IEnumerable<SelectListItem> CategoriesSelectList { get; set; }

        public List<int> StatusIds { get; set; }
        public IEnumerable<SelectListItem> StatusesSelectList { get; set; }

        public bool? DefaultFilter { get; set; }

        public OrdersFilterModel()
        {
            Address = new List<string>();
            CategoryIds = new List<int>();
            OrderTypeIds = new List<int>();
            StatusIds = new List<int>();
            RegistrationDate = new UnboundedPeriod();
            PerformanceDate = new UnboundedPeriod();
            Amount = new UnboundedDecimalRange();
            ServiceAmount = new UnboundedDecimalRange();
            MaterialAmount = new UnboundedDecimalRange();
            OtherAmount = new UnboundedDecimalRange();
        }

        public RouteValueDictionary GetRouteValues()
        {
            var result = new RouteValueDictionary();

            if (DefaultFilter.HasValue && DefaultFilter.Value == true)
            {
                result.Add("DefaultFilter", DefaultFilter);
            }
            if (!string.IsNullOrEmpty(OrderNumber))
            {
                result.Add("OrderNumber", OrderNumber);
            }
            if (OrderTypeIds.Any())
            {
                for (int i = 0; i < OrderTypeIds.Count(); i++)
                {
                    result.Add(string.Format("OrderTypeIds[{0}]", i), OrderTypeIds[i]);
                }
            }
            if (RegistrationDate != null)
            {
                if (RegistrationDate.StartDate.HasValue)
                {
                    result.Add("RegistrationDate.StartDate", RegistrationDate.StartDate.Value.ToShortDateString());
                }
                if (RegistrationDate.EndDate.HasValue)
                {
                    result.Add("RegistrationDate.EndDate", RegistrationDate.EndDate.Value.ToShortDateString());
                }
            }
            if (PerformanceDate != null)
            {
                if (PerformanceDate.StartDate.HasValue)
                {
                    result.Add("PerformanceDate.StartDate", PerformanceDate.StartDate.Value.ToShortDateString());
                }
                if (PerformanceDate.EndDate.HasValue)
                {
                    result.Add("PerformanceDate.EndDate", PerformanceDate.EndDate.Value.ToShortDateString());
                }
            }
            if (Amount != null)
            {
                if (Amount.From.HasValue)
                {
                    result.Add("Amount.From", Amount.From);
                }
                if (Amount.To.HasValue)
                {
                    result.Add("Amount.To", Amount.To);
                }
            }
            if (ServiceAmount != null)
            {
                if (ServiceAmount.From.HasValue)
                {
                    result.Add("ServiceAmount.From", ServiceAmount.From);
                }
                if (Amount.To.HasValue)
                {
                    result.Add("ServiceAmount.To", ServiceAmount.To);
                }
            }
            if (MaterialAmount != null)
            {
                if (MaterialAmount.From.HasValue)
                {
                    result.Add("MaterialAmount.From", MaterialAmount.From);
                }
                if (Amount.To.HasValue)
                {
                    result.Add("MaterialAmount.To", MaterialAmount.To);
                }
            }
            if (OtherAmount != null)
            {
                if (OtherAmount.From.HasValue)
                {
                    result.Add("OtherAmount.From", OtherAmount.From);
                }
                if (Amount.To.HasValue)
                {
                    result.Add("OtherAmount.To", OtherAmount.To);
                }
            }
            if (Address.Any())
            {
                for (int i = 0; i < Address.Count(); i++)
                {
                    result.Add(string.Format("Address[{0}]", i), Address[i]);
                }
            }
            if (CategoryIds.Any())
            {
                for (int i = 0; i < CategoryIds.Count(); i++)
                {
                    result.Add(string.Format("CategoryIds[{0}]", i), CategoryIds[i]);
                }
            }
            if (StatusIds.Any())
            {
                for (int i = 0; i < StatusIds.Count(); i++)
                {
                    result.Add(string.Format("StatusIds[{0}]", i), StatusIds[i]);
                }
            }

            return result;
        }
    }
}