﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    public enum StatisticsChartType
    {
        MaterialsByFilials,
        MaterialsByCategories,
        MaterialsByDescriptions,
        MaterialsCountByDescriptions,
        ServiceCategoryCountByFilials,
        ServiceDescriptionCountByFilials,
        ServiceDescriptionCount,
        ServiceCountByCategory,
        FinancesMaterials,
        FinancesSumAccrual
    }
}