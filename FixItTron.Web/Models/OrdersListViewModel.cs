﻿using FixItTron.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    public class OrdersListViewModel : ListPageViewModelBase
    {
        public IEnumerable<OrderTableRowModel> Orders { get; set; }
        public IEnumerable<GridHeaderModel> Headers { get; set; }
        public OrdersFilterModel Filter { get; set; }

        public OrdersListViewModel()
        {
            Orders = new List<OrderTableRowModel>();
        }
    }
}