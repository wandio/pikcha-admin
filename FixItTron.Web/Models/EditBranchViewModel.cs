﻿using FixItTron.Web.Models.Validations;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Models
{
    [Validator(typeof(EditBranchViewModelValidator))]
    public class EditBranchViewModel
    {
        public int BranchId { get; set; }
        public string BranchName { get; set; }
        public string OriginalBranchName { get; set; }
        public string Address { get; set; }
    }
}