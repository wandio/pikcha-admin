﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Models
{
    public class MenuItem
    {
        public string Title { get; set; }
        public ActionResult Action { get; set; }
        public string CssClass { get; set; }
        public bool Selected { get; set; }

        public override string ToString()
        {
            return Title;
        }
    }
}