﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Domain.Model
{
    public class UploadedFile
    {
        public byte[] Content { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }

        public bool HasFile()
        {
            return Content != null && Content.Any();
        }
    }
}
