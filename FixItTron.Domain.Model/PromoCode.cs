﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class PromoCode
    {
        public int PromoCodeId { get; set; }
        public string Code { get; set; }
        public int PrintCount { get; set; }
        public string Description { get; set; }
        public bool MultiUse { get; set; }
        public DateTime CreateDate { get; set; }
        public int? CreateAdminUserId { get; set; }
        public virtual AdminUser CreateAdminUser { get; set; }
        public int? EventId { get; set; }
        public virtual Event Event { get; set; }
    }
}
