﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class UserPromoCode
    {
        public int UserPromoCodeId { get; set; }
        public int? UserId { get; set; }
        public virtual User User { get; set; }
        public int?  PromoCodeId { get; set; }
        public virtual PromoCode PromoCode { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
