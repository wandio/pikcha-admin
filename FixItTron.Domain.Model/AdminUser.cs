﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class AdminUser : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {
        //public virtual Employee Employee { get; set; }
        public string FullName { get; set; }
        //public string Email { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? LastChangeDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsPowerUser { get; set; }
        public string ImageFileName { get; set; }

        //public async Task<ClaimsIdentity> GenerateUserIdentityAsync(Microsoft.AspNet.Identity.UserManager<AdminUser, int> manager)
        //{
        //    // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
        //    var userIdentity = await manager.CreateIdentityAsync(this, Microsoft.AspNet.Identity.DefaultAuthenticationTypes.ApplicationCookie);
        //    // Add custom user claims here
        //    return userIdentity;
        //}
    }
}