﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class User
    {
        public int UserId { get; set; }
        /// <summary>
        /// In case when user type is "Device" device id is stored in this field
        /// In case when user type is "Facebook" facebook id is stored in this field
        /// </summary>
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int AvailablePrints { get; set; }
        public int UsedPrints { get; set; }
        public DateTime? CreateDate { get; set; }
        public virtual ICollection<Photo> Photos { get; set; }
        public UserType UserType { get; set; }
    }
}
