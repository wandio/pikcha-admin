﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class Event
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        //public int? LogoId { get; set; }
        //public virtual Document Logo { get; set; }
        public string LogoFileName { get; set; }
        public string MobileLogoFileName { get; set; }
        public string InstagramLogoFileName { get; set; }
        public string FacebookLogoFileName { get; set; }
        public string BackgroundLogoFileName { get; set; }
        public DateTime CreateDate { get; set; }
        public int? CreateAdminUserId { get; set; }
        public virtual AdminUser CreateAdminUser { get; set; }
        public DateTime? LastChangeDate { get; set; }
        public int? ChangeAdminUserId { get; set; }
        public virtual AdminUser ChangeAdminUser { get; set; }
        public bool IsActive { get; set; }
        public int? CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public string BackgroundColor { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? PrintCount { get; set; }
        public bool ShowLogoOnShare { get; set; }
        public bool ShowLogoOnPreview { get; set; }
        public bool ShowBackgroundLogo { get; set; }

        public bool ShowSurvey { get; set; }
        public string FacebookAppSecret { get; set; }
        public string FacebookPageId { get; set; }
        public string FacebookPageAccessToken { get; set; }

        public string EventCode { get; set; }
    }
}
