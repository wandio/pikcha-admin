﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class Survey
    {
        public int SurveyId { get; set; }
        public User User { get; set; }
        public int UserId { get; set; }
        public DateTime CreateDate { get; set; }
        public SourceType SourceType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Gender? Gender { get; set; }
        public int EventId { get; set; }
        public Event Event { get; set; }

        public SurveyQuestion1Answer Question1 { get; set; }
        public SurveyQuestion2Answer Question2 { get; set; }
    }
}
