﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class Configuration
    {
        public int ConfigurationId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public DateTime LastChangeDate { get; set; }
    }
}
