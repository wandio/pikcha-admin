﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class Connection
    {
        public int ConnectionId { get; set; }
        public bool Connected { get; set; }
        public int EmployeeId { get; set; }
        public string PresentationConnectionId { get; set; }
    }
}
