﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class Role : IdentityRole<int, UserRole>
    {
        public virtual ICollection<RoleClaim> Permissions { get; set; }
    }
}
