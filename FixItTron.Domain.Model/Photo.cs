﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class Photo
    {
        public int PhotoId { get; set; }
        public int? CreateUserId { get; set; }
        public virtual User CreateUser { get; set; }
        public DateTime? CreateDate { get; set; }
        public string FileName { get; set; }
        public byte[] Data { get; set; }
        public SourceType SourceType { get; set; }
        public bool IsPrinted { get; set; }
    }
}
