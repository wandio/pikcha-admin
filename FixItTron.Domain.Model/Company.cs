﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    public class Company
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public int? CreateAdminUserId { get; set; }
        public virtual AdminUser CreateAdminUser { get; set; }
        public DateTime? LastChangeDate { get; set; }
        public int? ChangeAdminUserId { get; set; }
        public virtual AdminUser ChangeAdminUser { get; set; }
    }
}
