﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Model
{
    /// <summary>
    /// Class for storing document attachments to all the business objects. Classifier and ClassifierId will be used for searching in documents separately
    /// </summary>
    public class Document
    {
        public int DocumentId { get; set; }
        /// <summary>
        /// used for searching documents
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// to which item this file belongs - may be contractor, order, warehouse, etc
        /// </summary>
        public string Classifier { get; set; }
        /// <summary>
        /// to search, basically code of classifier
        /// </summary>
        public string ClassifierId { get; set; }
        public string Description { get; set; }
        public UploadedFile File { get; set; }
    }
}
