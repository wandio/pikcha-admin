﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Orders
{
    public class ChangeOrderMaterialsRequest
    {
        public int OrderId { get; set; }
        public int ScheduleId { get; set; }
        public ProductModel[] Lines { get; set; }
    }
    public class ChangeOrderMaterialsResponse
    {
        public decimal TotalDiscountAmount { get; set; }
    }
}