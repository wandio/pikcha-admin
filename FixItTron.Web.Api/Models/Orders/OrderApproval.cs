﻿using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Orders
{
    public class OrderApprovalRequest
    {
        public int ScheduleId { get; set; }
        public int? RejectReasonId { get; set; }
        public bool IsApproved { get; set; }
    }
    public class OrderApprovalResponse : BaseResponse<bool>
    {
    }
}