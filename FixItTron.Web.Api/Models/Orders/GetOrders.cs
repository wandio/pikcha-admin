﻿using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Orders
{
    public class GetOrdersRequest
    {
        public int EmployeeId { get; set; }
    }
    public class GetOrdersResponse: BaseResponse<List<SalesOrderItem>>
    {
    }
}