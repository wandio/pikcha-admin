﻿using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Orders
{
    public class GetMaterialsResponse:BaseResponse<List<MaterialItem>>
    {
    }
}