﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Orders
{
    public class ChangeScheduleStatusRequest
    {
        public int ScheduleId { get; set; }
        public int OrderScheduleStatus { get; set; }
        public int? OrderScheduleDoneStatus { get; set; }
        public bool? EmployeeArrived { get; set; }

        public string CancelComment { get; set; }
        public int? CancelReasonId { get; set; }

        public Dictionary<int, decimal> DoneServices { get; set; }
    }
    public class ChangeScheduleStatusResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}