﻿using Common.Enums;
using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Api.Models.Orders
{
    public class CreatePaymentRequest
    {
        public decimal? Amount { get; set; }
        public Common.Enums.PaymentMethod Method { get; set; }
        public DateTime? PayDate { get; set; }
        public string Notes { get; set; }
        public Guid ExternalId { get; set; }
        public int ResponsibleId { get; set; }
        public int? OrderId { get; set; }
    }

    public class CreatePaymentResponse : BaseResponse<bool>
    {
        public Guid ExternalId { get; set; }
        public PaymentStatus Status { get; set; }
    }
}
