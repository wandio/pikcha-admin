﻿using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Orders
{
    public class GetOrderRequest
    {
        public int OrderId { get; set; }
        public int ScheduleId { get; set; }
    }
    public class GetOrderResponse : BaseResponse<SalesOrderItem>
    {
    }
}