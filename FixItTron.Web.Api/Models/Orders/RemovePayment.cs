﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Orders
{
    public class RemovePaymentRequest
    {
        public Guid PaymentId { get; set; }
    }
    public class RemovePaymentResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}