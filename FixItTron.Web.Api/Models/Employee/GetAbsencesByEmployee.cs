﻿using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Employee
{
    public class GetAbsencesByEmployeeRequest
    {
        public int EmployeeID { get; set; }
    }

    public class GetAbsencesByEmployeeResponse: BaseResponse<IEnumerable<GetAbsencesByEmployeeResponseItem>>
    {
    }
}