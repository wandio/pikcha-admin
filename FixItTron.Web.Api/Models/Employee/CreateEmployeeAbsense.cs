﻿using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Employee
{
    public class CreateOrUpdateAbsenceRequest
    {
        public CreateOrUpdateAbsenceItem EmployeeAbsence { get; set; }
    }

    public class CreateOrUpdateAbsenseResponse : BaseResponse<Guid>
    {
    }
}