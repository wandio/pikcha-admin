﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Employee
{
    public class RegisterEmployeeDeviceRequest
    {
        public int EmployeeId { get; set; }
        public string RegistrationToken { get; set; }
    }
}