﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Shared
{
    public class Reason
    {
        public int ReasonId { get; set; }
        public string Name { get; set; }
        public int Type { get; set; }
    }
}