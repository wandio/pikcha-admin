﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Shared
{
    public class GetAbsencesByEmployeeResponseItem
    {
        public int EmployeeAbsenseId { get; set; }

        public int AbsenseTypeId { get; set; }

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public string Comment { get; set; }

        public Guid ExternalId { get; set; }

        public AbsenseStatus Status { get; set; }
    }
}