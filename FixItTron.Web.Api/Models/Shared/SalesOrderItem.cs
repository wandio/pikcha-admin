﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Shared
{
    public class SalesOrderItem
    {
        public int OrderId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime OrderDate { get; set; }
        public int PaymentTerm { get; set; }
        public int SalesType { get; set; }
        public int SalesOrderStatus { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal? TotalDiscountAmount { get; set; }
        public int? ConnectedSalesOrderId { get; set; }
        public bool IsPriority { get; set; }

        public string ContactPersonName { get; set; }
        public string ContactPersonNumber { get; set; }
        public string ClientName { get; set; }
        public string ClientCategory { get; set; }
        public string ClientAddress { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }

        public bool NeedsApproval { get; set; }

        public string CancelComment { get; set; }
        public IEnumerable<ScheduleItem> SalesOrderSchedules { get; set; }
        public IEnumerable<OrderServiceItem> Services { get; set; }
        public IEnumerable<OrderMaterialItem> Materials { get; set; }
    }
}