﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models
{
    public class ProductModel
    {
        public int ProductId { get; set; }
        public decimal Quantity { get; set; }
        public int? ParentLineId { get; set; }
        public int UnitOfMeasureId { get; set; }
    }
}