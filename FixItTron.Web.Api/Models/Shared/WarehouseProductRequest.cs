﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Shared
{
    public class WarehouseProductRequest
    {
        public int ProductId { get; set; }
        public decimal Quantity { get; set; }
    }
}