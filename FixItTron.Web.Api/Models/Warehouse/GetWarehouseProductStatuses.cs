﻿
using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Warehouse
{
    public class GetWarehouseRequestsStatusesRequest
    {
        public IEnumerable<Guid> WarehouseProductsRequestIds { get; set; }
    }
    public class GetWarehouseRequestsStatusesResponse
    {

    }
}