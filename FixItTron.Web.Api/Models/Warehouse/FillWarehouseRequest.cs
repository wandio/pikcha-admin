﻿using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Warehouse
{
    public class FillWarehouseRequest
    {
        public Guid WarehouseRequestId { get; set; }
        public int BoardId { get; set; }
        public virtual ICollection<WarehouseProductRequest> Products { get; set; }

    }
    public class FillWarehouseResponse : BaseResponse<bool>
    {
    }
}