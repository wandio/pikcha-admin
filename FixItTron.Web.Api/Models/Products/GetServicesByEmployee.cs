﻿using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.Models.Products
{
    public class GetServicesByEmployeeRequest 
    {
        public int EmployeeId { get; set; }
    }
    public class GetServicesByEmployeeResponse : BaseResponse<IEnumerable<ServiceItem>>
    {
    }
}