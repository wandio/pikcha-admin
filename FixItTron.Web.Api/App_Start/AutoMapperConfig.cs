﻿
using FixItTron.Web.Api.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Api.App_Start
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            AutoMapper.Mapper.CreateMap<Services.Models.Shared.SalesOrderItem, SalesOrderItem>();
            AutoMapper.Mapper.CreateMap<Services.Models.Shared.ScheduleItem, ScheduleItem>();
            AutoMapper.Mapper.CreateMap<Services.Models.Shared.BoardMaterialItem, BoardMaterialItem>();
            AutoMapper.Mapper.CreateMap<Services.Models.Shared.ServiceItem, ServiceItem>();
            AutoMapper.Mapper.CreateMap<Services.Models.Shared.OrderServiceItem, OrderServiceItem>();
            AutoMapper.Mapper.CreateMap<Services.Models.Shared.OrderMaterialItem, OrderMaterialItem>();
            AutoMapper.Mapper.CreateMap<Services.Models.Shared.AdditionalStatus, AdditionalStatus>();
            AutoMapper.Mapper.CreateMap<Services.Models.Shared.ScheduleServiceItem, ScheduleServiceItem>();

            AutoMapper.Mapper.CreateMap<FixItTron.Web.Api.Models.Shared.WarehouseProductRequest,
                Services.Models.Shared.WarehouseProductRequest>();
            AutoMapper.Mapper.CreateMap<FixItTron.Web.Api.Models.ProductModel,
               Services.Models.Shared.ProductModel>(); 
            AutoMapper.Mapper.CreateMap<FixItTron.Web.Api.Models.Employee.CreateOrUpdateAbsenceRequest,
              Services.Models.Lookup.AbsenseEmployee.CreateOrUpdateAbsenceRequest>();

            AutoMapper.Mapper.CreateMap<FixItTron.Web.Api.Models.Shared.CreateOrUpdateAbsenceItem ,
              Services.Models.Lookup.AbsenseEmployee.CreateOrUpdateAbsenceItem>();

            AutoMapper.Mapper.CreateMap<Services.Models.Lookup.AbsenseEmployee.GetAbsencesByEmployeeResponseItem,
                                                                        GetAbsencesByEmployeeResponseItem>();
            AutoMapper.Mapper.CreateMap<Services.Models.Shared.LookupModel,
                            LookUpModel>();
            
        }
    }
}