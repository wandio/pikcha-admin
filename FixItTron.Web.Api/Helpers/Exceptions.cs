﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Wandio.Domain.Services.Exceptions;

namespace FixItTron.Web.Api.Helpers
{
    public static class Exceptions
    {
        public static string HandleServiceOperationRulesException(ServiceOperationRulesException ex)
        {
            try
            {
                return String.Join(";", ex.GetDetails(true));
            }
            catch
            {
               return ex.Message;
            }
            
        }
    }
}