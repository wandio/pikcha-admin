﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace FixItTron.Web.Api.Helpers
{
    #region Error DelegationHandler

    //public class ErrorHandler : DelegatingHandler
    //{
    //    protected async override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request,
    //                                                 CancellationToken cancellationToken)
    //    {
    //        try
    //        {
    //            return await base.SendAsync(request, cancellationToken);
    //        }
    //        catch (Exception ex)
    //        {
    //            var responseMessage =request.CreateResponse(HttpStatusCode.InternalServerError, ex);

    //            return Task<HttpResponseMessage>.Factory.StartNew(() =>
    //                   responseMessage).Result;
    //        }
    //    }
    //} 
    #endregion
        
    public class ErrorHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            base.Handle(context);

            context.Result = new TextPlainErrorResult
            {
                Request = context.ExceptionContext.Request,
                Content = "Oops! Something went wrong."
            };
        }
        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return base.ShouldHandle(context);
        }

        private class TextPlainErrorResult : IHttpActionResult
        {
            public string Content { get; set; }
            public HttpRequestMessage Request { get; internal set; }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                HttpResponseMessage response =
                                 new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(Content);
                response.RequestMessage = Request;
                return Task.FromResult(response);
            }
        }
    }
}