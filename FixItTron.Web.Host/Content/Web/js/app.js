﻿var HandleFiles = function (files) {
    for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var imageType = /image.*/;

        if (!file.type.match(imageType)) {
            continue;
        }

        preview = $('.cropper');
        var img = document.createElement("img");
        img.classList.add("obj");
        img.file = file;
        preview.append(img);

        var reader = new FileReader();
        reader.onload = (function (aImg) { return function (e) { aImg.src = e.target.result; }; })(img);
        reader.readAsDataURL(file);
    }
}



var InitializePikcha = function (options) {
    $.blockUI.defaults.message = '<div class="loading-message"><div class="block-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></div>';
    $.blockUI.defaults.css = {};
    $.blockUI.defaults.baseZ = 2000;
    $.blockUI.defaults.overlayCSS.opacity = 0.2;
    var $rotate = $('#rotate');
    var $qrbottom = $('#generated-qr-bottom');
    var $qrcont = $('#generated-qr-cont');
    var $qr = $('#generated-qr');
    var $container = $('.container');
    var $camera = $('#camera');
    var $gallery = $('#gallery');
    var $header = $('#header');
    var $photo = $('#photo');
    var $croppicContainer = $('.croppic-cont');
    $cropContainerHeaderButtonCamera = $('.picture-select.camera');
    $cropContainerHeaderButtonGallery = $('.picture-select.gallery');
    var windowHeight = $(window).height();
    $rotate.hide();
    $container.height(windowHeight);
    var itemHeight = 400;
    if ($(window).height() > 900) {
        itemHeight = (windowHeight - $header.outerHeight()) / 2;
    }
    $($camera).css('line-height', itemHeight + 'px').outerHeight(itemHeight);
    $($gallery).css('line-height', itemHeight + 'px').outerHeight(itemHeight);
    $('.bg-bottom').height((windowHeight - $header.height()) / 2);
    $('#generated-qr-cont').height(windowHeight - $header.height() - 5);
    if (screen.width < 768) {
        $('.croppic-inner-cont').height(itemHeight * 1.5);
        //$('.dark-logo').css("top", $('.croppic-inner-cont').height() / 3);

        var targetWidth = $(window).width() * 0.6;

        $('#generated-qr').width(targetWidth);
        $('#generated-qr').height($(window).width() * 0.6);

        $('#actions a').width($('#generated-qr').outerWidth());
    }
    else {
        $('.croppic-inner-cont').height(itemHeight * 1.6);
    }
    if ($(window).height() < 700 && screen.width > 768) {
        $('#generated-qr').width(200);
        $('#generated-qr').height(200);
        $('.croppic-inner-cont').height(itemHeight * 1.6);
    }

    if (screen.height <= 480) {
        $('#generated-qr').css("top", "-210px");
        $('.take-another').css('bottom', '10px');
        $('.share-instagram').css('bottom', '110px');
        $('.share-facebook').css('bottom', '200px');
    }

    $camera.on('click', function (event) {
        event.preventDefault();
        $cropContainerHeaderButtonCamera.click();
    });
    $gallery.on('click', function (event) {
        event.preventDefault();
        $cropContainerHeaderButtonGallery.click();
    });
    $('.bg-bottom').click(function (event) {
    });
    $('.picture-select').change(function () {
        $gallery.hide();
        $camera.hide();
        $.blockUI();
        HandleFiles(this.files);
        var $image = $('.cropper img');
        $('.cropper img').load(function () {
            mainCropper = $('.cropper img').cropper({
                aspectRatio: 1 / 1,
                strict: true,
                background: false,
                highlight: false,
                guides: false,
                dragCrop: false,
                movable: false,
                resizable: false,
                responsive: false,
                autoCropArea: 1
            });
            ga('send', 'event', { eventCategory: 'In Preview Page', eventAction: 'click' });
        });
        $croppicContainer.show();
        $rotate.show();
        $.unblockUI();
    });
    $rotate.click(function (event) {
        event.preventDefault();
        if (!(typeof mainCropper === 'undefined')) {
            mainCropper.cropper("rotate", 90);
        }
    });
    $('.take-another').click(function () {
        window.location.reload();
    });
    $(document).on('click', '#privacy-agree', function () {
        if ($('#privacy-policy-agreed').is(':checked')) {
            document.cookie = "pikcha_privacy_policy_hide=true; expires=Fri, 31 Dec 9999 23:59:59 GMT";
        }
        $('#js-privacy-modal').remove();
        $('#crop').click();
    });
    $(document).on('click', '#crop', function () {
        var $privacyModal = $('#js-privacy-modal');
        if ($privacyModal.length) {
            $privacyModal.modal();
        }
        else {
            $.blockUI({ message: '<div class="loading-message"><div class="block-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div></br></br><span id="loading-text" style="font-size: 3em; color: white;">Image is uploading</br>This may take a moment</span></div>' });
            $rotate.hide();
            $croppicContainer.fadeOut('fast', function () {
                $('.cropper img').cropper("getCroppedCanvas");
            });
        }
    });




    var isMobile = false; //initiate as false
    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yabs\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;


    if (isMobile) {
        $('#menu').removeClass('hidden');
        $('.menu-toggle').removeClass('hidden');
        var menuWidth = $(window).width() * 8 / 10;

        var menuLeft = document.getElementById('menu');
        var showLeft = document.getElementById('showLeft');
        var close = document.getElementById('close-menu');
        showLeft.onclick = function () {
            classie.toggle(this, 'active');
            classie.toggle(menuLeft, 'cbp-spmenu-open');
            refreshBalance(options.userInfoUrl);
        };

        close.onclick = function () {
            classie.removeClass(menuLeft, 'cbp-spmenu-open');
        }

        $('#js-promo-code').submit(function () {
            event.preventDefault();
            $.blockUI();

            $.post(options.enterPromoUrl, $(this).serialize(), function (result) {
                $.unblockUI();
                if (result.success) {
                    refreshBalance(options.userInfoUrl);
                    alert('Hooray! Credits were added to your balance');
                }
                else {
                    if (!result.promoExists) {
                        alert("Sorry :( We couldn't find promotion code you entered");
                    }
                    else if (result.promoAlreadyUsed) {
                        alert("Sorry :( That promotion code is already used");
                    }
                }
            })
        });
        var drawer = getParameterByName('drawer');
        if (drawer == "1") {
            showLeft.click();
        }
    }

};

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function refreshBalance(userInfoUrl) {
    $.getJSON(userInfoUrl, function (data) {
        var userBalance = $('#js-user-balance').text('');
        var leftPrints = data.availablePrints - data.usedPrints;
        var available = data.availablePrints;

        userBalance.text(leftPrints + '/' + available);
    });
}