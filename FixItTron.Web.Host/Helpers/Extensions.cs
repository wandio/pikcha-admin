﻿using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Shared;
using FixItTron.Web.Host.Models.Infrastructure;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Wandio.Web.Mvc.Html;
using FluentValidation.Mvc;
using Wandio.Web.Mvc;
using Wandio.Domain.Services.Exceptions;
using System.Web;
using FixItTron.Web.Host.Models.Documents;
using AutoMapper;
using Common.Enums;
using FixItTron.Web.Host.Models;
using FixItTron.Services.Models.Document;
using Microsoft.Web;
using System.Web.Helpers;
using System.IO;
using FixItTron.Web.Host.Models.Shared;
using FixItTron.Services.Models.Lookup.Countries;
using System.Linq.Expressions;

namespace FixItTron.Web.Host.Helpers
{
    public static class Extensions
    {
        const string DeviceIdCookieName = "pkcd_vid";
        const string PrivacyPolicyHideCookieName = "pikcha_privacy_policy_hide";

        public static ResourceManager ResourceManger = new ResourceManager(typeof(ApplicationStrings));

        #region pikcha
        public static IEnumerable<SelectListItem> EventsSelectList(this ILookupService service)
        {
            return service.GetEvents().Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> CompaniesSelectList(this ILookupService service)
        {
            return service.GetCompanies().Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Id.ToString()
            }).PrependNull();
        }
        #endregion

        public static bool HasRight(this IPrincipal user, Securable securable, params Permissions[] permissions)
        {
            //if no permissions requested, return true
            if (securable == Securable.None || permissions.Length == 0)
            {
                return true;
            }

            var principal = user as ClaimsPrincipal;
            if (!principal.Identity.IsAuthenticated)
            {
                return false;
            }

            //var securableNames = Enum.GetNames(typeof(Securable));

            //var claims = principal.Claims.Where(c => securableNames.Contains(c.Type)).Select(x => new
            //{
            //    Type = (Securable)Enum.Parse(typeof(FixItTron.Services.Models.Shared.Securable), x.Type, true),
            //    Value = (Permissions)Enum.Parse(typeof(Permissions), x.Value, true)
            //});

            var securableNames = Enum.GetValues(typeof(Securable));
            List<int> types = new List<int>();
            foreach (var type in securableNames)
            {
                types.Add(Convert.ToInt32(type));
            }
            types.Remove(types.First());
            int smth;
            var claims = principal.Claims.Where(c => int.TryParse(c.Type, out smth) && types.Contains(Convert.ToInt32(c.Type))).Select(x => new
            {
                Type = (Securable)Enum.Parse(typeof(FixItTron.Services.Models.Shared.Securable), x.Type, true),
                Value = (Permissions)Enum.Parse(typeof(Permissions), x.Value, true)
            });

            //if (!claims.Any(c => c.Type == securable && permissions.Any(p => p.HasFlag(c.Value))))
            if (!claims.Any(c => c.Type == securable && permissions.Any(p => c.Value.HasFlag(p))))
            {
                return false;
            }

            return true;
        }

        public static IEnumerable<SelectListItem> EnumToSelectList<T>(this Controller controller, Func<T, bool> predicate = null) where T : struct, IComparable, IConvertible
        {
            var values = Enum.GetValues(typeof(T)).Cast<T>();

            if (predicate != null)
            {
                values = values.Where(predicate);
            }

            return values.Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()),
                Value = x.ToString()
            });
        }

        public static IEnumerable<SelectListItem> LegalFormsSelectList(this Controller controller)
        {
            return Enum.GetValues(typeof(LegalForm)).Cast<LegalForm>().Where(x => x != LegalForm.None).Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()),
                Value = x.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> NotificationSendStatusesSelectList(this Controller controller)
        {
            return Enum.GetValues(typeof(NotificationSendStatus)).Cast<NotificationSendStatus>().Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()),
                Value = x.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> YesNoSelectList(this Controller controller, string message = "")
        {
            var selectList = new List<SelectListItem>() { 
                new SelectListItem() { Text = ApplicationStrings.Yes, Value = bool.TrueString }, 
                new SelectListItem() { Text = ApplicationStrings.No, Value = bool.FalseString } 
            };

            if (!string.IsNullOrEmpty(message))
            {
                return selectList.PrependNull(message);
            }

            return selectList.PrependNull();
        }

        public static IEnumerable<SelectListItem> GendersSelectList(this Controller controller)
        {
            return Enum.GetValues(typeof(GenderModel)).Cast<GenderModel>().Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()),
                Value = x.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> ContractorStatusesSelectList(this Controller controller)
        {
            return Enum.GetValues(typeof(ContractorStatus)).Cast<ContractorStatus>().Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()),
                Value = x.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> QuestionnaireTypesSelectList(this Controller controller)
        {
            return Enum.GetValues(typeof(QuestionerTemplateType)).Cast<QuestionerTemplateType>().Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()),
                Value = x.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> AbsenseStatusesSelectList(this Controller controller)
        {
            return Enum.GetValues(typeof(AbsenseStatus)).Cast<AbsenseStatus>().Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()),
                Value = x.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> UserTypesSelectList(this Controller controller)
        {
            return Enum.GetValues(typeof(UserType)).Cast<UserType>().Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()),
                Value = x.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> SourceTypesSelectList(this Controller controller)
        {
            return Enum.GetValues(typeof(SourceType)).Cast<SourceType>().Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()) ?? x.ToString(),
                Value = x.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> SurveyQuestion1AnswersSelectList(this Controller controller)
        {
            return Enum.GetValues(typeof(SurveyQuestion1Answer)).Cast<SurveyQuestion1Answer>().Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()) ?? x.ToString(),
                Value = x.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> SurveyQuestion2AnswersSelectList(this Controller controller)
        {
            return Enum.GetValues(typeof(SurveyQuestion2Answer)).Cast<SurveyQuestion2Answer>().Select(x => new SelectListItem
            {
                Text = ResourceManger.GetString(x.ToString()) ?? x.ToString(),
                Value = x.ToString()
            }).PrependNull();
        }

        public static IEnumerable<SelectListItem> IsActiveSelectList(this Controller controller)
        {
            return new List<SelectListItem>() { 
                new SelectListItem() { Text = ApplicationStrings.Active, Value = bool.TrueString }, 
                new SelectListItem() { Text = ApplicationStrings.Inactive, Value = bool.FalseString } 
            }.PrependNull();
        }


        private static void hierarchySelectList(ProductCategoryLookup parent, int depth, IEnumerable<ProductCategoryLookup> subItems, List<SelectListItem> selectList)
        {
            string str = "";
            for (int i = 0; i < depth; i++)
            {
                str += "\xA0\xA0";
            }

            selectList.Add(new SelectListItem()
            {
                Text = str + parent.Name,
                Value = parent.Id.ToString()
            });

            int level = depth + 1;
            foreach (var item in subItems.Where(x => x.ParentId.HasValue && x.ParentId.Value == parent.Id))
            {
                hierarchySelectList(item, level, subItems, selectList);
            }
        }


        public static IEnumerable<SelectListItem> MapSelectList(this IEnumerable<LookupModel> lookup, bool prepentNull = true, bool setDefault = true, bool isSuggesion = false)
        {
            var selectList = Enumerable.Empty<SelectListItem>();
            if (isSuggesion)
            {
                selectList = lookup.Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Name,
                    Selected = setDefault && x.IsDefault
                });
            }
            else
            {
                selectList = lookup.Select(x => new SelectListItem()
                {
                    Text = x.Name,
                    Value = x.Id.ToString(),
                    Selected = setDefault && x.IsDefault
                });
            }

            if (!prepentNull)
            {
                return selectList;
            }

            return selectList.PrependNull();
        }


        public static ActionResult ListPageView(this Controller controller, EntityActions actionsToolbar, IEnumerable<TableHeaderModel> headers, IEnumerable<TableRowWithActionsModel> rows, FilterModelBase filter, string sortedBy, string sortOrder, int itemCount, Uri baseUrl, string title, string subTitle, int? pageSize = null, int? page = null, string tag = "", bool hasPager = true, bool hasSelectAll = false, bool lateActions = false)
        {
            var table = new TableModelBase();
            table.ActionsToolbar = actionsToolbar;
            table.Headers = headers;
            table.Rows = rows;
            table.SortedBy = sortedBy;
            table.SortOrder = sortOrder;
            table.Page = page;
            table.PageSize = pageSize;
            table.HasPager = hasPager;
            table.BaseUrl = controller.Request.Url;
            table.ItemCount = itemCount;
            table.HasSelectAll = hasSelectAll;
            table.Filter = filter;
            table.LateActions = lateActions;

            var viewModel = new ListPageViewModel(title, subTitle, tag, table);

            if (controller.Request.IsAjaxRequest())
            {
                return new PartialViewResult()
                {
                    ViewName = MVC.Shared.Views._Table,
                    ViewData = new ViewDataDictionary(viewModel.TableModel)
                };
            }
            else
            {
                return new ViewResult()
                {
                    ViewName = MVC.Shared.Views.ListPage,
                    ViewData = new ViewDataDictionary(viewModel)
                };
            }
        }

        [NonAction]
        public static void HandleValidationException(this Controller controller, ValidationException ex)
        {
            ValidationResult result = new ValidationResult(ex.Errors);
            result.AddToModelState(controller.ModelState, string.Empty);
        }

        public static PartialViewResult ConfirmationModal(this Controller controller, string confirmationText)
        {
            var result = new PartialViewResult()
            {
                ViewName = MVC.Shared.Views._ConfirmationModal,
            };
            result.ViewBag.Confirmation = confirmationText;
            return result;
        }


        public static PartialViewResult MessageModal(this Controller controller, string messageText)
        {
            var result = new PartialViewResult()
            {
                ViewName = MVC.Shared.Views._MessageModal,
            };
            result.ViewBag.Message = messageText;
            return result;
        }

        public static JsonResult ProcessServiceOperation(this Controller controller, Func<ResultResponse> action, string generalSuccessMessage, ActionResult returnToAction = null, string redirectAction = null)
        {
            try
            {
                var response = action();
                if (!response.Success)
                {
                    return controller.JsonFailure(response.ErrorMessage);
                }

                if (!string.IsNullOrEmpty(generalSuccessMessage))
                {
                    controller.TempData.AddSuccess(generalSuccessMessage);
                }

                if (returnToAction != null)
                {
                    return controller.JsonSuccess(redirect: controller.Url.Action(returnToAction));
                }
                else if (!string.IsNullOrEmpty(redirectAction))
                {
                    return controller.JsonSuccess(redirect: redirectAction);
                }
                return controller.JsonSuccess(generalSuccessMessage);
            }
            catch (ServiceOperationRulesException ex)
            {
                TagBuilder tag = PrepareServiceOperationRulesException(ex);

                return new JsonResult()
                {
                    Data = new { success = false, html = tag.ToString() }
                };
            }
        }

        public static TagBuilder PrepareServiceOperationRulesException(ServiceOperationRulesException ex)
        {
            TagBuilder tag = new TagBuilder("ul");
            tag.InnerHtml += "";
            IEnumerable<string> messages = new List<string>();

            try
            {
                messages = ex.GetDetails(true);//TODO:GetDetails throws exception when ServiceOperationRulesException is thrown directly. nuget must be corrected
            }
            catch
            {
                messages = new string[] { ex.Message };
            }

            foreach (var item in messages)
            {
                TagBuilder innerTag = new TagBuilder("li");
                innerTag.SetInnerText(item);
                tag.InnerHtml += innerTag.ToString();
            }

            return tag;
        }

        public static string HandleExceptionSchedule(this Controller controller, ServiceOperationRulesException ex)
        {
            string message = "";
            try
            {
                message = ex.GetDetails(true).First().Split('-').Last();
                return message;
            }
            catch
            {
                return null;
            }
        }
        /// <summary>
        /// puts exception details to modelstate
        /// </summary>
        /// <param name="allowHtml">must be false in non ajax</param>
        public static void HandleServiceOperationRulesException(this Controller controller, ServiceOperationRulesException ex, bool allowHtml = true)
        {
            IEnumerable<string> message;

            try
            {
                message = ex.GetDetails(true);//TODO:GetDetails throws exception when ServiceOperationRulesException is thrown directly. nuget must be corrected
            }
            catch
            {
                message = new string[] { ex.Message };
            }

            if (!allowHtml)
            {
                foreach (var item in message)
                {
                    controller.ModelState.AddModelError("", item);
                }
            }
            else
            {
                TagBuilder tag = PrepareServiceOperationRulesException(ex);
                controller.ModelState.AddModelError("", tag.ToString());
            }
        }

        public static string ToApplicationString(this Enum enumVal, string prefix = "")
        {
            if (enumVal != null)
            {
                string translatedValue = ResourceManger.GetString(string.Format("{0}{1}", prefix, enumVal.ToString()));
                if (string.IsNullOrEmpty(translatedValue))
                {
                    return enumVal.ToString();
                }
                return translatedValue;
            }
            return string.Empty;
        }

        public static DateTime GetStartDateFromDatePicker(this DateTimeViewModel model)
        {
            var datepicker = model.StartHour.Value.ToString();
            var splited = datepicker.Split('.');
            int hour = int.Parse(splited.First());
            int minute = int.Parse(splited.Last());
            var dateValue = model.Date.Value;

            if (splited.Count() == 1)
            {
                minute = 0;
            }
            var fullDate = new DateTime(dateValue.Year, dateValue.Month, dateValue.Day, hour, minute * 10, 0);
            return fullDate;
        }
        public static DateTime GetEndDateFromDatePicker(this DateTimeViewModel model)
        {
            var datepicker = model.EndHour.Value.ToString();
            var splited = datepicker.Split('.');
            int hour = int.Parse(splited.First());
            int minute = int.Parse(splited.Last());
            var dateValue = model.Date.Value;
            var day = model.EndHourIsInNextDay ? dateValue.Day + 1 : dateValue.Day;

            if (splited.Count() == 1)
            {
                minute = 0;
            }



            var fullDate = new DateTime(dateValue.Year, dateValue.Month, day, hour, minute * 10, 0);
            return fullDate;
        }

        #region documents
        public static ActionResult GetDocumentImpl(this Controller controller, ICollection<DocumentListItem> documents, int docId)
        {
            var document = documents.SingleOrDefault(doc => doc.DocumentId == docId);
            if (document != null)
            {
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = document.File.FileName,
                    Inline = false,
                };

                controller.Response.AppendHeader("Content-Disposition", cd.ToString());
                return new FileContentResult(document.File.Content, document.File.ContentType);
            }
            return new EmptyResult();
        }
        public static ActionResult GetDocumentImpl(this Controller controller, DocumentListItem document, int docId)
        {
            if (document != null)
            {
                var cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = document.File.FileName,
                    Inline = false,
                };

                controller.Response.AppendHeader("Content-Disposition", cd.ToString());
                return new FileContentResult(document.File.Content, document.File.ContentType);
            }
            return new EmptyResult();
        }
        public static ActionResult AddDocumentImpl(this Controller controller, ICollection<DocumentListItem> documents, string classifier, string classifierNumber, int classifierId, string identifier, string description, HttpPostedFileBase addFile, Action addAction)
        {
            byte[] contents = new byte[addFile.ContentLength];
            addFile.InputStream.Read(contents, 0, contents.Length);
            var documentFile = new UploadedFileListItem()
            {
                Content = contents,
                ContentType = addFile.ContentType,
                FileName = addFile.FileName
            };
            var document = new DocumentListItem()
            {
                Name = identifier,
                Description = description,
                File = documentFile,
                Classifier = classifier,
                ClassifierId = classifierNumber
            };
            documents.Add(document);
            addAction();
            var data = new
            {
                documentId = document.DocumentId,
                fileName = document.File.FileName,
                identifier = document.Name,
                id = classifierId,
                address = controller.Url.Action("document", new { id = classifierId, docId = document.DocumentId })
            };
            return new JsonResult
            {
                Data = data
            };
        }
        public static DocumentListItem GetDocumentImpl(this Controller controller, int Id, string classifier, string classifierNumber, string identifier, string description, HttpPostedFileBase addFile)
        {
            byte[] contents = new byte[addFile.ContentLength];
            addFile.InputStream.Read(contents, 0, contents.Length);
            var documentFile = new UploadedFileListItem()
            {
                Content = contents,
                ContentType = addFile.ContentType,
                FileName = addFile.FileName
            };
            var document = new DocumentListItem()
            {
                Name = identifier,
                Description = description,
                File = documentFile,
                Classifier = classifier,
                ClassifierId = classifierNumber
            };
            return document;
        }
        #region pikcha
        //----------------------------------------------------
        public static FixItTron.Services.Models.Event.DocumentListItem GetDocumentImpl(this Controller controller, int Id, string classifier, string classifierNumber, string identifier, string description, FixItTron.Services.Models.Event.UploadedFileListItem uploadedFileListItem)
        {
            var document = new FixItTron.Services.Models.Event.DocumentListItem()
            {
                Name = identifier,
                Description = description,
                File = uploadedFileListItem,
                Classifier = classifier,
                ClassifierId = classifierNumber
            };
            return document;
        }
        //-------------------------------------------------
        #endregion
        public static ActionResult DeleteDocumentImpl(this Controller controller, ICollection<DocumentListItem> documents, int docId, Action updateAction)
        {
            var document = documents.SingleOrDefault(doc => doc.DocumentId == docId);
            if (document != null)
            {
                documents.Remove(document);
                updateAction();
                return controller.JsonSuccess("წარმატებით");
            }
            return controller.JsonFailure("წარუმატებლად");
        }
        public static ActionResult DeleteDocumentImpl(this Controller controller, DocumentListItem document, int docId, Action deleteAction)
        {
            if (document != null)
            {
                deleteAction();
                return controller.JsonSuccess("წარმატებით");
            }
            return controller.JsonFailure("წარუმატებლად");
        }

        public static string HandleColorFromEnum(this Enum enm)
        {
            var colorEnumValuePairs = new Dictionary<string, string>
            {
                {SalesOrderScheduleStatus.Planned.ToString(),"#f3c511"},
                {SalesOrderScheduleStatus.Current.ToString(),"#f6f80a"},
                {SalesOrderScheduleStatus.Done.ToString(),"#5ec831"},
                {SalesOrderScheduleStatus.Canceled.ToString(),"#808080"},
                {SalesOrderScheduleStatus.OnTheRoad.ToString(),"#2bbdf5"}
            };


            return colorEnumValuePairs[enm.ToString()];
        }
        public static void AddAction(this EntityActions src, string url, string title)
        {
            src.Add(new EntityAction
            {
                Href = url,
                Title = title
            });
        }

        #endregion

        //public static string RenderViewToString(this Controller controller, string viewName, object model = null)
        //{
        //    ViewDataDictionary viewData = new ViewDataDictionary();
        //    viewData.Model = model;
        //    using (var sw = new StringWriter())
        //    {
        //        var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext,
        //                                                                 viewName);
        //        var viewContext = new ViewContext(controller.ControllerContext, viewResult.View,
        //                                     viewData, controller.TempData, sw);
        //        viewResult.View.Render(viewContext, sw);
        //        viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);
        //        return sw.GetStringBuilder().ToString();
        //    }
        //}

        public static IEnumerable<SelectListItem> GetHourSelectList(int number, int start = 0)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            for (int i = start; i <= number; i++)
            {
                result.Add(new SelectListItem()
                {
                    Value = i.ToString(),
                    Text = i.ToString().PadLeft(2, '0') + ":" + "00"
                });
                result.Add(new SelectListItem()
                {
                    Value = i.ToString() + ".3",
                    Text = i.ToString().PadLeft(2, '0') + ":" + "30"
                });
            }
            return result.PrependNull();
        }

        public static float GetHourFormatFloat(this float fl)
        {
            if (fl.ToString().Contains(".3"))
            {
                return fl + 0.2f;
            }
            return fl;
        }

        public static bool IsPrivacyPolicyHidden(this Controller controller)
        {
            var hideCookie = controller.Request.Cookies[PrivacyPolicyHideCookieName];
            return hideCookie != null && hideCookie.Value == "true";
        }

        public static string GetCurrentDeviceId(this Controller controller)
        {
            var deviceIdCookie = controller.Request.Cookies[DeviceIdCookieName];

            if (deviceIdCookie == null)
            {
                string newDeviceId = null;
                var apiService = DependencyResolver.Current.GetService<IApiService>();
                bool userExists = true;

                do
                {
                    newDeviceId = Guid.NewGuid().ToString();
                    var userCheckRequest = new Services.Models.Api.UserExistsRequest
                    {
                        UserName = newDeviceId,
                        UserType = Services.Models.Api.UserType.Device
                    };
                    userExists = apiService.UserExists(userCheckRequest).Exists;
                } while (userExists);

                var createUserRequest = new Services.Models.Api.CreateUserRequest
                {
                    UserName = newDeviceId,
                    UserType = Services.Models.Api.UserType.Device,

                };

                var createUserResponse = apiService.CreateUser(createUserRequest);
                if (!createUserResponse.Success)
                {
                    throw new InvalidOperationException("Could not create user");
                }

                deviceIdCookie = new HttpCookie(DeviceIdCookieName);
                deviceIdCookie.Expires = DateTime.MaxValue;
                deviceIdCookie.HttpOnly = true;
                deviceIdCookie.Value = newDeviceId;

                controller.HttpContext.Request.Cookies.Add(deviceIdCookie);
                controller.HttpContext.Response.Cookies.Add(deviceIdCookie);
            }

            return deviceIdCookie.Value;
        }

        //    var newDeviceId = Guid.NewGuid().ToString();
        //    var apiService = DependencyResolver.Current.GetService<IApiService>();
        //    if (!apiService.user)
        //    {

        //    }
        //}
    }
}
