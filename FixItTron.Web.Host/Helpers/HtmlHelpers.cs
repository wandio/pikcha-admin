﻿using Common.Enums;
using FixItTron.Services.Interfaces;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Wandio.Web.Mvc;

namespace FixItTron.Web.Host.Helpers
{
    public static class HtmlHelpers
    {
        public static MvcHtmlString DisplayTable(this HtmlHelper html, TableModelBase model)
        {
            return html.Partial(MVC.Shared.Views.ViewNames._Table, model);
        }

        public static MvcHtmlString DisplayPager(this HtmlHelper html, int page, int pageSize, int totalCount)
        {
            var model = new DataPagingInformation(pageSize, page, totalCount);
            return html.Partial(MVC.Shared.Views._Pager, model);
        }

        public static MvcHtmlString RequiredLabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            IDictionary<string, object> attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression<TModel, TValue>(expression, html.ViewData);
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);

            string lblText = metadata.DisplayName;

            if (string.IsNullOrEmpty(lblText))
            {
                lblText = Extensions.ResourceManger.GetString(htmlFieldName.Split(new char[] { '.' }).Last<string>());
            }

            if (string.IsNullOrEmpty(lblText))
            {
                lblText = metadata.PropertyName;
            }

            TagBuilder tagBuilder = new TagBuilder("label");
            tagBuilder.MergeAttribute("for", TagBuilder.CreateSanitizedId(html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName)));
            tagBuilder.SetInnerText(string.Format("{0}", lblText));
            tagBuilder.MergeAttributes<string, object>(attributes, true);
            tagBuilder.InnerHtml += "<span class=\"required\">*</span>";
            return MvcHtmlString.Create(tagBuilder.ToString());
        }

        public static MvcHtmlString LabelHelperFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            IDictionary<string, object> attributes = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression<TModel, TValue>(expression, html.ViewData);
            string htmlFieldName = ExpressionHelper.GetExpressionText(expression);

            string resolvedLabelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();

            TagBuilder tagBuilder = new TagBuilder("label");
            tagBuilder.MergeAttribute("for", TagBuilder.CreateSanitizedId(html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(htmlFieldName)));
            tagBuilder.SetInnerText(string.Format("{0}", resolvedLabelText));
            tagBuilder.MergeAttributes<string, object>(attributes, true);

            var isRequired = ModelValidatorProviders.Providers.GetValidators(metadata ?? ModelMetadata.FromStringExpression(htmlFieldName, html.ViewData), html.ViewContext).SelectMany(v => v.GetClientValidationRules()).Any(x => x.ValidationType.Equals("required", StringComparison.OrdinalIgnoreCase));

            if (metadata.IsRequired || isRequired)
            {
                TagBuilder span = new TagBuilder("span");
                span.AddCssClass("required");
                span.SetInnerText("*");
                tagBuilder.InnerHtml += span.ToString();
            }

            return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString ValidationHelperFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            return htmlHelper.ValidationMessageFor(expression, "", new { @class = "text-danger" });
        }

        private static RouteValueDictionary MergeAttributes(object htmlAttributes, object attributes)
        {
            RouteValueDictionary attributeDictionary = HtmlHelper.AnonymousObjectToHtmlAttributes(attributes);
            if (htmlAttributes != null)
            {
                foreach (var item in HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes))
                {
                    attributeDictionary.Add(item.Key, item.Value);
                }
            }
            return attributeDictionary;
        }

        public static MvcHtmlString AutoCompleteBoxFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> property, ActionResult action, object htmlAttributes = null)
        {
            UrlHelper url = new UrlHelper(html.ViewContext.RequestContext);

            object attributes = new
            {
                @class = "js-autocomplete form-control",
                autocomplete = "off",
                data_autocomplete = bool.TrueString.ToLower(),
                data_autocomplete_source = url.Action(action),
            };

            RouteValueDictionary attributeDictionary = MergeAttributes(htmlAttributes, attributes);

            TagBuilder inputGroup = new TagBuilder("div");
            inputGroup.AddCssClass("input-group");
            inputGroup.InnerHtml += html.TextBoxFor(property, attributeDictionary);
            inputGroup.InnerHtml += "<span class=\"input-group-addon js-autocomplete-trigger\"><i class=\"fa fa-search\"></i></span>";

            return MvcHtmlString.Create(inputGroup.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString GetLink(this UrlHelper url, ActionResult action)
        {
            return MvcHtmlString.Create(url.Action(action));
        }
    }
}
