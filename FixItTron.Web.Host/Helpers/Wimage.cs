﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Helpers
{
    public class Wimage
    {
        public string Base64 { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public decimal X { get; set; }
        public decimal Y { get; set; }


        public byte[] GetCroppedBase64(int cropWidth, int cropHeight, RotateFlipType rotate)
        {
            var bytes = Convert.FromBase64String(SplitBase64(this.Base64));

            using (var stream = new MemoryStream(bytes))
            {
                var croppedBitMap = CropImage(stream, Convert.ToInt32(this.X), Convert.ToInt32(this.Y), cropWidth, cropHeight, rotate);

                return croppedBitMap;
            }
        }

        private byte[] CropImage(Stream content, int x, int y, int width, int height, RotateFlipType rotate)
        {
            //Parsing stream to bitmap
            using (Bitmap sourceBitmap = new Bitmap(content))
            {
                sourceBitmap.RotateFlip(rotate);
                //Get new dimensions
                double sourceWidth = Convert.ToDouble(sourceBitmap.Size.Width);
                double sourceHeight = Convert.ToDouble(sourceBitmap.Size.Height);

                var croppedBitMap = sourceBitmap.Clone(new Rectangle(x, y, width, height), PixelFormat.DontCare);

                return GetBitmapBytes(croppedBitMap);
            }
        }


        private static byte[] GetBitmapBytes(Bitmap source)
        {
            //Settings to increase quality of the image
            ImageCodecInfo codec = ImageCodecInfo.GetImageEncoders()[4];
            EncoderParameters parameters = new EncoderParameters(1);
            parameters.Param[0] = new EncoderParameter(Encoder.Quality, 100L);

            //Temporary stream to save the bitmap
            using (MemoryStream tmpStream = new MemoryStream())
            {
                source.Save(tmpStream, codec, parameters);

                //Get image bytes from temporary stream
                byte[] result = new byte[tmpStream.Length];
                tmpStream.Seek(0, SeekOrigin.Begin);
                tmpStream.Read(result, 0, (int)tmpStream.Length);

                return result;
            }
        }

        private string SplitBase64(string base64)
        {
            return base64.Split(',')[1];
        }

    }
}