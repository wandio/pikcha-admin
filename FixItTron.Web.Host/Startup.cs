﻿using FixItTron.Web.Host.Helpers;
using Microsoft.Owin;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

[assembly: OwinStartupAttribute(typeof(FixItTron.Web.Host.Startup))]
namespace FixItTron.Web.Host
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Use<InvalidAuthenticationMiddleware>();
            ConfigureAuth(app);
        }
    }
}
