﻿using AutoMapper;
using FixItTron.Services;
using FixItTron.Services.Models.Document;
using FixItTron.Services.Models.User;
using FixItTron.Web.Host.Models.Documents;
using FixItTron.Web.Host.Models.Profile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Web.Mvc;
using Wandio.Web.Mvc.Html;
using FixItTron.Web.Host.Helpers;

namespace FixItTron.Web.Host.Controllers
{
    public partial class ProfileController : Controller
    {
        private FixItTronUserManager _userManager;
        private IMappingEngine _mapper;

        public ProfileController(FixItTronUserManager userManager, IMappingEngine mapper)
        {
            _userManager = userManager;
            _mapper = mapper;
        }

        // GET: Profile
        public virtual ActionResult Index()
        {
            var response = _userManager.GetEmployeeProfile(new GetEmployeeProfileRequest { Name = User.Identity.Name });//User.Identity.Name
            EmployeeProfileViewModel model = _mapper.Map<EmployeeProfileViewModel>(response.Employee);

            return View(model);
        }
        [HttpGet]
        public virtual ActionResult ChangePassword()
        {
            var viewModel = new ChangePasswordViewModel();
            viewModel.Name = User.Identity.Name;
            ViewBag.IsEdit = true;
            return PartialView(MVC.Shared.Views._AddEditModal, viewModel);
        }

        [HttpPost]
        public virtual ActionResult ChangePassword(ChangePasswordViewModel viewModel)
        {
            if (ModelState.IsValid)
            {

                var request = new ChangePasswordRequest()
                {
                    Name = viewModel.Name,
                    CurrentPassword = viewModel.CurrentPassword,
                    NewPassword = viewModel.NewPassword
                };
                var response = _userManager.ChangePassword(request);
                if (response.Success)
                {
                    return this.JsonSuccess();
                }
                ModelState.AddModelError(string.Empty, response.ErrorMessage);
            }
            ViewBag.IsEdit = true;
            return PartialView(MVC.Shared.Views._AddEditModal, viewModel);
        }

        public virtual ActionResult ProfilePicture(HttpPostedFileBase addFile)
        {
            var request = new ChangeProfileImageRequest();
            if (addFile != null)
            {
                if (addFile.ContentLength > 1024000 || !IsImage(addFile))
                {
                    TempData.AddError(ApplicationStrings.ImageTypeAndSizeFormat);
                    return RedirectToAction(MVC.Profile.Index());
                }

                byte[] contents = new byte[addFile.ContentLength];
                addFile.InputStream.Read(contents, 0, contents.Length);
                var documentFile = new FixItTron.Services.Models.Event.UploadedFileListItem()
                {
                    Content = Wandio.Web.Mvc.Html.ImageHelper.ResizeImage(contents, 120, 120),
                    ContentType = addFile.ContentType,
                    FileName = addFile.FileName
                };

                var documentToAdd = this.GetDocumentImpl(1, "Profile", "Profile", "Avatar", "Profile avatar", documentFile);
                request.Image = documentToAdd;
            }
            else
            {
                TempData.AddError(ApplicationStrings.FileIsRequired);
                return RedirectToAction(MVC.Profile.Index());
            }
            request.Name = User.Identity.Name;
            var response = _userManager.ChangeProfileImage(request);
            if (response.Success)
            {
                TempData.AddSuccess(ApplicationStrings.ProfilePictureUpdateSuccess);
                return RedirectToAction(MVC.Profile.Index());
            }

            ModelState.AddModelError(string.Empty, response.ErrorMessage);
            return View(MVC.Profile.Views.Index);
        }

        public virtual ActionResult ProfileImage(string name)
        {
            var response = _userManager.GetProfileImage(new GetProfileImageRequest { Name = name });
            if (!string.IsNullOrWhiteSpace(response.Image))
            {
                //TODO: change to AwsAdminImagePathPrefix
                return Content(string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.Image));
                //DocumentViewModel image = _mapper.Map<DocumentViewModel>(response.Image);
                //return new FileContentResult(image.File.Content, image.File.ContentType);
            }
            //DocumentViewModel image = _mapper.Map<DocumentViewModel>(_userManager.GetProfileImage(new GetProfileImageRequest { Name = User.Identity.Name }).Image);

            return Content("/Assets/img/avatar.png");
        }

        private bool IsImage(HttpPostedFileBase file)
        {
            if (file.ContentType.Contains("image"))
            {
                return true;
            }

            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }
    }
}