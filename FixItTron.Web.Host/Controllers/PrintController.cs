﻿using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Event;
using FixItTron.Services.Models.ImageUpload;
using FixItTron.Web.Host.Models.Print;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Host.Controllers
{
    [PrintAuthorizeAttribute]
    public partial class PrintController : Controller
    {
        readonly IImageUploadService _imageService;
        readonly IEventService _eventService;

        public PrintController(IImageUploadService imageService, IEventService eventService)
        {
            _imageService = imageService;
            _eventService = eventService;
        }

        public virtual ActionResult Index()
        {

            return View();
        }

        [HttpGet]
        public virtual ActionResult Print(int id)
        {
            var model = new PrintViewModel();

            int eventId = PrintAuthorizeAttribute.ReadCurrentEventId();

            var imageResponse = _imageService.GetImageById(new GetImageByIdRequest()
            {
                Id = id,
                EventId = eventId
            });
            imageResponse.FilePath = string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsImagePathPrefix, imageResponse.FileName);

            if (!imageResponse.Success)
            {
                return View(MVC.Print.Views.PrintError, (object)imageResponse.ErrorMessage);
            }

            model.Image = imageResponse.FilePath; // GetBase64(imageResponse.Image);

            var eventLogo = _eventService.GetEventLogo(new GetEventLogoRequest()
            {
                EventId = eventId
            });

            //TODO: change to AwsAdminImagePathPrefix
            model.EventLogo = string.IsNullOrWhiteSpace(eventLogo.LogoFileName) ? "/Assets/img/default_event_logo.png" : string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, eventLogo.LogoFileName);

            model.BackgroundLogo = string.IsNullOrWhiteSpace(eventLogo.BackgroundLogoFileName) ? "" : string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, eventLogo.BackgroundLogoFileName);

            model.BackgroundColor = eventLogo.BackgroundColor;

            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public virtual ActionResult Auth()
        {
            PrintAuthViewModel model = new PrintAuthViewModel();
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public virtual ActionResult Auth(PrintAuthViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = new AuthorizePrintRequest
                {
                    Username = model.UserName,
                    Password = model.Password
                };

                var response = _eventService.AuthorizePrint(request);

                if (response.Success)
                {
                    var eventId = response.EventId;
                    HttpCookie cookie = new HttpCookie(PrintAuthorizeAttribute.AuthCookieName);

                    cookie.Value = Convert.ToBase64String(System.Web.Security.MachineKey.Protect(Encoding.UTF8.GetBytes(eventId.ToString()), new[] { PrintAuthorizeAttribute.PrintPurpose }));

                    Response.Cookies.Add(cookie);

                    return RedirectToAction(MVC.Print.Index());
                }

                ModelState.AddModelError(string.Empty, response.ErrorMessage);
            }
            return View();
        }

        private string GetBase64(byte[] arr)
        {
            return string.Format("data:image/png;base64,{0}", Convert.ToBase64String(arr));
        }

        public virtual ActionResult LogOut()
        {
            if (Request.Cookies[PrintAuthorizeAttribute.AuthCookieName] != null)
            {
                var c = new HttpCookie(PrintAuthorizeAttribute.AuthCookieName);
                c.Expires = DateTime.Now.AddDays(-1);
                Response.Cookies.Add(c);
            }
            //_authManager.SignOut();
            return RedirectToAction(MVC.Print.Auth());
        }
    }
}