﻿using FixItTron.Services;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Api;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;
using static FixItTron.Web.Host.App_Start.ModelBindingConfig;

namespace FixItTron.Web.Host.Controllers
{
    [RoutePrefix("api/v1")]
    public class PikchaApiController : ApiController
    {
        readonly IApiService _apiService;
        readonly IChatbotService _chatbotService;
        public PikchaApiController()
        {
            _apiService = new FixItTron.Services.ApiService();
            _chatbotService = new Services.ChatbotService(_apiService);
        }

        //[HttpGet]
        //[Route("UserInfo")]
        //[ResponseType(typeof(string))]
        //public IHttpActionResult UserInfo([FromUri]UserInfoRequest request)
        //{
        //    return Ok(request.UserName);
        //}
        [HttpPost]
        [Route("ActivatePromoCode/")]
        [ResponseType(typeof(ActivatePromoCodeResponse))]
        public IHttpActionResult ActivatePromoCode([FromBody]ActivatePromoCodeRequest request)
        {
            var response = _apiService.ActivatePromoCode(request);
            return Ok(response);
        }

        [HttpPost]
        [Route("CreateUser/")]
        [ResponseType(typeof(CreateUserResponse))]
        public IHttpActionResult CreateUser([FromBody]CreateUserRequest request)
        {
            var response = _apiService.CreateUser(request);
            return Ok(response);
        }

        [HttpGet]
        [Route("GetUserInfo/")]
        [ResponseType(typeof(GetUserInfoResponse))]
        public IHttpActionResult GetUserInfo([FromUri]GetUserInfoRequest request)
        {
            var response = _apiService.GetUserInfo(request);
            return Ok(response);
        }

        [HttpPost]
        [Route("Upload/")]
        [ResponseType(typeof(UploadImageResponse))]
        public IHttpActionResult Upload([FromBody]UploadImageRequest request)
        {
            var response = _apiService.UploadImage(request);
            return Ok(response);
        }

        [HttpPost]
        [Route("Upload2/")]
        [ResponseType(typeof(UploadImage2Response))]
        public IHttpActionResult Upload2([FromBody]UploadImage2Request request)
        {
            var response = _apiService.UploadImage2(request);
            return Ok(response);
        }

        [HttpGet]
        [Route("UploadIsAvailable/")]
        [ResponseType(typeof(UploadIsAvailableResponse))]
        public IHttpActionResult UploadIsAvailable([FromUri]UploadIsAvailableRequest request)
        {
            var response = _apiService.UploadIsAvailable(request);
            return Ok(response);
        }

        [HttpGet]
        [Route("UserExists/")]
        [ResponseType(typeof(UserExistsResponse))]
        public IHttpActionResult UserExists([FromUri]UserExistsRequest request)
        {
            var response = _apiService.UserExists(request);
            return Ok(response);
        }

        [HttpGet]
        [Route("GetFacebookShareLogo/")]
        [ResponseType(typeof(GetFacebookShareLogoResponse))]
        public IHttpActionResult GetFacebookShareLogo()
        {
            var response = _apiService.GetFacebookShareLogo();
            if (!string.IsNullOrWhiteSpace(response.LogoUrl))
            {
                //TODO: change to AwsAdminImagePathPrefix
                response.LogoUrl = string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.LogoUrl);
            }
            return Ok(response);
        }

        [HttpGet]
        [Route("GetInstagramShareLogo/")]
        [ResponseType(typeof(GetInstagramShareLogoResponse))]
        public IHttpActionResult GetInstagramShareLogo()
        {
            var response = _apiService.GetInstagramShareLogo();
            if (!string.IsNullOrWhiteSpace(response.LogoUrl))
            {
                //TODO: change to AwsAdminImagePathPrefix
                response.LogoUrl = string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.LogoUrl);
            }
            return Ok(response);
        }

        [HttpGet]
        [Route("GetPreviewLogo/")]
        [ResponseType(typeof(GetPreviewLogoResponse))]
        public IHttpActionResult GetPreviewLogo()
        {
            var response = _apiService.GetPreviewLogo();
            if (!string.IsNullOrWhiteSpace(response.LogoUrl))
            {
                //TODO: change to AwsAdminImagePathPrefix
                response.LogoUrl = string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.LogoUrl);
            }
            return Ok(response);
        }

        public async Task<bool> VerifyRequestSignature()
        {
            IEnumerable<string> signatureHeaderValues;
            this.Request.Headers.TryGetValues("x-hub-signature", out signatureHeaderValues);
            if (signatureHeaderValues == null)
            {
                return false;
            }

            string signatureHeaderValue = signatureHeaderValues.FirstOrDefault();
            if (string.IsNullOrEmpty(signatureHeaderValue))
            {
                return false;
            }

            var signature = signatureHeaderValue.Split('=')[1];

            var body = await this.Request.Content.ReadAsByteArrayAsync();

            return _chatbotService.VerifyRequestSignature(signature, body);
        }

        [HttpGet]
        [Route("test/")]
        public HttpResponseMessage Test()
        {
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("webhook/")]
        public HttpResponseMessage Webhook(
            [FromUri(Name = "hub.mode")]string mode,
            [FromUri(Name = "hub.verify_token")]string verifyToken,
            [FromUri(Name = "hub.challenge")]string challenge)
        {
            //if (!await VerifyRequestSignature())
            //{
            //    return Request.CreateResponse(HttpStatusCode.Forbidden);
            //}

            if (mode == "subscribe" &&
                verifyToken == "pikcha_validation_token")
            {
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent(challenge, System.Text.Encoding.UTF8, "text/plain")
                };
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }
        }

        [HttpPost]
        [Route("webhook/")]
        public async Task<HttpResponseMessage> WebhookPost([FromBody]WebhookData request)
        {
            //if (!await VerifyRequestSignature())
            //{
            //    Request.CreateResponse(HttpStatusCode.Forbidden);
            //}

            var handled = await _chatbotService.HandleWebhookPost(request);

            if (!handled)
            {
                Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpPost]
        [Route("ValidateEventCode/")]
        [ResponseType(typeof(ValidateEventCodeResponse))]
        public IHttpActionResult ValidateEventCode([FromBody]ValidateEventCodeRequest request)
        {
            var response = _apiService.ValidateEventCode(request);
            return Ok(response);
        }

        [HttpPost]
        [Route("SubmitSurvey/")]
        [ResponseType(typeof(SubmitSurveyResponse))]
        public IHttpActionResult SubmitSurvey([FromBody]SubmitSurveyRequest request)
        {
            var response = _apiService.SubmitSurvey(request);
            return Ok(response);
        }

        [HttpGet]
        [Route("GetFacebookShareLogo2/")]
        [ResponseType(typeof(GetFacebookShareLogoResponse))]
        public IHttpActionResult GetFacebookShareLogo2(int eventId)
        {
            var response = _apiService.GetFacebookShareLogo2(eventId);
            if (!string.IsNullOrWhiteSpace(response.LogoUrl))
            {
                //TODO: change to AwsAdminImagePathPrefix
                response.LogoUrl = string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.LogoUrl);
            }
            return Ok(response);
        }

        [HttpGet]
        [Route("GetInstagramShareLogo2/")]
        [ResponseType(typeof(GetInstagramShareLogoResponse))]
        public IHttpActionResult GetInstagramShareLogo2(int eventId)
        {
            var response = _apiService.GetInstagramShareLogo2(eventId);
            if (!string.IsNullOrWhiteSpace(response.LogoUrl))
            {
                //TODO: change to AwsAdminImagePathPrefix
                response.LogoUrl = string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.LogoUrl);
            }
            return Ok(response);
        }

        [HttpGet]
        [Route("GetPreviewLogo2/")]
        [ResponseType(typeof(GetPreviewLogoResponse))]
        public IHttpActionResult GetPreviewLogo2(int eventId)
        {
            var response = _apiService.GetPreviewLogo2(eventId);
            if (!string.IsNullOrWhiteSpace(response.LogoUrl))
            {
                //TODO: change to AwsAdminImagePathPrefix
                response.LogoUrl = string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.LogoUrl);
            }
            return Ok(response);
        }
    }
}
