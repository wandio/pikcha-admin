﻿using FixItTron.Services;
using FixItTron.Services.Models.User;
using FixItTron.Web.Host.Models.Auth;
using FixItTron.Web.Host.Models.Validations;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Host.Controllers
{
    public partial class AuthController : Controller
    {
        private FixItTronUserManager _userManager;
        private FixItTronSignInManager _signInManager;
        private IAuthenticationManager _authManager;

        public AuthController(FixItTronUserManager userManager, FixItTronSignInManager signInManager, IAuthenticationManager authManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _authManager = authManager;
        }

        [HttpGet]
        [AllowAnonymous]
        public virtual ActionResult LogIn(string returnUrl)
        {
            var model = new LogInViewModel()
            {
                ReturnUrl = returnUrl
            };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public virtual ActionResult LogIn(LogInViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var response = _signInManager.Authenticate(new AuthenticateRequest() { UserName = model.UserName, Password = model.Password, RememberMe = model.RememberMe });

            if (response.Success)
            {
                if (!string.IsNullOrEmpty(model.ReturnUrl))
                {
                    return Redirect(model.ReturnUrl);
                }
                return RedirectToAction(MVC.Administration.Events.Index());
            }

            ModelState.AddModelError("", response.ErrorMessage);

            return View(model);
        }

        [HttpGet]
        public virtual ActionResult Register()
        {
            var response = _userManager.CreateUser(new FixItTron.Services.Models.User.CreateUserRequest()
            {
                UserName = "admin",
                Email = "khutso89@gmail.com",
                Password = "123"
            });

            if (response.Success)
            {
                return RedirectToAction(MVC.Home.Index());
            }

            ModelState.AddModelError("", response.ErrorMessage);

            return View();
        }

        public virtual ActionResult LogOut()
        {
            _authManager.SignOut();
            return RedirectToAction(MVC.Auth.LogIn());
        }
    }
}