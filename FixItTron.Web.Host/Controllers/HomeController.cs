﻿using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.ImageUpload;
using FixItTron.Web.Host.Helpers;
using FixItTron.Web.Host.Models.Home;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Host.Controllers
{
    public partial class HomeController : Controller
    {
        IImageUploadService _imageUploadService;
        IApiService _apiService;
        public HomeController(IImageUploadService imageUploadService, IApiService apiService)
        {
            this._imageUploadService = imageUploadService;
            this._apiService = apiService;
        }

        public virtual JsonResult GetUserInfo()
        {
            var deviceId = this.GetCurrentDeviceId();

            var userInfoResult = _apiService.GetUserInfo(new Services.Models.Api.GetUserInfoRequest()
            {
                UserName = deviceId,
                UserType = Services.Models.Api.UserType.Device
            });

            return Json(new { availablePrints = userInfoResult.AvailablePrints, usedPrints = userInfoResult.UsedPrints }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public virtual JsonResult EnterPromoCode(string code)
        {

            var deviceId = this.GetCurrentDeviceId();
            var promoCodeActivationRequest = new Services.Models.Api.ActivatePromoCodeRequest()
            {
                PromoCode = code,
                UserName = deviceId,
                UserType = Services.Models.Api.UserType.Device
            };

            var promoCodeActivationResult = _apiService.ActivatePromoCode(promoCodeActivationRequest);
            return Json(new
            {
                success = promoCodeActivationResult.Success,
                promoExists = promoCodeActivationResult.PromoCodeExists,
                promoAlreadyUsed = promoCodeActivationResult.PromoCodeIsAlreadyUsed,
            });

        }

        public virtual ActionResult Index()
        {
            return View();
        }

        [Route("~/Admin")]
        public virtual ActionResult Admin()
        {
            return RedirectToAction(MVC.Administration.Events.Index());
        }

        [Route("~/hey-there")]
        public virtual ActionResult Check()
        {
            //this will throw exception if anything is wrong
            _apiService.CheckHealth();

            return new HttpStatusCodeResult(200);
        }

        [HttpGet]
        public virtual ActionResult PrivacyPolicy()
        {
            if (this.IsPrivacyPolicyHidden())
            {
                return new EmptyResult();
            }
            return PartialView(MVC.Shared.Views._PrivacyPolicyModal);
        }

        [Route("~/Home/Upload")]
        [HttpPost]
        public virtual ActionResult Upload(ImageCropViewModel model)
        {
            var deviceId = this.GetCurrentDeviceId();

            var avalabilityRespionse = _apiService.UploadIsAvailable(new Services.Models.Api.UploadIsAvailableRequest() { UserName = deviceId, UserType = Services.Models.Api.UserType.Device });
            if (!avalabilityRespionse.Available)
            {
                return Json(new
                {
                    success = false,
                    message = "Awww Snap! Not enough credits! Type in a new code",
                    photoId = 0,
                    homeUrl = Url.Action(MVC.Home.Index()) + "?drawer=1"
                });
            }

            var image = new Wimage();
            image.Base64 = model.ImgUrl;
            image.X = model.ImgX;
            image.Y = model.ImgY;
            image.Height = model.ImgInitH;
            image.Width = model.ImgInitW;



            RotateFlipType rotation = RotateFlipType.RotateNoneFlipNone;

            switch (model.Rotation)
            {
                case 90:
                    rotation = RotateFlipType.Rotate90FlipNone;
                    break;
                case 180:
                    rotation = RotateFlipType.Rotate180FlipNone;
                    break;
                case 270:
                    rotation = RotateFlipType.Rotate270FlipNone;
                    break;
                default:
                    break;
            }

            var request = new UploadImageRequest()
            {
                Image = new AddImageItem()
                {
                    Data = image.GetCroppedBase64(Convert.ToInt32(model.CropW), Convert.ToInt32(model.CropH), rotation),
                    Username = User.Identity.Name
                }
            };

            var uploadRequest = new Services.Models.Api.UploadImage2Request()
            {
                ImageBase64 = Convert.ToBase64String(request.Image.Data),
                Source = Common.Enums.SourceType.Web,
                UserName = deviceId,
                UserType = Services.Models.Api.UserType.Device
            };
            var uploadResponse = _apiService.UploadImage2(uploadRequest);
            //var response = _imageUploadService.UploadImage(request);

            //if (response.Success)
            return Json(new
            {
                success = uploadResponse.Success,
                message = uploadResponse.Success ? "" : "Something went wrong, while uploading and it is not your fault",
                photoId = uploadResponse.PhotoId
            });

            //return this.JsonFailure();
        }

        public virtual ActionResult TestAws()
        {
            var request = new UploadImageRequest()
            {
                Image = new AddImageItem()
                {
                    Data = System.IO.File.ReadAllBytes(@"C:\Users\buulioni\Desktop\3.png"),
                    Username = User.Identity.Name
                }
            };


            var response = _imageUploadService.UploadImage(request);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}
