﻿using Common.Enums;
using FixItTron.Services.Interfaces;
using FixItTron.Web.Host.Models.Infrastructure;
using FixItTron.Web.Host.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FixItTron.Web.Host.Helpers;
using Common;

namespace FixItTron.Web.Host.Controllers
{
    public partial class MenuController : Controller
    {
        public MenuController()
        {
        }

        public static string operationIcon = "fa fa-folder-o";
        public static string configurationIcon = "fa fa-cogs";
        public static string informationIcon = "fa fa-info-circle";

        // GET: Menu
        public virtual ActionResult SideMenu()
        {
            string controllerName = ControllerContext.ParentActionViewContext.RouteData.Values["controller"].ToString();
            string actionName = ControllerContext.ParentActionViewContext.RouteData.Values["action"].ToString();

            var model = new SideMenuViewModel();

            var administration = SetUpAdministrationSection(controllerName);

            model.Items.Add(administration);

            ControllerContext.ParentActionViewContext.ViewBag.Menu = model;
            return PartialView(model);
        }

        private MenuItem SetUpAdministrationSection(string controllerName)
        {
            var administration = new MenuItem()
            {
                Title = ApplicationStrings.Administration,
                Selected = new[] { 
                    MVC.Administration.Companies.Name,
                    MVC.Administration.Configuration.Name,
                    MVC.Administration.Events.Name,
                    MVC.Administration.ApplicationUsers.Name,
                    MVC.Administration.PromoCodes.Name
                }.Contains(controllerName),
                Tag = "icon-settings",
                SubMenu = new List<MenuItem>() {
                    new MenuItem(){
                                Tag = "fa fa-cube",
                                Title = ApplicationStrings.Companies,
                                Href = Url.Action(MVC.Administration.Companies.Index()),
                                Selected = controllerName.Equals(MVC.Administration.Companies.Name)
                            },
                            new MenuItem(){
                                Tag = "fa fa-calendar-plus-o",
                                Title = ApplicationStrings.Events,
                                Href = Url.Action(MVC.Administration.Events.Index()),
                                Selected = controllerName.Equals(MVC.Administration.Events.Name)
                            },
                            new MenuItem(){
                                Tag = "fa fa-magnet",
                                Title = ApplicationStrings.PromoCodes,
                                Href = Url.Action(MVC.Administration.PromoCodes.Index()),
                                Selected = controllerName.Equals(MVC.Administration.PromoCodes.Name)
                            },
                            new MenuItem(){
                                Tag = "fa fa-gears",
                                Title = ApplicationStrings.Configurations,
                                Href = Url.Action(MVC.Administration.Configuration.Index()),
                                Selected = controllerName.Equals(MVC.Administration.Configuration.Name)
                            },
                            new MenuItem(){
                                Tag = "fa fa-users",
                                Title = ApplicationStrings.ApplicationUsers,
                                Href = Url.Action(MVC.Administration.ApplicationUsers.Index()),
                                Selected = controllerName.Equals(MVC.Administration.ApplicationUsers.Name)
                            },
                            new MenuItem(){
                                Tag = "fa fa-question",
                                Title = ApplicationStrings.Survey,
                                Href = Url.Action(MVC.Administration.Surveys.Index()),
                                Selected = controllerName.Equals(MVC.Administration.Surveys.Name)
                            },
                        }
                //SubMenu = new List<MenuItem>() { 
                //    new MenuItem(){
                //        Title = ApplicationStrings.Configuration,
                //        Tag = configurationIcon,
                //        Selected = new[] { 
                //            MVC.Administration.Companies.Name,
                //            MVC.Administration.Configuration.Name,
                //            MVC.Administration.Events.Name,
                //            MVC.Administration.PromoCodes.Name
                //        }.Contains(controllerName),
                //        SubMenu = new List<MenuItem>() {
                //            new MenuItem(){
                //                Title = ApplicationStrings.Events,
                //                Href = Url.Action(MVC.Administration.Events.Index()),
                //                Selected = controllerName.Equals(MVC.Administration.Events.Name)
                //            },
                //            new MenuItem(){
                //                Title = ApplicationStrings.Companies,
                //                Href = Url.Action(MVC.Administration.Companies.Index()),
                //                Selected = controllerName.Equals(MVC.Administration.Companies.Name)
                //            },
                //            new MenuItem(){
                //                Title = ApplicationStrings.PromoCodes,
                //                Href = Url.Action(MVC.Administration.PromoCodes.Index()),
                //                Selected = controllerName.Equals(MVC.Administration.PromoCodes.Name)
                //            },
                //            new MenuItem(){
                //                Title = ApplicationStrings.Configurations,
                //                Href = Url.Action(MVC.Administration.Configuration.Index()),
                //                Selected = controllerName.Equals(MVC.Administration.Configuration.Name)
                //            },
                            
                //        }
                //    }                    
                //}
            };
            return administration;
        }

        [ChildActionOnly]
        public virtual ActionResult BreadCrumbs()
        {
            var model = ControllerContext.ParentActionViewContext.ViewBag.Menu as SideMenuViewModel;

            var breadcrumbs = new Dictionary<string, string>();
            var activeSection = model.Items.SingleOrDefault(section => section.Selected == true);
            if (activeSection != null)
            {
                breadcrumbs.Add(activeSection.Title, "javascript:;");
                var activeSubMenu = activeSection.SubMenu.SingleOrDefault(menuItem => menuItem.Selected);
                if (activeSubMenu != null)
                {
                    breadcrumbs.Add(activeSubMenu.Title, activeSubMenu.Href);
                }
            }

            return View(breadcrumbs);
        }
    }
}