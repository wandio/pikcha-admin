﻿using FixItTron.Web.Host.Models.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Models.Print
{
    [Validator(typeof(PrintAuthViewModelValidator))]
    public class PrintAuthViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}