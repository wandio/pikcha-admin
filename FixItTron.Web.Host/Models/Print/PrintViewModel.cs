﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Models.Print
{
    public class PrintViewModel
    {
        public string Image { get; set; }
        public string EventLogo { get; set; }
        public string BackgroundColor { get; set; }
        public string BackgroundLogo { get; set; }
    }
}