﻿using FixItTron.Web.Host.Models.Auth;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Validations
{
    public class LogInViewModelValidator : AbstractValidator<LogInViewModel>
    {
        public LogInViewModelValidator()
        {
            RuleFor(order => order.Password).NotEmpty().WithMessage(ApplicationStrings.FieldIsRequired);
            RuleFor(order => order.UserName).NotEmpty().WithMessage(ApplicationStrings.FieldIsRequired);
        }
    }
}
