﻿using FixItTron.Web.Host.Models.Print;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Models.Validators
{
    public class PrintAuthViewModelValidator : AbstractValidator<PrintAuthViewModel>
    {
        public PrintAuthViewModelValidator()
        {
            RuleFor(model => model.UserName).NotEmpty();
            RuleFor(model => model.Password).NotEmpty();
        }
    }
}