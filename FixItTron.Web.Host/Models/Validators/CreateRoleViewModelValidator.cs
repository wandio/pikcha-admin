﻿using FixItTron.Web.Host.Areas.Administration.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Validations
{
    public class CreateRoleViewModelValidator : AbstractValidator<CreateRoleViewModel>
    {
        public CreateRoleViewModelValidator()
        {
            RuleFor(role => role.Name).NotEmpty().WithMessage(ApplicationStrings.FieldIsRequired);
            RuleFor(role => role.Permissions).NotEmpty().WithMessage(ApplicationStrings.FieldIsRequired);
        }
    }
}
