﻿using FixItTron.Web.Host.Models.Validations;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Auth
{
    [Validator(typeof(LogInViewModelValidator))]
    public class LogInViewModel
    {
        [Display(Name = "UserName", ResourceType = typeof(ApplicationStrings))]
        public string UserName { get; set; }
        [Display(Name = "Password", ResourceType = typeof(ApplicationStrings))]
        public string Password { get; set; }
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}
