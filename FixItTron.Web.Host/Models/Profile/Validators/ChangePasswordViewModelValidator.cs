﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Profile.Validators
{
    public class ChangePasswordViewModelValidator : AbstractValidator<ChangePasswordViewModel>
    {
        public ChangePasswordViewModelValidator()
        {
            RuleFor(p => p.NewPassword).NotEmpty();
            RuleFor(p => p.CurrentPassword).NotEmpty();
            RuleFor(p => p.VerifyPassword).NotEmpty();
            RuleFor(p => p.NewPassword).NotEqual(x => x.CurrentPassword).WithMessage(ApplicationStrings.PasswordsAreSame);
            RuleFor(p => p.VerifyPassword).Equal(x => x.NewPassword).WithMessage(ApplicationStrings.PasswordDoesNotMatch);
        }
    }
}
