﻿using FixItTron.Web.Host.Models.Profile.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Profile
{
    [Validator(typeof(ChangePasswordViewModelValidator))]
    public class ChangePasswordViewModel
    {
        public string Name { get; set; }
        [Display(Name = "CurrentPassword", ResourceType = typeof(ApplicationStrings))]
        public string CurrentPassword { get; set; }
        [Display(Name = "VerifyPassword", ResourceType = typeof(ApplicationStrings))]
        public string VerifyPassword { get; set; }
        [Display(Name = "NewPassword", ResourceType = typeof(ApplicationStrings))]
        public string NewPassword { get; set; }
    }
}
