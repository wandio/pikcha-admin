﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Infrastructure
{
    public class SideMenuViewModel
    {
        public List<MenuItem> Items { get; set; }

        public SideMenuViewModel()
        {
            Items = new List<MenuItem>();
        }
    }
}
