﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Infrastructure
{
    public class TableHeaderModel
    {
        public string Title { get; set; }
        public bool IsSortable { get; set; }
        public string SortBy { get; set; }

        public int? Width { get; set; }

        public TableHeaderModel()
        {

        }

        public TableHeaderModel(string title, bool isSortable = false, string sortBy = null, int? width = null)
        {
            this.Title = title;
            this.IsSortable = isSortable;
            this.SortBy = sortBy;
            this.Width = width;
        }
    }
}
