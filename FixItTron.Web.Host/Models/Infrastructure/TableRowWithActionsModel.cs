﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Web.Mvc;

namespace FixItTron.Web.Host.Models.Infrastructure
{
    public class TableRowWithActionsModel : TableRowModelBase
    {
        public EntityActions Actions { get; set; }
    }
}
