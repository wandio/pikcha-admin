﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Infrastructure
{
    public class ListPageViewModel
    {
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Tag { get; set; }

        public TableModelBase TableModel { get; set; }

        public ListPageViewModel()
        {
            TableModel = new TableModelBase();
        }

        public ListPageViewModel(string title, string subTitle, string tag, TableModelBase table)
        {
            Title = title;
            SubTitle = subTitle;
            Tag = tag;
            TableModel = table;
        }
    }
}
