﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Infrastructure
{
    public class NavTabsModel
    {
        public string ActiveTabIndex { get; set; }
        public bool DisableAllButActive { get; set; }
        public List<TabModel> Tabs { get; set; }

        public bool IsSelected(TabModel tab)
        {
            return tab.TabIndex == ActiveTabIndex;
        }

        public NavTabsModel()
        {
            Tabs = new List<TabModel>();
        }

        public string Class { get; set; }
    }

    public class TabModel
    {
        public string TabIndex { get; set; }
        public string Title { get; set; }
        public bool IsDisabled { get; set; }
        //public bool IsSelected { get; set; }
        public string Url { get; set; }
    }

    public static class TabIndex
    {
        public const string General = "General";
        public const string Objects = "Objects";
        public const string ContactPersons = "ContactPersons";
        public const string BankRequisites = "BankRequisites";
        public const string Note = "Note";
        public const string Documents = "Documents";
        public const string Agreements = "Agreements";
        public const string SalesOrders = "SalesOrders";
        public const string UnitOfMeasure = "UnitOfMeasure";
        public const string Files = "Files";
        public const string Sections = "Sections";
        public const string QuantityControll = "QuantityControll";
        public const string Conditions = "Conditions";
        public const string Services = "Services";
        public const string Accruals = "Accruals";
        public const string Schedule = "Schedule";
        public const string Limits = "Limits";

        /// <summary>
        /// used for navigation
        /// </summary>
        public static string ToTab(this string tabIndex)
        {
            return string.Format("tab_{0}", tabIndex.ToString().ToLower());
        }
    }
}