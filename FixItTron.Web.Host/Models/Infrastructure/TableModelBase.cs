﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Web.Mvc;

namespace FixItTron.Web.Host.Models.Infrastructure
{
    public class TableModelBase
    {
        public EntityActions ActionsToolbar { get; set; }
        public virtual IEnumerable<TableHeaderModel> Headers { get; set; }
        public virtual IEnumerable<TableRowModelBase> Rows { get; set; }
        public bool HasPager { get; set; }
        public bool HasToolbar { get { return ActionsToolbar != null; } }
        public bool HasFilter { get { return Filter != null; } }
        public string SortedBy { get; set; }
        public string SortOrder { get; set; }
        public int? PageSize { get; set; }
        public int? Page { get; set; }
        public int ItemCount { get; set; }
        public bool HasSelectAll { get; set; }
        public bool LateActions { get; set; }
        public Uri BaseUrl { get; set; }
        public FilterModelBase Filter { get; set; }

        public bool HasData
        {
            get
            {
                return Rows != null && Rows.Any();
            }
        }

        public TableModelBase(
            IEnumerable<TableHeaderModel> headers,
            IEnumerable<TableRowModelBase> rows,
            bool hasPager,
            string sortedBy,
            string sortOrder,
            int itemCount,
            bool hasSelectAll,
            Uri baseUrl,
            FilterModelBase filter,
            int? pageSize = null,
            int? page = null)
        {
            this.Headers = headers;
            this.Rows = rows;
            this.HasPager = hasPager;
            this.SortedBy = sortedBy;
            this.SortOrder = sortOrder;
            this.PageSize = pageSize;
            this.Page = Page;
            this.ItemCount = itemCount;
            this.HasSelectAll = hasSelectAll;
            this.BaseUrl = baseUrl;
            this.Filter = filter;
        }

        public TableModelBase() { }
    }
}
