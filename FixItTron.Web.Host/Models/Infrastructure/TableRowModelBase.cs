﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Infrastructure
{
    public class TableRowModelBase
    {
        /// <summary>
        /// Can be overriden with new for ids other than integer
        /// </summary>
        public int RowId { get; set; }

        /// <summary>
        /// Adds contextual disabled class
        /// </summary>
        public bool IsDisabled { get; set; }

        /// <summary>
        /// Adds contextual info class
        /// </summary>
        public bool IsInfo { get; set; } //add contextual info class
    }
}
