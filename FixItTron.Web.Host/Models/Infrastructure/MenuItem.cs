﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Infrastructure
{
    public class MenuItem
    {
        public string Title { get; set; }
        public string Href { get; set; }
        public string CssClass { get; set; }
        public bool Selected { get; set; }
        public string Tag { get; set; }

        public List<MenuItem> SubMenu { get; set; }

        public override string ToString()
        {
            return Title;
        }

        public MenuItem()
        {
            SubMenu = new List<MenuItem>();
        }
    }
}
