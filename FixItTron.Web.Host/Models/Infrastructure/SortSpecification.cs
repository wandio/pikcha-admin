﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Infrastructure
{
    public class SortSpecification
    {
        public string Sort { get; set; }
        public string SortBy { get; set; }
    }
}
