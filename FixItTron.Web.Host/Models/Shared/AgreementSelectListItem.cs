﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FixItTron.Web.Host.Models.Shared
{
    public class AgreementSelectListItem : SelectListItem
    {
        public Common.Enums.PaymentTerm? PaymentTerm { get; set; }
    }
}
