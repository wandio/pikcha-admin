﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FixItTron.Web.Host.Models
{
    public class TaxGroupSelectListItem : SelectListItem
    {
        public decimal Percent { get; set; }
    }
}
