﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Web.Mvc.Html;

namespace FixItTron.Web.Host.Models.Shared
{
    public class StartEndHourDateViewModel
    {
        public float? EndHour { get; set; }
    
        public float? StartHour { get; set; }

        public float? DurationBetweenSchedules { get; set; }
      
        public IEnumerable<SelectListItem> EndHourSelectList { get; set; }
        public IEnumerable<SelectListItem> StartHourSelectList { get; set; }
        public IEnumerable<SelectListItem> DurationBetweenSchedulesSelectList { get; set; }

        public StartEndHourDateViewModel()
        {
            DurationBetweenSchedulesSelectList = GetHourSelectList(23).PrependNull();
            StartHourSelectList = GetHourSelectList(23).PrependNull();
            EndHourSelectList = GetHourSelectList(23).PrependNull();
        }
       
        public static IEnumerable<SelectListItem> GetHourSelectList(int number, int start = 0)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            for (int i = start; i <= number; i++)
            {
                result.Add(new SelectListItem()
                {
                    Value = i.ToString(),
                    Text = i.ToString().PadLeft(2, '0') + ":" + "00"
                });
                result.Add(new SelectListItem()
                {
                    Value = i.ToString()+".3",
                    Text = i.ToString().PadLeft(2, '0') + ":" + "30"
                });
            }
            return result;
        }
    }
}