﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Services.Helpers;

namespace FixItTron.Web.Host.Models
{
    public class ProductOrderLineViewModelBase
    {
        public int SalesQuotationLineId { get; set; }
        public string ProductCategory { get; set; }
        public string ProductName { get; set; }
        public string BaseUnitOfMeasure { get; set; }
        public string UnitOfMeasure { get; set; }

        public int? ProductId { get; set; }
        [Display(Name = "BaseUnitOfMeasureShort", ResourceType = typeof(ApplicationStrings))]
        public int? BaseUnitOfMeasureId { get; set; }
        [Display(Name = "UnitOfMeasureShort", ResourceType = typeof(ApplicationStrings))]
        public int? UnitOfMeasureId { get; set; }
        [Display(Name = "BaseQuantityShort", ResourceType = typeof(ApplicationStrings))]
        public decimal? BaseQuantity { get; set; }

        [Display(Name = "QuantityShort", ResourceType = typeof(ApplicationStrings))]
        public decimal? Quantity { get; set; }

        public int? TaxGroupId { get; set; }
        public string TaxGroup { get; set; }

        public decimal DiscountAmount { get; set; }

        public List<int> AllowedMeasures { get; set; }
    }
}
