﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Models.Shared
{
    public class EmployeeInboxNotificationModel
    {
        public int EmployeeId { get; set; }
        public string Sender { get; set; }
        public string RecieveDate { get; set; }
        public string MessageBody { get; set; }
        public string SenderImageLink { get; set; }
        public string Subject { get; set; }
        public string MessageBodyPartial { get; set; }
        public int NotificationId { get; set; }
    }
}