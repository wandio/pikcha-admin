﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Models.Shared.Validators
{
    public class DateTimeViewModelValidator:AbstractValidator<DateTimeViewModel>
    {
        public DateTimeViewModelValidator()
        {
            RuleFor(model => model.Date).NotEmpty();
            RuleFor(model => model.StartHour).NotEmpty();
            RuleFor(model => model.EndHour).NotEmpty();
            //When(x => !x.EndHourIsInNextDay, () => 
            //{
            //    RuleFor(model => model.EndHour).GreaterThan(x => x.StartHour).WithMessage("დასრულების თარიღი მეტი უნდა იყოს დაწყების თარიღზე");
            //});
        }
    }
}