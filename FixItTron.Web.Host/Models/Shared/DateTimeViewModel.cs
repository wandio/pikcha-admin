﻿using FixItTron.Web.Host.Models.Shared.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Web.Mvc.Html;
namespace FixItTron.Web.Host.Models.Shared
{
    [Validator(typeof(DateTimeViewModelValidator))]
    public class DateTimeViewModel
    {
        public DateTime? Date { get; set; }

        [Display(Name = "JobDuration", ResourceType = typeof(ApplicationStrings))]
        public float? EndHour { get; set; }

        public float? StartHour { get; set; }

        public bool EndHourIsInNextDay { get; set; }

        public IEnumerable<SelectListItem> EndHourSelectList { get; set; }
        public IEnumerable<SelectListItem> StartHourSelectList { get; set; }

        public DateTimeViewModel()
        {
            StartHourSelectList = GetHourSelectList(23).PrependNull();
            EndHourSelectList = GetHourSelectList(71).PrependNull();
            EndHour = 1.3f;
            if (EndHour == null && StartHour == null)
            {
                StartHour = DateTime.Now.Hour;
            }
        }
        public static IEnumerable<SelectListItem> IntegerSelectList(int number, int start = 0)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            for (int i = start; i <= number; i++)
            {
                result.Add(new SelectListItem()
                {
                    Value = i.ToString(),
                    Text = i.ToString().PadLeft(2, '0') + ":" + "00"
                });
            }
            return result;
        }
        public static IEnumerable<SelectListItem> GetHourSelectList(int number, int start = 0)
        {
            List<SelectListItem> result = new List<SelectListItem>();
            for (int i = start; i <= number; i++)
            {
                result.Add(new SelectListItem()
                {
                    Value = i.ToString(),
                    Text = i.ToString().PadLeft(2, '0') + ":" + "00"
                });
                result.Add(new SelectListItem()
                {
                    Value = i.ToString() + ".3",
                    Text = i.ToString().PadLeft(2, '0') + ":" + "30"
                });
            }
            return result;
        }

        public float GetEndHour()
        {
            if (this.EndHour.Value.ToString().Contains(".3"))
            {
                return this.EndHour.Value + 0.2f;
            }
            return this.EndHour.Value;
        }
    }
}