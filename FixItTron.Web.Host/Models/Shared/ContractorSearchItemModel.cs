﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models
{
    public class ContractorSearchItemModel
    {
        public string text { get; set; }
        public string value { get; set; }
        public string IdentificationCode { get; set; }
        public string LegalForm { get; set; }
        public string Category { get; set; }
        public bool HasAgreement { get; set; }
        public bool BlackList { get; set; }
        public string Balance { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public ContractorStatus Status { get; set; }
    }
}
