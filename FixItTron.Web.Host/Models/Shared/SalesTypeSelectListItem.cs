﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FixItTron.Web.Host.Models.Shared
{
    public class SalesTypeSelectListItem : SelectListItem
    {
        public bool HasSalesOrder { get; set; }
    }
}
