﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Models.Shared
{
    public class InboxNotificationViewModel
    {
        public int UnreadCount { get; set; }
        public List<EmployeeInboxNotificationModel> Notifications { get; set; }
    }

}