﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Models.Home
{
    public class ImageCropViewModel
    {
        public string ImgUrl { get; set; }
        public int ImgInitW { get; set; }
        public int ImgInitH { get; set; }
        public int ImgW { get; set; }
        public int ImgH { get; set; }
        public decimal ImgY { get; set; }
        public decimal ImgX { get; set; }
        public decimal CropW { get; set; }
        public decimal CropH { get; set; }
        public int Rotation { get; set; }
    }
}