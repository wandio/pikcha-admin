﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Models.Documents
{
    public class DocumentEditorModel
    {
        public string UploadTo { get; set; }
        public int Id { get; set; }
        public string DeleteFrom { get; set; }
        public IEnumerable<DocumentViewModel> Documents { get; set; }
    }
}
