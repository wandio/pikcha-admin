﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using FixItTron.Web.Host.Helpers;

namespace FixItTron.Web.Host
{
    public class FixItTronAuthorizeAttribute : AuthorizeAttribute
    {
        public Securable Securable { get; set; }
        public Permissions[] Permissions { get; set; }

        public FixItTronAuthorizeAttribute(Securable securable, params Permissions[] permissions)
        {
            Securable = securable;
            Permissions = permissions;
        }

        public FixItTronAuthorizeAttribute() { }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //if not authenticated, return false
            if (!(httpContext.Request.IsAuthenticated))
            {
                return false;
            }

            return httpContext.User.HasRight(Securable, Permissions);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            //if not authenticated, just let him log on
            if (HttpContext.Current.Request.IsAuthenticated)
            {
                filterContext.Controller.TempData.Add("AuthMessage", ApplicationStrings.YouHaveNoRightToPerformThisAction);
            }
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}
