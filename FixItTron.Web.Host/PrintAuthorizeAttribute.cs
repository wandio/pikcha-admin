﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Host
{
    public class PrintAuthorizeAttribute : AuthorizeAttribute
    {
        public const string AuthCookieName = "PrintAuth";
        public const string PrintPurpose = "Print";

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            bool allowAnonymous = filterContext.ActionDescriptor.GetCustomAttributes(typeof(AllowAnonymousAttribute), true).Any();

            if (allowAnonymous)
            {
                return;
            }

            if (!AuthorizeCore(filterContext.HttpContext))
            {
                filterContext.Result = new RedirectToRouteResult(MVC.Print.Auth().GetRouteValueDictionary());
            }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var cookie = httpContext.Request.Cookies[AuthCookieName];
            if (cookie == null)
            {
                return false;
            }

            //if this throws exception, auth is not valid
            var eventId = ReadAuthorizedEventId(cookie.Value);
            return true;
        }

        private static int ReadAuthorizedEventId(string cookieValue)
        {
            var cookieValueBytes = Convert.FromBase64String(cookieValue);
            var unprotectedBytes = System.Web.Security.MachineKey.Unprotect(cookieValueBytes, new[] { "Print" });
            return Convert.ToInt32(Encoding.UTF8.GetString(unprotectedBytes));
        }

        public static int ReadCurrentEventId()
        {
            var cookie = HttpContext.Current.Request.Cookies[AuthCookieName];
            return ReadAuthorizedEventId(cookie.Value);
        }
    }
}