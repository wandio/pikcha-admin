﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Resources;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Host
{
    public class FixItTronModelMetadataProvider : DataAnnotationsModelMetadataProvider
    {
        static ResourceManager ApplicationStringsResourceManager { get; set; }
        static FixItTronModelMetadataProvider()
        {
            ApplicationStringsResourceManager = new ResourceManager(typeof(ApplicationStrings));
        }

        protected override ModelMetadata CreateMetadata(IEnumerable<Attribute> attributes, Type containerType, Func<object> modelAccessor, Type modelType, string propertyName)
        {
            var meta = base.CreateMetadata(attributes, containerType, modelAccessor, modelType, propertyName);
            if (!string.IsNullOrWhiteSpace(propertyName) && (modelType.IsPrimitive || modelType.IsValueType || modelType == typeof(string)))
            {
                var displayAttribute = attributes.SingleOrDefault(a => typeof(DisplayAttribute) == a.GetType());
                if (displayAttribute == null)
                {
                    if (propertyName.EndsWith("id", StringComparison.OrdinalIgnoreCase))
                    {
                        propertyName = propertyName.Remove(propertyName.Length - 2);
                    }
                    var appString = ApplicationStringsResourceManager.GetString(propertyName);
                    if (appString != null)
                    {
                        meta.DisplayName = appString;
                    }
                }

            }
            return meta;
        }
    }
}