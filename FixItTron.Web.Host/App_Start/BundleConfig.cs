﻿using System.Web.Optimization;

namespace FixItTron.Web.Host.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/style/global")
                .Include("~/Assets/plugins/font-awesome/css/font-awesome.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/simple-line-icons/simple-line-icons.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/bootstrap/css/bootstrap.css")
                .Include("~/Assets/plugins/uniform/css/uniform.default.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/bootstrap-switch/css/bootstrap-switch.css")
                .Include("~/Assets/plugins/bootstrap-toastr/toastr.css")
                .Include("~/Assets/plugins/select2/select2.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/bootstrap-datepicker/css/datepicker.css")
                .Include("~/Assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css")
                .Include("~/Assets/plugins/jstree/themes/default/style.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/jquery-ui/jquery-ui.min.css", new CssRewriteUrlTransform())
                .Include("~/Assets/plugins/typeahead/typeahead.css")
                .Include("~/Assets/plugins/fontawesome-iconpicker/fontawesome-iconpicker.css")
                .Include("~/Assets/plugins/bootstrap-colorpicker/css/colorpicker.css", new CssRewriteUrlTransform())
                );

            bundles.Add(new StyleBundle("~/style/theme")
                .Include("~/Assets/css/components.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/plugins.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/layout.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/themes/darkblue.css", new CssRewriteUrlTransform())
                .Include("~/Assets/css/custom.css", new CssRewriteUrlTransform())
                );

            bundles.Add(new ScriptBundle("~/bundles/core")
                .Include("~/Assets/plugins/jquery/jquery-1.11.2.js")
                .Include(
                "~/Assets/plugins/jquery-validation/jquery.validate.js",
                "~/Assets/plugins/jquery-validation/jquery.validate.unobtrusive.js",
                "~/Assets/plugins/jquery-validation/jquery.validate.unobtrusive.dynamic.js"
                )
                .Include("~/Assets/plugins/jquery-migrate-1.2.1.js")
                .Include("~/Assets/plugins/jquery-ui/jquery-ui.min.js")
                .Include("~/Assets/plugins/bootstrap/js/bootstrap.js")
                .Include("~/Assets/plugins/bootbox/bootbox.js")
                .Include("~/Assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.js")
                .Include("~/Assets/plugins/jquery-slimscroll/jquery.slimscroll.js")
                .Include("~/Assets/plugins/jquery-blockui/jquery.blockui.js")
                .Include("~/Assets/plugins/jquery-cookie/jquery.cokie.min.js")
                .Include("~/Assets/plugins/uniform/jquery.uniform.js")
                .Include("~/Assets/plugins/bootstrap-switch/js/bootstrap-switch.js")
                .Include("~/Assets/plugins/bootstrap-toastr/toastr.js")
                .Include("~/Assets/plugins/uri/URI.js"));

            bundles.Add(new ScriptBundle("~/bundles/global")
                .Include(
               "~/Assets/plugins/select2/select2.js",
               "~/Assets/plugins/select2/select2_locale_en.js",
               "~/Assets/plugins/select2/select2_locale_ka.js")
                //.Include(
                //"~/Assets/plugins/datatables/media/js/jquery.dataTables.js",
                //"~/Assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js")
               .Include(
               "~/Assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js",
               "~/Assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.en.js",
               "~/Assets/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.ka.js")
               .Include("~/Assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js")
               .Include("~/Assets/plugins/jstree/jstree.js")
               .Include("~/Assets/plugins/typeahead/typeahead.bundle.js")
               .Include("~/Assets/plugins/fontawesome-iconpicker/fontawesome-iconpicker.js")
               .Include("~/Assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js")
               .Include("~/Assets/scripts/metronic.js")
               .Include("~/Assets/scripts/layout.js")
               .Include("~/Assets/scripts/wandio.twable.js")
               .Include("~/Assets/scripts/wandio.contractorSelector.js")
               .Include("~/Assets/scripts/wandio.tableLineEditor.js")
               .Include("~/Assets/scripts/wandio.productBrowser.js")
               .Include("~/Assets/scripts/app.js")
              );


            bundles.Add(new ScriptBundle("~/bundles/documenteditor").Include(
                "~/Assets/plugins/jquery-file-upload/jquery.ui.widget.js",
                "~/Assets/plugins/jquery-file-upload/jquery.iframe-transport.js",
                "~/Assets/plugins/jquery-file-upload/jquery.fileupload.js",
                "~/Assets/scripts/document-editor.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/auth")
                .Include(
                "~/Assets/plugins/jquery/jquery-{version}.js",
                "~/Assets/plugins/jquery-validation/jquery.validate.js",
                "~/Assets/plugins/jquery-validation/jquery.validate.unobtrusive.js",
                "~/Assets/plugins/jquery-validation/jquery.validate.unobtrusive.dynamic.js",
                "~/Assets/plugins/bootstrap/js/bootstrap.js",
                "~/Assets/plugins/jquery-blockui/jquery.blockui.js",
                "~/Assets/plugins/jquery-cookie/jquery.cokie.min.js",
                "~/Assets/plugins/uniform/jquery.uniform.js",
                "~/Assets/scripts/metronic.js",
                "~/Assets/scripts/layout.js"
                ));


            bundles.Add(new StyleBundle("~/styles/schedulestyle").Include(
                "~/Assets/plugins/full-calendar/css/fullcalendar.min.css").Include(
                "~/Assets/plugins/full-calendar/css/scheduler.min.css",
                "~/Assets/plugins/bootstrap-daterangepicker/daterangepicker-bs2.css",
                "~/Assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"));

            bundles.Add(new ScriptBundle("~/bundles/schedulejs").Include(
                "~/Assets/plugins/full-calendar/js/moment.min.js").Include(
                "~/Assets/plugins/bootstrap-daterangepicker/daterangepicker.js",
                "~/Assets/plugins/full-calendar/js/fullcalendar.min.js").Include(
                "~/Assets/plugins/full-calendar/js/scheduler.js"
                // "~/Assets/Scripts/scheduleHelpers.js")
                ));


            bundles.Add(new StyleBundle("~/styles/bootstrap-multiselect").Include(
               "~/Assets/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css"));

            bundles.Add(new ScriptBundle("~/scripts/bootstrap-multiselect").Include(
                    "~/Assets/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js"));


            //This Bundle Loads on Front page (Do not delete)
            bundles.Add(new StyleBundle("~/styles/web")
            //.Include("~/Assets/plugins/bootstrap/css/bootstrap.css", new CssRewriteUrlTransform())
            .Include("~/Assets/plugins/font-awesome/css/font-awesome.css", new CssRewriteUrlTransform())
            .Include("~/Assets/plugins/Blueprint/css/default.css", new CssRewriteUrlTransform())
            .Include("~/Assets/plugins/Blueprint/css/component.css", new CssRewriteUrlTransform())
            .Include("~/Content/Web/css/web.css", new CssRewriteUrlTransform()));

            bundles.Add(new ScriptBundle("~/scripts/web").Include(
                "~/Assets/plugins/jquery/jquery-1.11.2.js",
                "~/Content/Web/js/jquery.BlockUI.js",
                "~/Content/Web/js/jquery.qrcode-0.11.0.js",
                //"~/Assets/plugins/bootstrap/js/bootstrap.js",
                 "~/Content/Web/js/cropper.min.js",
                "~/Assets/plugins/Blueprint/js/classie.js",
                "~/Content/Web/js/app.js"));

            BundleTable.EnableOptimizations = true;
        }
    }
}
