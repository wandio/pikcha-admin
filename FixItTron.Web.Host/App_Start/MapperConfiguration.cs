﻿using Common.Enums;
using FixItTron.Services.Models.Document;
using FixItTron.Services.Models.Lookup;
using FixItTron.Services.Models.Lookup.Reason;
using FixItTron.Services.Models.Lookup.Cities;
using FixItTron.Services.Models.Lookup.Countries;
using FixItTron.Services.Models.Lookup.ProductClass;
using FixItTron.Services.Models.Lookup.ProductType;
using FixItTron.Services.Models.Lookup.TaxGroups;
using FixItTron.Services.Models.Lookup.UnitOfMeasure;
using FixItTron.Services.Models.Role;
using FixItTron.Services.Models.Shared;
using FixItTron.Web.Host.Areas.Administration.Models;
using FixItTron.Web.Host.Helpers;
using FixItTron.Web.Host.Models;
using FixItTron.Web.Host.Models.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Services.Models.Lookup.WarehouseTypes;
using FixItTron.Services.Models.Lookup.ScheduleAdditionalStatuses;
using FixItTron.Services.Models.User;
using FixItTron.Web.Host.Models.Profile;
using FixItTron.Services.Models.Lookup.Position;
using FixItTron.Services.Models.Lookup.Currencies;
using FixItTron.Services.Models.Lookup.Banks;
using FixItTron.Services.Models.Lookup.PaymentClassifier;
using FixItTron.Services.Models.Lookup.BoxOffices;
using FixItTron.Services.Models.Lookup.AbsenseEmployee;
using FixItTron.Services.Models.Lookup.AbsenseTypes;
using FixItTron.Services.Models.Lookup.ObjectAdditionals;
using FixItTron.Services.Models.Company;
using FixItTron.Web.Host.Areas.Administration.Models.Companies;
using FixItTron.Services.Models.PromoCode;
using FixItTron.Web.Host.Areas.Administration.Models.PromoCodes;
using FixItTron.Services.Models.Configurations;
using FixItTron.Web.Host.Areas.Administration.Models.Configurations;
using FixItTron.Services.Models.Event;
using FixItTron.Web.Host.Areas.Administration.Models.Events;
using FixItTron.Web.Host.Areas.Administration.Models.ApplicationUsers;
using FixItTron.Services.Models.Lookup.ApplicationUsers;

namespace FixItTron.Web.Host.App_Start
{
    public static class MapperConfiguration
    {
        public static void Configure(AutoMapper.IConfiguration configuration)
        {
            #region pikcha

            #region company
            configuration.CreateMap<CompanyListItem, CompanyTableRowModel>()
                .ForMember(dest => dest.RowId, options => options.MapFrom(src => src.CompanyId));
            configuration.CreateMap<AddEditCompanyItem, AddEditCompanyViewModel>();
            configuration.CreateMap<AddEditCompanyViewModel, AddEditCompanyItem>();
            #endregion

            #region promoCode
            configuration.CreateMap<PromoCodeListItem, PromoCodeTableRowModel>()
                .ForMember(dest => dest.RowId, options => options.MapFrom(src => src.PromoCodeId));
            configuration.CreateMap<AddEditPromoCodeItem, AddEditPromoCodeViewModel>();
            configuration.CreateMap<AddEditPromoCodeViewModel, AddEditPromoCodeItem>();
            #endregion

            #region configuration
            configuration.CreateMap<ConfigurationItem, ConfigurationTableRowModel>()
                .ForMember(dest => dest.RowId, options => options.MapFrom(src => src.ConfigurationId));
            configuration.CreateMap<ConfigurationItem, EditConfigurationViewModel>();
            #endregion

            #region event
            configuration.CreateMap<EventListItem, EventTableRowModel>()
                .ForMember(dest => dest.RowId, options => options.MapFrom(src => src.EventId));
            configuration.CreateMap<AddEditEventItem, AddEditEventViewModel>();
            configuration.CreateMap<AddEditEventViewModel, AddEditEventItem>()
                //.ForMember(dest => dest.LogoFileName, options => options.MapFrom(src => src.LogoFileName))
                .ForMember(dest => dest.MobileLogoFileName, options => options.Ignore())
                .ForMember(dest => dest.InstagramLogoFileName, options => options.Ignore())
                .ForMember(dest => dest.FacebookLogoFileName, options => options.Ignore())
                .ForMember(dest => dest.BackgroundLogoFileName, options => options.Ignore())
                .ForMember(dest => dest.EndDate, options => options.ResolveUsing(src =>
                {
                    return src.EndDate.AddHours(src.EndTimeHour.Hours).AddMinutes(src.EndTimeHour.Minutes);
                }))
                .ForMember(dest => dest.StartDate, options => options.ResolveUsing(src =>
                {
                    return src.StartDate.AddHours(src.StartTimeHour.Hours).AddMinutes(src.StartTimeHour.Minutes);
                })); ;
            configuration.CreateMap<FixItTron.Services.Models.Event.DocumentListItem, FixItTron.Web.Host.Areas.Administration.Models.Events.DocumentViewModel>();
            configuration.CreateMap<FixItTron.Services.Models.Event.UploadedFileListItem, FixItTron.Web.Host.Areas.Administration.Models.Events.UploadedFileModel>();
            #endregion

            #endregion

            #region roles
            configuration.CreateMap<RoleListItem, RoleTableRowModel>()
                .ForMember(dest => dest.RowId, options => options.MapFrom(src => src.RoleId));
            configuration.CreateMap<CreateRoleViewModel, CreateRoleRequest>();
            configuration.CreateMap<EditRolePermissionModel, EditRolePermissionItem>();
            configuration.CreateMap<GetRoleResponse, CreateRoleViewModel>();
            configuration.CreateMap<EditRolePermissionItem, EditRolePermissionModel>();
            configuration.CreateMap<CreateRoleViewModel, UpdateRoleRequest>();
            #endregion

            #region appUsers
            configuration.CreateMap<ApplicationUserListItem, ApplicationUserTableRowModel>()
                .ForMember(dest => dest.RowId, options => options.MapFrom(src => src.UserId));
            configuration.CreateMap<SurveyListItem, SurveyTableRowModel>()
                .ForMember(dest => dest.RowId, options => options.MapFrom(src => src.SurveyId));
            #endregion
        }
    }
}
