﻿using FixItTron.Services.Models.Api;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace FixItTron.Web.Host.App_Start
{
    public static class ModelBindingConfig
    {
        public static void Configure()
        {
            ModelBinders.Binders.Add(typeof(DateTime?), new DateTimeModelBinder());
            ModelBinders.Binders.Add(typeof(DateTime), new DateTimeModelBinder());
        }

        public class DateTimeModelBinder : IModelBinder
        {
            public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                Debug.Assert(bindingContext.ModelType == typeof(DateTime) || bindingContext.ModelType == typeof(DateTime?), "this ModelBinder should be used for DateTimes!");

                ValueProviderResult valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, valueProviderResult);

                DateTime result;
                if (valueProviderResult != null && !string.IsNullOrWhiteSpace(valueProviderResult.AttemptedValue))
                {
                    try
                    {
                        if (DateTime.TryParse(valueProviderResult.AttemptedValue, out result) || DateTime.TryParse(valueProviderResult.AttemptedValue, CultureInfo.CurrentCulture, DateTimeStyles.None, out result))
                        {
                            return result;
                        }
                        else
                        {
                            throw new FormatException();
                        }
                    }
                    catch (FormatException ex)
                    {
                        bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex);
                    }
                }

                return null;
            }
        }
    }
}
