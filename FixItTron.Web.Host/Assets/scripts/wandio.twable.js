﻿$.widget("wandio.twable", {
    options: {
        hasSort: true,
        hasPage: true,
        hasFilter: true,
        ajax: {
            isActive: false,
            target: '' //class or id of table, which this plugin is initialized on. E.g. .js-table
        },
        pageSizeQueryParam: 'pagesize',
        pageQueryParam: 'page',
        sortOrderQueryParam: 'sortorder',
        sortByQueryParam: 'sortby'
    },

    _create: function () {
        var self = this;
        var tableContainer = self.element.parent();
        self.container = tableContainer;
        var hasSortOrderQuery = false;
        var isSortAsc = false;

        if (self.element.data('filter') != undefined) {
            self.options.hasFilter = self.element.data('filter');
        }
        if (self.element.data('sort') != undefined) {
            self.options.hasSort = self.element.data('sort');
        }
        if (self.element.data('page') != undefined) {
            self.options.hasPage = self.element.data('page');
        }

        self.baseURI = new URI(self.element.data('current-url'));

        this._on(self.element, {
            'click .lazy-actions .entity-action .btn': function (event) {
                event.preventDefault();
                var $this = $(this);
                var spinClass = 'fa-spin fa-circle-o-notch';
                if (!$this.next().length) {
                    var icon = $this.find('.fa');
                    icon.addClass(spinClass);
                    $.get($this.parents('.lazy-actions').data('actions-dropdown'), { id: $this.parents('tr').data('id') })
                        .done(function (data) {
                            if (data) {
                                $(data).insertAfter($this);
                            }
                            else {
                                $this.fadeOut('fast');
                            }
                        })
                        .always(function () {
                            icon.removeClass(spinClass);
                        });
                }
            }
        });

        //#region sort
        if (self.options.hasSort) {
            self.Sorts = tableContainer.find('.js-sort a');
            if (self.baseURI.hasQuery(self.options.sortOrderQueryParam, 'desc') ||
                self.baseURI.hasQuery(self.options.sortOrderQueryParam, 'asc')) {
                hasSortOrderQuery = true;
                isSortAsc = self.baseURI.hasQuery(self.options.sortOrderQueryParam, 'asc')
            }

            $.each(this.Sorts, function (index, item) {
                var sortColLink = $(item);
                var val = sortColLink.data(self.options.sortByQueryParam);
                var url = self._updateUrl(val, self.options.sortByQueryParam);

                var sortCol = sortColLink.parent();

                if (sortCol.hasClass('sorting_asc')) {
                    url = self._updateUrl('desc', self.options.sortOrderQueryParam);
                }
                else {
                    url = self._updateUrl('asc', self.options.sortOrderQueryParam);
                }

                sortColLink.attr('href', url.toString());
            });

            if (self.options.ajax.isActive) {
                self._on(this.Sorts, {
                    'click': function (event) {
                        event.preventDefault();
                        var url = $(event.target).attr('href');
                        self._processAjax(tableContainer, url);
                    }
                });
            }
        }
        //#endregion sort

        //#region page
        if (self.options.hasPage) {
            if (!hasSortOrderQuery) {
                var baseURI = self._getBaseURI();
                baseURI.removeQuery(self.options.sortByQueryParam);
                baseURI.removeQuery(self.options.sortOrderQueryParam);
            }
            else {
                self._updateUrl(isSortAsc ? 'asc' : 'desc', self.options.sortOrderQueryParam);
            }

            var pagerContainer = tableContainer.find('.js-pager-row');
            self.pageSizes = pagerContainer.find('.js-page-sizes li a');
            self.pages = pagerContainer.find('.js-paging li a');
            self.curPage = pagerContainer.data('page');
            self.curPageSize = pagerContainer.data('size');

            $.each(this.pages, function (index, item) {
                var page = $(item);
                var val = page.data('val');
                var url = self._updatePageUrl(val);
                page.attr('href', url.toString());
            });

            $.each(this.pageSizes, function (index, item) {
                var page = $(item);
                var val = page.data('val');
                var url = self._updatePageSizeUrl(val);
                page.attr('href', url.toString());
            });

            if (self.options.ajax.isActive) {
                self._on(this.pages, {
                    'click': function (event) {
                        event.preventDefault();
                        var url = $(event.target).attr('href');
                        self._processAjax(tableContainer, url);
                    }
                });

                self._on(this.pageSizes, {
                    'click': function (event) {
                        event.preventDefault();
                        var url = $(event.target).attr('href');
                        self._processAjax(tableContainer, url);
                    }
                });
            }
        }
        //#endregion page

        //#region filter
        if (self.options.hasFilter) {
            self.filters = tableContainer.find('.js-filter');
            var clearFilter = tableContainer.find('.js-clear-filter');
            clearFilter.attr('href', self._getClearUrl());

            self._on(this.filters, {
                'keyup :input': function (event) {
                    if (event.keyCode == 13) {
                        event.preventDefault();
                        var $this = $(event.target);
                        var $row = $this.parents('tr.filter').find(':submit.filter-submit').click();
                    }
                },
                'click :submit.filter-submit': function (event) {
                    event.preventDefault();
                    var $this = $(event.target);
                    var $row = $(event.delegateTarget);
                    self._processFilter(tableContainer, $row);
                },
                'click :submit.filter-cancel': function (event) {
                    event.preventDefault();

                    if (self.options.ajax.isActive) {
                        //window.location.origin + self.baseURI.path()
                        self._processAjax(tableContainer, this._getClearUrl());
                    }
                    else {
                        window.location.href = this._getClearUrl();
                    }
                }
            });
        }
        //#endregion filter
    },

    export: function () {
        this._processFilter(this.container, this.filters, true);
    },

    _processFilter: function (tableContainer, $row, _export) {
        self = this;
        var q = $row.find(':input').serializeArray();

        for (var ind in q) {
            if (self.baseURI.hasQuery(q[ind].name) === false) {
                self.baseURI.addQuery(q[ind].name, q[ind].value);
            } else {
                self.baseURI.setQuery(q[ind].name, q[ind].value);
            }
        }

        self._updatePageSizeUrl(self.curPageSize);

        if (_export === true) {
            if (self.baseURI.hasQuery('export') === false) {
                self.baseURI.addQuery('export', 'true');
            } else {
                self.baseURI.setQuery('export', 'true');
            }
            window.location.href = self.baseURI.toString();
        }
        else {
            if (self.options.ajax.isActive) {
                self._processAjax(tableContainer);
            }
            else {
                window.location.href = self.baseURI.toString();
            }

            var clearFilter = tableContainer.find('.js-clear-filter');
            clearFilter.attr('href', self._getClearUrl());
        }
    },

    _processAjax: function (tableContainer, url) {
        var self = this;
        var href = url ? url : self.baseURI.toString();
        if (Metronic && Metronic.blockUI) {
            Metronic.blockUI({ target: tableContainer, animate: true });
        }
        $.get(href, function (data) {
            tableContainer.replaceWith(data);
            $(self.options.ajax.target).twable({
                ajax: self.options.ajax
            });
        }).complete(function () {
            Metronic.unblockUI({ target: tableContainer });
        });
    },

    _updatePageUrl: function (val) {
        var url = this._updateUrl(val, this.options.pageQueryParam);
        url = this._updateUrl(this.curPageSize, this.options.pageSizeQueryParam);
        return url;
    },

    _updatePageSizeUrl: function (val) {
        var url = this._updateUrl(val, this.options.pageSizeQueryParam);
        url = this._updateUrl(this.curPage, this.options.pageQueryParam);
        return url;
    },

    _updateUrl: function (val, query) {
        var url = this._getBaseURI();
        url = url.setSearch(query, val);
        return url;
    },

    _getBaseURI: function () {
        return this.baseURI;
    },

    _getClearUrl: function () {
        return window.location.origin + window.location.pathname;
    }
});