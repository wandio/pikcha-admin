﻿var salesQuotationEditor = function () {

    var handleObject = function () {
        $(document).on('click', 'button.js-save-object', function (event) {
            event.preventDefault();

            Metronic.blockUI({ animate: true, target: '.js-object-tab' });

            var $this = $(this);
            var $form = $this.closest('form');
            $.validator.unobtrusive.reParse($form);
            $.post($form.attr('action'), $form.serialize(), replaceObject);
        });

        function replaceObject(response) {
            if (response.data && response.data.view) {
                $('.js-object-tab').empty().html(response.data.view);
            }
            else if (response.redirect) {
                window.location.href = response.redirect;
            }
            else {
                $('.js-object-tab').empty().html(response);
            }

            Metronic.unblockUI();
        }
    };

    var handleOrder = function () {

        $(document).on('change', '.js-discount-type', function () {
            var $this = $(this);
            if ($this.val() == 'DiscountPercent') {
                $('.js-discount-val-container i').show();
            }
            else {
                $('.js-discount-val-container i').hide();
            }
        });

        $(document).on('change', '.js-price-rule, .js-discount-type, .js-discount-val, .js-select-salestype, .js-agreement', function () {

            var discountType = $('.js-discount-type :selected').val();
            var discountVal = $('.js-discount-val').val();

            var priceRule = $('select.js-price-rule');
            var priceRuleId = priceRule.val();
            var url = priceRule.data('recalculate-url');
            var salesType = $('.js-select-salestype:input').val();
            var agreementId = $('select.js-agreement').val();
            $.post(url, {
                priceRuleId: priceRuleId,
                discountType: discountType,
                discountVal: discountVal,
                salesType: salesType,
                agreementId: agreementId
            }, function (data) {
                if (data.OnlyTotals) {
                    var $subTotals = $('.subtotals');
                    $subTotals.find('.totalamountwithouttax .value').text(data.TotalAmountWithoutTax.toAmount());
                    $subTotals.find('.totaltaxamount .value').text(data.TotalTaxAmount.toAmount());
                    $subTotals.find('.totalprice .value').text(data.TotalPrice.toAmount());
                    $subTotals.find('.totaldiscountamount .value').text(data.TotalDiscountAmount.toAmount());
                } else {
                    replaceLines(data);
                }
            });
        });

        $(document).on('click', 'a.js-service', function (event) {
            event.preventDefault();
            var $this = $(this);
            App.initEntityAction($this, replaceLines, null);
        });

        function replaceLines(response) {
            if (!response.data.isService) {
                $('.js-material-table').empty().html(response.data.view);
            }
            else {
                $('.js-services-table').empty().html(response.data.view);
            }

            $('.js-total-table').empty().html(response.data.viewTotal);
        }
    };

    var handleAgreements = function () {
        $(document).on('change', '.js-agreement', function () {
            var $this = $(this);
            var paymentTerm = $('select#PaymentTerm');
            var term = $this.find(':selected').data('term');
            if (term) {
                paymentTerm.val(term);
                paymentTerm.trigger('change');
            }
            else {
                paymentTerm.val('');
                paymentTerm.trigger('change');
            }
        });
    };

    var handleSalesTypes = function () {
        $(document).on('change', '.js-select-salestype', function () {
            var $this = $(this);
            var salesOrdersContainer = $('.js-salesorder-container')
            var salesOrdersSelectList = salesOrdersContainer.find('select.js-select-salesorder');
            if ($this.find(':selected').data('salesorder')) {
                var url = new URI($this.data('salesorders-url'));

                var customerId = $('#CustomerId').val();
                if (customerId) {
                    url.removeQuery('customerId');
                    url.addQuery('customerId', customerId);
                }

                $.get(url, function (data) {
                    salesOrdersSelectList.select2('destroy').empty();
                    $.each(data, function (index, item) {
                        var option = $('<option>');
                        option.val(item.Id);
                        option.text(item.Name);
                        salesOrdersSelectList.append(option);
                    });
                });

                salesOrdersContainer.show();
            }
            else {
                salesOrdersContainer.hide();
                salesOrdersSelectList.select2('destroy').empty();
            }
        });
    };

    return {
        initObject: function () {
            handleObject();
        },

        initOrder: function () {
            handleOrder();
        },

        initAgreements: function () {
            handleAgreements();
        },

        initSalesTypes: function () {
            handleSalesTypes();
        }
    };
}();


function FillContractorObjects(url, targetId, urlForContactPerson, responsibleId) {
    FillDropDownValuesOf(url, targetId, urlForContactPerson, responsibleId);
}
function FillContractorAgreements(url, targetId) {
    FillDropDownValuesOf(url, targetId);
}
function FillContractorContactPersons(url, targetId) {
    FillDropDownValuesOf(url, targetId);
}
function FillContractorObjectResponsiblePersons(url, targetId) {
    FillDropDownValuesOf(url, targetId);
}
function FillDropDownValuesOf(url, targetDropDownId, urlForContactPerson, responsibleId) {
    var customerId = $('#CustomerId').val();
    if (customerId.length !== 0) {
        $.ajax({
            url: url,
            type: "GET",
            dataType: "JSON",
            data: { contractorId: customerId },
            success: function (objects) {
                $("#" + targetDropDownId).html("");
                $("#" + targetDropDownId).append($('<option></option>'));
                var isSelected = false;
                var onlyOne = objects.length == 1;
                $.each(objects, function (i, obj) {
                    var option = $('<option></option>').val(obj.Id).html(obj.Name);
                    if (onlyOne) {
                        option.attr('selected', true);
                        if (targetDropDownId == 'ContractorObjectId') {
                            FillObjectContactPersons(urlForContactPerson, responsibleId, obj.Id);
                        }
                    }
                    if (obj.PaymentTerm) {
                        option.attr('data-term', obj.PaymentTerm);
                        if (!isSelected) {
                            var paymentTerm = $('select#PaymentTerm');
                            paymentTerm.val(obj.PaymentTerm);
                            paymentTerm.trigger('change');
                            isSelected = true;
                            option.attr('selected', true);
                        }
                    }
                    $("#" + targetDropDownId).append(option);
                });
            }
        });
    }
    else {
        clearValues(targetDropDownId);
    }
}
function FillDropDownValuesOfProduct(idProduct, url, targetDropDownId) {
    var productId = idProduct.val();
    if (productId.length !== 0) {
        $.ajax({
            url: url + '?productId=' + productId,
            type: "GET",
            success: function (objects) {
                $("#" + targetDropDownId).html("");
                $.each(objects, function (i, obj) {
                    $("#" + targetDropDownId).append(
                        $('<option></option>').val(obj.Id).html(obj.Name));
                });
            }
        });
    }
    else {
        clearValues(targetDropDownId);
    }
}
function FillDropDownValuesOfProductUnit(idProduct, url, targetDropDownId) {
    var productId = idProduct.val();
    if (productId.length !== 0) {
        $.ajax({
            url: url + '?productId=' + productId,
            type: "GET",
            success: function (obj) {
                $("#" + targetDropDownId).html("");
                $("#" + targetDropDownId).append(
                    $('<option></option>').val(obj.Id).html(obj.Name));
            }
        });
    }
    else {
        clearValues(targetDropDownId);
    }
}
function clearValues(dropDownId) {
    $("#" + dropDownId).html('');
    $("#" + dropDownId).select2('data', null);
}
function FillObjectContactPersons(url, targetDropDownId, id) {
    var customerId = $('#CustomerId').val(); //can be used with FillDropDownValuesOf
    var objectId = id || $('#ContractorObjectId').val();
    if (customerId.length !== 0) {
        $.ajax({
            url: url,
            type: "GET",
            dataType: "JSON",
            data: { contractorId: customerId, objectId: objectId },
            success: function (objects) {
                $("#" + targetDropDownId).html("");
                $("#" + targetDropDownId).append($('<option></option>'));
                var isSelected = false;
                var onlyOne = objects.length == 1;
                var isSelected = false
                $.each(objects, function (i, obj) {
                    var option = $('<option></option>').val(obj.Id).html(obj.Name);
                    if (onlyOne) {
                        option.attr('selected', true);
                        isSelected = true;
                    }
                    if (!isSelected && obj.IsDefault) {
                        option.attr('selected', true);
                        isSelected = true;
                    }
                    if (obj.PaymentTerm) {
                        option.attr('data-term', obj.PaymentTerm);
                        if (!isSelected) {
                            var paymentTerm = $('select#PaymentTerm');
                            paymentTerm.val(obj.PaymentTerm);
                            paymentTerm.trigger('change');
                            isSelected = true;
                            option.attr('selected', true);
                        }
                    }
                    $("#" + targetDropDownId).append(option);
                });
            }
        });
    }
    else {
        clearValues(targetDropDownId);
    }
}
