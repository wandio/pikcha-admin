﻿$.widget("wandio.tableLineEditor", {
    options: {
        LineTemplateSelector: '#table-line-tpl',
        inputName: 'Lines'
    },
    _create: function () {
        var self = this;
        this.lineTemplate = $(this.options.LineTemplateSelector).html();
        this.addTrigger = this.element.find('a.add');
        this.form = this.element.parents('form');
        this.linesTable = this.element.find('table');
        this._on(this.form, {
            'submit': function (event) {
                var $this = $(event.target);
                if (!(event.isDefaultPrevented()) && $this.valid()) {
                    var lines = this._getLines();
                    //reindex inputs
                    $.each(lines, function (index, value) {
                        $(value).find(':input').each(function (inputIndex, input) {
                            var $input = $(input);
                            var currentName = $input.attr('name');
                            if (currentName) {
                                var newName = self.options.inputName + '[' + index + '].' + currentName.substring(currentName.lastIndexOf('.') + 1);
                                $input.attr('name', newName);
                            }
                        });
                    });
                }
            }
        });
        this._on(this.linesTable, {
            'blur :input': function (event) {
                $this = $(event.target);
                var valid = this.form.validate().element($this);
                if (!valid) {
                    event.preventDefault();
                    event.stopPropagation();
                    $this.focus();
                }
                else {
                    this._getParentLine($this).addClass('read-only');
                }
            },
            'focus :input': function (event) {
                this._makeEditable(this._getParentLine($(event.target)));
            },
            'click a.delete': function (event) {
                event.preventDefault();
                var self = this;
                var $this = $(event.target);
                var options;
                options = {
                    message: 'ნამდვილად გსურთ ჩანაწერის წაშლა?',
                    buttons: {
                        cancel: {
                            label: 'გაუქმება',
                            className: "btn-default",
                            callback: function () {
                                bootbox.hideAll();
                            }
                        },
                        approve: {
                            label: 'დიახ',
                            className: "btn-danger",
                            callback: function (event) {
                                event.preventDefault();
                                bootbox.hideAll();
                                $this.parents('tr').fadeOut('fast', function () {
                                    $(this).remove();
                                });
                                return false;
                            }
                        }
                    }
                };
                bootbox.dialog(options);
            }
        });
        this._on(this.addTrigger, {
            click: function (event) {
                event.preventDefault();
                this._addLine();
            }
        });
    },
    _addLine: function (batch) {
        var newLine = $(this.lineTemplate);
        this.linesTable.append(newLine);
        if (!batch) {
            $.validator.unobtrusive.reParse(this.form);
            reinitSelect2();
            newLine.removeClass('read-only');
        }
        return newLine;
    },
    _makeEditable: function ($line) {
        if ($line.hasClass('read-only')) {
            $line.removeClass('read-only');
        }
    },
    _getParentLine: function ($input) {
        return $input.closest('tr.line');
    },
    _getLines: function () {
        return this.linesTable.find('tr.line');
    },
    addLines: function (lines, onAddLine) {
        var self = this;
        if (lines) {
            $(lines).each(function () {
                var lineData = this;
                var $line = self._addLine(true);
                if (onAddLine) {
                    onAddLine($line, lineData);
                }
            });
        }
    },
    clearLines: function () {
        this._getLines().remove();
    }
});

function reinitSelect2() {
    if ($().select2) {
        $("tr.line select").select2('destroy');
        $("tr.line select").select2({
            allowClear: true
        });
    }
}