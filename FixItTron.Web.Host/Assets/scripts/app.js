﻿var App = function () {

    function ckIsDefined() {
        return typeof CKEDITOR != 'undefined';
    }

    //#region modalactions
    var handleModalActions = function () {
        $(document).on('click', 'a.js-entity-action:not(.custom), button.js-entity-action:not(.custom)', function (event) {
            event.preventDefault();
            var $this = $(this);
            handleEntityAction($this);
        });
        $(document).on('click', 'a.js-entity-action.selection-dependent', function (event) {
            event.preventDefault();
            var $this = $(this);
            handleEntityAction($this);
        });
        $(document).on('click', '.lazy-actions .entity-action .btn', function (event) {
            event.preventDefault();
            var $this = $(this);
            var spinClass = 'fa-spin fa-circle-o-notch';
            if (!$this.next().length) {
                var icon = $this.find('.fa');
                icon.addClass(spinClass);
                $.get($this.parents('.lazy-actions').data('actions-dropdown'), { id: $this.parents('tr').data('id') })
                    .done(function (data) {
                        if (data && $(data).children().length) {
                            $(data).insertAfter($this);
                        }
                        else {
                            $this.fadeOut('fast');
                        }
                    })
                    .always(function () {
                        icon.removeClass(spinClass);
                    });
            }
        });
    };

    var handleEntityAction = function ($action, successCallback, cancelCallback) {
        Metronic.blockUI({ animate: true });

        var url = $action.attr('href');

        if ($action.hasClass('selection-dependent')) {
            var allSelected = $('.js-selection-all').is(':checked');

            var queryPart = '';
            var serializedIds;

            if (allSelected) {
                var idsRequired = $('.js-selection-all').data('required-ids');
                if (idsRequired) {
                    serializedIds = $('.f-select-row input:checked').serialize();
                    queryPart = serializedIds;
                }
                else {
                    //queryPart = 'allSelected=' + allSelected;
                    serializedIds = $('.f-select-row input').serialize();
                    queryPart = serializedIds;
                }
            }
            else {
                if ($('.f-select-row input:checked').length == 0) {
                    Metronic.unblockUI();
                    return;
                }
                serializedIds = $('.f-select-row input:checked').serialize();
                queryPart = serializedIds;
            }

            url += '?' + queryPart;//'&' + queryPart;
        }

        $.get(url, function (result) {
            if (result.redirect) {
                Metronic.blockUI({ animate: true });
                window.location.href = result.redirect;
            }
            else {
                Metronic.unblockUI();
                setupModalActionModal(result, successCallback, cancelCallback);
            }
        }).done(function () {
            $('.product-browser').productBrowser();
            $('.js-popup-switcher').switcher({ dependents: 'js-checker-container' });
            handleAutoComplete();
            if (ckIsDefined()) {
                CKEDITOR.replace('ck', {
                    customConfig: 'custom_config.js'
                });
            }
        });
    };

    var setupModalActionModal = function (data, successCallback, cancelCallback) {
        var $existing = $('#js-entity-action-modal');
        var $modal = $(data);
        var $form;
        if ($existing.length) {
            if (data.success != undefined) {
                $existing.find('.modal-footer').hide();
                var alert = data.success ? "alert-success" : "alert-danger";
                var message = data.html != undefined ? data.html : data.message;
                $existing.find('.modal-body').empty().append($('<div class="alert ' + alert + '"></div>').html(message));
            }
            else {
                $existing.html('<div class="modal-backdrop in"></div>' + $modal.html());
            }
            $form = $existing.find('form');
        }
        else {
            $('body').append($modal);
            $modal.filter('.modal').modal();
            $modal.on('hidden.bs.modal', function (e) {
                $(this).remove();
            })
            $form = $modal.find('form');
        }
        if ($form.length) {
            $.validator.unobtrusive.reParse($form);
            $form.on('submit', function (event) {
                event.preventDefault();
                if ($form.valid()) {
                    Metronic.blockModal();
                    if ($form.data('uploader')) {
                        var fileUpload = $form.fileupload({
                            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                            maxFileSize: 100000
                        });

                        if ($('#doc-file').val() != "") {
                            fileUpload.fileupload('send', {
                                files: $('#doc-file').prop('files'),
                                url: $form.data('upload-to'),
                                formData: $form.find(':input').serializeArray()
                            }).done(function (result) {
                                Metronic.unblockModal();
                                //window.location.reload();
                                if (result.success) {
                                    if (successCallback) {
                                        successCallback(result);
                                        $modal.modal('hide');
                                    }
                                    else if (result.redirect) {
                                        window.location.href = result.redirect;
                                    }
                                    else {
                                        window.location.reload();
                                    }
                                }
                                else {
                                    setupModalActionModal(result, successCallback, cancelCallback);
                                }
                            });
                        }
                        else {
                            postMethod($form, $modal, successCallback);
                        }
                    }
                    else {
                        postMethod($form, $modal, successCallback);
                    }

                }
            });
        }
        Metronic.unblockModal();
    };

    var postMethod = function ($form, $modal, successCallback, cancelCallback) {

        if (ckIsDefined()) {
            for (instance in CKEDITOR.instances)
                CKEDITOR.instances[instance].updateElement();
        }

        $.post($form.attr('action'), $form.serialize(), function (result) {
            if (result.success) {
                if (successCallback) {
                    successCallback(result);
                    $modal.modal('hide');
                }
                else if (result.redirect) {
                    window.location.href = result.redirect;
                }
                else {
                    window.location.reload();
                }
            }
            else {
                setupModalActionModal(result, successCallback, cancelCallback);
            }
        }).done(function () {
            Metronic.unblockModal();
            $('.product-browser').productBrowser();
            $('.js-popup-switcher').switcher({ dependents: 'js-checker-container' });
            handleAutoComplete();
            if (ckIsDefined()) {
                CKEDITOR.replace('ck', {
                    customConfig: 'custom_config.js'
                });
            }
        });
    };

    var handleRedirect = function (redirect) {
        var urlToRedirect = new URI(redirect);
        if (urlToRedirect.fragment() != null && urlToRedirect.fragment() != '') {
            window.location.href = redirect;
            location.reload();
        }
        else {
            window.location.href = urlToRedirect;
        }
    };
    //#endregion

    var handleBlocking = function () {
        $.blockUI.defaults.message = '<div class="page-spinner-bar"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>';
        $.blockUI.defaults.css = { padding: 0, margin: 0, width: "30%", top: "40%", left: "35%", 'text-align': 'center' };
        $.blockUI.defaults.overlayCSS.opacity = '0.1';
        $.blockUI.defaults.overlayCSS.background = 'black';
        $.blockUI.defaults.baseZ = 160000;
    };

    var handleSelect2 = function () {
        if ($().select2) {
            $('.select2me').select2('destroy');
            $('.select2me').select2({
                placeholder: "",
                allowClear: true,
                minimumResultsForSearch: 5
            });
        }
    };

    var handleSelection = function () {
        function isNullOrEmpty(number) {
            return number == null || number == '';
        }

        $(document).on('click', '.f-select-row input', function () {
            var $this = $(this);

            //if ($this.is(':checked')) {
            //    $this.parent('span').removeClass('checked');
            //    $this.removeAttr('checked');
            //} else {
            //    $this.parent('span').addClass('checked');
            //    $this.attr('checked', true);
            //}

            $('#uniform-f-select-all span').removeClass('checked');
            $('.js-selection-all').prop('checked', false);

            var container = $('#js-selection-counter span');
            var totalCount = $('.f-select-row span[class*=checked]').length;

            container.text(totalCount);
            $('#js-selection-counter').show();
        });

        $(document).on('click', '.js-selection-all', function () {
            var $this = $(this);
            var total = $('.js-total-count').data('totalcount');

            if (isNullOrEmpty(total)) {
                total = 0;
            }

            $('#js-selection-counter').show();

            if ($this.is(':checked')) {

                $('.f-select-row .checker span').addClass('checked');
                //$('.f-select-row .checker span').each(function() {
                //    var $this = $(this);
                //        if (!$this.hasClass('checked')) {
                //            $this.addClass('checked');
                //        }

                //});

                $('.f-select-row input').prop('checked', true);

                $('#js-selection-counter span').text(total);
            }
            else {
                $('.f-select-row .checker span').removeClass('checked');
                $('.f-select-row input').prop('checked', false);

                $('#js-selection-counter span').text(0);
            }
        });

        if ($('.js-selection-all').is(':checked')) {
            $('.f-select-row .checker span').addClass('checked');
            $('.f-select-row input').prop('checked', true);
        }
    };

    var handlePickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            //language: 'ka',
            rtl: false,
            autoclose: true,
            format: 'dd.mm.yyyy',
            todayHighlight: true
            //startDate: new Date()
        });

        $('.colorpicker').colorpicker();

        if (jQuery().timepicker) {
            $('.timepicker-24').timepicker({
                defaultTime: false,
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });

            // handle input group button click
            $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function (e) {
                e.preventDefault();
                $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
            });
        }
    };

    var handleSwitcher = function () {
        $.widget("custom.switcher", {
            options: {
                dependents: 'js-checker-container'
            },
            _create: function () {
                var self = this;
                this._check(self);

                this._on(self.element, {
                    click: function (e) {
                        this._check(self);
                    }
                });
            },

            _check: function (element) {
                var self = element;
                var selector = '.' + self.options.dependents;
                if (!self.element.is(':checked')) {
                    $(selector).attr('readonly', true);
                }
                else {
                    $(selector).attr('readonly', false);
                }
            }
        });
    };

    var handleTooltip = function () {
        $('[data-toggle="tooltip"]').tooltip();
    };

    var handleActiveTab = function () {
        $(document).on('click', '.js-sidemenu a, .js-reset-tab', function (e) {
            $.removeCookie('_activeTab');
        });
        $(document).on('click', '.tabbable:not(.custom) [data-toggle="tab"]', function (e) {
            var $this = $(this);
            $.removeCookie('_activeTab');
            $.cookie('_activeTab', $this.attr('href'), { expires: 1 });
        });
        var tabHash = $.cookie('_activeTab');

        var splitedHref = window.location.href.split('#');
        if (splitedHref.length > 1) {
            tabHash = splitedHref[splitedHref.length - 1];
        }

        if (tabHash) {
            var tabid = tabHash.substr(1);
            $('a[href="#' + tabid + '"]').parents('.tab-pane:hidden').each(function () {
                var tabid = $(this).attr("id");
                $('a[href="#' + tabid + '"]').click();
            });
            $('a[href="#' + tabid + '"]').click();
        }
    };

    var handleContractorSelectors = function () {
        $('.customer-selector').contractorSelector({
            placeholder: 'მოძებნეთ კლიენტი'
        });
        //$('.vendor-selector').contractorSelector({
        //    placeholder: 'მოძებნეთ მომწოდებელი'
        //});
        //$('.contractor-selector').contractorSelector({
        //    placeholder: 'მოძებნეთ კონტრაქტორი'
        //});
    };

    var handlePopover = function () {
        $('[data-toggle="popover"]').popover({
            html: true,
            trigger: 'click',
        });

        $(document).on('click', function (e) {
            if (!$(e.target).is('.js-popover') && !$(e.target).is('.popover-content')) {
                $('[data-toggle="popover"]').popover('hide');
            }
        });
    };

    var handleTableSubItems = function () {
        $(document).on('click', 'tr.parent span.row-details', function () {
            var $this = $(this);
            if ($this.hasClass('row-details-close')) {
                $this.removeClass('row-details-close');
                $this.addClass('row-details-open');
            }
            else {
                $this.removeClass('row-details-open');
                $this.addClass('row-details-close');
            }
            $this.parents('.parent').nextUntil('tr.parent').fadeToggle('fast');
        });
    };

    var handlePrototypeHelpers = function () {

        Number.prototype.toQuantity = function () {
            return this.toFixed(3);
        };

        Number.prototype.toAmount = function (highPrecision) {
            if (highPrecision) {
                return this.toFixed(5);
            }
            return this.toFixed(2);
        };

        Number.prototype.toCost = function () {
            return this.toFixed(4);
        };

        String.prototype.toAmount = function (highPrecision) {
            var amount = parseFloat(this);
            if (highPrecision) {
                return amount.toFixed(5);
            }
            return amount.toFixed(2);
        };

        //"{0} is dead, but {1} is alive! {0} {2}".format("ASP", "ASP.NET")
        if (!String.prototype.format) {
            String.prototype.format = function () {
                var args = arguments;
                return this.replace(/{(\d+)}/g, function (match, number) {
                    return typeof args[number] != 'undefined'
                      ? args[number]
                      : match
                    ;
                });
            };
        }

        //String.format('{0} is dead, but {1} is alive! {0} {2}', 'ASP', 'ASP.NET');
        if (!String.format) {
            String.format = function (format) {
                var args = Array.prototype.slice.call(arguments, 1);
                return format.replace(/{(\d+)}/g, function (match, number) {
                    return typeof args[number] != 'undefined'
                      ? args[number]
                      : match
                    ;
                });
            };
        }
    };

    var handleDelay = (function () {
        var timer = 0;
        return function (callback, ms) {
            clearTimeout(timer);
            timer = setTimeout(callback, ms);
        };
    })();

    var handleAutoComplete = function () {
        //$(".js-autocomplete:not(.ui-autocomplete-input)").each(function (index, value) {
        //    var $value = $(value);

        //    var trigger = $value.parent().find('.js-autocomplete-trigger');

        //    $value.autocomplete({
        //        source: function (request, response) {
        //            var $city = $('#CityId');
        //            var $district = $('#DistrictId');
        //            $.getJSON($value.data('autocomplete-source'), { term: request.term, cityId: $city.val(), districtId: $district.val() }, response);
        //        },
        //        minLength: 0
        //    });

        //    trigger.button().click(function () {
        //        $value.autocomplete("search", '');
        //    });
        //});

        $(".js-autocomplete:not(.ui-autocomplete-input):not(.attached)").each(function (index, value) {
            var $value = $(value);

            var streets = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: $value.data('autocomplete-source'),
                    replace: function (url, query) {
                        var $city = $('#CityId');
                        var $district = $('#DistrictId');
                        var uri = new URI(url);
                        uri.addQuery('term', query);
                        uri.addQuery('cityId', $city.val());
                        uri.addQuery('districtId', $district.val());
                        return uri.toString();
                    },
                    filter: function (response) {
                        return response;
                    },
                    cache: false
                }
            });

            streets.initialize();

            $value.typeahead({
                hint: false,
                minLength: 1
            }, {
                displayKey: 'label',
                source: streets.ttAdapter()
            });

            $value.addClass('attached');
        });
    }

    var handleFieldTypes = function () {
        $(document).on('change', '.js-select-fieldtype', function () {
            var $this = $(this);
            var answersContainer = $('.js-answer-container');

            if ($this.find(':selected').data('fieldtype')) {
                answersContainer.show();
            }
            else {
                answersContainer.hide();
            }
        });
    };

    var handleBodyTemplates = function () {
        $(document).on('change', '.js-select-subject', function () {
            var $this = $(this);
            var bodyContainer = $('.js-bodytemplate-input');

            var msg = $this.find(':selected').data('bodytemplate');
            bodyContainer.val(msg);

        });
    };

    var handleDeliveryTypes = function () {
        $(document).on('change', '.js-select-deliverytype', function () {
            var $this = $(this);
            var bodyContainer = $('.js-body-container');
            var CKbodyContainer = $('.js-ckbody-container');

            if ($this.find(':selected').data('deliverytype')) {
                CKbodyContainer.show();
                bodyContainer.hide();
            }
            else {
                bodyContainer.show();
                CKbodyContainer.hide();
            }
        });
    };

    var handleAgreementTotalCost = function () {
        $(document).on('change', '.js-agreement-period, .js-agreement-costvalue', function () {
            //var $this = $(this);
            var cost = $('.js-agreement-costvalue').val();
            var period = $('.js-agreement-period');
            var totalcost = $('.js-agreement-totalcost');

            if (period.find(':selected').val() == 'Month') {
                cost *= 12;
            }
            totalcost.html(parseFloat(cost).toFixed(2));
        });
    };

    var handleBackgroungColor = function () {
        $(document).on('keyup', '.js-backgroundcolor-input', function () {
            changeColor($(this));
        });
        $(document).on('change', '.js-backgroundcolor-input', function () {
            changeColor($(this));
        });
    };

    //function changeColor($this) {
    //    if ($this.val().length != 7) return;

    //    var emptyContainer = $('.event-user-image');
    //    var coloredContainer = $('.event-logo-outer-container');

    //    coloredContainer.css('background-color', $this.val());
    //    emptyContainer.css('background-color', 'white');
    //}

    var handleExport = function () {
        $(document).on('click', 'a.js-entity-action.export', function (event) {
            event.preventDefault();
            $('.js-table.dataTable').twable('export');
        });
    }

    return {
        init: function () {
            $(document).ajaxError(function (event, jqxhr, settings, thrownErro) {
                //it readyState is 0 when many ajax requests are occures in userSeart (orderDetails)
                if (jqxhr.readyState !== 0) {
                    Metronic.unblockModal();
                    Metronic.unblockUI();
                    bootbox.alert('Oops! Something went wrong');
                }
            });

            $(document).ajaxStop(function () {
                $.unblockUI();
            });

            handleBackgroungColor();

            //$(document).ajaxSuccess(function () {
            //    Metronic.initAjax();
            //});

            $(document).ajaxComplete(function () {
                Metronic.initAjax();
                handlePickers();
                $('.icp-auto').iconpicker();
            });

            handlePrototypeHelpers();

            handlePickers();
            handleSwitcher();
            handleTooltip();
            handleModalActions();
            handleActiveTab();
            handleContractorSelectors();
            handlePopover();
            handleExport();

            //handleFieldTypes();
            //handleAgreementTotalCost();
            //handleDeliveryTypes();
            //handleSelection();
            //handleBodyTemplates();

            handleTableSubItems();
            $('.icp-auto').iconpicker();
            $('.slimScrollDiv').slimScroll({ height: '250px' });
        },
        initEntityAction: function ($action, successCallback, cancelCallback) {
            handleEntityAction($action, successCallback, cancelCallback);
        },
        initDelay: function (callback, ms) {
            handleDelay(callback, ms);
        },
        initAutoComplete: function () {
            handleAutoComplete();
        }
    };
}();

function changeColor($this) {
    if ($this.val().length != 7) return;

    var emptyContainer = $('.event-user-image');
    var coloredContainer = $('.event-logo-outer-container');

    coloredContainer.css('background-color', $this.val());
    emptyContainer.css('background-color', 'white');
}