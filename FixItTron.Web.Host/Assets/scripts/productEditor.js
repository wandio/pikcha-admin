﻿var productEditor = function () {

    handleService = function () {
        $(document).on('change', 'select.select-product, input.select-product', function () {
            var $this = $(this);
            var $line = getLine($this);
            var $measure = $line.find('select.select-measure');
            var $baseMeasure = $line.find('select.select-base-measure');

            if ($this.val()) {
                var url = $this.data('details-url');
                var customerVal = $('#CustomerId').val();
                var agreementVal = $('#AgreementId').val();
                var lineTpl = $('#product-tpl').html();
                var tplOptions = $(lineTpl).find('option');

                var uri = new URI(url);
                uri.addQuery('id', $this.val());
                uri.addQuery('customerId', customerVal);
                uri.addQuery('agreementId', agreementVal);
                $.get(uri, function (data) {
                    $line.data('product-details', data);

                    var existingValue = $measure.val();
                    var allowedMeasures = [];
                    allowedMeasures.push(data.BaseMeasure.MeasureId);

                    allowedBaseOptions = filterMeasures(allowedMeasures, tplOptions);
                    $baseMeasure.select2("destroy").empty().append(allowedBaseOptions.clone());
                    $baseMeasure.val(data.DefaultMeasure.MeasureId);

                    $.each(data.Measures, function (index, value) {
                        allowedMeasures.push(value.MeasureId);
                    });

                    //filter out unallowed options from data
                    var allowedOptions = filterMeasures(allowedMeasures, tplOptions);

                    $measure.select2("destroy").empty().append(allowedOptions.clone());
                    $measure.val(data.DefaultMeasure.MeasureId);

                    setCostByCurrentMeasure($line, data);

                    if (data.TaxGroupId) {
                        $line.find('select.select-taxgroup').val(data.TaxGroupId).trigger('change');
                    }

                    setQuantity($line, 1, true);
                });
            }
            else {
                clearLine($line);
            }
        });

        $(document).on('change', '.select-taxgroup, .js-quantity, .js-salesprice', function () {
            var $this = $(this);
            var $line = getLine($this);
            if ($this.is('.js-quantity')) {
                var $measure = $line.find('select.select-measure');
                var $baseMeasure = $line.find('select.select-base-measure');

                if ($measure.val() && $baseMeasure.val() && $measure.val() == $baseMeasure.val()) {
                    var baseQuantity = $line.find('.js-base-quantity');
                    baseQuantity.val($this.val());
                }
            }
            recalculateLineTotal($line);
        });

        $(document).on('change', '.select-measure', function () {
            var $this = $(this);
            var $line = getLine($this);
            var $baseMeasure = $line.find('select.select-base-measure');
            //if ($this.val()) {
            //    if ($this.val() == $baseMeasure.val()) {
            //        $baseMeasure.closest('.form-group').hide();
            //    }
            //    else {
            //        var baseQuantity = $line.find('.js-base-quantity');
            //        baseQuantity.val('');
            //        $baseMeasure.closest('.form-group').show();
            //    }
            //}
            //else {
            //    $baseMeasure.closest('.form-group').hide();
            //}

            var productData = getSelectedProductData($line);
            if (productData) {
                setCostByCurrentMeasure($line, productData);
                recalculateLineTotal($line);
            }
        });
    };

    getCurrentMeasure = function ($line, data) {
        var value = $line.find('select.select-measure').val();
        if (value) {
            if (value == data.BaseMeasure.MeasureId) {
                return data.BaseMeasure;
            }
            var measure = $.grep(data.Measures, function (e) { return e.MeasureId == value; })[0];
            return measure;
        }

        return data.BaseMeasure;
    };

    setCostByCurrentMeasure = function ($line, data) {
        var cost = formatAmount(getCostInMeasure(data, getCurrentMeasure($line, data)));
        setCost($line, cost);
        recalculateLineTotal($line);
    };

    getSelectedProductData = function ($line) {
        return $line.data('product-details');
    };

    getCostInMeasure = function (data, measure) {
        if (!data) {
            if (console) {
                console.log('data is null');
                return null;
            }
        }
        var salesPrices = data.SalesPrice;
        var measureId = measure.MeasureId;
        for (var i = 0; i < salesPrices.length; i++) {
            var salesPrice = salesPrices[i];
            if (salesPrice.MeasureId == measureId) {
                return salesPrice.Price;
            }
        }
    };

    getQuantity = function ($line) {
        var value;
        value = $line.find('input.js-quantity').val();
        return parseFloat(value);
    };

    setQuantity = function ($line, quantity, triggerChange) {
        var quantity = $line.find('input.js-quantity').val(quantity);
        if (triggerChange) {
            quantity.trigger('change');
        }
    }

    getCost = function ($line) {
        var value;
        value = $line.find('input.js-salesprice').val();
        return parseFloat(value);
    };

    setCost = function ($line, cost) {
        var parsed = parseFloat(cost);
        if (!isNaN(parsed)) {
            $line.find('input.js-salesprice').val(formatAmount(parsed));
        }
    };

    recalculateLineTotal = function ($line) {
        var currentCost = getCost($line);
        var currentQuantity = getQuantity($line);

        var $tax = $line.find('select.select-taxgroup');
        var $taxAmount = $line.find('.js-tax-amount');
        var $amountWithoutTax = $line.find('.js-amount-without-tax');

        if ($tax.val()) {
            var percent = $tax.find('option:selected').data('percent');
            var taxAmount = 0;
            var amountWithoutTax = 0;
            var cost = getCost($line);
            if (cost) {
                amountWithoutTax = formatAmount(cost / (1 + (percent / 100)));
                taxAmount = formatAmount(cost - amountWithoutTax);
                var formatedWithoutTax = formatAmount(amountWithoutTax * currentQuantity);
                $amountWithoutTax.val(formatedWithoutTax);
                $taxAmount.val(taxAmount * currentQuantity);
            }
        }
        else {
            $taxAmount.val('');
            $amountWithoutTax.val(formatAmount(cost));
        }

        setLineTotalPrice($line, currentCost * currentQuantity);
    };

    setLineTotalPrice = function ($line, lineTotal) {
        var span = $line.find('span.amount');
        if (isNaN(lineTotal)) {
            span.text('');
        }
        else {
            span.text(formatAmount(lineTotal));
        }
    };

    clearLine = function ($line) {
        $line.find('select.select-measure').select2("destroy").empty();
        $line.find('select.select-base-measure').select2("destroy").empty();

        $line.find('.js-tax-amount').val('');
        $line.find('.js-amount-without-tax').val('');
        $line.find('.js-salesprice').val('');

        $(".line select").select2({
            allowClear: true
        });
    };

    getLine = function (element) {
        return element.closest('.line');
    };

    filterMeasures = function (allowedMeasures, options) {
        return $(options).filter(function (index) {
            var val = $(this).attr('value');
            //allow empty
            if (!val) {
                return true;
            }
            var index = allowedMeasures.indexOf(parseInt(val));
            return index != -1;
        });
    };

    formatAmount = function (number) {
        if (isNaN(number)) {
            return;
        }
        return number.toAmount();
    };

    return {
        initService: function () {
            handleService();
        }
    };

}();