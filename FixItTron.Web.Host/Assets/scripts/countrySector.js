﻿$(document).ready(function () {
    var citiesUrl = "";
    var districtsurl = "";
    var streetsurl = "";
    var ditrictListurl = "";
    var streetListurl = "";

    $(document).on('change', '#CountryId', function () {
        var countryDropDown = $('#CountryId');
        var cityDropDown = $('#CityId');
        var districtDropDown = $('#DistrictId');
        var streetDropDown = $('#StreetId');
        citiesUrl = countryDropDown.data("citys-url");
        districtsurl = countryDropDown.data("districts-url");
        streetsurl = countryDropDown.data("streets-url");
        ditrictListurl = countryDropDown.data("ditrictlist-url");
        streetListurl = countryDropDown.data("streetlist-url");

        loadCountriesCities(countryDropDown.val());
        FillDropDownValues(ditrictListurl, districtDropDown, countryDropDown.val());
        FillDropDownValues(streetListurl, streetDropDown, countryDropDown.val());

        cityDropDown.on('change', function () {
            loadCityDistricts(cityDropDown.val());
            loadStreetsByCities(cityDropDown.val());
        });
        districtDropDown.on('change', function () {
            loadStreetsByDistricts(districtDropDown.val());
        });
    });


    function loadCountriesCities(countryId) {
        var url = citiesUrl;
        var city = $("#CityId");
        FillDropDownValues(url, city, countryId);
    }
    function loadCityDistricts(cityId) {
        var url = districtsurl;
        var district = $("#DistrictId");
        FillDropDownValues(url, district, cityId);
    }
    function loadStreetsByCities(cityId) {
        var url = streetsurl;
        var street = $("#StreetId");
        FillDropDownValues(url, street, cityId);
    }
    function loadStreetsByDistricts(districtId) {
        var url = streetsurl;
        var street = $("#StreetId");
        FillDropDownValuesStreets(url, street, districtId);
    }
    function FillDropDownValues(url, target, byId) {
        if (byId != undefined && byId != "") {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                data: { id: byId },
                success: function (objects) {
                    target.html("");
                    target.append($('<option></option>'));
                    $.each(objects, function (i, obj) {
                        target.append(
                            $('<option></option>').val(obj.Id).html(obj.Name));
                    });
                }
            });
        }
        else {
            clearValues(target);
        }
    }
    function FillDropDownValuesStreets(url, target, districtId) {
        if (districtId != undefined && districtId != "") {
            $.ajax({
                url: url,
                type: "GET",
                dataType: "JSON",
                data: { districtId: districtId },
                success: function (objects) {
                    target.html("");
                    target.append($('<option></option>'));
                    $.each(objects, function (i, obj) {
                        target.append(
                            $('<option></option>').val(obj.Id).html(obj.Name));
                    });
                }
            });
        }
        else {
            var cityId = $("#CityId").val();
            loadStreetsByCities(cityId);
        }
    }

    function clearValues(dropDownId) {
        $("#" + dropDownId).html('');
        $("#" + dropDownId).select2('data', null);
    }

    App.initAutoComplete();
});