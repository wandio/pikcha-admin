﻿var priceRuleEditor = function () {

    var handleFilters = function () {
        $(document).on('change', '.js-filterby', function () {
            var $this = $(this);
            var val = $this.val();
            var categoryTpl = $($('#js-category-template').html());
            var classTpl = $($('#js-class-template').html());

            var attrContainer = $('.js-attribute-container');

            if (val == 'Class') {
                attrContainer.empty().html(classTpl);
            }
            else if (val == 'Category') {
                attrContainer.empty().html(categoryTpl);
            }
            else {
                attrContainer.find('select').empty();
            }

            attrContainer.find('select').select2({
                placeholder: "",
                allowClear: true,
                minimumResultsForSearch: 5
            });
        });

        $(document).on('click', '.js-remove-selected-product', function (e) {
            $(this).parents('tr.line').remove();
            var lines = $('form.js-pricerulefilter-form').find('table.multiple-selected-products tr.line');
            reIndexFilters(lines);
        });

        function reIndexFilters(lines) {
            $.each(lines, function (index, value) {
                $(value).find(':input').each(function (innerIndex, input) {
                    var $input = $(input);
                    var currentName = $input.attr('name');
                    if (currentName) {
                        var newName = 'ProductIds[' + index + '].' + currentName.substring(currentName.lastIndexOf('.') + 1);
                        $input.attr('name', newName);
                    }
                });
            });
        }
    };

    var handleSelect2 = function () {
        if ($().select2) {
            $('.select2me').select2('destroy');
            $('.select2me').select2({
                placeholder: "",
                allowClear: true,
                minimumResultsForSearch: 5
            });
        }
    };

    return {
        init: function () {
            handleFilters();
        }
    };
}();