﻿var hubHelper = function () {
    function updateColorOnNewNotification() {
        $('.notifyColor').css("background-color", "#1caf9a");
    }

    return {
        updateInboxCount: function (element, count) {
            $('.notifyColor').css("background-color", "red");
            var hasClass = $(".unreadCount").hasClass("badge badge-default");
            if (!hasClass) {
                $(".unreadCount").addClass("badge badge-default");
            }
            element.html(count);
            setTimeout(updateColorOnNewNotification, 20000);
        },
        updateInboxNotificationsList: function (targetElement, unreadNotification) {
            targetElement.prepend("<li><a href='/Home/InboxNotifications/Details/" + unreadNotification.NotificationId + "'>" +
                                "<span class='time'>" + unreadNotification.RecieveDateAgoText + "</span>" +
                                "<span class='details'>" +
                                    "<span class='label label-sm label-icon label-danger'>" +
                                    "<i class='fa fa-bolt'></i>" +
                                    "</span>"+unreadNotification.MessageBodyPartial +"</span></a></li>");


        }
    };
}();