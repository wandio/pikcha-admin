﻿var employeeServiceCheck = (function () {
    return {
        init: function (isQuotation) {
            $(document).on("click", ".btnEmployeeCheckYes", function (e) {
                e.preventDefault();
                changeCheckboxes(false);
                $("#scheduleForm").submit();
            });

            $(document).on("click", ".btnEmployeeCheckNo", function () {
                changeCheckboxes(false);
            });
            function changeCheckboxes(status) {
                $(".checkbox-inline1 input").each(function (k, v) {
                    $(v).attr("disabled", status);
                });
                $("#confirmMessage").hide();
                $("#modalFooter").show();
            }
            function showScheduleFormValidation(errortext) {
                $("#scheduleFormValidation").css("display", "block").text(errortext + "\n")
            }
            function hideScheduleFormValidation() {
                $("#scheduleFormValidation").css("display", "none").text("")
            }
            $(document).on("click", ".btnSubmit", (function (e) {
                e.preventDefault();

                var serviceIds = new Array();
                var employeeId = $("select#EmployeeId").val();
                if (employeeId == "") {
                    showScheduleFormValidation("გთხოვთ აირჩიეთ ბრიგადა");
                    return false;
                }
                var url = $(this).data("urlemployeeservicescheck");
                $(".checkbox-inline1 input:checked").each(function (k, v) {
                    var elementId = v.getAttribute("id").split("__")[0].toString();
                    var serviceId = $("#" + elementId + "__ServiceId").val();
                    serviceIds.push(serviceId);
                });
                if (serviceIds == 0) {
                    showScheduleFormValidation("გთხოვთ აირჩიეთ სერვისები");
                    return false;
                }
                $(".checkbox-inline1 input").each(function (k, v) {
                    $(v).attr("disabled", true);
                });

                $.ajax({
                    url: url,
                    traditional: true,
                    data: { employeeId: employeeId, serviceIds: serviceIds, isQuotation: isQuotation },
                    success: function (data) {
                        if (data.hasServices) {
                            $(".checkbox-inline1 input").each(function (k, v) {
                                $(v).attr("disabled", false);
                            });
                            $("#scheduleForm").submit();

                        }
                        else {
                            $("#modalFooter").hide();
                            $("#confirmMessage").show();
                        }
                    }
                });
            }));

        }
    };
}());