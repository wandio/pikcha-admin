﻿var statuses = new Array();
var copyUrl = '';
var dropUrl = "";
var addScheduleUrl = "";
var addScheduleUrlQuotation = "";
var employeeHasScheduleInDateUrl = "";
var employeeServicesUrl = "";
var employeehaschoosenservicesurl = "";
var employeeServicesQuotationUrl = "";
var employeeListUrl = "";
var schedulesListUrl = "";
var schedulesListUrlQuotation = "";
var dropQuotationUrl = "";

var start = new moment();
var end = new moment().add(5, 'days');
var endday = end.format('D');
var startday = start.format('D');
var stepDays = moment.duration(end.diff(start)).days();

var scheduleDropStartEmployeeId = 0;
var messageError = "თანამშრომლის კვალიფიკაცია არ შეესაბამება არჩეულ სერვის(ებ)ს.";
var months = ['იანვარი', 'თებერვალი', 'მარტი', 'აპრილი', 'მაისი', 'ივნისი', 'ივლისი', 'აგვისტო', 'სექტემბერი', 'ოქტომბერი', 'ნოემბერი', 'დეკემბერი'];

$(function () {
    $("#statusSelect").multiselect({
        selectAllText: 'ყველა',
        allSelectedText: "ყველა",
        nonSelectedText: "სტატუსი",
        onChange: function (option, checked, select) {
            if (!checked) {
                var index = statuses.indexOf(option.val());
                statuses.splice(index, 1);
            }
            $("#statusSelect option:selected").each(function (data, val) {
                if (statuses.indexOf(val.value) == -1) {
                    statuses.push(val.value);
                }
            });
            $('#calendar').fullCalendar('rerenderEvents');
        }
    })

    var urlContainer = $("#urls");
    copyUrl = urlContainer.data('copyurl');
    dropUrl = urlContainer.data('dropurl');
    addScheduleUrl = urlContainer.data('addscheduleurl');
    addScheduleUrlQuotation = urlContainer.data('addscheduleurlquotation');
    employeeHasScheduleInDateUrl = urlContainer.data('employeehasscheduleindateurl');
    employeeServicesUrl = urlContainer.data('employeeservicesurl');
    employeehaschoosenservicesurl = urlContainer.data('employeehaschoosenservicesurl');
    employeeServicesQuotationUrl = urlContainer.data('employeeservicesquotationurl');
    employeeListUrl = urlContainer.data('employeelisturl');
    schedulesListUrl = urlContainer.data('scheduleslisturl');
    schedulesListUrlQuotation = urlContainer.data('scheduleslisturlquotation');
    dropQuotationUrl = urlContainer.data('dropquotationurl');
});

function changeCheckboxStatus(makeDisable) {
    $("#dialog-message input.checkservices").each(function (k, v) { $(v).attr("disabled", makeDisable); })
}


function HandleEmployeeServiceCheckConfirmMessage(showMessage) {
    if (showMessage) {
        $("#confirmMessageSelector").show();
        $(".ui-dialog-buttonset").hide();
        changeCheckboxStatus(true);
    }
    else {
        $("#confirmMessageSelector").hide();
        $(".ui-dialog-buttonset").show();
        changeCheckboxStatus(false);

    }
}

function clearStatusesSelect() {
    statuses = new Array();
    $("#statusSelect").multiselect('clearSelection');
    $(".checked").attr("class", "");
}


function ClearValidationMessage() {
    var validationLabel = $("#validation");
    $(validationLabel).text("");
}
function ShowValidationMessage(text) {
    ClearValidationMessage();
    var validationLabel = $("#validation");
    validationLabel.removeClass("display-hide");
    $(validationLabel).append(text + '\n');
}

function clearPopupValues() {
    $("#SupporterEmployeeId").val('');
    $('#dialog-message').find('textarea').val('');
    $('#dialog-message').find('input:checkbox:checked').trigger("click");
    $('#dialog-message').find('input:checkbox:checked').removeAttr("checked");
}

function renderPopover(event) {

    if (event.isNew) {
        return;
    }
    var tooltip = '<div class="tooltipevent">' +
                    "<label style='margin:5px;'>შეკვეთის #: " + "<b>" + event.order.code + "</b>" + "</label>" + "</br>" +
                    "<label style='margin:5px'>დრო: " + "<b>" + event.order.orderDate + "</b>" + "</label>" + "</br></br>" +
                    "<label style='margin:5px'>კლიენტი: " + "<b>" + event.order.customer + "</b>" + "</label>" + "</br>" +
                    "<label style='margin:5px'>ტელეფონი: " + "<b>" + event.order.phone + "</b>" + "</label>" + "</br>" +
                    "<label style='margin:5px'>მობილური: " + "<b>" + event.order.mobilePhone + "</b>" + "</label>" + "</br>" +
                    "<label style='margin:5px'>დაკვეთა: " + "<b>" + event.order.job + "</b>" + "</label>" + "</br>" +
                    "<label style='margin:5px'>ობიექტი: " + "<b>" + event.order.address + "</b>" + "</label>" + "</br>" +
                    "<label style='margin:5px'>ბრიგადა: " + "<b>" + event.employee + "</b>" + "</label>" + "</br>" +
                    "<label style='margin:5px'>სტატუსი: " + "<b>" + event.status + "</b>" + "</label>" + "</br>" +
                    "<label style='margin:5px'>თანხა: " + "<b>" + event.order.price + "</b>" + "</label>" + "</br>";
    if (event.isCanceled) {
        tooltip += "<label style='margin:5px'>გაუქმების მიზეზი: " + "<b>" + event.cancelReason + "</b>" + "</label>" + "</br>";
    }
    $.each(event.icon.split(','), function (i, data) {
        tooltip += "</hr><label style='margin:5px;'> <i style='margin-left:5px' class='" + data + "'></i></label>"
    })
    '</div>';
    $("body").append(tooltip);
    $(this).mouseover(function (e) {
        $(this).css('z-index', 10000);
        $('.tooltipevent').fadeIn('500');
        $('.tooltipevent').fadeTo('1000', 1.9);
    }).mousemove(function (e) {
        $('.tooltipevent').css('top', e.pageY - 150);
        $('.tooltipevent').css('left', e.pageX + 50);
    });
}
function showConfirmDialog(message, event, revertFunc, url) {
    bootbox.confirm({
        title: "",
        message: message,
        buttons: {
            confirm: {
                label: "დადასტურება",
                className: "btn-success pull-right scheduleConfirmButton"
            },
            cancel: {
                label: "გაუქმება",
                className: "btn-default pull-right"
            }

        },
        callback: function (result) {
            if (result) {
                makeScheduleCopy(event, revertFunc, url, true);
            }
            else {
                revertFunc();
            }

        }
    });
}

function makeScheduleUpdate(event, revertFunc, url, isQuotation, isFromSelector) {

    if (isQuotation) {
        url = dropQuotationUrl;
    }
    var request = {
        NewStartDateTime: event.start.format("YYYY-MM-DD HH:mm"),
        NewEndDateTime: event.end.format("YYYY-MM-DD HH:mm"),
        NewEmployeeId: event.resourceId,
        ScheduleId: event.id,
        IsFromSelector: isFromSelector,
        IsFromQuotation: isQuotation
    }

    var data = JSON.stringify(request);
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (!data.success) {
                revertFunc();
            }
        },
        error: function () {
            revertFunc();
        }
    });
}
function makeScheduleCopy(event, revertFunc, url, isFromSelector) {
    var scheduleCopyInfo = {
        StartDateTime: event.start,
        EndDateTime: event.end,
        OriginalScheduleId: event.id,
        NewEmployeeId: event.resourceId,
        IsQuotation: event.isQuotation
    };

    var data = JSON.stringify(scheduleCopyInfo);
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.Success) {
                $('#calendar').fullCalendar('refetchEvents');
            }
            else if (data.MakeDrop) {
                if (event.isQuotation) {
                    makeScheduleUpdate(event, revertFunc, dropQuotationUrl, event.isQuotation, isFromSelector);
                }
                else {
                    makeScheduleUpdate(event, revertFunc, dropUrl, event.isQuotation, isFromSelector);
                }
            }
            else {
                revertFunc();
            }
        },
        error: function () {
            revertFunc();
        }
    });
}

function AddSchedule(employeeId, date, url, isQuotation) {

    var endHour = $("#EndHour").val();

    if (endHour === '') {
        ShowValidationMessage('სამუშაოს ხანგრძლივობა აუცილებელია');
        return;
    }
    $('#EmployeeId').val(employeeId);

    var serializedValues = $("#dialog-message :input").serializeArray();


    if (endHour.indexOf(".3") !== -1) {
        var num = Number(endHour) + 0.2;
        endHour = num;
    }

    var momentstart = date.format("YYYY-MM-DD HH:mm");
    //event to render immediately after add new schedule
    var event = {
        id: 150 * Math.random(),
        resourceId: employeeId,
        title: "",
        start: momentstart,
        end: date.add(endHour, 'hours'),
        color: "#f3c511",
        icon: "",
        isNew: true,
        isQuotation: isQuotation
    };

    var icons = [];
    var additionalStatuses = $('.schedule-status-item');
    $.each(additionalStatuses, function (index, item) {
        var icon = $(item).find('.icon');
        var isSelected = $(item).find('.isselected');
        if (isSelected.is(':checked')) {
            icons.push('fa ' + icon.val());
        }
    });

    event.icon = icons.join();

    $.ajax({
        url: url,
        type: "POST",
        data: serializedValues,
        success: function (data) {
            if (!data.isException) {
                var validationLabel = $("#validation");
                validationLabel.addClass("display-hide");
                event.id = data.id;
                $('#calendar').fullCalendar('renderEvent', event, true);
                $("#dialog-message").dialog("close");
                clearPopupValues();
            }
            else {
                ShowValidationMessage(data.Message);
            }
        },
        error: function () {

        }
    });
}

function makeScheduleDrop(scheduleDropStartEmployeeId, event, url, dropAction, updateAction) {
    if (scheduleDropStartEmployeeId != event.resourceId) {
        $.ajax({
            url: url,
            traditional: true,
            data: { scheduleId: event.id, employeeId: event.resourceId, isQuotation: event.isQuotation },
            success: function (data) {
                dropAction(data);
            }
        });
    }
    else {
        updateAction();
    }
}

function makeScheduleRender(event, element, statuses) {
    if (!event.isFake) {
        if (!event.isQuotation) {
            element.bind('dblclick', function () {
                window.open("/Sales/SalesOrders/Edit/" + event.order.id, "_blank");
            });
        }

        if (event.isPartialDone) {
            element.find(".fc-content").css("background", "repeating-linear-gradient(135deg, #5ec831, #fff 5px, #5ec831 5px, #fff 5px)");
        }

        var icons = event.icon.split(',');

        $.each(icons, function (i, data) {
            if (data != "") {
                element.find(".fc-content").prepend("<i style='margin-left:5px' class='" + data + "'></i>");
            }
        });
        if (statuses.length > 0) {
            if (statuses.indexOf(event.status) == -1) {
                return false;
            }
        }
        return true;
    }
}
function makeDayRender(hasConfig, cell, date, workingHours) {
    if (hasConfig) {
        $(cell).css("background-color", "#bfbfbf");
        var hour = Number(date.hours() + "." + date.minutes());
        if (hour.toString().indexOf(".3") !== -1) {
            var num = Number(hour) + 0.2;
            hour = num;
        }
        if (workingHours.length > 0) {
            if ($.inArray(hour, workingHours) != -1) {
                $(cell).css("background-color", "#88c3ef");
            }
        }
    }
}

function subscribeDateRangeChange(InitCalendar) {
    $('#dashboard-report-range').on('apply.daterangepicker', function (ev, picker) {
        var newStartDate = new moment(picker.startDate);
        var newEndDate = new moment(picker.endDate);
        InitCalendar(newStartDate, newEndDate);
    });
}

function updateCalendar(InitCalendar) {
    var data = $('#dashboard-report-range').data('daterangepicker');
    var newStartDate = new moment(data.startDate);
    var newEndDate = new moment(data.endDate);
    InitCalendar(newStartDate, newEndDate);
}

function handleViewRenderCalendar(view) {
    $('#dashboard-report-range span').html(view.start.format('D MMMM, YYYY') + ' - ' + view.end.format('D MMMM, YYYY'));
    $('#dashboard-report-range').data('daterangepicker').setStartDate(view.start);
    $('#dashboard-report-range').data('daterangepicker').setEndDate(view.end);
}

function handleAddScheduleLogic(resourceObj, date, isQuotation, employeeHasNotServicesErrorMessage) {
    var employeeId = resourceObj.id;
    var selectedStartDate = date.format("YYYY-MM-DD HH:mm");


    $.ajax({
        url: employeeHasScheduleInDateUrl,
        type: "GET",
        dataType: "json",
        async: false,
        data: { startDate: selectedStartDate, employeeId: employeeId },
        contentType: 'application/json; charset=utf-8',
        success: function (data) {
            if (data.has) {
                var employeeName = $("#calendar").fullCalendar('getResourceById', employeeId);
                $("#empName").val(employeeName.title);
                $("#startDate").val(selectedStartDate);

                var serviceIds = new Array();
                $(".checks").each(function (dt, val) {
                    serviceIds.push($(val).data("id"));
                });
                var urlServices = "";
                var addUrl = "";
                if (isQuotation) {
                    addUrl = addScheduleUrlQuotation;
                    urlServices = employeeServicesQuotationUrl;
                }
                else {
                    urlServices = employeeServicesUrl;
                    addUrl = addScheduleUrl;
                }

                var items = new Array();

                $("#btnYes").off().on("click", function () {
                    changeCheckboxStatus(false);//make checkboxes available before make post request,disabled elements values will not post
                    AddSchedule(employeeId, date, addUrl, isQuotation);
                });
                $("#btnNo").off().on("click", function () {
                    HandleEmployeeServiceCheckConfirmMessage(false);
                });
                $.getJSON(urlServices, { "employeeId": employeeId }).done(function (data) {
                    $.each(data, function (key, val) {
                        if (serviceIds.indexOf(val) != "-1") {
                            items.push(val);
                        }
                    });
                    Metronic.unblockModal();
                    $("#dialog-message").dialog(
                    {

                        autoOpen: false,
                        width: "500px",
                        resizable: false,
                        modal: true,
                        buttons: {
                            "დახურვა": function () {
                                ClearValidationMessage();
                                $("#dialog-message").dialog("close");
                            },
                            "შენახვა": function () {
                                ClearValidationMessage();
                                var selectedServiceIds = new Array();
                                $("#dialog-message input.checkservices:checked").each(function (k, v) {
                                    var elementId = v.getAttribute("id").split("__")[0].toString();
                                    var serviceId = $("#" + elementId + "__ServiceId").val();
                                    selectedServiceIds.push(serviceId);
                                });
                                var selectedSupportEmployee = $("#SupporterEmployeeId option:selected").val();
                                if (selectedSupportEmployee == employeeId) {
                                    ShowValidationMessage("დამხმარე თანამშრომელი და მთავარი უნდა იყოს სხვადასხვა");
                                    return;
                                }

                                if (selectedServiceIds.length > 0) {
                                    $.ajax({
                                        url: employeehaschoosenservicesurl,
                                        traditional: true,
                                        data: { employeeId: employeeId, serviceIds: selectedServiceIds, isQuotation: isQuotation },
                                        success: function (data) {
                                            if (data.hasServices) {
                                                AddSchedule(employeeId, date, addUrl, isQuotation);
                                            }
                                            else {
                                                HandleEmployeeServiceCheckConfirmMessage(true);
                                            }
                                        }
                                    });
                                }
                                else {
                                    ShowValidationMessage('სერვისების არჩევა აუცილებელია');
                                    return;
                                }
                            },
                        },

                        open: function () {
                            $($('.ui-dialog-buttonset').children()[0]).addClass("btn btn-default");
                            $($('.ui-dialog-buttonset').children()[1]).addClass("btn btn-success");
                            $('.ui-widget-overlay').css('background', '#333');
                            var closeBtn = $('.ui-dialog-titlebar-close');
                            $(closeBtn).html("");
                            ClearValidationMessage();
                            closeBtn.append('<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span><span class="ui-button-text"></span>');
                        },
                        close: function () {
                            ClearValidationMessage();
                            clearPopupValues();
                            HandleEmployeeServiceCheckConfirmMessage(false);
                        },
                    });
                    $("#dialog-message").dialog("open");

                });
            }
            else {
                Metronic.unblockModal();
            }
        },
    });
}
function hanldeScheduleUpdate(event, revertFunc, isQuotation) {
    if (isQuotation) {
        makeScheduleUpdate(event, revertFunc, dropQuotationUrl, isQuotation, true);
    }
    else {
        makeScheduleUpdate(event, revertFunc, dropUrl, isQuotation, true);
    }
}

function getCalendarInitObject(st, en, stepDays, isQuotation, hasConfig, workingHours) {
    clearStatusesSelect();
    var schedulesUrl = "";
    if (isQuotation) {
        schedulesUrl = schedulesListUrlQuotation;
    }
    else {
        schedulesUrl = schedulesListUrl;
    }

    return {
        now:st,
        timezone: 'local',
        handleWindowResize: true,
        resourceAreaWidth: '23%',
        //slotLabelInterval:"00:30",
        //slotLabelFormat: 'dd mm yyyy',
        slotMinutes: 30,
        slotDuration: "00:30:00",
        defaultTimedEventDuration: "00:30:00",
        end: end,
        schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
        editable: true,
        //contentHeight: 'auto',
        aspectRatio: 2.5,
        //eventOverlap: false,
        scrollTime: moment().subtract('hours', 1),
        eventBackgroundColor: "green",
        header: {
            left: '',//title
            center: ''
        },
        defaultView: 'timelineThreeDays',
        views: {
            timelineThreeDays: {
                type: 'timeline',
                duration: { days: stepDays },
            }
        },
        resourceLabelText: 'ბრიგადა',
        monthNamesShort: ['იანვარი', 'თებერვალი', 'მარტი', 'აპრილი', 'მაისი', 'ივნისი', 'ივლისი', 'აგვისტო', 'სექტემბერი', 'ოქტომბერი', 'ნოემბერი', 'დეკემბერი'],
        monthNames: ['იანვარი', 'თებერვალი', 'მარტი', 'აპრილი', 'მაისი', 'ივნისი', 'ივლისი', 'აგვისტო', 'სექტემბერი', 'ოქტომბერი', 'ნოემბერი', 'დეკემბერი'],
        dayNames: ['კვირა', 'ორშაბათი', 'სამშაბათი', 'ოთხშაბათი', 'ხუთშაბათი', 'პარასკევი', 'შაბათი'],
        dayNamesShort: ['კვირ', 'ორშ', 'სამშ', 'ოთხშ', 'ხუთშ', 'პარ', 'შაბ'],
        buttonText: { today: 'დღეს' },
        resources: {
            url: employeeListUrl,
        },
        events: {
            url: schedulesUrl,
        },
        eventDragStart: function (event, jsEvent, ui, view) {
            scheduleDropStartEmployeeId = event.resourceId;
        },
        eventDragStop: function (event, jsEvent, ui, view) {

        },
        dayClick: function (date, jsEvent, view, resourceObj) {
            Metronic.blockModal({ animate: true });
            handleAddScheduleLogic(resourceObj, date, isQuotation, messageError);
        },
        dayRender: function (date, cell, v) {
            makeDayRender(hasConfig, cell, date, workingHours);
        },
        eventMouseover: function (event, jsEvent, view) {
            renderPopover(event);
        },
        eventMouseout: function (calEvent, jsEvent) {
            $(this).css('z-index', 8);
            $('.tooltipevent').remove();
        },
        eventDrop: function (event, delta, revertFunc, jsEvent, ui, view) {
            var urlaction = '/Administration/EmployeeSchedule/EmployeeHasServicesBySchedule';
            makeScheduleDrop(scheduleDropStartEmployeeId, event, urlaction,
            function (data) {
                if (data.hasServices) {
                    makeScheduleCopy(event, revertFunc, copyUrl, true);
                }
                else {
                    showConfirmDialog("თანამშრომელზე არ არის გაწერილი აღნიშნული სერვისი, ნამდვილად გსურთ შეასრულოს ეს სერვისი?", event, revertFunc, copyUrl);
                }
            },
            function () {
                if (event.isQuotation) {
                    hanldeScheduleUpdate(event, revertFunc, event.isQuotation);
                }
                else {
                    makeScheduleUpdate(event, revertFunc, dropUrl, event.isQuotation, true);
                }
            });

        },
        eventResize: function (event, delta, revertFunc, jsEvent, ui, view) {
            makeScheduleUpdate(event, revertFunc, dropUrl, isQuotation, true);
        },
        eventRender: function (event, element) {
            return makeScheduleRender(event, element, statuses);
        },
        viewRender: function (view, element) {
            handleViewRenderCalendar(view);
            Metronic.unblockModal();

        },
        loading: function (isLoading, view) {
            if (!isLoading) {
                Metronic.unblockModal();
            }

        }
    }
}

//baseSchedule Helpers
function setIdsToEmployees() {
    var emps = $("#calendar").fullCalendar('getResources');
    var tds = $($(".fc-resource-area")[1]).find("table tr");
    $(tds).each(function (data, tr) {
        var name = $($(tr).find(".fc-cell-text")).text();
        var result = $.grep(emps, function (e) { return e.title == name; })[0];
        $(tr).attr("id", result.id);

    });
}