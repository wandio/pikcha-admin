﻿$.widget("wandio.contractorSelector", {
    options: {
        placeholder: '',
        disableNew: false
    },

    _create: function () {
        var self = this;
        var container = self.element.parents('.contractor-browser');
        self.searchBrowser = container.find('.search-browser');
        self.searchTrigger = container.find('.js-search-trigger');
        self.clickTrigger = container.find('.js-click-trigger');
        self.url = self.element.data('request-url');

        var table = this._getTableTemplate();
        self.searchBrowser.append(table);

        this._on(this.searchTrigger, {
            'keyup': function (event) {
                event.preventDefault();
                self.searchBrowser.addClass('active');
                var $this = $(event.target);
                var term = $this.val();

                if (!term) {
                    self.element.val('');
                }

                if (term.length >= 3) {
                    App.initDelay(function () {
                        self._clearLines();
                        self._getContractors(null, term);
                    }, 500);
                }
            }
        });

        this._on(this.searchBrowser, {
            'click tr.line': function (event) {
                self.searchBrowser.removeClass('active');
                var that = event.target;
                var $line = $(that).parents('tr.line');
                var id = $line.data('id');
                var name = $line.find('.name').text();
                self.element.val(id);
                self.searchTrigger.val(name);
            }
        });

        this._on(this.clickTrigger, {
            click: function (event) {
                event.preventDefault();
                self.searchBrowser.addClass('active');
                var id = $(event.target).parents('.contractor-browser').find('.contractor-selector').val();
                if (id) {
                    self._clearLines();
                    self._getContractors(id);
                }
                else {
                    var term = self.searchTrigger.val();
                    self._clearLines();
                    self._getContractors(null, term);
                }
            }
        });

        this._hideBrowser();
    },
    _clearLines: function () {
        var self = this;
        var table = self.searchBrowser.find('table');
        table.find('tr.line').remove();
    },
    _getContractors: function (contractorId, term) {
        var self = this;
        var id = null;
        if (contractorId) {
            id = contractorId;
        }
        $.get(self.url, { id: id, term: term, disableNew: self.options.disableNew }, function (data) {
            var table = self.searchBrowser.find('table');
            table.find('tbody').append(data.view);
            //data.forEach(function (item, index) {
            //    var cell = self._getCell(item);
            //    table.append(cell);
            //});
        });
    },
    _getTableTemplate: function () {
        var tableTemplate = '<table class=\"table table-hover table-bordered table-striped\">' +
            '<thead><tr>' +
            '<th width="20%">{0}</th>' +
            '<th>{1}</th>' +
            '<th>{2}</th>' +
            '<th>{3}</th>' +
            '<th>{4}</th>' +
            //'<th>{5}</th>' +
            '<th>{5}</th>' +
            '<th>{6}</th>' +
            '<th>{7}</th>' +
            '<th>{8}</th>' +
            '</tr></thead>' +
            '<tbody></tbody>' + '</table>';

        return String.format(tableTemplate, 'სახელი', 'იურ. ფორმა', 'საიდ. კოდი', 'კატეგორია', 'ხელშეკრულება', /*'შავი სია',*/ 'ბალანსი', 'ტელეფონი', 'მისამართი', 'სტატუსი');
    },
    _getCell: function (contractor) {
        var template = '<tr class="line" data-id="{10}">' +
            '<td class="name">{0}</td>' +
            '<td>{1}</td>' +
            '<td>{2}</td>' +
            '<td>{3}</td>' +
            '<td>{4}</td>' +
            '<td>{5}</td>' +
            '<td>{6}</td>' +
            '<td>{7}</td>' +
            '<td>{8}</td>' +
            '<td>{9}</td>' +
            '</tr>';
        return String.format(template, contractor.text, contractor.LegalForm, contractor.IdentificationCode, contractor.Category, contractor.HasAgreement, contractor.BlackList, contractor.Balance, contractor.Phone, contractor.Address, contractor.Status, contractor.value);
    },
    _hideBrowser: function () {
        var self = this;
        $('html').click(function (e) {
            if ($(e.target).closest('.contractor-browser').length == 0) {
                self.searchBrowser.removeClass('active');
                self._clearLines();
            }
        });
    }
});