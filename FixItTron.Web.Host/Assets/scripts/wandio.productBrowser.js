﻿$.widget("wandio.productBrowser", {
    options: {
    },

    _create: function () {
        var self = this;
        self.categoriesUrl = self.element.data('categories-url');
        self.productsUrl = self.element.data('request-url');

        self.searchTrigger = self.element.find('a.js-selector-trigger');
        self.searchBrowser = self.element.find('.search-browser');
        self.categoryTree = self.element.find('.js-category-tree');
        self.productContainer = self.element.find('.js-products');
        self.searchInput = self.element.find('#search');
        self.selectedProduct = self.element.find('.selected-product');
        self.selectAll = self.element.find('.js-all');

        this._on(this.searchTrigger, {
            'click': function (event) {
                event.preventDefault();
                self.categoryTree.jstree('destroy');
                self.categoryTree.jstree({
                    "core": {
                        "check_callback": true,
                        "themes": {
                            "responsive": false
                        },
                        'data': {
                            'url': function (node) {
                                return self.categoriesUrl;
                            },
                            'data': function (node) {
                                return { 'id': node.id };
                            }
                        }
                    },
                    "types": {
                        "default": {
                            "icon": "fa fa-folder icon-state-warning icon-lg"
                        },
                        "file": {
                            "icon": "fa fa-file icon-state-warning icon-lg"
                        }
                    },
                    "plugins": ["types"]
                }).on('loaded.jstree', function (e, data) {
                    self.searchBrowser.addClass('active');
                    var categoryId = data.instance.element.jstree('get_selected')[0];
                    self._getProducts(categoryId);
                }).on('select_node.jstree', function (e, data) {
                    var categoryId = data.selected[0];
                    self._getProducts(categoryId);
                });
            }
        });

        this._on(this.selectAll, {
            'click': function (event) {
                event.preventDefault();
                var $this = $(event.target);
                var items = self.productContainer.find('li input.js-check');
                $.each(items, function (index, item) {
                    $(item).trigger('click');
                });
            }
        });

        this._on(this.productContainer, {
            'click li': function (event) {
                var $this = $(event.target);
                var isCheck = false;
                if (!$this.is('li')) {
                    $this = $this.parents('li');
                    isCheck = true;
                }
                var id = $this.data('id');
                var isMultiple = self.element.data('multiple');//TODO:
                if (isMultiple) {
                    if (isCheck) {
                        var check = $this.find('.js-check');

                        var container = self._getContainer();
                        var $template = self._getTemplate($this);
                        var lines = container.find('tr.line');
                        var inputsToRemove = lines.find('input.product-id[value="' + id + '"]');
                        if (inputsToRemove.length == 0) {
                            container.append($template);
                        }
                        else {
                            inputsToRemove.parents('tr').remove();
                        }

                        lines = container.find('tr.line');//fetch updated lines
                        $.each(lines, function (index, value) {
                            $(value).find(':input').each(function (innerIndex, input) {
                                var $input = $(input);
                                var currentName = $input.attr('name');
                                if (currentName) {
                                    var newName = 'ProductIds[' + index + '].' + currentName.substring(currentName.lastIndexOf('.') + 1);
                                    $input.attr('name', newName);
                                }
                            });
                        });
                    }
                }
                else {
                    self.element.find('.selected-product').val(id);
                    self.element.find('.selected-product').change();//input trigger change
                    self.element.find('.selected-product-name').val($this.find('span.item-text').text());

                    self.searchBrowser.removeClass('active');
                }
            }
        });
        this._on(this.searchInput, {
            'keyup': function (event) {
                var $this = $(event.target);
                var value = $this.val();
                this.productContainer.find('li').each(function () {
                    if ($(this).text().indexOf(value) > -1) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }
                });
            }
        }),

        this._hideBrowser();
    },

    _getTemplate: function (item) {
        var self = this;

        var id = item.data('id');
        var category = item.data('category');
        var code = item.data('code');
        var name = item.find('span.item-text').text();

        var templateId = '#' + self.element.data('multiple-template');
        var $template = $($(templateId).html());

        $template.find('span.product-name').text(name);
        $template.find('span.product-category').text(category);
        $template.find(':input.product-category').val(category);
        $template.find(':input.product-code').val(code);
        $template.find(':input.product-id').val(id);
        $template.find(':input.product-name').val(name);

        return $template;
    },

    _getContainer: function () {
        var self = this;
        var multipleTargetId = '.' + self.element.data('multiple-target');
        var container = $(multipleTargetId);
        return container;
    },

    _getProducts: function (categoryId) {
        if (categoryId == -1) {
            categoryId = null;
        }

        var self = this;
        var isMultiple = self.element.data('multiple');
        $.get(self.productsUrl, {
            categoryId: categoryId
        }, function (data) {
            self.productContainer.empty();

            data.forEach(function (item, index) {
                var productItem = '';
                var classVal = '';

                if (isMultiple) {
                    var container = self._getContainer();
                    var inputCheck = '<input type="checkbox" class="js-check" />';

                    var linesIds = container.find('tr.line input.product-id').map(function () { return $(this).val(); }).get();
                    if (linesIds.indexOf(item.value) > -1) {
                        inputCheck = '<input type="checkbox" class="js-check" checked />';
                    }

                    productItem = String.format('<li class="{0}" data-code="{1}" data-category="{2}" data-id="{3}">{5} {2} - <span class="item-text">{1} {4}</span></li>', classVal, item.code, item.category, item.value, item.text, inputCheck);
                } else {
                    var selectedProductId = self.selectedProduct.val();
                    classVal = selectedProductId == item.value ? "selected" : "";
                    productItem = String.format('<li class="{0}" data-code="{1}" data-category="{2}" data-id="{3}">{2} - <span class="item-text">{1} {4}</span></li>', classVal, item.code, item.category, item.value, item.text);
                }

                self.productContainer.append(productItem);
            });
        });
    },

    _hideBrowser: function () {
        var self = this;
        $('html').click(function (e) {
            if ($(e.target).closest('.product-browser').length == 0) {
                self.searchBrowser.removeClass('active');
            }
        });
    }
});