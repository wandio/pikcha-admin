﻿var contractorEditor = function () {

    handleForm = function () {
        $(document).on('change', '.js-legalform', function () {
            var legalForm = $(this);
            var labelToChange = $('.js-label');
            var legalFormValToCheck = legalForm.data('legalform');

            var currentText = labelToChange.text();
            personalnumberText = labelToChange.data('personalnumber');
            identificationcodeText = labelToChange.data('identificationcode');

            var legalentity = $('.js-legalentity');
            var individuals = $('.js-individuals');

            if (legalForm.val() == legalFormValToCheck) {
                legalentity.removeClass('hide');
                individuals.addClass('hide');
                labelToChange.text(identificationcodeText);
            }
            else {
                legalentity.addClass('hide');
                individuals.removeClass('hide');
                labelToChange.text(personalnumberText);
            }

            //handleSelect2();
        });

        $(document).on('click', '.js-add-additional', function () {
            var $this = $(this);
            var temlateId = '#js-additionals-template';
            if ($this.hasClass('popup')) {
                temlateId = '#js-additionals-popup-template';
            }
            var template = $(temlateId).html();
            var container = $this.parent().next('.js-additionals-container');
            container.append($(template));

            var lines = $this.parents('.js-additionals').find('.js-additionals-container .row');
            reIndexAdditionals(lines, '');
        });

        $(document).on('submit', '.js-contractor-form', function () {
            var lines = $('.js-additionals-container .row');
            reIndexAdditionals(lines, 'GeneralInfo.Object.');
        });

        $(document).on('click', '.js-remove-additional', function () {
            var $this = $(this);
            $this.parent().fadeOut().remove();
            var lines = $this.parents('.js-additionals').find('.js-additionals-container .row');
            reIndexAdditionals(lines, '');
        });

        $(document).on('keyup', '.js-phonenumber', function () {
            var classname = $(this).data("classname");
            var msg = $(this).data("invalidphone");
            var isCotactPerson = $(this).data("iscontactperson");
            var isGeorgia = false;//$('.countryclass option:selected').text() == "საქართველო";
            if (isCotactPerson) {
                isGeorgia = $('.js-contractor-object option:selected').data('isgeorgia');
            }

            validatePhonenumber(classname, msg, isGeorgia);
        });

        $(document).on('keyup', '.js-customer', function () {
            var classname = $(this).data("classname");
            var msg = $(this).data("mustbemsg");

            var digit = classname == '.mobilenumber' ? 9 : 7;
            var span = $(classname);
            if (($(classname + "input").val().replace(/ /g, '')).length > digit) {
                span.html(msg);
            }
            else {
                span.html('');
            }
        });

        $(document).on('change', '.js-contractor-object', function () {
            var isGeorgia = $('.js-contractor-object option:selected').data('isgeorgia');

            validatePhoneForContactPerson(isGeorgia);
        });

        $(document).on('keyup', '.js-identification-code', function () {
            var span = $(".identification-code");
            var input = $('.js-identification-code');

            var code = input.val();
            if (code.length > 11) {
                var tooLongMsg = input.data("toolong");
                span.html(tooLongMsg);
                return;
            }

            var filter = /[^0-9]/;
            if (filter.test(code)) {
                var onlyNumberMsg = input.data("onlynumber");
                span.html(onlyNumberMsg);
            }
            else { span.html(''); }
        });
    }

    function validatePhoneForContactPerson(isGeorgia) {
        var mobile = ".cpmobilenumber"; // cp = contact person
        var phone = ".cpphonenumber";
        var msg = $(mobile + 'input').data("invalidphone");
        validatePhonenumber(mobile, msg, isGeorgia);
        validatePhonenumber(phone, msg, isGeorgia);
    }

    function validatePhonenumber(f, msg, isGeorgia) {
        var span = $(f);
        if (!validatePhone(f, isGeorgia)) {
            span.html(msg);
        }
        else {
            span.html('');
        }
    }

    function validatePhone(f, isGeorgia) {
        var cl = f + "input";
        var a = $(cl).val();//document.getElementById(Object_PhoneNumber).value;
        if (a.length > 0) {
            var filter = /[^0-9.+\-()\s]/;
            if (isGeorgia) {
                filter = /^(\+\d{1,2}\s)?\(?\d{2,3}\)?[\s-]?\d{2,3}[\s-]?\d{2,3}$/;
                return filter.test(a);
            }
            else {
                return !filter.test(a);
            }
        }
        return true;
    }

    function reIndexAdditionals(lines, part) {
        $.each(lines, function (index, value) {
            $(value).find(':input').each(function (innerIndex, input) {
                var $input = $(input);
                var currentName = $input.attr('name');
                if (currentName) {
                    var newName = part + 'Additionals[' + index + '].' + currentName.substring(currentName.lastIndexOf('.') + 1);
                    $input.attr('name', newName);
                }
            });
        });
    }

    var handleRS = function () {
        $(document).on('click', 'a#search-rs', function (event) {
            event.preventDefault();
            var duplicateContractors = $('.duplicate-contractors');
            duplicateContractors.removeClass('field-validation-error');

            var $taxIdInput = $('.js-identification-code');
            var taxId = $taxIdInput.val();
            if (!taxId) {
                return;
            }
            var url = $(this).attr('href');
            $.getJSON(url, { taxId: taxId }, function (result) {
                if (!result.success) {
                    bootbox.alert(result.message);
                    duplicateContractors.text('');
                    duplicateContractors.removeClass('field-validation-error');
                }
                else {
                    $('#Name').val(result.name);
                    if (result.dplCount) {
                        duplicateContractors.text("აღნიშნული კოდით უკვე რეგისტრირებულია " + result.dplCount + " ობიექტი");
                        duplicateContractors.addClass('field-validation-error');
                    }
                    else {
                        duplicateContractors.text('');
                        duplicateContractors.removeClass('field-validation-error');
                    }
                    if (result.isTaxPayer) {
                        $('#TaxGroupId').val(1);//18% დ.ღ.გ
                    }
                }
            });
        });
    };

    return {
        init: function () {
            handleForm();
        },
        initRS: function () {
            handleRS();
        }
    };
}();