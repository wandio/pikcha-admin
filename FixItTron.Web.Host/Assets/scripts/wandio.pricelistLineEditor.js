﻿$.widget("wandio.priceListLineEditor", {
    options: {
        productSelectionBatchUrl: '',
        productDetailsUrl: '',
        productLineTemplateSelector: '#table-line-tpl',
        costIsEditable: false,
        autoConvertPrice: true,
        autoSetCost: true,
        useSalesPrice: true,
        afterRecalculateLineTotal: null,
        useBaseMeasure: true,
        discount: null,
        highPrecision: false,
        useproductselector: false
    },
    _create: function () {
        var self = this;
        this.lineTemplate = $(this.options.productLineTemplateSelector).html();
        this.addTrigger = this.element.find('a.add');
        this.form = this.element.parents('form');
        this.linesTable = this.element.find('table');
        this._on(this.form, {
            'submit': function (event) {
                var $this = $(event.target);
                if (!(event.isDefaultPrevented()) && $this.valid()) {
                    var lines = this._getLines();
                    //reindex inputs
                    $.each(lines, function (index, value) {
                        $(value).find(':input').each(function (inputIndex, input) {
                            var $input = $(input);
                            var currentName = $input.attr('name');
                            if (currentName) {
                                var newName = 'Lines[' + index + '].' + currentName.substring(currentName.indexOf('.') + 1);
                                $input.attr('name', newName);
                            }
                        });
                    });
                }
            }
        });
        this._on(this.linesTable, {
            'blur :input': function (event) {
                $this = $(event.target);
                var valid = this.form.validate().element($this);
                if (!valid) {
                    event.preventDefault();
                    event.stopPropagation();
                    $this.focus();
                }
                else {
                    this._getParentLine($this).addClass('read-only');
                }
            },
            'focus :input': function (event) {
                this._makeEditable(this._getParentLine($(event.target)));
            },
            'click a.delete': function (event) {
                event.preventDefault();
                var self = this;
                var $this = $(event.target);
                var options;
                options = {
                    message: 'ნამდვილად გსურთ ჩანაწერის წაშლა?',
                    buttons: {
                        cancel: {
                            label: 'გაუქმება',
                            className: "btn-default",
                            callback: function () {
                                bootbox.hideAll();
                            }
                        },
                        approve: {
                            label: 'დიახ',
                            className: "btn-danger",
                            callback: function (event) {
                                event.preventDefault();
                                bootbox.hideAll();
                                $this.parents('tr').fadeOut('fast', function () {
                                    $(this).remove();
                                });
                                return false;
                            }
                        }
                    }
                };
                bootbox.dialog(options);
            }
        });
        this._on(this.addTrigger, {
            click: function (event) {
                event.preventDefault();
                this._addLine();
            }
        });
        this._on(this.linesTable, {
            'change :input.select-product': function (event) {
                var self = this;
                var useProdSelector = this.options.useproductselector;
                if (event.isImmediatePropagationStopped()) {
                    return;
                }
                event.stopImmediatePropagation();
                var $this = $(event.target);
                var $line = this._getParentLine($this);
                var $val = this._getSelectedProductId($line);
                if (!$val) {
                    this._disableInputs($line);
                    return;
                }

                this._enableInputs($line);
                this._getSelectedProductData($line).done(function (data) {
                    if (data) {
                        self._applyProductSelectionDetails($line, data);
                        reinitSelect2(useProdSelector);
                    }
                });

                $line.find('input.quantity').focus();
            },
            'change select.measure': function (event) {
                var $this = $(event.target);
                var $line = this._getParentLine($this);
                if (this.options.autoSetCost) {
                    var self = this;
                    this._getSelectedProductData($line).done(function (data) {
                        self._setCostByCurrentMeasure($line, data);
                    });
                }
            }
        });

        $('.product-browser').productBrowser();
    },
    _setOption: function (key, value) {
        if (key === "costIsEditable") {
            if (this.options.costIsEditable != value) {
                this._changeCostIsEditable(value);
            }
        }
        this._super(key, value);
    },
    _changeCostIsEditable: function (value) {
        //change to be editable
        var lines = this._getLines();
        var self = this;
        if (value) {
            lines.find('input.cost').prop('readonly', false);
        }
        else {
            lines.each(function () {
                var $line = $(this);
                self._getSelectedProductData($line).done(function (data) {
                    if (data) {
                        self._setCostByCurrentMeasure($line, data);
                    }
                });
            });
        }
    },
    _setOptions: function (options) {
        this._super(options);
    },
    _setCostByCurrentMeasure: function ($line, data) {
        var cost = this._formatAmount(this._getCostInMeasure(data, this._getCurrentMeasure($line, data)));
        this._setCost($line, cost);
        //this._recalculateLineTotal($line);
    },
    _addLine: function (batch) {
        var newLine = $(this.lineTemplate);
        this.linesTable.append(newLine);
        if (!batch) {
            $.validator.unobtrusive.reParse(this.form);
            reinitSelect2(this.options.useproductselector);
            newLine.removeClass('read-only');
        }
        return newLine;
    },
    _makeEditable: function ($line) {
        if ($line.hasClass('read-only')) {
            $line.removeClass('read-only');
            var $productSelect = $line.find(':input.select-product');
            var value = $productSelect.val();
            if (!value) {
                this._disableInputs($line);
            }
            else {
                this._enableInputs($line);
            }
        }
    },
    _disableInputs: function ($line) {
        $line.find('select.measure').empty().val('');
        $line.find(':input:not(.select-product)').prop('readonly', true).val('');
    },
    _enableInputs: function ($line) {
        $line.find(':input:not(.select-product)').prop('readonly', false);
        if (!(this.options.costIsEditable)) {
            $line.find(':input.cost').prop('readonly', true);
        }
    },
    _getCurrentMeasure: function ($line, data) {
        var value = $line.find('select.measure').val();
        if (value) {
            if (value == data.BaseMeasure.MeasureId) {
                return data.BaseMeasure;
            }
            var measure = $.grep(data.Measures, function (e) { return e.MeasureId == value; })[0];
            return measure;
        }
        if (this.options.useBaseMeasure) {
            return data.BaseMeasure;
        }
        else {
            return data.DefaultMeasure;
        }
    },
    //cost, price, salesprice, everything depending on options
    _getCostInMeasure: function (data, measure) {
        if (!data) {
            if (console) {
                console.log('data is null');
                return null;
            }
        }
        var salesPrices = data.SalesPrice;
        var measureId = measure.MeasureId;
        for (var i = 0; i < salesPrices.length; i++) {
            var salesPrice = salesPrices[i];
            if (salesPrice.MeasureId == measureId) {

                if (this.options.discount) {
                    var discountVal = this.options.discount;
                    discountVal = parseFloat(discountVal).toAmount()
                    if (!isNaN(discountVal)) {
                        var price = salesPrice.Price;
                        return this._recalculateDiscount(price, discountVal);
                    }
                }

                return salesPrice.Price;
            }
        }
    },
    _applyProductSelectionDetails: function ($line, data, skipSelect2) {
        //measures
        $line.data('product-details', data);
        var $measure = $line.find('select.measure');
        var existingValue = $measure.val();
        var allowedMeasures = [];
        allowedMeasures.push(data.BaseMeasure.MeasureId);

        $.each(data.Measures, function (index, value) {
            allowedMeasures.push(value.MeasureId);
        });

        //filter out unallowed options from data
        var allowedOptions = $(':input.measure option', this.lineTemplate).filter(function (index) {
            var val = $(this).attr('value');
            //allow empty
            if (!val) {
                return true;
            }
            var index = allowedMeasures.indexOf(parseInt(val));
            return index != -1;
        });
        $measure.select2("destroy").empty().append(allowedOptions.clone());

        if (this.options.useBaseMeasure) {
            $measure.val(data.BaseMeasure.MeasureId);
        }
        else {
            $measure.val(data.DefaultMeasure.MeasureId);
        }

        if (!skipSelect2) {
            reinitSelect2(this.options.useproductselector);
        }
        if (this.options.autoSetCost) {
            var cost = this._getCostInMeasure(data, this._getCurrentMeasure($line, data));
            this._setCost($line, cost);
        }
    },
    _getParentLine: function ($input) {
        return $input.closest('tr.line');
    },
    _getSelectedProductData: function ($line) {
        var existing = $line.data('product-details');
        if (existing != null) {
            var def = $.Deferred();
            def.resolve(existing);
            return def.promise();
        }
        else {
            var requestData = { id: this._getSelectedProductId($line) };
            var self = this;
            return $.get(this.options.productDetailsUrl, requestData).done(function (data) {
                $line.data('product-details', data);
            });
        }
    },
    _setProductId: function ($line, productId, triggerChange) {
        var productId = $line.find(':input.select-product').val(productId);
        if (triggerChange) {
            productId.trigger('change');
        }
    },
    _setProductName: function ($line, productName) {
        var productName = $line.find(':input.selected-product-name').val(productName);
    },
    _setMeasure: function ($line, measure, triggerChange) {
        var measure = $line.find('select.measure').val(measure);
        if (triggerChange) {
            measure.trigger('change');
        }
    },
    _getSelectedProductId: function ($line) {
        return $line.find(':input.select-product').val();
    },
    _getCost: function ($line) {
        var value;
        value = $line.find('input.cost').val();
        return parseFloat(value);
    },
    _setCost: function ($line, cost) {
        var parsed = parseFloat(cost);
        if (!isNaN(parsed)) {
            $line.find('input.cost').val(this._formatAmount(parsed));
        }
    },
    _getLines: function () {
        return this.linesTable.find('tr.line');
    },
    _recalculateDiscount: function (price, discount) {
        if (discount < 0) {
            discount *= -1;
            return this._formatAmount(price - (price * (discount / 100)));
        }
        return this._formatAmount((price + (price * (discount / 100))));
    },
    addLines: function (lines, onAddLine) {
        var self = this;
        if (lines) {
            $(lines).each(function () {
                var lineData = this;
                var $line = self._addLine(true);
                self._setMeasure($line, lineData.unitOfMeasure);
                self._setProductId($line, lineData.productId, false);
                self._setProductName($line, lineData.productName);
                self._setCost($line, lineData.cost);
                if (onAddLine) {
                    onAddLine($line, lineData);
                }
            });
            //self.applyProductSelectionBatch(lines);
        }
    },
    applyProductSelectionBatch: function (lines) {
        var ids = [];
        lines.forEach(function (val) {
            ids.push(val.productId + "&");
        });
        var query = "?ids=" + ids.join("ids=");
        var self = this;
        $.get(this.options.productSelectionBatchUrl + query).done(function (data) {
            var lines = self._getLines();
            for (var i = 0; i < data.length; i++) {
                self._applyProductSelectionDetails($(lines[i]), data[i], true);
            }
            reinitSelect2(true);
        });
    },
    clearLines: function () {
        this._getLines().remove();
    },
    _formatAmount: function (number) {
        if (isNaN(number)) {
            return;
        }
        if (this.options.highPrecision) {
            return number.toAmount(true);
        }
        return number.toAmount();
    }
});

function reinitSelect2(useProdSelector) {

    if (useProdSelector) {
        $('.product-browser').productBrowser();
    }

    $("tr.line select").select2({
        allowClear: true
    });
}