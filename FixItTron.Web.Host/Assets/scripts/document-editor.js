﻿$(function () {
    $(document).on('click', '.document-editor a.add', function (event) {
        event.preventDefault();
        var $this = $(this);
        var options;
        var content = $($('#document-dialog-template').html());
        options = {
            message: content,
            buttons: {
                cancel: {
                    label: 'გაუქმება',
                    className: "btn-default",
                    callback: function () {
                        bootbox.hideAll();
                    }
                },
                approve: {
                    label: 'დამატება',
                    className: "btn-success",
                    callback: function (event) {
                        event.preventDefault();
                        var form = $('form.add-doc-form');
                        if (form.valid()) {
                            var dialog = $(this);
                            var fileUpload = form.fileupload({
                            });

                            fileUpload.fileupload('send', {
                                files: $('#doc-file').prop('files'),
                                url: form.data('upload-to'),
                                formData: form.find(':input').serializeArray()
                            }).done(function (data) {
                                window.location.reload();
                                //var $table = $this.siblings('table.table.documents');
                                //var row = $('<tr></tr>')
                                //    .append($('<td></td>').append(
                                //            $('<a></a>').text(data.identifier).attr('href', data.address).data('id', data.id).data('item-id', data.documentId)))
                                //    .append($('<td></td>').text(data.fileName))
                                //    .append($('<td></td>').append(
                                //            $('<a  class="delete-doc action-icon delete"><i class="fa fa-trash-o"></i></a>').data('title', data.identifier).attr('href', data.address).data('id', data.id).data('item-id', data.documentId)))
                                //.appendTo($table);
                                //Metronic.unblockUI('.modal-content');
                                //docDialog.modal('hide');
                            });

                            Metronic.blockUI({ target: '.modal-content' });
                        }
                        return false;
                    }
                }
            }
        }

        var docDialog = bootbox.dialog(options);
        $.validator.unobtrusive.parse(document);
    });
    $(document).on('click', '.document-editor a.delete-doc', function (event) {
        event.preventDefault();
        var $this = $(this);
        var options;

        options = {
            title: $this.data('title'),
            message: 'ნამდვილად გსურთ ჩანაწერის წაშლა?',
            buttons: {
                cancel: {
                    label: 'გაუქმება',
                    className: "btn-default",
                    callback: function () {
                        bootbox.hideAll();
                    }
                },
                approve: {
                    label: 'დიახ',
                    className: "btn-danger",
                    callback: function (event) {
                        event.preventDefault();

                        $.ajax({
                            url: $this.attr('href'),
                            type: 'post',
                            data: {
                                id: $this.data('id'),
                                docId: $this.data('item-id'),
                            }
                        }).done(function (data) {
                            $this.parents('tr').fadeOut('fast', function () {
                                $(this).remove();
                            });
                            Metronic.unblockUI('.modal-content');
                            docDialog.modal('hide');
                            window.location.reload();
                        });

                        Metronic.blockUI({ target: '.modal-content' });
                        return false;
                    }
                }
            }
        }

        var docDialog = bootbox.dialog(options);
    });
});