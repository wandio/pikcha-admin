//CKEDITOR.editorConfig = function (config) {
//    config.toolbar = 'Cover';
//};

/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

//CKEDITOR.editorConfig = function (config) {
//    config.language = 'ka';
//    config.enterMode = CKEDITOR.ENTER_DIV;
//    config.removePlugins = 'magicline';
//    config.toolbar = [
//        //{ name: 'newpage', items: ['NewPage', 'Preview'] },
//        ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'],
//       { name: 'numbers', items: ['NumberedList', 'BulletedList', 'Indent', 'Outdent', '-', 'JustifyLeft', 'JustifyRight', 'JustifyCenter', 'JustifyBlock', ] },
//       { name: 'document', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar'] },
//       { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', 'RemoveFormat'] },
//       ['TextColor', 'BGColor'],
//       ['Styles'],
//       ['Format'],
//       ['Font'],
//       ['FontSize']
//    ];
//};

CKEDITOR.editorConfig = function (config) {
    config.language = 'ka';
    config.toolbarGroups = [
		{ name: 'clipboard', groups: ['clipboard', 'undo'] },
		{ name: 'editing', groups: ['find', 'selection'] },
		{ name: 'links' },
        { name: 'document', items: ['Image', 'Table', 'HorizontalRule', 'SpecialChar'] },
		{ name: 'forms' },
		{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
		{ name: 'tools' },
		'/',
		{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'] },
		{ name: 'styles' },
		{ name: 'colors' }
    ];

    config.removeButtons = 'Subscript,Superscript';
};
