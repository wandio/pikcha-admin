﻿/**
 * PDF-specfic configuration
 */
AmCharts.exportPDF = {
    "format": "PDF",
    "content": [{
        "image": "reference",
        "fit": [523.28, 769.89] // fit image to A4
    }]
};

/**
 * Print-specfic configuration
 */
AmCharts.exportPrint = {
    "format": "PRINT",
    "label": "ბეჭდვა"
};

/**
 * Define main universal config
 */
AmCharts.exportCFG = {
    "enabled": true,
    "libs": {
        "path": "../assets/plugins/_amcharts/plugins/export/libs/"
    },
    "menu": [{
        "class": "export-main",
        "label": "ექსპორტი",
        "menu": [{
            "label": "შენახვა ...",
            "menu": ["PNG", "JPG", "SVG", AmCharts.exportPDF]
        }, AmCharts.exportPrint]
    }]
};