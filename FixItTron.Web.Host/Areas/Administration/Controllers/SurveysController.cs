﻿using AutoMapper;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Lookup.ApplicationUsers;
using FixItTron.Web.Host.Areas.Administration.Models.ApplicationUsers;
using FixItTron.Web.Host.Helpers;
using FixItTron.Web.Host.Models.Infrastructure;
using FlexCel.Core;
using FlexCel.XlsAdapter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;
using Wandio.Web.Mvc;
using Wandio.Web.Mvc.Html;

namespace FixItTron.Web.Host.Areas.Administration.Controllers
{
    public partial class SurveysController : Controller
    {
        ILookupService _lookupService;
        IMappingEngine _mapper;

        public SurveysController(ILookupService lookupService, IMappingEngine mapper)
        {
            _lookupService = lookupService;
            _mapper = mapper;
        }

        // GET: Administration/Surveys
        public virtual ActionResult Index(SurveyFilterViewModel filter, int page = 1, int pagesize = 25, string sortBy = "User.Username", string sortOrder = null, bool export = false)
        {
            var headers = new List<TableHeaderModel>() {
                new TableHeaderModel(ApplicationStrings.Event, true, "Event.Name", 20),
                new TableHeaderModel(ApplicationStrings.UserName, true, "User.Username", 10),
                new TableHeaderModel(ApplicationStrings.Type, true, "User.UserType", 10),
                new TableHeaderModel(ApplicationStrings.Source, true, "SourceType", 10),
                new TableHeaderModel(ApplicationStrings.FirstName, true, "FirstName", 8),
                new TableHeaderModel(ApplicationStrings.LastName, true, "LastName", 8),
                new TableHeaderModel(ApplicationStrings.Email, true, "Email", 7),
                new TableHeaderModel(ApplicationStrings.CreateDate, true, "CreateDate", 10),
                new TableHeaderModel(ApplicationStrings.Gender, true, "Gender", 7),
                new TableHeaderModel(ApplicationStrings.Question1, true, "Question1", 7),
                new TableHeaderModel(ApplicationStrings.Question2, true, "Question2", 10),
            };

            filter.Question1AnswersSelectList = this.SurveyQuestion1AnswersSelectList();
            filter.Question2AnswersSelectList = this.SurveyQuestion2AnswersSelectList();
            filter.GendersSelectList = this.GendersSelectList();
            filter.UserTypesSelectList = this.UserTypesSelectList();
            filter.SourceTypesSelectList = this.SourceTypesSelectList();

            var response = _lookupService.GetSurveyList(new GetSurveyListRequest
            {
                EventName = filter.EventName,
                Username = filter.Username,
                UserType = filter.UserType,
                SourceType = filter.SourceType,
                FirstName = filter.FirstName,
                LastName = filter.LastName,
                CreateDate = filter.CreateDate,
                Email = filter.Email,
                Gender = filter.Gender,
                Question1 = filter.Question1,
                Question2 = filter.Question2,
                PageSize = pagesize,
                Page = page,
                SortBy = sortBy,
                SortOrder = (sortOrder == "desc" ? SortOrder.Desc : SortOrder.Asc)
            });

            var rows = _mapper.Map<IEnumerable<SurveyTableRowModel>>(response.Surveys);

            if (export) return ExportSurvey(rows, filter);

            foreach (var item in rows)
            {
                item.Actions = GetSurveyItemActions(item.RowId);
            }
            return this.ListPageView(GetEntityActionsToolbar(), headers, rows, filter, sortBy, sortOrder, response.TotalCount, Request.Url, ApplicationStrings.Survey, ApplicationStrings.Survey, pagesize, page, hasSelectAll: true);
        }

        public virtual ActionResult ExportSurvey(IEnumerable<SurveyTableRowModel> data, SurveyFilterViewModel filter)
        {
            using (var stream = new MemoryStream())
            {
                XlsFile file = new XlsFile();
                file.NewFile(4);

                int row = 1;
                var headers = new[] {
                    ApplicationStrings.Event,
                    ApplicationStrings.UserName,
                    ApplicationStrings.Type,
                    ApplicationStrings.Source,
                    ApplicationStrings.FirstName,
                    ApplicationStrings.LastName,
                    ApplicationStrings.Email,
                    ApplicationStrings.CreateDate,
                    ApplicationStrings.Gender,
                    ApplicationStrings.Question1,
                    ApplicationStrings.Question2,
                };

                var fHeader = file.GetDefaultFormat;
                fHeader.Font.Style = TFlxFontStyles.Bold;
                fHeader.FillPattern.Pattern = TFlxPatternStyle.Automatic;
                fHeader.FillPattern.BgColor = TExcelColor.FromArgb(233, 242, 247, 0);

                int headerFormat = file.AddFormat(fHeader);

                //filters
                //file.SetCellValue(row, 1, ApplicationStrings.Period, headerFormat);
                //file.SetCellValue(row++, 2, string.Format("{0} - {1}", filter.FromDate.ToReportDate(), filter.ToDate.ToReportDate()), headerFormat);
                //file.SetCellValue(row, 1, ApplicationStrings.LegalForm, headerFormat);
                //if (filter.LegalForms != null)
                //{
                //    string forms = "";
                //    foreach (var form in filter.LegalForms)
                //    {
                //        forms += form.ToApplicationString() + "; ";
                //    }
                //    file.SetCellValue(row++, 2, forms, headerFormat);
                //}
                //else row++;
                //file.SetCellValue(row, 1, ApplicationStrings.Category, headerFormat);
                //file.SetCellValue(row++, 2, filter.AgreementType.HasValue ? filter.AgreementType.Value.ToApplicationString() : string.Empty, headerFormat);

                //row++;
                //headers
                for (int i = 0; i < headers.Length; i++)
                {
                    file.SetCellValue(row, i + 1, headers[i], headerFormat);
                }

                row++;

                WriteRows(file, ref row, data);

                file.AutofitCol(1, 10, false, 1.2f);

                file.Save(stream);

                return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "export.xls");
            }
        }

        protected void WriteRows(XlsFile file, ref int row, IEnumerable<SurveyTableRowModel> data)
        {
            var fText = file.GetDefaultFormat;
            fText.Format = "@";
            int textFormat = file.AddFormat(fText);

            var fNumber = file.GetDefaultFormat;
            fNumber.Format = "#,##0.00";
            int numberFormat = file.AddFormat(fNumber);

            if (data.Any())
            {
                foreach (var dataRow in data)
                {
                    file.SetCellValue(1, row, 1, dataRow.EventName, textFormat);
                    file.SetCellValue(1, row, 2, dataRow.Username, textFormat);
                    file.SetCellValue(1, row, 3, dataRow.UserType.ToApplicationString(), textFormat);
                    file.SetCellValue(1, row, 4, dataRow.SourceType.ToApplicationString(), textFormat);
                    file.SetCellValue(1, row, 5, dataRow.FirstName, textFormat);
                    file.SetCellValue(1, row, 6, dataRow.LastName, textFormat);
                    file.SetCellValue(1, row, 7, dataRow.Email, textFormat);
                    file.SetCellValue(1, row, 8, dataRow.CreateDate.Value.ToString(), textFormat);
                    file.SetCellValue(1, row, 9, dataRow.Gender.ToApplicationString(), textFormat);
                    file.SetCellValue(1, row, 10, dataRow.Question1.ToApplicationString(), textFormat);
                    file.SetCellValue(1, row, 11, dataRow.Question2.ToApplicationString(), textFormat);

                    row++;
                }
            }
        }

        [HttpGet]
        public virtual ActionResult Delete(int id)
        {
            return this.ConfirmationModal(ApplicationStrings.ConfirmDeleteApplicationUsers);
        }

        [HttpPost]
        public virtual ActionResult Delete(int id, bool confirmed = false)
        {
            return this.ProcessServiceOperation(() =>
            {
                return _lookupService.DeleteApplicationUser(new DeleteApplicationUserRequest() { ApplicationUserId = id });
            },
            ApplicationStrings.DeleteApplicationUsersSuccess,
            MVC.Administration.ApplicationUsers.Index());
        }

        [HttpGet]
        public virtual ActionResult DeleteSelected(int[] ids)
        {
            if (ids != null)
            {
                return this.ConfirmationModal(ApplicationStrings.ConfirmDeleteApplicationUsers);
            }
            TempData.AddError(ApplicationStrings.ChooseApplicationUsers);
            //return RedirectToAction(MVC.Sales.SMSSender.Index().AddRouteValue("Status", "Active"));
            return this.JsonFailure();
        }

        [HttpPost]
        public virtual ActionResult DeleteSelected(int[] ids, bool confirmed = false)
        {
            return this.ProcessServiceOperation(() =>
            {
                return _lookupService.DeleteApplicationUsers(new DeleteApplicationUsersRequest() { Ids = ids.ToList() });
            },
            ApplicationStrings.DeleteApplicationUsersSuccess,
            MVC.Administration.ApplicationUsers.Index());
        }

        private EntityActions GetSurveyItemActions(int userId)
        {
            var result = new EntityActions();

            //result.Add(true, ApplicationStrings.Edit, Url.Action(MVC.Administration.Companies.Edit(userId)));
            //result.Add(true, ApplicationStrings.Delete, Url.Action(MVC.Administration.ApplicationUsers.Delete(userId)));

            return result;
        }

        protected EntityActions GetEntityActionsToolbar()
        {
            EntityActions actions = new EntityActions();

            //actions.Add(new EntityAction()
            //{
            //    Title = ApplicationStrings.Delete,
            //    Href = Url.Action(MVC.Administration.ApplicationUsers.DeleteSelected()),
            //    Icon = "fa-minus",
            //    BootstrapContext = BootstrapContext.Default,
            //    IsCustomAction = true,
            //    CustomActionName = "selection-dependent enabled",
            //});

            actions.Add(new EntityAction()
            {
                Title = ApplicationStrings.ExportToExcel,
                Href = Url.Action(MVC.Administration.Surveys.Index()),
                Icon = "fa-excel",
                BootstrapContext = BootstrapContext.Default,
                IsCustomAction = true,
                CustomActionName = "export",
            });

            return actions;
        }
    }
}