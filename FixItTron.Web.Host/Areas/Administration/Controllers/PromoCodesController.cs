﻿using AutoMapper;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.PromoCode;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;
using Wandio.Web.Mvc;
using Wandio.Web.Mvc.Html;
using FixItTron.Web.Host.Helpers;
using FixItTron.Web.Host.Areas.Administration.Models.PromoCodes;
using System.IO;
using FlexCel.XlsAdapter;

namespace FixItTron.Web.Host.Areas.Administration.Controllers
{
    [FixItTronAuthorize]
    public partial class PromoCodesController : Controller
    {
        IPromoCodeService _promoCodeService;
        IMappingEngine _mapper;
        ILookupService _lookupService;

        public PromoCodesController(IPromoCodeService PromoCodeservice, IMappingEngine mapper, ILookupService lookupService)
        {
            _promoCodeService = PromoCodeservice;
            _mapper = mapper;
            _lookupService = lookupService;
        }

        // GET: Administration/PromoCodes
        public virtual ActionResult Index(PromoCodeFilterViewModel filter, int page = 1, int pagesize = 25, string sortBy = "Code", string sortOrder = null)
        {
            var headers = new List<TableHeaderModel>() { 
                new TableHeaderModel(ApplicationStrings.Code, true, "Code", 20),
                new TableHeaderModel(ApplicationStrings.Event, true, "Event.Name", 25),
                new TableHeaderModel(ApplicationStrings.PrintCount, true, "PrintCount", 5),
                new TableHeaderModel(ApplicationStrings.MultiUse, true, "MultiUse", 15),
                new TableHeaderModel(ApplicationStrings.Description, true, "Description", 15),
            };

            filter.YesNoSelectList = this.YesNoSelectList();
            filter.EventsSelectList = _lookupService.GetEvents().MapSelectList();

            var response = _promoCodeService.GetPromoCodeList(new GetPromoCodeListRequest
            {
                Code = filter.Code,
                PrintCount = filter.PrintCount,
                EventId = filter.EventId,
                MultiUse = filter.MultiUse,
                Description = filter.Description,
                PageSize = pagesize,
                Page = page,
                SortBy = sortBy,
                SortOrder = (sortOrder == "desc" ? SortOrder.Desc : SortOrder.Asc)
            });

            var rows = _mapper.Map<IEnumerable<PromoCodeTableRowModel>>(response.PromoCodes);
            foreach (var item in rows)
            {
                item.Actions = GetPromoCodeItemActions(item.RowId);
            }
            return this.ListPageView(GetEntityActionsToolbar(), headers, rows, filter, sortBy, sortOrder, response.TotalCount, Request.Url, ApplicationStrings.PromoCodes, ApplicationStrings.PromoCodes, pagesize, page, hasSelectAll: true);
        }

        [HttpGet]
        public virtual ActionResult Create()
        {
            var model = new AddEditPromoCodeViewModel();
            model.EventsSelectList = _lookupService.GetEvents().MapSelectList();
            return PartialView(MVC.Shared.Views._AddEditModal, model);
        }

        [HttpPost]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Create)]
        public virtual ActionResult Create(AddEditPromoCodeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = _mapper.Map<AddEditPromoCodeItem>(model);
                request.UserName = User.Identity.Name;
                var response = _promoCodeService.CreatePromoCode(new CreatePromoCodeRequest { PromoCode = request });
                if (response.Success)
                {
                    return this.JsonSuccess();
                }

                ModelState.AddModelError(string.Empty, response.ErrorMessage);
            }

            model.EventsSelectList = _lookupService.GetEvents().MapSelectList();
            return PartialView(MVC.Shared.Views._AddEditModal, model);
        }

        [HttpGet]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Update)]
        public virtual ActionResult Edit(int id)
        {
            var response = _promoCodeService.GetPromoCodeForUpdate(new GetPromoCodeForUpdateRequest { PromoCodeId = id });

            AddEditPromoCodeViewModel model = _mapper.Map<AddEditPromoCodeViewModel>(response.PromoCode);
            model.EventsSelectList = _lookupService.GetEvents().MapSelectList();

            ViewBag.IsEdit = true;
            return PartialView(MVC.Shared.Views._AddEditModal, model);
        }

        [HttpPost]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Update)]
        public virtual ActionResult Edit(int id, AddEditPromoCodeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = new AddEditPromoCodeItem() { PromoCodeId = id };
                request = Mapper.Map<AddEditPromoCodeViewModel, AddEditPromoCodeItem>(model, request);
                //request.UserName = User.Identity.Name;
                var response = _promoCodeService.UpdatePromoCode(new UpdatePromoCodeRequest { PromoCode = request });
                if (response.Success)
                {
                    return this.JsonSuccess();
                }
                ModelState.AddModelError(string.Empty, response.ErrorMessage);
            }
            model.EventsSelectList = _lookupService.GetEvents().MapSelectList();
            ViewBag.IsEdit = true;
            return PartialView(MVC.Shared.Views._AddEditModal, model);
        }

        [HttpGet]
        public virtual ActionResult Delete(int id)
        {
            return this.ConfirmationModal(ApplicationStrings.ConfirmDeletePromoCode);
        }

        [HttpPost]
        public virtual ActionResult Delete(int id, bool confirmed = false)
        {
            return this.ProcessServiceOperation(() =>
            {
                return _promoCodeService.DeletePromoCode(new DeletePromoCodeRequest() { PromoCodeId = id });
            },
            ApplicationStrings.DeletePromoCodeSuccess,
            MVC.Administration.PromoCodes.Index());
        }

        [HttpGet]
        public virtual ActionResult DeleteSelected(int[] ids)
        {
            if (ids != null)
            {
                return this.ConfirmationModal(ApplicationStrings.ConfirmDeletePromoCode);
            }
            TempData.AddError(ApplicationStrings.ChoosePromoCodes);
            //return RedirectToAction(MVC.Sales.SMSSender.Index().AddRouteValue("Status", "Active"));
            return this.JsonFailure();
        }

        [HttpPost]
        public virtual ActionResult DeleteSelected(int[] ids, bool confirmed = false)
        {
            return this.ProcessServiceOperation(() =>
            {
                return _promoCodeService.DeletePromoCodes(new DeletePromoCodesRequest() { Ids = ids.ToList() });
            },
            ApplicationStrings.DeletePromoCodeSuccess,
            MVC.Administration.PromoCodes.Index());
        }

        #region Import PromoCodes

        [HttpGet]
        public virtual ActionResult ImportPromoCodes()
        {
            return PartialView();
        }

        [HttpPost]
        public virtual ActionResult ImportPromoCodes(HttpPostedFileBase addFile)
        {
            List<AddEditPromoCodeItem> promoCode = null;
            if (addFile == null)
            {
                ModelState.AddModelError("", ApplicationStrings.FileIsRequired);
            }
            else
            {
                byte[] contents = new byte[addFile.ContentLength];
                addFile.InputStream.Read(contents, 0, contents.Length);
                promoCode = GetPromoCodeDataFromExcel(contents);
                if (promoCode.Count == 0)
                {
                    ModelState.AddModelError("", ApplicationStrings.FileIsNotCorrectlyFilled);
                }
            }

            if (ModelState.IsValid)
            {
                var response = _promoCodeService.AddPromoCodes(new AddPromoCodesRequest { PromoCodes = promoCode });
                if (response.Success)
                {
                    return this.JsonSuccess();
                }
                ModelState.AddModelError("", response.ErrorMessage);
                //return RedirectToAction(MVC.Administration.PromoCodes.Index());
            }

            return PartialView();
        }

        private List<AddEditPromoCodeItem> GetPromoCodeDataFromExcel(byte[] fileData)
        {
            MemoryStream stream = new MemoryStream(fileData);
            var result = new List<AddEditPromoCodeItem>();
            XlsFile file = new XlsFile();
            file.Open(stream);

            // Analysing file data
            int rowCount = file.GetRowCount(1);

            for (int rowIndex = 5; rowIndex <= rowCount; rowIndex++)
            {
                if ((file.GetCellValue(rowIndex, 1) == null || string.IsNullOrEmpty(file.GetCellValue(rowIndex, 1).ToString())) &&
                    (file.GetCellValue(rowIndex, 2) == null || string.IsNullOrEmpty(file.GetCellValue(rowIndex, 2).ToString())) &&
                    (file.GetCellValue(rowIndex, 3) == null || string.IsNullOrEmpty(file.GetCellValue(rowIndex, 3).ToString())))
                {
                    break;
                }

                var promoCode = new AddEditPromoCodeItem();

                promoCode.Code = Convert.ToString(file.GetCellValue(rowIndex, 1));
                int printCount;
                if (Int32.TryParse(Convert.ToString(file.GetCellValue(rowIndex, 2)), out printCount))
                {
                    promoCode.PrintCount = printCount;
                }
                promoCode.MultiUse = Convert.ToString(file.GetCellValue(rowIndex, 3)).ToLower() == "yes" ? true : false;
                promoCode.UserName = User.Identity.Name;
                promoCode.Description = Convert.ToString(file.GetCellValue(rowIndex, 4));
                promoCode.EventName = Convert.ToString(file.GetCellValue(rowIndex, 5));

                result.Add(promoCode);
            }
            return result;
        }

        [HttpGet]
        public virtual ActionResult GetPromoCodesImportTemplate()
        {
            var docPath = Path.Combine(Server.MapPath("~/Files"), "PromoCodeTemplate.xlsx");
            return File(docPath, "application/vnd.ms-excel", "PromoCodeTemplate.xlsx");
        }

        #endregion

        private EntityActions GetPromoCodeItemActions(int promoCodeId)
        {
            var result = new EntityActions();

            result.Add(true, ApplicationStrings.Edit, Url.Action(MVC.Administration.PromoCodes.Edit(promoCodeId)));
            result.Add(true, ApplicationStrings.Delete, Url.Action(MVC.Administration.PromoCodes.Delete(promoCodeId)));

            return result;
        }

        protected EntityActions GetEntityActionsToolbar()
        {
            EntityActions actions = new EntityActions();

            //if (User.HasRight(Securable.Roles, Permissions.Create))
            {
                actions.Add(new EntityAction()
                {
                    Title = ApplicationStrings.CreatePromoCode,
                    Href = Url.Action(MVC.Administration.PromoCodes.Create()),
                    Icon = "fa-plus",
                    BootstrapContext = BootstrapContext.Default,
                });

                actions.Add(new EntityAction()
                {
                    Title = ApplicationStrings.ImportFromExcel,
                    Href = Url.Action(MVC.Administration.PromoCodes.ImportPromoCodes()),
                    Icon = "fa-upload",
                    BootstrapContext = BootstrapContext.Default,
                });

                actions.Add(new EntityAction()
                {
                    Title = ApplicationStrings.Delete,
                    Href = Url.Action(MVC.Administration.PromoCodes.DeleteSelected()),
                    Icon = "fa-minus",
                    BootstrapContext = BootstrapContext.Default,
                    IsCustomAction = true,
                    CustomActionName = "selection-dependent enabled",
                });
            }

            return actions;
        }
    }
}