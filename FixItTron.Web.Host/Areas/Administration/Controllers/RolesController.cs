﻿using AutoMapper;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Role;
using FixItTron.Web.Host.Areas.Administration.Models;
using FixItTron.Web.Host.Controllers;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;
using Wandio.Web.Mvc;
using FixItTron.Web.Host.Helpers;
using Wandio.Web.Mvc.Html;
using FixItTron.Services.Models.Shared;

namespace FixItTron.Web.Host.Areas.Administration.Controllers
{
    //[FixItTronAuthorize(Securable.Roles, Permissions.Read)]
    public partial class RolesController : Controller
    {
        IRoleService _roleService;
        IMappingEngine _mapper;

        public RolesController(IRoleService roleService, IMappingEngine mapper)
        {
            _roleService = roleService;
            _mapper = mapper;
        }

        public virtual ActionResult Index(RoleFilterViewModel filter, int page = 1, int pagesize = 25, string sortBy = "Name", string sortOrder = null)
        {
            var headers = new List<TableHeaderModel>() { 
                new TableHeaderModel(ApplicationStrings.Name, true, "Name"),
                new TableHeaderModel(ApplicationStrings.EmployeeCount, width:40)
            };

            var response = _roleService.GetRoleList(new GetRoleListRequest
            {
                Name = filter.Name,
                PageSize = pagesize,
                Page = page,
                SortBy = sortBy,
                SortOrder = (sortOrder == "desc" ? SortOrder.Desc : SortOrder.Asc)
            });
            var rows = _mapper.Map<IEnumerable<RoleTableRowModel>>(response.Roles);
            return this.ListPageView(GetEntityActionsToolbar(), headers, rows, filter, sortBy, sortOrder, response.TotalCount, Request.Url, ApplicationStrings.Roles, ApplicationStrings.RoleList, pagesize, page);
        }

        [HttpGet]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Create)]
        public virtual ActionResult Create()
        {
            var viewModel = new CreateRoleViewModel();
            SetSelectLists(viewModel);
            return View(viewModel);
        }

        [HttpPost]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Create)]
        public virtual ActionResult Create(CreateRoleViewModel viewModel, bool continueEdit = false)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var request = Mapper.Map<CreateRoleRequest>(viewModel);
                    var response = _roleService.CreateRole(request);
                    return RedirectToAction(continueEdit ? MVC.Administration.Roles.Edit(response.RoleId) : MVC.Administration.Roles.Index());
                }
                catch (FluentValidation.ValidationException ex)
                {
                    this.HandleValidationException(ex);
                }
            }
            SetSelectLists(viewModel);
            return View(viewModel);
        }

        public void SetSelectLists(CreateRoleViewModel viewModel)
        {
            Func<Securable[], IDictionary<Securable, Permissions>> filterConfiguration = filter => viewModel.Configuration.Where(config => filter.Contains(config.Key)).ToDictionary(x => x.Key, x => x.Value);

            Func<string, Securable[], Securable[], Securable[], EditRoleSection> trinity = (title, operations, information, configuration) => new EditRoleSection()
            {
                Title = title,
                SubSections = new List<EditRoleSection>()
                {
                    new EditRoleSection
                    {
                        Title = ApplicationStrings.Operations,
                        Permissions = filterConfiguration(operations)
                    },
                    new EditRoleSection
                    {
                        Title = ApplicationStrings.Information,
                        Permissions = filterConfiguration(information)
                    },
                    new EditRoleSection
                    {
                        Title = ApplicationStrings.Configuration,
                        Permissions = filterConfiguration(configuration)
                    }
                }
            };

            viewModel.Configuration = _roleService.GetConfiguration();
            viewModel.Permissions = viewModel.Permissions ?? new List<EditRolePermissionModel>();
            viewModel.Sections = new List<EditRoleSection>();

            viewModel.Sections.Add(trinity(ApplicationStrings.Sales,
                new[] { Securable.Schedules, Securable.SalesQuotations, Securable.SalesOrders, Securable.Invoices, Securable.InboundPayments },
                new[] { Securable.CustomerFinancials },
                new[] { Securable.Customers, Securable.ObjectAdditionals, Securable.ContractorCategories, Securable.PriceLists, Securable.PriceRules, Securable.Jobs,
                Securable.TaxGroups, Securable.Countries, Securable.Cities, Securable.Streets, Securable.Districts, Securable.ScheduleAdditionalStatuses }));

            viewModel.Sections.Add(trinity(ApplicationStrings.Products, new[] { Securable.None }, new[] { Securable.None },
                new[] { Securable.Services, Securable.Materials, Securable.ServiceCategories, Securable.MaterialCategories, Securable.Warehouses, Securable.WarehouseTypes,
                Securable.ServiceClasses, Securable.MaterialTypes, Securable.UnitOfMeasures }));

            viewModel.Sections.Add(trinity(ApplicationStrings.Products,
                new[] { Securable.BankOperations, Securable.BoxOfficeOperations },
                new[] { Securable.None },
                new[] { Securable.BankAccounts, Securable.BoxOffices, Securable.PaymentClassifiers, Securable.Banks, Securable.Currencies }));

            viewModel.Sections.Add(trinity(ApplicationStrings.Administration, new[] { Securable.None }, new[] { Securable.None },
                new[] { Securable.SystemConfiguration, Securable.Roles, Securable.Employees, Securable.Positions, Securable.Reasons, Securable.ScheduleGeneralConfiguration,
                Securable.EmployeeSchedule, Securable.WorkingSchedules, Securable.EmployeeAbsences, Securable.AbsenceTypes, Securable.CodeGenerations,
                Securable.Questionnaires, Securable.NotificationTemplates }));
            
            //viewModel.Sections.Add(trinity(ApplicationStrings.Purchases,
            //    new[] { Securable.PurchaseQuotes, Securable.PurchaseOrders, Securable.OutboundPayments, Securable.IncomingWaybills, },
            //    new[] { Securable.VendorFinancials }, new[] { Securable.ExpenseTypes, Securable.Vendors }));
            //viewModel.Sections.Add(trinity(ApplicationStrings.ProductsAndWarehouseMenuName,
            //    new[] { Securable.WarehouseAdjustments, Securable.WarehouseTransfers, Securable.WarehouseTransferOrders, Securable.Waybills, Securable.InventoryChecks },
            //    new[] { Securable.StockRemains, Securable.TransferOrderByProducts, },
            //    new[] { Securable.Products, Securable.Warehouses, Securable.WarehouseTypes, Securable.ProductCategories, Securable.Brands, Securable.UnitOfMeasures, Securable.ProductKinds }));
            //viewModel.Sections.Add(new EditRoleSection()
            //{
            //    Title = ApplicationStrings.Production,
            //    Permissions = filterConfiguration(new[] { Securable.WorkOrders, Securable.BillOfMaterials })
            //});
            //viewModel.Sections.Add(new EditRoleSection()
            //{
            //    Title = ApplicationStrings.FinancialMovement,
            //    SubSections = new List<EditRoleSection>()
            //    {
            //        new EditRoleSection
            //        {
            //            Title = ApplicationStrings.Operations,
            //            Permissions = filterConfiguration(new []{ Securable.BankOperations, Securable.BoxOfficeOperations, Securable.BankStatementImports })
            //        },
            //        new EditRoleSection
            //        {
            //            Title = ApplicationStrings.Configuration,
            //            Permissions = filterConfiguration(new []{ Securable.BoxOffices, Securable.Banks, Securable.Currencies, Securable.BankAccounts, Securable.PaymentClassifiers, Securable.BankStatementImportSettings })
            //        }
            //    }
            //});

            //viewModel.Sections.Add(new EditRoleSection()
            //{
            //    Title = ApplicationStrings.Administration,
            //    SubSections = new List<EditRoleSection>()
            //    {
            //        //new EditRoleSection
            //        //{
            //        //    Title = ApplicationStrings.Information,
            //        //    Permissions = filterConfiguration(new []{ Securable.Audits, Securable.Documents})
            //        //},
            //        new EditRoleSection
            //        {
            //            Title = ApplicationStrings.Configuration,
            //            Permissions = filterConfiguration(new []{  Securable.Employees,Securable.Roles })
            //        }
            //    }
            //});

            //viewModel.Sections.Add(new EditRoleSection()
            //{
            //    Title = ApplicationStrings.Reports,
            //    Permissions = filterConfiguration(new[] { Securable.ProductSalesReport, Securable.ProductSalesByCustomersReport, Securable.PaymentsByCustomersReport, Securable.PaymentsReport, Securable.PurchasesReport, Securable.ProductsFlowReport, Securable.ManufacturingReport, Securable.Top10ProductSales, Securable.Top10ContractorSales, Securable.ContractorsByCategory })
            //});

            //viewModel.Sections.Add(new EditRoleSection()
            //{
            //    Title = ApplicationStrings.MobileClient,
            //    Permissions = filterConfiguration(new[] { Securable.MobileClient })
            //});

            foreach (var item in viewModel.Configuration)
            {
                if (!(viewModel.Permissions.Any(p => p.Securable == item.Key)))
                {
                    var rolePermission = new EditRolePermissionModel();
                    rolePermission.Securable = item.Key;
                    viewModel.Permissions.Add(rolePermission);
                }
            }

            var sectionsByConfig = viewModel.Configuration.Select(x => x.Key).ToList();
            var sectionsByViewMOdel = viewModel.Sections.Where(s => s.SubSections != null).SelectMany(s => s.SubSections).SelectMany(s => s.Permissions).Select(p => p.Key).ToList();
            var except = sectionsByConfig.Except(sectionsByViewMOdel);
        }

        [HttpGet]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Update)]
        public virtual ActionResult Edit(int id)
        {
            var role = _roleService.GetRole(new GetRoleRequest() { RoleId = id });
            this.EnsureResourceFound(role);
            var viewModel = new CreateRoleViewModel();
            viewModel = Mapper.Map<GetRoleResponse, CreateRoleViewModel>(role, viewModel);
            SetSelectLists(viewModel);
            ModelState.Clear();
            return View(viewModel);
        }

        [HttpPost]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Update)]
        public virtual ActionResult Edit(int id, CreateRoleViewModel viewModel, bool continueEdit = false)
        {
            var role = _roleService.GetRole(new GetRoleRequest() { RoleId = id });
            if (ModelState.IsValid)
            {
                try
                {
                    this.EnsureResourceFound(role);
                    var request = new UpdateRoleRequest() { RoleId = id };
                    request = Mapper.Map<CreateRoleViewModel, UpdateRoleRequest>(viewModel, request);
                    _roleService.UpdateRole(request);
                    return RedirectToAction(continueEdit ? MVC.Administration.Roles.Edit(id) : MVC.Administration.Roles.Index());
                }
                catch (FluentValidation.ValidationException ex)
                {
                    this.HandleValidationException(ex);
                }
            }
            SetSelectLists(viewModel);
            return View(viewModel);
        }

        protected EntityActions GetEntityActionsToolbar()
        {
            EntityActions actions = new EntityActions();

            //if (User.HasRight(Securable.Roles, Permissions.Create))
            {
                actions.Add(new EntityAction()
                {
                    IsCustomAction = true,
                    Title = ApplicationStrings.CreateRole,
                    Href = Url.Action(MVC.Administration.Roles.Create()),
                    Icon = "fa-plus",
                    BootstrapContext = BootstrapContext.Default,
                });
            }

            return actions;
        }
    }
}