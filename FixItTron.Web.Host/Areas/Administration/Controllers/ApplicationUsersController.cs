﻿using AutoMapper;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Lookup.ApplicationUsers;
using FixItTron.Web.Host.Areas.Administration.Models.ApplicationUsers;
using FixItTron.Web.Host.Helpers;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;
using Wandio.Web.Mvc;
using Wandio.Web.Mvc.Html;

namespace FixItTron.Web.Host.Areas.Administration.Controllers
{
    public partial class ApplicationUsersController : Controller
    {
        ILookupService _lookupService;
        IMappingEngine _mapper;

        public ApplicationUsersController(ILookupService lookupService, IMappingEngine mapper)
        {
            _lookupService = lookupService;
            _mapper = mapper;
        }

        // GET: Administration/Companies
        public virtual ActionResult Index(ApplicationUserFilterViewModel filter, int page = 1, int pagesize = 25, string sortBy = "Username", string sortOrder = null)
        {
            var headers = new List<TableHeaderModel>() {
                new TableHeaderModel(ApplicationStrings.UserName, true, "Username", 20),
                new TableHeaderModel(ApplicationStrings.Name, true, "FullName", 8),
                new TableHeaderModel(ApplicationStrings.Email, true, "Email", 7),
                new TableHeaderModel(ApplicationStrings.AvailablePrints, true, "AvailablePrints", 5),
                new TableHeaderModel(ApplicationStrings.UsedPrints, true, "UsedPrints", 5),
                new TableHeaderModel(ApplicationStrings.CreateDate, true, "CreateDate", 10),
                new TableHeaderModel(ApplicationStrings.Type, true, "UserType", 10),
            };

            filter.UserTypesSelectList = this.UserTypesSelectList();

            var response = _lookupService.GetApplicationUserList(new GetApplicationUserListRequest
            {
                Username = filter.Username,
                AvailablePrints = filter.AvailablePrints,
                CreateDate = filter.CreateDate,
                Email = filter.Email,
                FullName = filter.FullName,
                UsedPrints = filter.UsedPrints,
                UserType = filter.UserType,
                PageSize = pagesize,
                Page = page,
                SortBy = sortBy,
                SortOrder = (sortOrder == "desc" ? SortOrder.Desc : SortOrder.Asc)
            });

            var rows = _mapper.Map<IEnumerable<ApplicationUserTableRowModel>>(response.Users);
            foreach (var item in rows)
            {
                item.Actions = GetApplicationUserItemActions(item.RowId);
            }
            return this.ListPageView(GetEntityActionsToolbar(), headers, rows, filter, sortBy, sortOrder, response.TotalCount, Request.Url, ApplicationStrings.ApplicationUsers, ApplicationStrings.ApplicationUsers, pagesize, page, hasSelectAll: true);
        }

        [HttpGet]
        public virtual ActionResult Delete(int id)
        {
            return this.ConfirmationModal(ApplicationStrings.ConfirmDeleteApplicationUsers);
        }

        [HttpPost]
        public virtual ActionResult Delete(int id, bool confirmed = false)
        {
            return this.ProcessServiceOperation(() =>
            {
                return _lookupService.DeleteApplicationUser(new DeleteApplicationUserRequest() { ApplicationUserId = id });
            },
            ApplicationStrings.DeleteApplicationUsersSuccess,
            MVC.Administration.ApplicationUsers.Index());
        }

        [HttpGet]
        public virtual ActionResult DeleteSelected(int[] ids)
        {
            if (ids != null)
            {
                return this.ConfirmationModal(ApplicationStrings.ConfirmDeleteApplicationUsers);
            }
            TempData.AddError(ApplicationStrings.ChooseApplicationUsers);
            //return RedirectToAction(MVC.Sales.SMSSender.Index().AddRouteValue("Status", "Active"));
            return this.JsonFailure();
        }

        [HttpPost]
        public virtual ActionResult DeleteSelected(int[] ids, bool confirmed = false)
        {
            return this.ProcessServiceOperation(() =>
            {
                return _lookupService.DeleteApplicationUsers(new DeleteApplicationUsersRequest() { Ids = ids.ToList() });
            },
            ApplicationStrings.DeleteApplicationUsersSuccess,
            MVC.Administration.ApplicationUsers.Index());
        }

        private EntityActions GetApplicationUserItemActions(int userId)
        {
            var result = new EntityActions();

            //result.Add(true, ApplicationStrings.Edit, Url.Action(MVC.Administration.Companies.Edit(userId)));
            result.Add(true, ApplicationStrings.Delete, Url.Action(MVC.Administration.ApplicationUsers.Delete(userId)));

            return result;
        }

        protected EntityActions GetEntityActionsToolbar()
        {
            EntityActions actions = new EntityActions();

            actions.Add(new EntityAction()
            {
                Title = ApplicationStrings.Delete,
                Href = Url.Action(MVC.Administration.ApplicationUsers.DeleteSelected()),
                Icon = "fa-minus",
                BootstrapContext = BootstrapContext.Default,
                IsCustomAction = true,
                CustomActionName = "selection-dependent enabled",
            });

            return actions;
        }
    }
}