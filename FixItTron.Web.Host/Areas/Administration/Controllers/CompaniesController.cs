﻿using AutoMapper;
using FixItTron.Services.Interfaces;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;
using Wandio.Web.Mvc;
using Wandio.Web.Mvc.Html;
using FixItTron.Web.Host.Helpers;
using FixItTron.Services.Models.Company;
using FixItTron.Web.Host.Areas.Administration.Models.Companies;

namespace FixItTron.Web.Host.Areas.Administration.Controllers
{
    [FixItTronAuthorize]
    public partial class CompaniesController : Controller
    {
        ICompanyService _companyService;
        IMappingEngine _mapper;

        public CompaniesController(ICompanyService Companieservice, IMappingEngine mapper)
        {
            _companyService = Companieservice;
            _mapper = mapper;
        }

        // GET: Administration/Companies
        public virtual ActionResult Index(CompanyFilterViewModel filter, int page = 1, int pagesize = 25, string sortBy = "Name", string sortOrder = null)
        {
            var headers = new List<TableHeaderModel>() { 
                new TableHeaderModel(ApplicationStrings.Name, true, "Name"),
            };

            var response = _companyService.GetCompanyList(new GetCompanyListRequest
            {
                Name = filter.Name,
                PageSize = pagesize,
                Page = page,
                SortBy = sortBy,
                SortOrder = (sortOrder == "desc" ? SortOrder.Desc : SortOrder.Asc)
            });

            var rows = _mapper.Map<IEnumerable<CompanyTableRowModel>>(response.Companies);
            foreach (var item in rows)
            {
                item.Actions = GetCompanyItemActions(item.RowId);
            }
            return this.ListPageView(GetEntityActionsToolbar(), headers, rows, filter, sortBy, sortOrder, response.TotalCount, Request.Url, ApplicationStrings.Companies, ApplicationStrings.Companies, pagesize, page);
        }

        [HttpGet]
        public virtual ActionResult Create()
        {
            var model = new AddEditCompanyViewModel();
            return PartialView(MVC.Shared.Views._AddEditModal, model);
        }

        [HttpPost]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Create)]
        public virtual ActionResult Create(AddEditCompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = _mapper.Map<AddEditCompanyItem>(model);
                request.UserName = User.Identity.Name;
                var response = _companyService.CreateCompany(new CreateCompanyRequest { Company = request });
                if (response.Success)
                {
                    return this.JsonSuccess();
                }

                ModelState.AddModelError(string.Empty, response.ErrorMessage);
            }

            return PartialView(MVC.Shared.Views._AddEditModal, model);
        }

        [HttpGet]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Update)]
        public virtual ActionResult Edit(int id)
        {
            var response = _companyService.GetCompanyForUpdate(new GetCompanyForUpdateRequest { CompanyId = id });

            AddEditCompanyViewModel model = _mapper.Map<AddEditCompanyViewModel>(response.Company);

            ViewBag.IsEdit = true;
            return PartialView(MVC.Shared.Views._AddEditModal, model);
        }

        [HttpPost]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Update)]
        public virtual ActionResult Edit(int id, AddEditCompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = new AddEditCompanyItem() { CompanyId = id };
                request = Mapper.Map<AddEditCompanyViewModel, AddEditCompanyItem>(model, request);
                request.UserName = User.Identity.Name;
                var response = _companyService.UpdateCompany(new UpdateCompanyRequest { Company = request });
                if (response.Success)
                {
                    return this.JsonSuccess();
                }
                ModelState.AddModelError(string.Empty, response.ErrorMessage);
            }
            ViewBag.IsEdit = true;
            return PartialView(MVC.Shared.Views._AddEditModal, model);
        }

        [HttpGet]
        public virtual ActionResult Delete(int id)
        {
            return this.ConfirmationModal(ApplicationStrings.ConfirmDeleteCompany);
        }

        [HttpPost]
        public virtual ActionResult Delete(int id, bool confirmed = false)
        {
            return this.ProcessServiceOperation(() =>
            {
                return _companyService.DeleteCompany(new DeleteCompanyRequest() { CompanyId = id });
            },
            ApplicationStrings.DeleteCompanySuccess,
            MVC.Administration.Companies.Index());
        }

        private EntityActions GetCompanyItemActions(int CompanyId)
        {
            var result = new EntityActions();

            result.Add(true, ApplicationStrings.Edit, Url.Action(MVC.Administration.Companies.Edit(CompanyId)));
            result.Add(true, ApplicationStrings.Delete, Url.Action(MVC.Administration.Companies.Delete(CompanyId)));

            return result;
        }

        protected EntityActions GetEntityActionsToolbar()
        {
            EntityActions actions = new EntityActions();

            //if (User.HasRight(Securable.Roles, Permissions.Create))
            {
                actions.Add(new EntityAction()
                {
                    Title = ApplicationStrings.CreateCompany,
                    Href = Url.Action(MVC.Administration.Companies.Create()),
                    Icon = "fa-plus",
                    BootstrapContext = BootstrapContext.Default,
                });
            }

            return actions;
        }
    }
}