﻿using AutoMapper;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Configurations;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;
using Wandio.Web.Mvc;
using FixItTron.Web.Host.Helpers;
using FixItTron.Web.Host.Areas.Administration.Models.Configurations;

namespace FixItTron.Web.Host.Areas.Administration.Controllers
{
    [FixItTronAuthorize]
    public partial class ConfigurationController : Controller
    {
        IConfigurationsService _configurationsService;
        IMappingEngine _mapper;

        public ConfigurationController(IConfigurationsService configurationsService, IMappingEngine mapper)
        {
            _configurationsService = configurationsService;
            _mapper = mapper;
        }

        // GET: Administration/Configurations
        public virtual ActionResult Index(ConfigurationFilterViewModel filter, int page = 1, int pagesize = 25, string sortBy = "Name", string sortOrder = null)
        {
            var headers = new List<TableHeaderModel>() { 
                new TableHeaderModel(ApplicationStrings.Name, true, "Name"),
                new TableHeaderModel(ApplicationStrings.Value, true, "Value"),
            };

            var response = _configurationsService.GetConfigurationList(new GetConfigurationListRequest
            {
                Name = filter.Name,
                Value = filter.Value,
                PageSize = pagesize,
                Page = page,
                SortBy = sortBy,
                SortOrder = (sortOrder == "desc" ? SortOrder.Desc : SortOrder.Asc)
            });

            var rows = _mapper.Map<IEnumerable<ConfigurationTableRowModel>>(response.Configurations);
            foreach (var item in rows)
            {
                item.Actions = GetConfigurationItemActions(item.RowId);
            }
            return this.ListPageView(null, headers, rows, filter, sortBy, sortOrder, response.TotalCount, Request.Url, ApplicationStrings.Configurations, ApplicationStrings.Configurations, pagesize, page);
        }

        [HttpGet]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Update)]
        public virtual ActionResult Edit(int id)
        {
            var response = _configurationsService.GetConfigurationForUpdate(new GetConfigurationForUpdateRequest { ConfigurationId = id });

            EditConfigurationViewModel model = _mapper.Map<EditConfigurationViewModel>(response.Configuration);

            ViewBag.IsEdit = true;
            return PartialView(MVC.Shared.Views._AddEditModal, model);
        }

        [HttpPost]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Update)]
        public virtual ActionResult Edit(int id, EditConfigurationViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = new UpdateConfigurationRequest() { ConfigurationId = id, Value = model.Value };
                //request = Mapper.Map<EditConfigurationViewModel, AddEditConfigurationItem>(model, request);
                //request.UserName = User.Identity.Name;
                var response = _configurationsService.UpdateConfiguration(request);
                if (response.Success)
                {
                    return this.JsonSuccess();
                }
                ModelState.AddModelError(string.Empty, response.ErrorMessage);
            }
            ViewBag.IsEdit = true;
            return PartialView(MVC.Shared.Views._AddEditModal, model);
        }

        private EntityActions GetConfigurationItemActions(int configurationId)
        {
            var result = new EntityActions();

            result.Add(true, ApplicationStrings.Edit, Url.Action(MVC.Administration.Configuration.Edit(configurationId)));
            //result.Add(true, ApplicationStrings.Delete, Url.Action(MVC.Administration.Configurations.Delete(ConfigurationId)));

            return result;
        }
    }
}