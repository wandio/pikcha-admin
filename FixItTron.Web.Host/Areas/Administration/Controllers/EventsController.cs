﻿using AutoMapper;
using FixItTron.Services.Interfaces;
using FixItTron.Services.Models.Event;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;
using Wandio.Web.Mvc;
using Wandio.Web.Mvc.Html;
using FixItTron.Web.Host.Helpers;
using FixItTron.Web.Host.Areas.Administration.Models.Events;

namespace FixItTron.Web.Host.Areas.Administration.Controllers
{
    [FixItTronAuthorize]
    public partial class EventsController : Controller
    {
        IEventService _eventService;
        IMappingEngine _mapper;
        ILookupService _lookupService;

        public EventsController(IEventService Eventservice, IMappingEngine mapper, ILookupService lookupService)
        {
            _eventService = Eventservice;
            _mapper = mapper;
            _lookupService = lookupService;
        }

        // GET: Administration/Events
        public virtual ActionResult Index(EventFilterViewModel filter, int page = 1, int pagesize = 25, string sortBy = "Name", string sortOrder = null)
        {
            var headers = new List<TableHeaderModel>() {
                new TableHeaderModel(ApplicationStrings.Name, true, "Name", 20),
                new TableHeaderModel(ApplicationStrings.EventCode, true, "EventCode", 20),
                new TableHeaderModel(ApplicationStrings.Company, true, "Company.Name", 20),
                new TableHeaderModel(ApplicationStrings.UserName, true, "Username", 13),
                new TableHeaderModel(ApplicationStrings.StartDate, true, "StartDate", 10),
                new TableHeaderModel(ApplicationStrings.EndDate, true, "EndDate", 10),
                new TableHeaderModel(ApplicationStrings.PrintCount, true, "PrintCount", 7),
                new TableHeaderModel(ApplicationStrings.ShowLogoOnShare, true, "ShowLogoOnShare", 7),
                new TableHeaderModel(ApplicationStrings.ShowLogoOnPreview, true, "ShowLogoOnPreview", 7),
                new TableHeaderModel(ApplicationStrings.IsActive, true, "IsActive", 10),
            };

            filter.YesNoSelectList = this.YesNoSelectList();
            filter.CompaniesSelectList = _lookupService.GetCompanies().MapSelectList();

            var response = _eventService.GetEventList(new GetEventListRequest
            {
                Name = filter.Name,
                EventCode = filter.EventCode,
                CompanyId = filter.CompanyId,
                IsActive = filter.IsActive,
                Username = filter.Username,
                StartDate = filter.StartDate,
                EndDate = filter.EndDate,
                PrintCount = filter.PrintCount,
                ShowLogoOnPreview = filter.ShowLogoOnPreview,
                ShowLogoOnShare = filter.ShowLogoOnShare,
                PageSize = pagesize,
                Page = page,
                SortBy = sortBy,
                SortOrder = (sortOrder == "desc" ? SortOrder.Desc : SortOrder.Asc)
            });

            var rows = _mapper.Map<IEnumerable<EventTableRowModel>>(response.Events);
            foreach (var item in rows)
            {
                item.Actions = GetEventItemActions(item.RowId);
            }
            return this.ListPageView(GetEntityActionsToolbar(), headers, rows, filter, sortBy, sortOrder, response.TotalCount, Request.Url, ApplicationStrings.Events, ApplicationStrings.Events, pagesize, page);
        }

        [HttpGet]
        public virtual ActionResult Create()
        {
            var model = new AddEditEventViewModel();
            model.IsActive = true;
            model.StartDate = DateTime.Now;
            model.EndDate = DateTime.Now;
            model.CompaniesSelectList = _lookupService.GetCompanies().MapSelectList();
            return View(model);
        }

        [HttpPost]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Create)]
        public virtual ActionResult Create(HttpPostedFileBase addFile, HttpPostedFileBase addBackgroundFile, HttpPostedFileBase addMobileFile, HttpPostedFileBase addInstagramFile, HttpPostedFileBase addFacebookFile, AddEditEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = _mapper.Map<AddEditEventItem>(model);
                request.UserName = User.Identity.Name;
                if (addBackgroundFile != null)
                {
                    request.ShowBackgroundLogo = true;
                }
                var response = _eventService.CreateEvent(new CreateEventRequest { Event = request });
                if (response.Success)
                {
                    TempData.AddSuccess(ApplicationStrings.EventCreateSuccess);

                    var logos = new List<LogoItemViewModel>();
                    logos.Add(new LogoItemViewModel { File = addFile, LogoType = Common.Enums.LogoType.MainLogo });
                    logos.Add(new LogoItemViewModel { File = addMobileFile, LogoType = Common.Enums.LogoType.MobileLogo });
                    logos.Add(new LogoItemViewModel { File = addInstagramFile, LogoType = Common.Enums.LogoType.InstagramLogo });
                    logos.Add(new LogoItemViewModel { File = addFacebookFile, LogoType = Common.Enums.LogoType.FacebookLogo });
                    logos.Add(new LogoItemViewModel { File = addBackgroundFile, LogoType = Common.Enums.LogoType.BackgroundLogo });

                    foreach (var logo in logos)
                    {
                        var requestLogo = new ChangeEventLogoRequest();
                        if (logo.File != null)
                        {
                            byte[] contents = new byte[logo.File.ContentLength];
                            logo.File.InputStream.Read(contents, 0, contents.Length);
                            var documentFile = new UploadedFileListItem()
                            {
                                Content = contents,//Wandio.Web.Mvc.Html.ImageHelper.ResizeImage(contents, 120, 120),
                                ContentType = logo.File.ContentType,
                                FileName = logo.File.FileName
                            };

                            var documentToAdd = this.GetDocumentImpl(1, "Event", "Event", "Logo", " Event Logo", documentFile);
                            requestLogo.Logo = documentToAdd;
                            requestLogo.LogoType = logo.LogoType;
                            requestLogo.EventId = response.EventId;
                            var responseLogo = _eventService.ChangeEventLogo(requestLogo);
                            if (responseLogo.Success)
                            {
                                TempData.AddSuccess(ApplicationStrings.LogoUpdateSuccess);
                            }
                        }
                    }
                    return RedirectToAction(MVC.Administration.Events.Edit(response.EventId));
                }

                ModelState.AddModelError(string.Empty, response.ErrorMessage);
            }

            model.CompaniesSelectList = _lookupService.GetCompanies().MapSelectList();
            return View(model);
        }

        [HttpGet]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Update)]
        public virtual ActionResult Edit(int id)
        {
            var response = _eventService.GetEventForUpdate(new GetEventForUpdateRequest { EventId = id });

            AddEditEventViewModel model = _mapper.Map<AddEditEventViewModel>(response.Event);
            
            //Should be moved to mapper
            model.StartTimeHour = new TimeSpan(response.Event.StartDate.Hour, response.Event.StartDate.Minute, 0);
            model.EndTimeHour = new TimeSpan(response.Event.EndDate.Hour, response.Event.EndDate.Minute, 0);

            model.CompaniesSelectList = _lookupService.GetCompanies().MapSelectList();

            //TODO: change to AwsAdminImagePathPrefix
            model.LogoFileName = model.HasImage ? string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.Event.LogoFileName)
                : "/Assets/img/default_event_logo.png";
            model.BackgroundLogoFileName = !string.IsNullOrWhiteSpace(model.BackgroundLogoFileName) ? string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.Event.BackgroundLogoFileName)
                : "";
            model.MobileLogoFileName = !string.IsNullOrWhiteSpace(model.MobileLogoFileName) ? string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.Event.MobileLogoFileName)
                : "/Assets/img/default_event_logo.png";
            model.InstagramLogoFileName = !string.IsNullOrWhiteSpace(model.InstagramLogoFileName) ? string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.Event.InstagramLogoFileName)
                : "/Assets/img/default_event_logo.png";
            model.FacebookLogoFileName = !string.IsNullOrWhiteSpace(model.FacebookLogoFileName) ? string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.Event.FacebookLogoFileName)
                : "/Assets/img/default_event_logo.png";
            model.HasImage = true;
            ViewBag.IsEdit = true;
            return View(model);
        }

        [HttpPost]
        //[FixItTronAuthorize(Securable.Roles, Permissions.Update)]
        public virtual ActionResult Edit(int id, HttpPostedFileBase addFile, HttpPostedFileBase addBackgroundFile, HttpPostedFileBase addMobileFile, HttpPostedFileBase addInstagramFile, HttpPostedFileBase addFacebookFile, AddEditEventViewModel model)
        {
            if (ModelState.IsValid)
            {
                var request = new AddEditEventItem() { EventId = id };
                request = Mapper.Map<AddEditEventViewModel, AddEditEventItem>(model, request);
                request.UserName = User.Identity.Name;
                var response = _eventService.UpdateEvent(new UpdateEventRequest { Event = request });
                if (response.Success)
                {
                    TempData.AddSuccess(ApplicationStrings.EventEditSuccess);

                    var logos = new List<LogoItemViewModel>();
                    logos.Add(new LogoItemViewModel { File = addFile, LogoType = Common.Enums.LogoType.MainLogo });
                    logos.Add(new LogoItemViewModel { File = addMobileFile, LogoType = Common.Enums.LogoType.MobileLogo });
                    logos.Add(new LogoItemViewModel { File = addInstagramFile, LogoType = Common.Enums.LogoType.InstagramLogo });
                    logos.Add(new LogoItemViewModel { File = addFacebookFile, LogoType = Common.Enums.LogoType.FacebookLogo });
                    logos.Add(new LogoItemViewModel { File = addBackgroundFile, LogoType = Common.Enums.LogoType.BackgroundLogo });

                    foreach (var logo in logos)
                    {
                        var requestLogo = new ChangeEventLogoRequest();
                        if (logo.File != null)
                        {
                            //if (addFile.ContentLength > 1024000 || !IsImage(addFile))
                            //{
                            //    TempData.AddError(ApplicationStrings.ImageTypeAndSizeFormat);
                            //    return RedirectToAction(MVC.Profile.Index());
                            //}

                            byte[] contents = new byte[logo.File.ContentLength];
                            logo.File.InputStream.Read(contents, 0, contents.Length);
                            var documentFile = new UploadedFileListItem()
                            {
                                Content = contents,//Wandio.Web.Mvc.Html.ImageHelper.ResizeImage(contents, 120, 120),
                                ContentType = logo.File.ContentType,
                                FileName = logo.File.FileName
                            };

                            var documentToAdd = this.GetDocumentImpl(1, "Event", "Event", "Logo", " Event Logo", documentFile);
                            requestLogo.Logo = documentToAdd;
                            requestLogo.LogoType = logo.LogoType;
                            requestLogo.EventId = request.EventId;
                            var responseLogo = _eventService.ChangeEventLogo(requestLogo);
                            if (responseLogo.Success)
                            {
                                TempData.AddSuccess(ApplicationStrings.LogoUpdateSuccess);
                                //return RedirectToAction(MVC.Administration.Events.Edit());
                            }
                        }
                    }

                    //else
                    //{
                    //    TempData.AddError(ApplicationStrings.FileIsRequired);
                    //    return RedirectToAction(MVC.Profile.Index());
                    //}
                    return RedirectToAction(MVC.Administration.Events.Edit());
                }
                ModelState.AddModelError(string.Empty, response.ErrorMessage);

            }
            model.CompaniesSelectList = _lookupService.GetCompanies().MapSelectList();
            ViewBag.IsEdit = true;
            return View(model);
        }

        [HttpGet]
        public virtual ActionResult Delete(int id)
        {
            return this.ConfirmationModal(ApplicationStrings.ConfirmDeleteEvent);
        }

        [HttpPost]
        public virtual ActionResult Delete(int id, bool confirmed = false)
        {
            return this.ProcessServiceOperation(() =>
            {
                return _eventService.DeleteEvent(new DeleteEventRequest() { EventId = id });
            },
            ApplicationStrings.DeleteEventSuccess,
            MVC.Administration.Events.Index());
        }

        public virtual ActionResult EventLogo(int id)
        {
            var response = _eventService.GetEventLogo(new GetEventLogoRequest { EventId = id });
            if (!string.IsNullOrWhiteSpace(response.LogoFileName))
            {
                //DocumentViewModel image = _mapper.Map<DocumentViewModel>(response.Logo);
                //TODO: change to AwsAdminImagePathPrefix
                return File(string.Format(FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix, response.LogoFileName), "png");
                //return new FileContentResult(image.File.Content, image.File.ContentType);
            }
            //DocumentViewModel image = _mapper.Map<DocumentViewModel>(_userManager.GetProfileImage(new GetProfileImageRequest { Name = User.Identity.Name }).Image);

            return File("/Assets/img/avatar.png", "png");
        }

        public virtual ActionResult UpdateLogo(HttpPostedFileBase addFile)
        {
            //var request = new ChangeProfileImageRequest();
            //if (addFile != null)
            //{
            //    if (addFile.ContentLength > 1024000 || !IsImage(addFile))
            //    {
            //        TempData.AddError(ApplicationStrings.ImageTypeAndSizeFormat);
            //        return RedirectToAction(MVC.Profile.Index());
            //    }

            //    byte[] contents = new byte[addFile.ContentLength];
            //    addFile.InputStream.Read(contents, 0, contents.Length);
            //    var documentFile = new UploadedFileListItem()
            //    {
            //        Content = Wandio.Web.Mvc.Html.ImageHelper.ResizeImage(contents, 120, 120),
            //        ContentType = addFile.ContentType,
            //        FileName = addFile.FileName
            //    };

            //    var documentToAdd = this.GetDocumentImpl(1, "Profile", "Profile", "Avatar", "Profile avatar", documentFile);
            //    request.Image = documentToAdd;
            //}
            //else
            //{
            //    TempData.AddError(ApplicationStrings.FileIsRequired);
            //    return RedirectToAction(MVC.Profile.Index());
            //}
            //request.Name = User.Identity.Name;
            //var response = _userManager.ChangeProfileImage(request);
            //if (response.Success)
            //{
            //    TempData.AddSuccess(ApplicationStrings.ProfilePictureUpdateSuccess);
            //    return RedirectToAction(MVC.Profile.Index());
            //}

            //ModelState.AddModelError(string.Empty, response.ErrorMessage);
            return View(MVC.Profile.Views.Index);
        }

        private bool IsImage(HttpPostedFileBase file)
        {
            if (file.ContentType.Contains("image"))
            {
                return true;
            }

            string[] formats = new string[] { ".jpg", ".png", ".gif", ".jpeg" };

            return formats.Any(item => file.FileName.EndsWith(item, StringComparison.OrdinalIgnoreCase));
        }

        private EntityActions GetEventItemActions(int EventId)
        {
            var result = new EntityActions();

            result.Add(new EntityAction
            {
                IsCustomAction = true,
                Href = Url.Action(MVC.Administration.Events.Edit(EventId)),
                Title = ApplicationStrings.Edit
            });
            result.Add(new EntityAction
            {
                Href = Url.Action(MVC.Administration.Events.Delete(EventId)),
                Title = ApplicationStrings.Delete
            });

            //result.Add(true, ApplicationStrings.Edit, Url.Action(MVC.Administration.Events.Edit(EventId)));
            //result.Add(true, ApplicationStrings.Delete, Url.Action(MVC.Administration.Events.Delete(EventId)));

            return result;
        }

        protected EntityActions GetEntityActionsToolbar()
        {
            EntityActions actions = new EntityActions();

            //if (User.HasRight(Securable.Roles, Permissions.Create))
            {
                actions.Add(new EntityAction()
                {
                    IsCustomAction = true,
                    Title = ApplicationStrings.CreateEvent,
                    Href = Url.Action(MVC.Administration.Events.Create()),
                    Icon = "fa-plus",
                    BootstrapContext = BootstrapContext.Default
                });
            }

            return actions;
        }
    }
}