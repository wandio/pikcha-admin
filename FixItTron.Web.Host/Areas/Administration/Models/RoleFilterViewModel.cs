﻿using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Areas.Administration.Models
{
    public class RoleFilterViewModel : FilterModelBase
    {
        public string Name { get; set; }
    }
}
