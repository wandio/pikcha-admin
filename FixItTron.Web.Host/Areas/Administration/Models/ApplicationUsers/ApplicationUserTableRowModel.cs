﻿using Common.Enums;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.ApplicationUsers
{
    public class ApplicationUserTableRowModel : TableRowWithActionsModel
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int AvailablePrints { get; set; }
        public int UsedPrints { get; set; }
        public DateTime? CreateDate { get; set; }
        public UserType UserType { get; set; }
    }
}