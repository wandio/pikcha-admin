﻿using Common.Enums;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;

namespace FixItTron.Web.Host.Areas.Administration.Models.ApplicationUsers
{
    public class ApplicationUserFilterViewModel : FilterModelBase
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public UnboundedRange<int> AvailablePrints { get; set; }
        public UnboundedRange<int> UsedPrints { get; set; }
        public UnboundedPeriod CreateDate { get; set; }
        public UserType? UserType { get; set; }
        public IEnumerable<SelectListItem> UserTypesSelectList { get; set; }
    }
}