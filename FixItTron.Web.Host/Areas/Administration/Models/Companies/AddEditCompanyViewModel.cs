﻿using FixItTron.Web.Host.Areas.Administration.Models.Companies.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.Companies
{
    [Validator(typeof(AddEditCompanyViewModelValidator))]
    public class AddEditCompanyViewModel
    {
        public int CompanyId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
    }
}