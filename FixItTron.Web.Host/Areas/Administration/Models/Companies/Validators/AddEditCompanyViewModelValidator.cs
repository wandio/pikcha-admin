﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.Companies.Validators
{
    public class AddEditCompanyViewModelValidator : AbstractValidator<AddEditCompanyViewModel>
    {
        public AddEditCompanyViewModelValidator()
        {
            RuleFor(model => model.Name).NotEmpty();
        }
    }
}