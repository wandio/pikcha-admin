﻿using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.Companies
{
    public class CompanyFilterViewModel : FilterModelBase
    {
        public string Name { get; set; }
    }
}