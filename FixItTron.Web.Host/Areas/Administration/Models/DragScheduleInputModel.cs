﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models
{
    public class DragScheduleInputModel
    {
        public int ScheduleId { get; set; }
        public DateTime NewStartDateTime { get; set; }
        public DateTime NewEndDateTime { get; set; }
        public int? NewEmployeeId { get; set; }
        public bool? IsFromQuotation { get; set; }
        public bool? IsFromSelector { get; set; }
    }
}