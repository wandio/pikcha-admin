﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.Events.Validators
{
    public class AddEditEventViewModelValidator : AbstractValidator<AddEditEventViewModel>
    {
        public AddEditEventViewModelValidator()
        {
            RuleFor(model => model.Name).NotEmpty();
            RuleFor(model => model.CompanyId).NotEmpty();
            RuleFor(model => model.Username).NotEmpty();
            RuleFor(model => model.Password).NotEmpty();
        }
    }
}