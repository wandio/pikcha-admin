﻿using FixItTron.Web.Host.Areas.Administration.Models.Events.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FluentValidation.Attributes;

namespace FixItTron.Web.Host.Areas.Administration.Models.Events
{
    [Validator(typeof(AddEditEventViewModelValidator))]
    public class AddEditEventViewModel
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public string EventCode { get; set; }
        public string Description { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<SelectListItem> CompaniesSelectList { get; set; }
        [Display(Name = "Company", ResourceType = typeof(ApplicationStrings))]
        public int? CompanyId { get; set; }
        [Display(Name = "Logo", ResourceType = typeof(ApplicationStrings))]
        public bool HasImage { get; set; }
        public string BackgroundColor { get; set; }
        public DateTime StartDate { get; set; }
        public TimeSpan StartTimeHour { get; set; }
        public DateTime EndDate { get; set; }
        public TimeSpan EndTimeHour { get; set; }
        public bool ShowLogoOnShare { get; set; }
        public bool ShowLogoOnPreview { get; set; }
        public string LogoFileName { get; set; }
        [Display(Name = "MobilePreviewLogo", ResourceType = typeof(ApplicationStrings))]
        public string MobileLogoFileName { get; set; }
        [Display(Name = "InstagramShareLogo", ResourceType = typeof(ApplicationStrings))]
        public string InstagramLogoFileName { get; set; }
        [Display(Name = "FacebookShareLogo", ResourceType = typeof(ApplicationStrings))]
        public string FacebookLogoFileName { get; set; }
        [Display(Name = "BackgroundLogo", ResourceType = typeof(ApplicationStrings))]
        public string BackgroundLogoFileName { get; set; }
        public bool ShowBackgroundLogo { get; set; }

        [Display(Name = "ShowSurvey", ResourceType = typeof(ApplicationStrings))]
        public bool ShowSurvey { get; set; }
        [Display(Name = "FacebookAppSecret", ResourceType = typeof(ApplicationStrings))]
        public string FacebookAppSecret { get; set; }
        [Display(Name = "FacebookPageId", ResourceType = typeof(ApplicationStrings))]
        public string FacebookPageId { get; set; }
        [Display(Name = "FacebookPageAccessToken", ResourceType = typeof(ApplicationStrings))]
        public string FacebookPageAccessToken { get; set; }
    }
}