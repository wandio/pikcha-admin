﻿using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.Events
{
    public class EventTableRowModel : TableRowWithActionsModel
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public bool IsActive { get; set; }
        public string Company { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? PrintCount { get; set; }
        public bool ShowLogoOnShare { get; set; }
        public bool ShowLogoOnPreview { get; set; }
        public string EventCode { get; set; }
    }
}