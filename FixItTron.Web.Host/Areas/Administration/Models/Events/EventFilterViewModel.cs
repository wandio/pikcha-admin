﻿using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;

namespace FixItTron.Web.Host.Areas.Administration.Models.Events
{
    public class EventFilterViewModel : FilterModelBase
    {
        public string Name { get; set; }
        public string EventCode { get; set; }
        [Display(Name = "UserName", ResourceType = typeof(ApplicationStrings))]
        public string Username { get; set; }
        public IEnumerable<SelectListItem> YesNoSelectList { get; set; }
        public bool? IsActive { get; set; }
        public IEnumerable<SelectListItem> CompaniesSelectList { get; set; }
        [Display(Name = "Company", ResourceType = typeof(ApplicationStrings))]
        public int? CompanyId { get; set; }
        public UnboundedPeriod StartDate { get; set; }
        public UnboundedPeriod EndDate { get; set; }
        public UnboundedRange<int> PrintCount { get; set; }
        public bool? ShowLogoOnShare { get; set; }
        public bool? ShowLogoOnPreview { get; set; }
    }
}