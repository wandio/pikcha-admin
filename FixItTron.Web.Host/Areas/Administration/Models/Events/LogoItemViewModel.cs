﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.Events
{
    public class LogoItemViewModel
    {
        public HttpPostedFileBase File { get; set; }
        public LogoType LogoType { get; set; }
    }
}