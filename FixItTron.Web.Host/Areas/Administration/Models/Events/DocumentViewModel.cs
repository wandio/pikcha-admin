﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.Events
{
    public class DocumentViewModel
    {
        public int DocumentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public UploadedFileModel File { get; set; }
    }
}