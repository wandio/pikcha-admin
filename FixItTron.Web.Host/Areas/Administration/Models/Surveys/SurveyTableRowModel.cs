﻿using Common.Enums;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.ApplicationUsers
{
    public class SurveyTableRowModel : TableRowWithActionsModel
    {
        public string EventName { get; set; }
        public string Username { get; set; }
        public UserType UserType { get; set; }
        public SourceType SourceType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? CreateDate { get; set; }
        public Gender? Gender { get; set; }
        public SurveyQuestion1Answer Question1 { get; set; }
        public SurveyQuestion2Answer Question2 { get; set; }
    }
}