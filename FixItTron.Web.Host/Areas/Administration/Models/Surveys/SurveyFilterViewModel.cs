﻿using Common.Enums;
using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;

namespace FixItTron.Web.Host.Areas.Administration.Models.ApplicationUsers
{
    public class SurveyFilterViewModel : FilterModelBase
    {
        public string EventName { get; set; }
        public string Username { get; set; }
        public UserType? UserType { get; set; }
        public SourceType? SourceType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public UnboundedPeriod CreateDate { get; set; }
        public Gender? Gender { get; set; }
        public SurveyQuestion1Answer? Question1 { get; set; }
        public SurveyQuestion2Answer? Question2 { get; set; }

        public IEnumerable<SelectListItem> Question1AnswersSelectList { get; set; }
        public IEnumerable<SelectListItem> Question2AnswersSelectList { get; set; }
        public IEnumerable<SelectListItem> GendersSelectList { get; set; }

        public IEnumerable<SelectListItem> UserTypesSelectList { get; set; }
        public IEnumerable<SelectListItem> SourceTypesSelectList { get; set; }
    }
}