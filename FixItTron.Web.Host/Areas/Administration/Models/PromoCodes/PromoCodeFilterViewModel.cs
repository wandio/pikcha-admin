﻿using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Wandio.Domain.Services.Models;

namespace FixItTron.Web.Host.Areas.Administration.Models.PromoCodes
{
    public class PromoCodeFilterViewModel : FilterModelBase
    {
        public bool? MultiUse { get; set; }
        public IEnumerable<SelectListItem> YesNoSelectList { get; set; }
        public string Code { get; set; }
        public UnboundedRange<int> PrintCount { get; set; }
        public IEnumerable<SelectListItem> EventsSelectList { get; set; }
        [Display(Name = "Event", ResourceType = typeof(ApplicationStrings))]
        public int? EventId { get; set; }
        public string Description { get; set; }
    }
}