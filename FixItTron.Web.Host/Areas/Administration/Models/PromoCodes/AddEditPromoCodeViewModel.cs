﻿using FixItTron.Web.Host.Areas.Administration.Models.PromoCodes.Validators;
using System;
using FluentValidation.Attributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FixItTron.Web.Host.Areas.Administration.Models.PromoCodes
{
    [Validator(typeof(AddEditPromoCodeViewModelValidator))]
    public class AddEditPromoCodeViewModel
    {
        public int PromoCodeId { get; set; }
        public string Code { get; set; }
        public int? PrintCount { get; set; }
        public string Description { get; set; }
        public bool MultiUse { get; set; }
        public IEnumerable<SelectListItem> EventsSelectList { get; set; }
        [Display(Name = "Event", ResourceType = typeof(ApplicationStrings))]
        public int? EventId { get; set; }
        public string UserName { get; set; }
    }
}