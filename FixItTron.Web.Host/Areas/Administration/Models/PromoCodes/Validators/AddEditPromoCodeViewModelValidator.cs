﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.PromoCodes.Validators
{
    public class AddEditPromoCodeViewModelValidator : AbstractValidator<AddEditPromoCodeViewModel>
    {
        public AddEditPromoCodeViewModelValidator()
        {
            RuleFor(model => model.Code).NotEmpty();
            RuleFor(model => model.PrintCount).NotEmpty();
        }
    }
}