﻿using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.PromoCodes
{
    public class PromoCodeTableRowModel : TableRowWithActionsModel
    {
        public string Code { get; set; }
        public int PrintCount { get; set; }
        public bool MultiUse { get; set; }
        public string Event { get; set; }
        public string Description { get; set; }
    }
}