﻿using FixItTron.Web.Host.Areas.Administration.Models.Configurations.Validators;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.Configurations
{
    [Validator(typeof(EditConfigurationViewModelValidator))]
    public class EditConfigurationViewModel
    {
        public int ConfigurationId { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}