﻿using FixItTron.Web.Host.Models.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.Configurations
{
    public class ConfigurationTableRowModel : TableRowWithActionsModel
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}