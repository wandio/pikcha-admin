﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models.Configurations.Validators
{
    public class EditConfigurationViewModelValidator : AbstractValidator<EditConfigurationViewModel>
    {
        public EditConfigurationViewModelValidator()
        {
            RuleFor(model => model.Value).NotEmpty();
        }
    }
}