﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Web.Host.Areas.Administration.Models
{
    public class EditRoleSection
    {
        public string Title { get; set; }
        public IDictionary<Securable, Permissions> Permissions { get; set; }
        public IList<EditRoleSection> SubSections { get; set; }

        public EditRoleSection()
        {
            SubSections = Enumerable.Empty<EditRoleSection>().ToList();
        }
    }
}
