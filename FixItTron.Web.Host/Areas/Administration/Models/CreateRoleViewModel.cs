﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Attributes;
using FixItTron.Web.Host.Models.Validations;

namespace FixItTron.Web.Host.Areas.Administration.Models
{
    [Validator(typeof(CreateRoleViewModelValidator))]
    public class CreateRoleViewModel
    {
        [Display(Name = "Name", ResourceType = typeof(ApplicationStrings))]
        public string Name { get; set; }
        public IList<EditRolePermissionModel> Permissions { get; set; }
        public IDictionary<Securable, Permissions> Configuration { get; set; }
        public IList<EditRoleSection> Sections { get; set; }
    }
}
