﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models
{
    public class DeleteEmployeeScheduleInputModel
    {
        public int ScheduleId { get; set; }
    }
}