﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FixItTron.Web.Host.Areas.Administration.Models
{
    public class AddEmployeeScheduleInputModel
    {
        public int EmployeeId { get; set; }
        public DateTime Date { get; set; }
        public string Shift { get; set; }
        public bool Checked { get; set; }
    }
}