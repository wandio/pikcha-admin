﻿using FixItTron.Services;
using FixItTron.Services.Interfaces;
using FixItTron.Web.Host.App_Start;
using FluentValidation;
using FluentValidation.Mvc;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FixItTron.Web.Host
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            System.Web.Http.GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            MapperConfiguration.Configure(AutoMapper.Mapper.Configuration);
            FluentValidationModelValidatorProvider.Configure();
            ModelMetadataProviders.Current = new FixItTronModelMetadataProvider();
            ClientDataTypeModelValidatorProvider.ResourceClassKey = "ValidationMessages";
            DefaultModelBinder.ResourceClassKey = "ValidationMessages";
            ValidatorOptions.ResourceProviderType = typeof(ValidationMessages);
            ModelBindingConfig.Configure();

            FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAccessKeyId = ConfigurationManager.AppSettings["AWS_AccessKeyId"];
            FixItTron.Services.Helpers.AmazonImageUploaderService.AwsSecretAccessKey = ConfigurationManager.AppSettings["AWS_SecretAccessKey"];
            FixItTron.Services.Helpers.AmazonImageUploaderService.AwsImagePathPrefix = ConfigurationManager.AppSettings["AWS_ImagePathPrefix"];
            FixItTron.Services.Helpers.AmazonImageUploaderService.AwsAdminImagePathPrefix = ConfigurationManager.AppSettings["AWS_AdminImagePathPrefix"];

            ChatbotService.AppSecret = ConfigurationManager.AppSettings["Chatbot_AppSecret"];
            ChatbotService.PageAccessToken = ConfigurationManager.AppSettings["Chatbot_PageAccessToken"];
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ka-GE");
            Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";

            Thread.CurrentThread.CurrentUICulture = new CultureInfo("ka-GE");
            Thread.CurrentThread.CurrentUICulture.NumberFormat.NumberDecimalSeparator = ".";

            Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
            Thread.CurrentThread.CurrentUICulture.DateTimeFormat.ShortDatePattern = "dd.MM.yyyy";
        }
        protected void Application_End()
        {
            //var employeeService = DependencyResolver.Current.GetService<IEmployeeService>();
            //employeeService.DisconnectAllUsers();
        }
    }
}
