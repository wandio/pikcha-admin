﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.WBSReference.Protocol
{
    public class WBSCredentials
    {
        const string USERNAME_PROPERTY_NAME = "WBSAccount";
        const string PASSWORD_PROPERTY_NAME = "WBSPassword";
        const string SELLER_ID_PROPERTY_NAME = "WBSSellerId";

        public string UserName { get; private set; }
        public string Password { get; private set; }
        public string SellerId { get; private set; }

        public WBSCredentials(string userName, string password, string sellerId)
        {
            Contract.Requires(string.IsNullOrWhiteSpace(userName), "The 'userName' parameter should not be null or empty.");
            Contract.Requires(string.IsNullOrWhiteSpace(password), "The 'password' parameter should not be null or empty.");
            Contract.Requires(string.IsNullOrWhiteSpace(sellerId), "The 'sellerId' parameter should not be null or empty.");

            this.UserName = userName;
            this.Password = password;
            this.SellerId = sellerId;
        }

        public static WBSCredentials ReadFromConfig()
        {
            var userName = System.Configuration.ConfigurationManager.AppSettings[USERNAME_PROPERTY_NAME];
            var password = System.Configuration.ConfigurationManager.AppSettings[PASSWORD_PROPERTY_NAME];
            var sellerId = System.Configuration.ConfigurationManager.AppSettings[SELLER_ID_PROPERTY_NAME];

            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password) || string.IsNullOrWhiteSpace(sellerId))
            {
                throw new System.Configuration.ConfigurationErrorsException("Unable to read credentials");
            }

            return new WBSCredentials(userName, password, sellerId);
        }
    }
}
