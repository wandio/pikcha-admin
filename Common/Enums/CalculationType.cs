﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    /// <summary>
    /// ByAmount = 1, ByQuantity = 2
    /// </summary>
    public enum CalculationType
    {
        ByAmount = 1,
        ByQuantity = 2
    }
}
