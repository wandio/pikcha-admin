﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum SalesOrderScheduleStatus
    {
        Planned = 0,
        OnTheRoad = 1,
        Current = 2,
        Canceled = 3,
        Done = 4
    }
}
