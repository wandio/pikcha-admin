﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum QuestionerTemplateType
    {
        Service = 1,
        Customer = 2,
        SalesOrder = 3
    }
}
