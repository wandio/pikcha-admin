﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum PriceRuleFilterType
    {
        Attribute = 1,
        Product = 2
    }
}
