﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Enums
{
    public enum FieldType
    {
        Radios = 1,
        Text = 2,
        TextArea = 3,
        DropDown = 4,
        DateTime = 5,
        MultipleChecks = 6,
        Boolean = 7
    }
}
