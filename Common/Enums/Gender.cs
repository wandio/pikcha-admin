﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Enums
{
    public enum Gender
    {
        Male = 1,
        Female = 2
    }
}
