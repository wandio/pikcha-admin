﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum RequestStatus
    {
        Approved = 2,
        Rejected = 3,
        Pending = 4
    }
}
