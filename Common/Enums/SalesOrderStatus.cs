﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum SalesOrderStatus
    {
        Saved = 0,
        Ordered = 1,
        Current = 2,
        Completed = 3,
        Canceled = 4
    }
}
