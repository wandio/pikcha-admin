﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum PriceRuleFilterBy
    {
        Class = 1,
        Category = 2
    }
}
