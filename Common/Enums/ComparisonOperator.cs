﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    /// <summary>
    /// More = 1, Less = 2, Equels = 3, Each = 4
    /// </summary>
    public enum ComparisonOperator
    {
        More = 1,
        Less = 2,
        Equels = 3,
        Each = 4
    }
}
