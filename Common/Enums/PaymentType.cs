﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum PaymentType
    {
        None,
        /// <summary>
        /// When amount is paid to company
        /// </summary>
        Inbound,
        /// <summary>
        /// When company pays 
        /// </summary>
        Outbound
    }
}
