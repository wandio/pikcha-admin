﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum SourceType
    {
        Web = 1,
        IOS = 2,
        Android = 3,
        Messenger = 4
    }
}
