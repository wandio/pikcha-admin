﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum LegalForm
    {
        None = 0,
        LegalEntity = 1,
        Individuals = 2
    }
}
