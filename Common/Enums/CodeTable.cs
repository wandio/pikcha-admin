﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Enums
{
    public enum CodeTable
    {
        Product = 1,
        Warehouse = 2,
        WarehouseAdjustment = 3,
        WarehouseTransferOrder = 4,
        WarehouseTransfer = 5,
        WorkOrder = 6,
        BillOfMaterials = 7,
        Contractor = 8,
        PurchaseOrder = 9,
        Payment = 10,
        SalesOrder = 11,
        PriceList = 12,
        ReturnOrder = 13,
        BoxOffice = 14,
        SalesQuotation = 15,
        PriceRule = 16,
        Material = 17,
        Service = 18,
        ContractorAgreement = 19
    }
}