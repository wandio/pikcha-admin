﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum SurveyQuestion1Answer
    {
        NoAnswer = 0,
        AsSoonAsPossible = 1,
        NextWinter = 2,
        ThisCommingSummer = 3,
        IDontKnow = 4
    }

    public enum SurveyQuestion2Answer
    {
        NoAnswer = 0,
        GapYear = 1,
        Volunteer = 2,
        Internship = 3,
        StudyAbroad = 4,
        Summer = 5
    }
}
