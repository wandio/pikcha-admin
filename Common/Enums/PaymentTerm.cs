﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Enums
{
    public enum PaymentTerm
    {
        None = 0,
        Cash = 1,
        Consignation = 2
    }
}
