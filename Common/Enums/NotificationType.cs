﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum NotificationType
    {
        AfterSalesOrderRegistration = 1,
        ScheduleManualChange = 2,
        OnPromoSMS = 3,
        BeforeOrderStartedToEmloyee = 4,
        BeforeOrderEndedToEmployee = 5,
        OrderCanceledOrChanged = 6,
        EmployeeNoActionOrder = 7,//დაკონფიგურირებული დროის შუალედის გასვლისას არ დაადასტურა ან უარყო შეკვეთა
        OnEmployeeLate = 8,
        EmployeeIncreaseDate = 9,//ხელოსანმა მონიშნა ვადის გაგრძელება
        EmployeeScheduleOutOfDate = 10,//ხელოსანმა არ დაასრულა და ვადა გაუვიდა განრიგს
        EmployeeNotStartOrder = 11,//განრიგის დრო დადგა და მივიდას არ დააჭირა ხელოსანმა
        EmployeeChangeOrder = 12,// ხელოსანმა მოახდინა შეკვეთის ცვლილება,(შეკვეთა დაასრულა, გააუქმა, ან შეცვალა თანხა)
        AgreementOutOfDate = 13,//კონტრაქტს გასდის ვადა
        SMSTemplate = 14
    }
}
