﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    /// <summary>
    /// WholeOrder = 1, ByLine = 2
    /// </summary>
    public enum ConditionType
    {
        WholeOrder = 1,
        ByLine = 2
    }
}
