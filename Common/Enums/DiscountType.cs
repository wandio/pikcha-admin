﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    /// <summary>
    /// DiscountPercent = 1, DiscountAmount = 2, Price = 3
    /// </summary>
    public enum DiscountType
    {
        DiscountPercent = 1,
        DiscountAmount = 2,
        ConcretePrice = 3
    }
}
