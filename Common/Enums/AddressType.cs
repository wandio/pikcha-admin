﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Enums
{
    public enum AddressType
    {
        Legal = 1,
        Physical = 2
    }
}
