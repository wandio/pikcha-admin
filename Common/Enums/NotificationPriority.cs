﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum NotificationPriority
    {
        Success = 1,
        Info = 2,
        Warning = 3,
        Alert = 4
    }
}
