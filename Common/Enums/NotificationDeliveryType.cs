﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum NotificationDeliveryType
    {
        SMS = 1,
        Email=2,
        Push = 3,
        Internal = 4
    }
}
