﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum InvoiceStatus
    {
        Saved = 1,
        Processed = 2,
        Closed = 3,
        Canceled = 4
    }
}
