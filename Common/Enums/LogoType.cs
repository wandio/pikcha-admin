﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enums
{
    public enum LogoType
    {
        MainLogo = 1,
        MobileLogo = 2,
        InstagramLogo = 3,
        FacebookLogo = 4,
        BackgroundLogo = 5
    }
}
