﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class CommonExtensions
    {
        public static string GetAgoTimeText(this DateTime date)
        {
            const int SECOND = 1;
            const int MINUTE = 60 * SECOND;
            const int HOUR = 60 * MINUTE;
            const int DAY = 24 * HOUR;
            const int MONTH = 30 * DAY;

            var ts = new TimeSpan(DateTime.Now.Ticks - date.Ticks);
            double delta = Math.Abs(ts.TotalSeconds);

            if (delta < 1 * MINUTE)
            {
                return ts.Seconds == 1 ? "ერთი წამის წინ" : ts.Seconds + " წამის წინ";
            }
            if (delta < 2 * MINUTE)
            {
                return "ერთი წუთის წინ";
            }
            if (delta < 45 * MINUTE)
            {
                return ts.Minutes + " წუთის წინ";
            }
            if (delta < 90 * MINUTE)
            {
                return "ერთი საათის წინ";
            }
            if (delta < 24 * HOUR)
            {
                return ts.Hours + " საათის წინ";
            }
            if (delta < 48 * HOUR)
            {
                return "გუშინ";
            }
            if (delta < 30 * DAY)
            {
                return ts.Days + " დღის წინ";
            }
            if (delta < 12 * MONTH)
            {
                int months = Convert.ToInt32(Math.Floor((double)ts.Days / 30));
                return months <= 1 ? "ერთი თვის წინ" : months + " თვის წინ";
            }
            else
            {
                int years = Convert.ToInt32(Math.Floor((double)ts.Days / 365));
                return years <= 1 ? "ერთი წლის წინ" : years + " წლის წინ";
            }
        }
    }
}
