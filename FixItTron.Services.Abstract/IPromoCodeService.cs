﻿using FixItTron.Services.Models.PromoCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Interfaces
{
    public interface IPromoCodeService
    {
        GetPromoCodeListResponse GetPromoCodeList(GetPromoCodeListRequest request);
        CreatePromoCodeResponse CreatePromoCode(CreatePromoCodeRequest request);
        DeletePromoCodeResponse DeletePromoCode(DeletePromoCodeRequest request);
        GetPromoCodeForUpdateResponse GetPromoCodeForUpdate(GetPromoCodeForUpdateRequest request);
        UpdatePromoCodeResponse UpdatePromoCode(UpdatePromoCodeRequest request);
        DeletePromoCodesResponse DeletePromoCodes(DeletePromoCodesRequest request);
        AddPromoCodesResponse AddPromoCodes(AddPromoCodesRequest request);
    }
}
