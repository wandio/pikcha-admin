﻿using FixItTron.Services.Models.ImageUpload;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Interfaces
{
    public interface IImageUploadService
    {
        UploadImageResponse UploadImage(UploadImageRequest request);

        GetImageByIdResponse GetImageById(GetImageByIdRequest request);
    }
}
