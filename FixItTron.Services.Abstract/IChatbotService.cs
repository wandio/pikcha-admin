﻿using FixItTron.Services.Models.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Interfaces
{
    public interface IChatbotService
    {
        bool VerifyRequestSignature(string signature, byte[] body);
        Task<bool> HandleWebhookPost(WebhookData request);
    }
}
