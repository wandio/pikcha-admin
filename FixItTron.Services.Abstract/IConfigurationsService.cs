﻿using FixItTron.Services.Models.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Interfaces
{
    public interface IConfigurationsService
    {
        GetConfigurationListResponse GetConfigurationList(GetConfigurationListRequest request);
        GetConfigurationForUpdateResponse GetConfigurationForUpdate(GetConfigurationForUpdateRequest request);
        UpdateConfigurationResponse UpdateConfiguration(UpdateConfigurationRequest request);
    }
}
