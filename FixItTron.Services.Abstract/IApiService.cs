﻿using FixItTron.Services.Models.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Interfaces
{
    public interface IApiService
    {
        ActivatePromoCodeResponse ActivatePromoCode(ActivatePromoCodeRequest request);
        CreateUserResponse CreateUser(CreateUserRequest request);
        GetUserInfoResponse GetUserInfo(GetUserInfoRequest request);
        UploadImageResponse UploadImage(UploadImageRequest request);
        UploadImage2Response UploadImage2(UploadImage2Request request);
        UploadIsAvailableResponse UploadIsAvailable(UploadIsAvailableRequest request);

        UserExistsResponse UserExists(UserExistsRequest request);
        Models.Api.GetFacebookShareLogoResponse GetFacebookShareLogo();
        Models.Api.GetInstagramShareLogoResponse GetInstagramShareLogo();
        GetPreviewLogoResponse GetPreviewLogo();

        Models.Api.GetFacebookShareLogoResponse GetFacebookShareLogo2(int eventId);
        Models.Api.GetInstagramShareLogoResponse GetInstagramShareLogo2(int eventId);
        GetPreviewLogoResponse GetPreviewLogo2(int eventId);

        CheckHealthResponse CheckHealth();

        CreateSurveyResponse CreateSurvey(CreateSurveyRequest request);
        Models.Api.AnswerSurveyQuestion1Response AnswerSurveyQuestion1(Models.Api.AnswerSurveyQuestion1Request request);
        Models.Api.AnswerSurveyQuestion2Response AnswerSurveyQuestion2(Models.Api.AnswerSurveyQuestion2Request request);

        GetEventSurveyResponse GetEventSurvey(GetEventSurveyRequest request);

        Models.Api.ValidateEventCodeResponse ValidateEventCode(Models.Api.ValidateEventCodeRequest request);
        Models.Api.SubmitSurveyResponse SubmitSurvey(Models.Api.SubmitSurveyRequest request);
    }
}
