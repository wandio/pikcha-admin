﻿using Common.Enums;
using FixItTron.Services.Models.Lookup;
using FixItTron.Services.Models.Lookup.AbsenseEmployee;
using FixItTron.Services.Models.Lookup.AbsenseTypes;
using FixItTron.Services.Models.Lookup.Banks;
using FixItTron.Services.Models.Lookup.BoxOffices;
using FixItTron.Services.Models.Lookup.Reason;
using FixItTron.Services.Models.Lookup.Cities;
using FixItTron.Services.Models.Lookup.Countries;
using FixItTron.Services.Models.Lookup.Currencies;
using FixItTron.Services.Models.Lookup.Districts;
using FixItTron.Services.Models.Lookup.ObjectAdditionals;
using FixItTron.Services.Models.Lookup.PaymentClassifier;
using FixItTron.Services.Models.Lookup.Position;
using FixItTron.Services.Models.Lookup.ProductClass;
using FixItTron.Services.Models.Lookup.ProductType;
using FixItTron.Services.Models.Lookup.ScheduleAdditionalStatuses;
using FixItTron.Services.Models.Lookup.TaxGroups;
using FixItTron.Services.Models.Lookup.UnitOfMeasure;
using FixItTron.Services.Models.Lookup.WarehouseTypes;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Services.Models.Lookup.ApplicationUsers;

namespace FixItTron.Services.Interfaces
{
    public interface ILookupService
    {
        #region selectLists

        #region pikcha
        IEnumerable<LookupModel> GetEvents();
        IEnumerable<LookupModel> GetCompanies();
        #endregion

        #endregion

        #region application users
        GetApplicationUserListResponse GetApplicationUserList(GetApplicationUserListRequest request);

        DeleteApplicationUserResponse DeleteApplicationUser(DeleteApplicationUserRequest request);

        DeleteApplicationUsersResponse DeleteApplicationUsers(DeleteApplicationUsersRequest request);
        #endregion

        #region surveys

        GetSurveyListResponse GetSurveyList(GetSurveyListRequest request);

        #endregion
    }
}
