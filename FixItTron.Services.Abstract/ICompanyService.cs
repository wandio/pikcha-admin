﻿using FixItTron.Services.Models.Company;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Interfaces
{
    public interface ICompanyService
    {
        GetCompanyListResponse GetCompanyList(GetCompanyListRequest request);
        CreateCompanyResponse CreateCompany(CreateCompanyRequest request);
        DeleteCompanyResponse DeleteCompany(DeleteCompanyRequest request);
        GetCompanyForUpdateResponse GetCompanyForUpdate(GetCompanyForUpdateRequest request);
        UpdateCompanyResponse UpdateCompany(UpdateCompanyRequest request);
    }
}
