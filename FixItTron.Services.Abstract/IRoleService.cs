﻿using FixItTron.Services.Models.Role;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Interfaces
{
    public interface IRoleService
    {
        GetRoleListResponse GetRoleList(GetRoleListRequest request);
        Dictionary<Securable, Permissions> GetConfiguration();
        CreateRoleResponse CreateRole(CreateRoleRequest request);
        GetRoleResponse GetRole(GetRoleRequest request);
        UpdateRoleResponse UpdateRole(UpdateRoleRequest request);
    }
}
