﻿using FixItTron.Services.Models.Event;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Interfaces
{
    public interface IEventService
    {
        GetEventListResponse GetEventList(GetEventListRequest request);
        CreateEventResponse CreateEvent(CreateEventRequest request);
        DeleteEventResponse DeleteEvent(DeleteEventRequest request);
        GetEventForUpdateResponse GetEventForUpdate(GetEventForUpdateRequest request);
        UpdateEventResponse UpdateEvent(UpdateEventRequest request);
        GetEventLogoResponse GetEventLogo(GetEventLogoRequest request);
        ChangeEventLogoResponse ChangeEventLogo(ChangeEventLogoRequest request);
        AuthorizePrintResponse AuthorizePrint(AuthorizePrintRequest request);
    }
}
