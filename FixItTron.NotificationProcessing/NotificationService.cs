﻿using FixItTron.Services.Models.Notification;
using Hangfire;
using Hangfire.SqlServer;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using FixitronService = FixItTron.Services;
using System.Net.Http;


namespace FixItTron.NotificationProcessing
{
    public partial class NotificationService : ServiceBase
    {
        public const string LogSourceName = "FixItTronNotifier";
        public const string LogName = "FixItTronService";
        private BackgroundJobServer _server;
        //EventLog _log = new EventLog(LogName);

        public NotificationService()
        {
            InitializeComponent();

            GlobalConfiguration.Configuration.UseSqlServerStorage("FixItTronDbContext");

            //if (!EventLog.SourceExists(LogSourceName))
            //{
            //    EventLog.CreateEventSource(LogSourceName, LogName);
            //}
            //_log.Source = LogSourceName;

        }

        protected override void OnStart(string[] args)
        {
            try
            {
                _server = new BackgroundJobServer();
                RecurringJob.AddOrUpdate(() => ProcessNotifications(), Cron.Minutely);
            }
            catch (Exception ex)
            {
                LogErrorEvent(ex);
            }
        }
        public void UpdateOperators()
        {
            try
            {
                var notif = new FixitronService.NotificationService();
                var response = notif.ProcessUpCommingScheduleNotifications(new ProcessUpCommingScheduleNotificationsRequest { });
                if (response.Success)
                {
                    LogSuccessEvent();
                    UpdateOperatorsToFixitronHubs();
                }
            }
            catch (Exception ex)
            {
                LogErrorEvent(ex);
            }
        }
        public void ProcessNotifications()
        {
            try
            {
                var notif = new FixitronService.NotificationService();
                notif.ProcessNotifications(new ProcessNotificationsRequest { });
            }
            catch (Exception ex)
            {
                LogErrorEvent(ex);
            }
        }
        protected override void OnStop()
        {
            _server.Dispose();
        }
        public void UpdateOperatorsToFixitronHubs()
        {
            var baseUrl = ConfigurationManager.AppSettings["FixitronBaseUrl"];
            var baseUri = new Uri(baseUrl);
            var updateUrl = ConfigurationManager.AppSettings["NotifyOperatorsUrl"];

            using (var client = new HttpClient())
            {
                client.BaseAddress = baseUri;
                var data = client.GetAsync(updateUrl).Result;
            }
        }
        private void LogErrorEvent(Exception ex)
        {
            //_log.WriteEntry(string.Format("Notification Failed: {0}", ex.ToString()), EventLogEntryType.Error);
        }
        private void LogSuccessEvent()
        {
            //_log.WriteEntry("Notification Sent Successfuly", EventLogEntryType.Information);
        }
    }
}
