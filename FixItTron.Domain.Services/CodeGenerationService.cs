﻿using Common.Enums;
using FixItTron.Domain.Db;
using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Services
{
    public static class CodeGenerationService
    {
        public static object _syncLock = new object();

        //private static string GetNextCode(FixItTronDbContext db, CodeTable table)
        //{
        //    lock (_syncLock)
        //    {
        //        var codeGen = db.Set<CodeGeneration>().SingleOrDefault(x => x.CodeTable == table);
        //        if (codeGen == null)
        //        {
        //            throw new InvalidOperationException("CodeGeneration not found");
        //        }

        //        var newCode = codeGen.CurrentStep;
        //        var incrementStep = codeGen.IncrementStep;
        //        codeGen.CurrentStep = newCode + incrementStep;

        //        return string.Concat(codeGen.Prefix, codeGen.CurrentStep.ToString().PadLeft(codeGen.DigitPartLegth, '0'), codeGen.Suffix);
        //    }
        //}

        private static string GetNextCode(PikchaDbContext _Db, CodeTable table)
        {
            var result = string.Empty;

            lock (_syncLock)
            {
                using (SqlConnection connection = new SqlConnection(_Db.Database.Connection.ConnectionString))
                {
                    using (SqlCommand command = connection.CreateCommand())
                    {
                        var commandText = string.Format(
@"DECLARE @NewCode INT
SET @NewCode = (SELECT CurrentStep FROM dbo.CodeGenerations WHERE CodeTable = {0})

UPDATE  codegenerations
SET     CurrentStep = @NewCode + IncrementStep
WHERE   CodeTable = {0};


SELECT  Prefix ,
        @NewCode,
        Suffix ,
        DigitPartLegth
FROM    codegenerations
WHERE   CodeTable = {0};", (int)table);

                        command.CommandType = System.Data.CommandType.Text;
                        command.CommandText = commandText;

                        connection.Open();
                        var codeGen = command.ExecuteReader();
                        if (!codeGen.Read())
                        {
                            throw new InvalidOperationException("Could not get code");
                        }

                        var prefix = codeGen.GetString(0);
                        var currentStep = codeGen.GetInt32(1);
                        var suffix = codeGen.GetString(2);
                        var digitPartLength = codeGen.GetInt32(3);

                        result = string.Concat(prefix, currentStep.ToString().PadLeft(digitPartLength, '0'), suffix);

                        return result;
                    }
                }
            }
        }

        public static string GetNextContractorAgreementCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.ContractorAgreement);
        }

        public static string GetNextContractorCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.Contractor);
        }

        public static string GetNextPriceListCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.PriceList);
        }

        public static string GetNextMaterialCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.Material);
        }

        public static string GetNextBoxOfficeCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.BoxOffice);
        }

        public static string GetNextServiceCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.Service);
        }

        public static string GetNextWarehouseCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.Warehouse);
        }

        public static string GetNextSalesOrderCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.SalesOrder);
        }

        public static string GetNextSalesQuotationCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.SalesQuotation);
        }

        public static string GetNextPriceRuleCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.PriceRule);
        }

        public static string GetNextPaymentCode(this PikchaDbContext db)
        {
            return GetNextCode(db, CodeTable.Payment);
        }
    }
}
