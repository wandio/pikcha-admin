﻿using FixItTron.Domain.Db.Configurations;
using FixItTron.Domain.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Validation;
using System.Diagnostics;
using FixItTron.Domain.Model;
using MySql.Data.EntityFramework;

namespace FixItTron.Domain.Db
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class PikchaDbContext : DbContext
    {
        public PikchaDbContext()
            : this("PikchaDbContext")
        {
            Database.SetInitializer<PikchaDbContext>(null);
        }

        public PikchaDbContext(string nameOrConnectionString = "PikchaDbContext")
            : base(nameOrConnectionString)
        {
            Database.SetInitializer<PikchaDbContext>(null);
            this.Database.Log = log => Debug.WriteLine(log);
        }

        public DbSet<UserPromoCode> UserPromoCodes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AdminUserTypeConfiguration());
            modelBuilder.Configurations.Add(new UserRoleTypeConfiguration());
            modelBuilder.Configurations.Add(new RoleTypeConfiguration());
            modelBuilder.Configurations.Add(new RoleClaimTypeConfiguration());
            //modelBuilder.Configurations.Add(new CodeGenerationTypeConfiguration());
            //modelBuilder.Configurations.Add(new ContractorTypeConfiguration());
            //modelBuilder.Configurations.Add(new ContractorObjectTypeConfiguration());
            //modelBuilder.Configurations.Add(new ContractorObjectAdditionalTypeConfiguration());
            //modelBuilder.Configurations.Add(new ContractorObjectAdditionalItemTypeConfiguration());
            //modelBuilder.Configurations.Add(new ContractorCategoryTypeConfiguration());
            //modelBuilder.Configurations.Add(new BankAccountTypeConfiguration());
            //modelBuilder.Configurations.Add(new ContactPersonTypeConfiguration());
            //modelBuilder.Configurations.Add(new CountryTypeConfiguration());
            //modelBuilder.Configurations.Add(new CityTypeConfiguration());
            //modelBuilder.Configurations.Add(new StreetTypeConfiguration());
            //modelBuilder.Configurations.Add(new DistrictTypeConfiguration());
            modelBuilder.Configurations.Add(new DocumentTypeConfiguration());
            modelBuilder.Configurations.Add(new UploadedFileTypeConfiguration());
            //modelBuilder.Configurations.Add(new ProductTypeConfiguration());
            //modelBuilder.Configurations.Add(new ProductUnitOfMeasureConfiguration());
            //modelBuilder.Configurations.Add(new PriceListProductTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesOrderTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesOrderLineTypeConfiguration());
            //modelBuilder.Configurations.Add(new PriceListTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesQuotationTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesQuotationLineTypeConfiguration());
            //modelBuilder.Configurations.Add(new EmployeeTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesOrderScheduleTypeConfiguration());
            //modelBuilder.Configurations.Add(new PriceRuleTypeConfiguration());
            //modelBuilder.Configurations.Add(new PriceRuleConditionTypeConfiguration());
            //modelBuilder.Configurations.Add(new PriceRuleFilterTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesOrderScheduleServiceTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesQuotationScheduleServiceTypeConfiguration());
            //modelBuilder.Configurations.Add(new GeneralScheduleConfigTypeConfiguration());
            //modelBuilder.Configurations.Add(new ProductCategoryTypeConfiguration());
            //modelBuilder.Configurations.Add(new EmployeeScheduleTypeConfiguration());
            //modelBuilder.Configurations.Add(new InvoiceTypeConfiguration());
            //modelBuilder.Configurations.Add(new InvoiceLineTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesQuotationScheduleTypeConfiguration());
            //modelBuilder.Configurations.Add(new WarehouseTypeConfiguration());
            //modelBuilder.Configurations.Add(new WarehouseProductTypeConfiguration());
            //modelBuilder.Configurations.Add(new ScheduleAdditionalStatusTypeConfiguration());
            //modelBuilder.Configurations.Add(new WorkingScheduleTypeConfiguration());
            //modelBuilder.Configurations.Add(new WorkingScheduleDayTypeConfiguration());
            //modelBuilder.Configurations.Add(new PositionTypeConfiguration());
            //modelBuilder.Configurations.Add(new ContractorAgreementTypeConfiguration());
            //modelBuilder.Configurations.Add(new ContractorAgreementConditionTypeConfiguration());
            //modelBuilder.Configurations.Add(new BoardTypeConfiguration());
            //modelBuilder.Configurations.Add(new WarehouseRequestTypeConfiguration());
            //modelBuilder.Configurations.Add(new WarehouseProductRequestTypeConfiguration());
            //modelBuilder.Configurations.Add(new PaymentClassifierTypeConfiguration());
            //modelBuilder.Configurations.Add(new EmployeeAbsenseTypeConfiguration());
            //modelBuilder.Configurations.Add(new AbsenseTypeTypeConfiguration());
            //modelBuilder.Configurations.Add(new QuestionerTemplateQuestionAnswerTypeConfiguration());
            //modelBuilder.Configurations.Add(new QuestionerTemplateQuestionTypeConfiguration());
            //modelBuilder.Configurations.Add(new QuestionerTemplateTypeConfiguration());
            //modelBuilder.Configurations.Add(new QuotationQuestionerAnswerTypeConfiguration());
            //modelBuilder.Configurations.Add(new QuestionerAnswerItemTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesQuotationLineQuestionerTypeConfiguration());
            //modelBuilder.Configurations.Add(new ContractorObjectQuestionerTypeConfiguration());
            //modelBuilder.Configurations.Add(new ContractorObjectQuestionerAnswerTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesOrderQuestionerTypeConfiguration());
            //modelBuilder.Configurations.Add(new SalesOrderQuestionerAnswerTypeConfiguration());
            //modelBuilder.Configurations.Add(new BoxOfficeTypeConfiguration());
            //modelBuilder.Configurations.Add(new PaymentTypeConfiguration());
            //modelBuilder.Configurations.Add(new SystemConfigurationTypeConfiguration());
            //modelBuilder.Configurations.Add(new SystemQuestionerTemplateTypeConfiguration());
            //modelBuilder.Configurations.Add(new ReasonTypeConfiguration());
            //modelBuilder.Configurations.Add(new NotificationTypeConfiguration());
            //modelBuilder.Configurations.Add(new NotificationTemplateTypeConfiguration());
            //modelBuilder.Configurations.Add(new InboxNotificationTypeConfiguration());
            //modelBuilder.Configurations.Add(new ConnectionTypeConfiguration());
            //modelBuilder.Configurations.Add(new NotificationTemplateItemTypeConfiguration());
            //modelBuilder.Configurations.Add(new LeadTypeConfiguration());
            modelBuilder.Configurations.Add(new EventTypeConfiguration());
            modelBuilder.Configurations.Add(new CompanyTypeConfiguration());
            modelBuilder.Configurations.Add(new ConfigurationTypeConfiguration());
            //modelBuilder.Configurations.Add(new AdminUserTypeConfiguration());
            modelBuilder.Configurations.Add(new PromoCodeTypeConfiguration());
            modelBuilder.Configurations.Add(new UserTypeConfiguration());
            modelBuilder.Configurations.Add(new SurveyTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        public static PikchaDbContext Create()
        {
            return new PikchaDbContext();
        }

        public override int SaveChanges()
        {
            var now = DateTime.Now;

            foreach (var item in ChangeTracker.Entries<IChangeDateTrackedEntity>().Where(entity => entity.State == EntityState.Added || entity.State == EntityState.Modified))
            {
                item.Entity.LastChangeDate = now;
            }

            foreach (var item in ChangeTracker.Entries<IExternallyIdentifiedEntity>().Where(entity => (entity.State == EntityState.Added || entity.State == EntityState.Modified) && entity.Entity.ExternalId == Guid.Empty))
            {
                item.Entity.ExternalId = Guid.NewGuid();
            }

            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                string errorMessage = string.Format("{0} - ", ex.Message);

                foreach (var validationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        errorMessage += string.Format("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }

                throw new DbEntityValidationException(errorMessage, ex.EntityValidationErrors);
            }
        }
    }
}
