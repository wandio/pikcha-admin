﻿namespace FixItTron.Domain.Db.Migrations
{
    using Common.Enums;
    using FixItTron.Domain.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FixItTron.Domain.Db.PikchaDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(FixItTron.Domain.Db.PikchaDbContext context)
        {
            ////if (System.Diagnostics.Debugger.IsAttached == false)
            ////    System.Diagnostics.Debugger.Launch();

            //InitCodeGenerations(context);
            InsertUsers(context);
            InsertConfigurations(context);
            ////InsertAddresses(context);

            ////InsertContracorCategories(context);
            ////InsertJobs(context);
            ////InsertAdditionalItems(context);
            //InsertTaxGroups(context);
            //InsertCurrencies(context);
            ////InsertBanks(context);

            ////InsertProductCategories(context);
            ////InsertProductClasses(context);
            ////InsertProductTypes(context);
            //InsertUnitOfMeasures(context);
            //context.SaveChanges();
            ////InsertProducts(context);
            ////context.SaveChanges();
            ////InsertProductUnitOfMeasure(context);
            ////context.SaveChanges();
            ////InsertPriceLists(context);

            ////InsertEmployee(context);
            //InsertRules(context);
        }

        private static void InsertConfigurations(PikchaDbContext context)
        {
            var freePrints = new FixItTron.Domain.Model.Configuration() { Name = "FREE_PRINTS", Value = "3", LastChangeDate = DateTime.Now };
            var validatePrint = new FixItTron.Domain.Model.Configuration() { Name = "PRINT_VALIDATION", Value = "0", LastChangeDate = DateTime.Now };


            context.Set<FixItTron.Domain.Model.Configuration>().AddOrUpdate(x => x.Name, freePrints);
            context.Set<FixItTron.Domain.Model.Configuration>().AddOrUpdate(x => x.Name, validatePrint);
            context.SaveChanges();
        }

        private static void InsertUsers(PikchaDbContext context)
        {
            var admin = new AdminUser()
            {
                Email = "admin@gmail.com",
                PasswordHash = "AIs1QvJDaWf8wE1myVmH+YHdMEnyyxoNbE+CEoBoALqJ+Utb5Y692s1oRCmDrzuIpg==",
                SecurityStamp = "2c2a4370-e04a-4d17-b932-45a8b284a8b6",
                UserName = "admin",
                CreateDate = DateTime.Now
            };

            //var adminEmp = new Employee()
            //{
            //    FirstName = "admin",
            //    LastName = "admin",
            //    IdentificationNumber = "00000000000",
            //    EMail = "admin@mistermaster.ge",
            //    Status = EmployeeStatus.Active,
            //    User = admin
            //};
            //context.Set<Employee>().AddOrUpdate(x => x.IdentificationNumber, adminEmp);
            context.Set<AdminUser>().AddOrUpdate(x => x.UserName, admin);
            context.Set<Role>().AddOrUpdate(x => x.Name, new Role() { Name = "Admin" });
        }
    }
}
