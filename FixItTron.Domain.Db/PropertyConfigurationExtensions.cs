﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Db
{
    public static class PropertyConfigurationExtensions
    {
        public static StringPropertyConfiguration IsUniqueCode(this StringPropertyConfiguration property)
        {
            return property
                .IsRequired()
                .HasColumnType("varchar")
                .HasMaxLength(20)
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_Code") { IsUnique = true }));
        }
    }
}
