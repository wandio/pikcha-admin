﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Db.Configurations
{
    public class PromoCodeTypeConfiguration : EntityTypeConfiguration<PromoCode>
    {
        public PromoCodeTypeConfiguration()
        {
            Property(x => x.Code)
               .IsRequired()
               .HasColumnType("nvarchar")
               .HasMaxLength(20)
               .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_Code") { IsUnique = true }));
            Property(x => x.Description).HasMaxLength(800);
            HasOptional(x => x.Event).WithMany().HasForeignKey(x => x.EventId);
            HasOptional(x => x.CreateAdminUser).WithMany().HasForeignKey(x => x.CreateAdminUserId);
        }
    }
}
