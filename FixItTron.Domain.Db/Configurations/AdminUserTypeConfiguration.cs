﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Data.Entity.ModelConfiguration.Configuration;

namespace FixItTron.Domain.Db.Configurations
{
    class AdminUserTypeConfiguration : EntityTypeConfiguration<AdminUser>
    {
        public AdminUserTypeConfiguration()
        {
            Property(x => x.UserName).IsName();
            Property(x => x.Email).IsEmail();
            Property(X => X.PhoneNumber).HasColumnType("varchar").HasMaxLength(50);
            Property(X => X.FullName).HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.ImageFileName).HasMaxLength(200);
        }
    }
}
