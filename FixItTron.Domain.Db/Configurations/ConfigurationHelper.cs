﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Db.Configurations
{
    public static class ConfigurationHelper
    {
        /// <summary>
        /// Used in costs, Scale is 4 
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static DecimalPropertyConfiguration IsCost(this DecimalPropertyConfiguration configuration)
        {
            return configuration.HasPrecision(18, 4);
        }

        public static DecimalPropertyConfiguration IsQuantity(this DecimalPropertyConfiguration configuration)
        {
            return configuration.HasPrecision(18, 3);
        }

        /// <summary>
        /// Used in everythin related to prices and price totals. Formatted as 18, 2
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static DecimalPropertyConfiguration IsAmount(this DecimalPropertyConfiguration configuration)
        {
            return configuration.HasPrecision(18, 5);
        }

        /// <summary>
        /// Used in work orders and work order totals, Formatted as 18, 5
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static DecimalPropertyConfiguration IsHighPrecision(this DecimalPropertyConfiguration configuration)
        {
            return configuration.HasPrecision(18, 5);
        }

        public static DecimalPropertyConfiguration IsWeight(this DecimalPropertyConfiguration property)
        {
            return property.HasPrecision(18, 3);
        }
    }
}
