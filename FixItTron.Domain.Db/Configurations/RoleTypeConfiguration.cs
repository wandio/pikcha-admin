﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Data.Entity.ModelConfiguration.Configuration;

namespace FixItTron.Domain.Db.Configurations
{
    public class RoleTypeConfiguration : EntityTypeConfiguration<Role>
    {
        public RoleTypeConfiguration()
        {
            HasMany(role => role.Permissions).WithRequired(permission => permission.Role);
            Property(priceList => priceList.Name).IsRequired().IsName();
        }
    }
}
