﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Db.Configurations
{
    public class SurveyTypeConfiguration : EntityTypeConfiguration<Survey>
    {
        public SurveyTypeConfiguration()
        {
            Property(x => x.FirstName).HasMaxLength(200);
            Property(x => x.LastName).HasMaxLength(200);
            Property(x => x.Email).HasMaxLength(800);
            HasRequired(x => x.User).WithMany().HasForeignKey(x => x.UserId);
            HasRequired(x => x.Event).WithMany().HasForeignKey(x => x.EventId);
        }
    }
}
