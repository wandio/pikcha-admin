﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Data.Entity.ModelConfiguration.Configuration;

namespace FixItTron.Domain.Db.Configurations
{
    public class ConfigurationTypeConfiguration : EntityTypeConfiguration<Configuration>
    {
        public ConfigurationTypeConfiguration()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(200);
            Property(x => x.Value).HasMaxLength(50);
        }
    }
}
