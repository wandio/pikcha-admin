﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Db.Configurations
{
    public class EventTypeConfiguration : EntityTypeConfiguration<Event>
    {
        public EventTypeConfiguration()
        {
            Property(x => x.Name).HasMaxLength(200);
            Property(x => x.Description).HasMaxLength(800);
            Property(x => x.Username).HasMaxLength(50);
            Property(x => x.Password).HasMaxLength(50);
            Property(x => x.BackgroundColor).HasMaxLength(10);
            Property(x => x.LogoFileName).HasMaxLength(200);
            Property(x => x.FacebookLogoFileName).HasMaxLength(200);
            Property(x => x.InstagramLogoFileName).HasMaxLength(200);
            Property(x => x.MobileLogoFileName).HasMaxLength(200);
            HasOptional(x => x.CreateAdminUser).WithMany().HasForeignKey(x => x.CreateAdminUserId);
            HasOptional(x => x.ChangeAdminUser).WithMany().HasForeignKey(x => x.ChangeAdminUserId);
        }
    }
}
