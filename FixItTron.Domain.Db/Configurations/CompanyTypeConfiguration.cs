﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Db.Configurations
{
    public class CompanyTypeConfiguration : EntityTypeConfiguration<Company>
    {
        public CompanyTypeConfiguration()
        {
            Property(x => x.Name).HasMaxLength(200);
            Property(x => x.Description).HasMaxLength(800);
            HasOptional(x => x.CreateAdminUser).WithMany().HasForeignKey(x => x.CreateAdminUserId);
            HasOptional(x => x.ChangeAdminUser).WithMany().HasForeignKey(x => x.ChangeAdminUserId);
        }
    }
}
