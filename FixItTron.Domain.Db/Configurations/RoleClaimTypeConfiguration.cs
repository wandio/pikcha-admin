﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Db.Configurations
{
    public class RoleClaimTypeConfiguration : EntityTypeConfiguration<RoleClaim>
    {
        public RoleClaimTypeConfiguration()
        {
            HasKey(rolePermission => new { rolePermission.RoleId, rolePermission.ClaimType, rolePermission.ClaimValue });
            HasRequired(rolePermission => rolePermission.Role).WithMany().HasForeignKey(rolePermission => rolePermission.RoleId);
        }
    }
}
