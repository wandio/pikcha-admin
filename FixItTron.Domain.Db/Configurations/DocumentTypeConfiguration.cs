﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Data.Entity.ModelConfiguration.Configuration;

namespace FixItTron.Domain.Db.Configurations
{
    public class DocumentTypeConfiguration : EntityTypeConfiguration<Document>
    {
        public DocumentTypeConfiguration()
        {
            Property(x => x.ClassifierId).IsName().IsRequired();
            Property(x => x.Classifier).IsName().IsRequired();
            Property(x => x.Name).HasColumnType("nvarchar").HasMaxLength(200).IsRequired();
            Property(x => x.Description).HasColumnType("nvarchar").HasMaxLength(800);
        }
    }

    public class UploadedFileTypeConfiguration : ComplexTypeConfiguration<UploadedFile>
    {
        public UploadedFileTypeConfiguration()
        {
            Property(x => x.FileName).IsRequired().HasColumnType("nvarchar").HasMaxLength(200).HasColumnName("FileName");
            Property(x => x.Content).IsRequired().HasColumnName("Content");
            Property(x => x.ContentType).IsName().IsRequired().HasColumnName("ContentType").HasMaxLength(200);
        }
    }
}
