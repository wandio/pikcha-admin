﻿using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Domain.Db.Configurations
{
    public class UserTypeConfiguration : EntityTypeConfiguration<User>
    {
        public UserTypeConfiguration()
        {
            Property(cfg => cfg.Username).IsRequired().HasMaxLength(50);
            HasMany(cfg => cfg.Photos).WithOptional(user => user.CreateUser).HasForeignKey(user => user.CreateUserId);
            Property(cfg => cfg.FullName).IsOptional().HasMaxLength(400);
            Property(cfg => cfg.Email).IsOptional().HasMaxLength(200);
        }
    }
}
