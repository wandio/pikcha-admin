﻿INSERT [dbo].[PaymentClassifiers] ([Name], [Code], [IsSystem], [Type]) VALUES (N'სალაროდან თანხის გატანა', N'BOXEXP', 1, 2)
INSERT [dbo].[PaymentClassifiers] ([Name], [Code], [IsSystem], [Type]) VALUES (N'სალაროში თანხის შეტანა', N'BOXINC', 1, 1)
INSERT [dbo].[PaymentClassifiers] ([Name], [Code], [IsSystem], [Type]) VALUES (N'ანგარიშიდან თანხის გატანა', N'BANKEXP', 1, 2)
INSERT [dbo].[PaymentClassifiers] ([Name], [Code], [IsSystem], [Type]) VALUES (N'ანგარიშზე თანხის შეტანა', N'BANKINC', 1, 1)
INSERT [dbo].[PaymentClassifiers] ([Name], [Code], [IsSystem], [Type]) VALUES (N'გადახდა კონტრაქტორისთვის', N'CONTREXP', 1, 2)
INSERT [dbo].[PaymentClassifiers] ([Name], [Code], [IsSystem], [Type]) VALUES (N'შემოსავალი კონტრაქტორისგან', N'CONTRINC', 1, 1)

INSERT [dbo].[ScheduleAdditionalStatus] ([Name], [Status], [Description], [Icon]) VALUES (N'სხვა დრო არ აწყობს', 0, NULL, N'fa-calendar')
INSERT [dbo].[ScheduleAdditionalStatus] ([Name], [Status], [Description], [Icon]) VALUES (N'შესათანხმებელია დრო', 0, NULL, N'fa-clock-o')
INSERT [dbo].[ScheduleAdditionalStatus] ([Name], [Status], [Description], [Icon]) VALUES (N'დასარეკია წინასწარ', 0, NULL, N'fa-phone')
INSERT [dbo].[ScheduleAdditionalStatus] ([Name], [Status], [Description], [Icon]) VALUES (N'წასაღებია მასალა', 0, NULL, N'fa-briefcase')
INSERT [dbo].[ScheduleAdditionalStatus] ([Name], [Status], [Description], [Icon]) VALUES (N'მანქანა გაფუჭდა', 1, NULL, N'fa-ambulance')

--productcategory is required in products table
insert into productcategories(name,isservice)
values(N'სისტემური',1)

INSERT INTO dbo.Products
        ( Name ,
          Code ,
          Description ,
          IsActive ,
          IsService ,
          CreateDate ,
          ProductCategoryId ,
          ProductClassId ,
          BaseUnitOfMeasureId ,
          Cost ,
          SalesPrice ,
          GuaranteeDays ,
          GuaranteeNote ,
          LastChangeDate ,
          ExternalId ,
          ProductTypeId ,
          ExternalIdentifier ,
          TaxGroupId ,
          HourlyMeasuring ,
          AgreementVisibility
        )
VALUES  ( N'დარიცხული სერვისი' , -- Name - nvarchar(50)
          'Accrual' , -- Code - varchar(20)
          NULL , -- Description - nvarchar(800)
          1 , -- IsActive - bit
          1 , -- IsService - bit
          '2016-01-05 10:24:24' , -- CreateDate - datetime
          1 , -- ProductCategoryId - int
          1 , -- ProductClassId - int
          2 , -- BaseUnitOfMeasureId - int
          0 , -- Cost - decimal
          0 , -- SalesPrice - decimal
          NULL , -- GuaranteeDays - int
          NULL , -- GuaranteeNote - nvarchar(800)
          '2016-01-05 10:24:24' , -- LastChangeDate - datetime
          NEWID() , -- ExternalId - uniqueidentifier
          NULL , -- ProductTypeId - int
          NULL , -- ExternalIdentifier - nvarchar(max)
          NULL , -- TaxGroupId - int
          0 , -- HourlyMeasuring - bit
          1  -- AgreementVisibility - int
        )
GO

ALTER PROCEDURE recalculate_boxOffice_statement
    @targetBoxOfficeId INT
AS 
    BEGIN

        DECLARE @boxOfficeId INT

        DECLARE boffice_cursor CURSOR
        FOR
            SELECT  b.BoxOfficeId
            FROM    dbo.BoxOffices b
            WHERE   ( @targetBoxOfficeId IS NULL
                      OR BoxOfficeId = @targetBoxOfficeId
                    )

        OPEN boffice_cursor       

        FETCH NEXT FROM boffice_cursor INTO @boxOfficeId

        WHILE @@FETCH_STATUS = 0 
            BEGIN
                DECLARE @balance DECIMAL = 0;

                WITH    p AS ( SELECT   StartingBalance ,
                                        EndingBalance ,
                                        Amount ,
                                        Type ,
                                        Val = SUM(IIF(p.Type = 1, amount,-1*amount)) OVER ( ORDER BY CAST(p.PayDate AS DATE) , p.PaymentId )
                               FROM     dbo.Payments p
                               WHERE    p.Method = 1
                                        AND p.BoxOfficeId = @boxOfficeId
                                        AND P.Status = 3
                             )
                    UPDATE  p
                    SET     p.StartingBalance = p.Val - p.Amount ,
                            p.EndingBalance = p.Val

                FETCH NEXT FROM boffice_cursor INTO @boxOfficeId
            END
        CLOSE boffice_cursor;
        DEALLOCATE boffice_cursor;

    END

GO

ALTER PROCEDURE recalculate_bankAccount_statement
    @targetBankAccountId INT
AS 
    BEGIN

        DECLARE @bankAccountId INT

        DECLARE boffice_cursor CURSOR
        FOR
            SELECT  b.BankAccountId
            FROM    dbo.bankAccounts b
            WHERE   ( @targetBankAccountId IS NULL
                      OR BankAccountId = @targetBankAccountId
                    )

        OPEN boffice_cursor       

        FETCH NEXT FROM boffice_cursor INTO @bankAccountId

        WHILE @@FETCH_STATUS = 0 
            BEGIN
                DECLARE @balance DECIMAL = 0;

                WITH    p AS ( SELECT   StartingBalance ,
                                        EndingBalance ,
                                        Amount ,
                                        Type ,
                                        Val = SUM(IIF(p.Type = 1, amount,-1*amount)) OVER ( ORDER BY CAST(p.PayDate AS DATE) , p.PaymentId )
                               FROM     dbo.Payments p
                               WHERE    p.Method = 2
                                        AND p.BankAccountId = @bankAccountId
                                        AND P.Status = 3
                             )
                    UPDATE  p
                    SET     p.StartingBalance = p.Val - p.Amount ,
                            p.EndingBalance = p.Val

                FETCH NEXT FROM boffice_cursor INTO @bankAccountId
            END
        CLOSE boffice_cursor;
        DEALLOCATE boffice_cursor;

    END

GO

INSERT INTO dbo.Products
        ( Name ,
          Code ,
          Description ,
          IsActive ,
          IsService ,
          CreateDate ,
          ProductCategoryId ,
          ProductClassId ,
          BaseUnitOfMeasureId ,
          Cost ,
          SalesPrice ,
          GuaranteeDays ,
          GuaranteeNote ,
          LastChangeDate ,
          ExternalId ,
          ProductTypeId ,
          ExternalIdentifier ,
          TaxGroupId ,
          HourlyMeasuring ,
          AgreementVisibility
        )
VALUES  ( N'სააბონენტო მომსახურება' , -- Name - nvarchar(50)
          'Subscription' , -- Code - varchar(20)
          NULL , -- Description - nvarchar(800)
          1 , -- IsActive - bit
          1 , -- IsService - bit
          '2016-01-05 10:24:24' , -- CreateDate - datetime
          1 , -- ProductCategoryId - int
          1 , -- ProductClassId - int
          2 , -- BaseUnitOfMeasureId - int
          0 , -- Cost - decimal
          0 , -- SalesPrice - decimal
          NULL , -- GuaranteeDays - int
          NULL , -- GuaranteeNote - nvarchar(800)
          '2016-01-05 10:24:24' , -- LastChangeDate - datetime
          NEWID() , -- ExternalId - uniqueidentifier
          NULL , -- ProductTypeId - int
          NULL , -- ExternalIdentifier - nvarchar(max)
          NULL , -- TaxGroupId - int
          0 , -- HourlyMeasuring - bit
          1  -- AgreementVisibility - int
        )
GO

INSERT INTO Reasons(name,type,isactive,code,issystem)
VALUES(N'განრიგის გადანაცვლება', 2, 1, 'ScheduleReassigned', 1)

--#region codegenerations
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (2, N'WR', N'', 0, 1, 5, 1)
GO
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (8, N'CTR', N'', 0, 1, 5, 1)
GO
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (10, N'PAY', N'', 0, 1, 5, 1)
GO
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (11, N'SO', N'', 0, 1, 5, 1)
GO
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (12, N'PL', N'', 0, 1, 5, 1)
GO
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (14, N'BO', N'', 0, 1, 5, 1)
GO
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (15, N'SQ', N'', 0, 1, 5, 1)
GO
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (16, N'PR', N'', 0, 1, 5, 1)
GO
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (17, N'MT', N'', 0, 1, 5, 1)
GO
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (18, N'SR', N'', 0, 1, 5, 1)
GO
INSERT [dbo].[CodeGenerations] ([CodeTable], [Prefix], [Suffix], [StartFrom], [IncrementStep], [DigitPartLegth], [CurrentStep]) VALUES (19, N'CA', N'', 0, 1, 5, 1)
GO
--#endregion

--#region pricerule
SET IDENTITY_INSERT [dbo].[PriceRules] ON 
GO
INSERT [dbo].[PriceRules] ([PriceRuleId], [Code], [Name], [ActiveFrom], [ActiveTo], [IsActive], [IsBase], [LastChangeDate], [ExactCondition], [PriceListId]) 
VALUES (1, N'001', N'ყველაფერი 35 ლარად', CAST(N'2016-01-26 19:27:26.293' AS DateTime), NULL, 1, 0, CAST(N'2016-01-26 19:27:26.313' AS DateTime), 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[PriceRules] OFF
GO
SET IDENTITY_INSERT [dbo].[PriceRuleConditions] ON 

GO
INSERT [dbo].[PriceRuleConditions] ([PriceRuleConditionId], [PriceRuleId], [CalculationType], [ConditionType], [Operator], [ConditionValue], [UnitOfMeasureId], [DiscountType], [DiscountValue], [CurrencyId]) 
VALUES (1, 1, 2, 1, 3, CAST(1.00 AS Decimal(18, 2)), 2, 3, CAST(35.00 AS Decimal(18, 2)), NULL)
GO
INSERT [dbo].[PriceRuleConditions] ([PriceRuleConditionId], [PriceRuleId], [CalculationType], [ConditionType], [Operator], [ConditionValue], [UnitOfMeasureId], [DiscountType], [DiscountValue], [CurrencyId]) 
VALUES (2, 1, 2, 1, 3, CAST(2.00 AS Decimal(18, 2)), 2, 3, CAST(0.00 AS Decimal(18, 2)), NULL)
GO
INSERT [dbo].[PriceRuleConditions] ([PriceRuleConditionId], [PriceRuleId], [CalculationType], [ConditionType], [Operator], [ConditionValue], [UnitOfMeasureId], [DiscountType], [DiscountValue], [CurrencyId]) 
VALUES (3, 1, 2, 1, 1, CAST(2.00 AS Decimal(18, 2)), 2, 3, CAST(10.00 AS Decimal(18, 2)), NULL)
GO
SET IDENTITY_INSERT [dbo].[PriceRuleConditions] OFF
GO
SET IDENTITY_INSERT [dbo].[PriceRuleFilters] ON 
GO
INSERT [dbo].[PriceRuleFilters] ([PriceRuleFilterId], [PriceRuleId], [FilterType], [FilterBy], [ProductCategoryId], [ProductClassId]) VALUES (2, 1, 1, 1, NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[PriceRuleFilters] OFF
GO
--#endregion