﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class SendApiQuickReply
    {
        public string content_type { get; set; }
        public string title { get; set; }
        public string payload { get; set; }
    }

    public class SendApiAttachment
    {
        public string type { get; set; }
        public SendApiAttachmentPayload payload { get; set; }
    }

    public class SendApiAttachmentPayload
    {
        public string url { get; set; }
        public string template_type { get; set; }
        public string text { get; set; }
        public IEnumerable<SendApiAttachmentButton> buttons { get; set; }
    }

    public class SendApiAttachmentButton
    {
        public string type { get; set; }
        public string url { get; set; }
        public string title { get; set; }
        public string payload { get; set; }
    }

    public class SendApiMessage
    {
        public string text { get; set; }
        public SendApiAttachment attachment { get; set; }
        public string metadata { get; set; }
        public IEnumerable<SendApiQuickReply> quick_replies { get; set; }
    }

    public class SendApiRecipient
    {
        public string id { get; set; }
    }

    public class SendApiPayload
    {
        public SendApiRecipient recipient { get; set; }
        public SendApiMessage message { get; set; }
    }

    public class FacebookUserProfile
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
    }
}
