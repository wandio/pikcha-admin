﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class WebhookAttachmentPayload
    {
        public string url { get; set; }
    }

    public class WebhookAttachment
    {
        public string type { get; set; }
        public WebhookAttachmentPayload payload { get; set; }
    }

    public class WebhookQuickReply
    {
        public string payload { get; set; }
    }

    public class WebhookMessage
    {
        public bool is_echo { get; set; }
        public string mid { get; set; }
        public string metadata { get; set; }
        public string text { get; set; }
        public string seq { get; set; }
        public WebhookQuickReply quick_reply { get; set; }
        public IEnumerable<WebhookAttachment> attachments { get; set; }
    }

    public class WebhookSender
    {
        public string id { get; set; }
    }

    public class WebhookRecipient
    {
        public string id { get; set; }
    }

    public class WebhookMessagingEvent
    {
        public WebhookSender sender { get; set; }
        public WebhookRecipient recipient { get; set; }
        public string timestamp { get; set; }
        public WebhookMessage message { get; set; }
        public WebhookPostback postback { get; set; }
    }

    public class WebhookPostback
    {
        public string payload { get; set; }
        public WebhookReferral referral { get; set; }
    }

    public class WebhookReferral
    {
        public string @ref { get; set; }
        public string source { get; set; }
        public string type { get; set; }
    }

    public class WebhookPageEntry
    {
        public string id { get; set; }
        public string time { get; set; }
        public IEnumerable<WebhookMessagingEvent> messaging { get; set; }
    }

    public class WebhookData
    {
        public string @object { get; set; }
        public IEnumerable<WebhookPageEntry> entry { get; set; }
    }
}
