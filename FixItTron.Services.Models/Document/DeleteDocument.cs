﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Document
{
    public class DeleteDocumentRequest
    {
        public int DocId { get; set; }
    }
    public class DeleteDocumentResponse : ResultResponse
    {

    }
}
