﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Document
{
    public class AddDocumentRequest
    {
        public int Id { get; set; }
        public DocumentListItem Document { get; set; }
    }

    public class AddDocumentResponse : ResultResponse
    {

    }
}
