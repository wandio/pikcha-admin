﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Document
{
    public class GetDocumentRequest
    {
        public int Id { get; set; }
        public int DocId { get; set; }
    }
    public class GetDocumentResponse
    {
        public DocumentListItem Document { get; set; }
    }
}
