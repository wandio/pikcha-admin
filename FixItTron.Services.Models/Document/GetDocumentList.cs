﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Document
{
    public class GetDocumentListRequest
    {
        public int Id { get; set; }
    }

    public class GetDocumentListResponse
    {
        public IEnumerable<DocumentListItem> Documents { get; set; }
    }
}
