﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.User
{
    public class GetEmployeeProfileRequest
    {
        public string Name { get; set; }
    }
    public class GetEmployeeProfileResponse
    {
        public EmployeeProfileItem Employee { get; set; }
    }
}
