﻿using FixItTron.Services.Models.Document;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.User
{
    public class ChangeProfileImageRequest
    {
        public string Name { get; set; }
        public Event.DocumentListItem Image { get; set; }
    }
    public class ChangeProfileImageResponse : ResultResponse
    {

    }
}
