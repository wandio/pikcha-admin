﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.User
{
    public class CreateUserRequest
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int? EmployeeId { get; set; }
        public string[] Roles { get; set; }
    }

    public class CreateUserResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
