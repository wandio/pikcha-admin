﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FixItTron.Services.Models.Shared;

namespace FixItTron.Services.Models.User
{
    public class ChangePasswordRequest
    {
        public string Name { get; set; }
        public string CurrentPassword { get; set; }
        public string NewPassword { get; set; }
    }
    public class ChangePasswordResponse : ResultResponse
    {
        
    }
}
