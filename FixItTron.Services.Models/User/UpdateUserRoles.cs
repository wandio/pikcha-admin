﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.User
{
    public class UpdateUserRolesRequest
    {
        public int UserId { get; set; }
        public string[] Roles { get; set; }
    }
    public class UpdateUserRolesResponse : ResultResponse
    {

    }
}
