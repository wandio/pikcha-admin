﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.User
{
    public class GetUserRolesRequest
    {
        public int UserId { get; set; }
    }
    public class GetUserRolesResponse
    {
        public IEnumerable<int> RoleIds { get; set; }
    }
}
