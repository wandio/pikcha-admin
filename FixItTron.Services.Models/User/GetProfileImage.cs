﻿using FixItTron.Services.Models.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.User
{
    public class GetProfileImageRequest
    {
        public string Name { get; set; }
    }
    public class GetProfileImageResponse
    {
        public string Image { get; set; }
    }
}
