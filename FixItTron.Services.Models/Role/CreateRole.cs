﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Role
{
    public class CreateRoleRequest
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public IList<EditRolePermissionItem> Permissions { get; set; }
    }

    public class CreateRoleResponse : ResultResponse
    {
        public int RoleId { get; set; }
    }
}
