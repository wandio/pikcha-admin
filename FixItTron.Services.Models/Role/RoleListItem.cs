﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Role
{
    public class RoleListItem
    {
        public int RoleId { get; set; }
        public string Name { get; set; }
        public int UserCount { get; set; }
    }
}
