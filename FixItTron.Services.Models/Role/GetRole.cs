﻿using FixItTron.Services.Models.Role;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Role
{
    public class GetRoleRequest
    {
        public int RoleId { get; set; }
    }

    public class GetRoleResponse
    {
        public string Name { get; set; }
        public IList<EditRolePermissionItem> Permissions { get; set; }
    }
}
