﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Role
{
    public class GetRoleListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
    }

    public class GetRoleListResponse : PagedListResponseBase
    {
        public IEnumerable<RoleListItem> Roles { get; set; }
    }
}
