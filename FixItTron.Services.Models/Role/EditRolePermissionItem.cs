﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Role
{
    public class EditRolePermissionItem
    {
        public Securable Securable { get; set; }
        public bool Create { get; set; }
        public bool Read { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }
        public bool Book { get; set; }
        public bool Approve { get; set; }
        public bool Post { get; set; }
        public bool Cancel { get; set; }
        public bool PowerPermission { get; set; }
    }
}
