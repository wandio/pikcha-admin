﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.PromoCode
{
    public class PromoCodeListItem
    {
        public int PromoCodeId { get; set; }
        public string Code { get; set; }
        public int PrintCount { get; set; }
        public bool MultiUse { get; set; }
        public string Event { get; set; }
        public string Description { get; set; }
    }
}
