﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.PromoCode
{
    public class DeletePromoCodesRequest
    {
        public List<int> Ids { get; set; }
    }
    public class DeletePromoCodesResponse : ResultResponse
    {

    }
}
