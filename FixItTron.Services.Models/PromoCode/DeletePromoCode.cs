﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.PromoCode
{
    public class DeletePromoCodeRequest
    {
        public int PromoCodeId { get; set; }
    }

    public class DeletePromoCodeResponse : ResultResponse
    {

    }
}
