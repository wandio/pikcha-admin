﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.PromoCode
{
    public class UpdatePromoCodeRequest
    {
        public AddEditPromoCodeItem PromoCode { get; set; }
    }

    public class UpdatePromoCodeResponse : ResultResponse
    {
    }
}
