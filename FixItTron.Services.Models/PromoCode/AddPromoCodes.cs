﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.PromoCode
{
    public class AddPromoCodesRequest
    {
        public IEnumerable<AddEditPromoCodeItem> PromoCodes { get; set; }
    }
    public class AddPromoCodesResponse : ResultResponse
    {

    }
}
