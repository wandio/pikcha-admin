﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.PromoCode
{
    public class AddEditPromoCodeItem
    {
        public int PromoCodeId { get; set; }
        public string Code { get; set; }
        public int PrintCount { get; set; }
        public string Description { get; set; }
        public bool MultiUse { get; set; }
        public int? EventId { get; set; }
        public string UserName { get; set; }
        public string EventName { get; set; }
    }
}
