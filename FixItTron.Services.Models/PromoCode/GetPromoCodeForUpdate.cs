﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.PromoCode
{
    public class GetPromoCodeForUpdateRequest
    {
        public int PromoCodeId { get; set; }
    }

    public class GetPromoCodeForUpdateResponse
    {
        public AddEditPromoCodeItem PromoCode { get; set; }
    }
}
