﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.PromoCode
{
    public class GetPromoCodeListRequest : SortedAndPagedListRequestBase
    {
        public bool? MultiUse { get; set; }
        public string Code { get; set; }
        public UnboundedRange<int> PrintCount { get; set; }
        public int? EventId { get; set; }
        public string Description { get; set; }
    }

    public class GetPromoCodeListResponse : PagedListResponseBase
    {
        public IEnumerable<PromoCodeListItem> PromoCodes { get; set; }
    }
}
