﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Event
{
    public class EventListItem
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public string EventCode { get; set; }
        public string Username { get; set; }
        public bool IsActive { get; set; }
        public string Company { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int? PrintCount { get; set; }
        public bool ShowLogoOnShare { get; set; }
        public bool ShowLogoOnPreview { get; set; }
    }
}
