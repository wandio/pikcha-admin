﻿using Common.Enums;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Event
{
    public class ChangeEventLogoRequest
    {
        public int EventId { get; set; }
        public DocumentListItem Logo { get; set; }
        public LogoType LogoType { get; set; }
    }
    public class ChangeEventLogoResponse : ResultResponse
    {

    }
}
