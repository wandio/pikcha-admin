﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Event
{
    public class AuthorizePrintRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class AuthorizePrintResponse : ResultResponse
    {
        public int EventId { get; set; }
    }
}
