﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Event
{
    public class DocumentListItem
    {
        public int DocumentId { get; set; }

        public string Name { get; set; }

        public string Classifier { get; set; }

        public string ClassifierId { get; set; }
        public string Description { get; set; }

        public UploadedFileListItem File { get; set; }
    }
}
