﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Event
{
    public class GetEventForUpdateRequest
    {
        public int EventId { get; set; }
    }

    public class GetEventForUpdateResponse
    {
        public AddEditEventItem Event { get; set; }
    }
}
