﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Event
{
    public class AddEditEventItem
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Username { get; set; }
        public string UserName { get; set; } //logged users username
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public int? CompanyId { get; set; }
        public bool? HasImage { get; set; }
        public string BackgroundColor { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool ShowLogoOnShare { get; set; }
        public bool ShowLogoOnPreview { get; set; }
        public bool ShowBackgroundLogo { get; set; }

        public string LogoFileName { get; set; }
        public string BackgroundLogoFileName { get; set; }
        public string MobileLogoFileName { get; set; }
        public string InstagramLogoFileName { get; set; }
        public string FacebookLogoFileName { get; set; }

        public bool ShowSurvey { get; set; }
        public string FacebookAppSecret { get; set; }
        public string FacebookPageId { get; set; }
        public string FacebookPageAccessToken { get; set; }
        public string EventCode { get; set; }
    }
}
