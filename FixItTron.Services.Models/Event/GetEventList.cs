﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Event
{
    public class GetEventListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public string EventCode { get; set; }
        public string Username { get; set; }
        public bool? IsActive { get; set; }
        public int? CompanyId { get; set; }
        public UnboundedPeriod StartDate  { get; set; }
        public UnboundedPeriod EndDate { get; set; }
        public UnboundedRange<int> PrintCount { get; set; }
        public bool? ShowLogoOnShare { get; set; }
        public bool? ShowLogoOnPreview { get; set; }
    }

    public class GetEventListResponse : PagedListResponseBase
    {
        public IEnumerable<EventListItem> Events { get; set; }
    }
}
