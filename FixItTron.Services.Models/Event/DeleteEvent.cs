﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Event
{
    public class DeleteEventRequest
    {
        public int EventId { get; set; }
    }

    public class DeleteEventResponse : ResultResponse
    {

    }
}
