﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Event
{
    public class GetEventLogoRequest
    {
        public int EventId { get; set; }
    }
    public class GetEventLogoResponse
    {
        public string LogoFileName { get; set; }
        public string BackgroundColor { get; set; }
        public string BackgroundLogoFileName { get; set; }
    }
}
