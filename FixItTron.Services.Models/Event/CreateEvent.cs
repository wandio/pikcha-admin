﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Event
{
    public class CreateEventRequest
    {
        public AddEditEventItem Event { get; set; }
    }

    public class CreateEventResponse : ResultResponse
    {
        public int EventId { get; set; }
    }
}
