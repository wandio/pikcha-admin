﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Banks
{
    public class CreateBankRequest
    {
        public AddEditBankItem Bank { get; set; }
    }

    public class CreateBankResponse : ResultResponse
    {
        public int BankId { get; set; }
    }
}
