﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Banks
{
    public class GetBankForUpdateRequest
    {
        public int BankId { get; set; }
    }

    public class GetBankForUpdateResponse
    {
        public AddEditBankItem Bank { get; set; }
    }
}
