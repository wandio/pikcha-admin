﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.Banks
{
    public class GetBankListRequest : SortedAndPagedListRequestBase
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public bool? IsActive { get; set; }
    }

    public class GetBankListResponse : PagedListResponseBase
    {
        public IEnumerable<AddEditBankItem> Banks { get; set; }
    }
}
