﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Banks
{
    public class DeleteBankRequest
    {
        public int BankId { get; set; }
    }

    public class DeleteBankResponse : ResultResponse
    {

    }
}
