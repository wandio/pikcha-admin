﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.ProductType
{
    public class GetProductTypeListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
    }

    public class GetProductTypeListResponse : PagedListResponseBase
    {
        public IEnumerable<ProductTypeListItem> ProductTypes { get; set; }
    }
}
