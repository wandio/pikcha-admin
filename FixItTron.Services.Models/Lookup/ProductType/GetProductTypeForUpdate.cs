﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ProductType
{
    public class GetProductTypeForUpdateRequest
    {
        public int ProductTypeId { get; set; }
    }

    public class GetProductTypeForUpdateResponse
    {
        public ProductTypeListItem ProductType { get; set; }
    }
}
