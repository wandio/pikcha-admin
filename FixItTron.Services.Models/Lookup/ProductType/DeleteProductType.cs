﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ProductType
{
    public class DeleteProductTypeRequest
    {
        public int ProductTypeId { get; set; }
    }

    public class DeleteProductTypeResponse : ResultResponse
    {

    }
}
