﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ProductType
{
    public class CreateProductTypeRequest
    {
        public int ProductTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class CreateProductTypeResponse : ResultResponse
    {
        public int ProductTypeId { get; set; }
    }
}
