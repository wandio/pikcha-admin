﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.TaxGroups
{
    public class GetTaxGroupForUpdateRequest
    {
        public int TaxGroupId { get; set; }
    }

    public class GetTaxGroupForUpdateResponse
    {
        public TaxGroupListItem TaxGroup { get; set; }
    }
}
