﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.TaxGroups
{
    public class GetTaxGroupListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public UnboundedRange<decimal> Percent { get; set; }
    }

    public class GetTaxGroupListResponse : PagedListResponseBase
    {
        public IEnumerable<TaxGroupListItem> TaxGroups { get; set; }
    }
}
