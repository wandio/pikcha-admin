﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.TaxGroups
{
    public class DeleteTaxGroupRequest
    {
        public int TaxGroupId { get; set; }
    }

    public class DeleteTaxGroupResponse : ResultResponse
    {

    }
}
