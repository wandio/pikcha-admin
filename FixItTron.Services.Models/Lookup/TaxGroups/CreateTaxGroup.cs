﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.TaxGroups
{
    public class CreateTaxGroupRequest
    {
        public int TaxGroupId { get; set; }
        public string Name { get; set; }
        public decimal Percent { get; set; }
    }

    public class CreateTaxGroupResponse : ResultResponse
    {
        public int TaxGroupId { get; set; }
    }
}
