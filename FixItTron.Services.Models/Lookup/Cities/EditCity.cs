﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Cities
{
    public class EditCityRequest
    {
        public int CityId { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }
        public bool IsDefault { get; set; }
    }

    public class EditCityResponse : ResultResponse
    {

    }
}
