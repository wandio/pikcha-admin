﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.Cities
{
    public class GetCityListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public string CountryName { get; set; }
    }

    public class GetCityListResponse : PagedListResponseBase
    {
        public IEnumerable<CityModel> Cities { get; set; }
    }
}
