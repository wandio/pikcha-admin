﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Cities
{
    public class DeleteCityRequest
    {
        public int CityId { get; set; }
    }

    public class DeleteCityResponse : ResultResponse
    {

    }
}
