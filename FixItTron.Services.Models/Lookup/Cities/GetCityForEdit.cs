﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Cities
{

    public class GetCityForEditRequest
    {
        public int CityId { get; set; }
    }

    public class GetCityForEditResponse
    {
        public CityModel City { get; set; }
    }
}
