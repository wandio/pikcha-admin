﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ApplicationUsers
{
    public class DeleteApplicationUsersRequest
    {
        public List<int> Ids { get; set; }
    }
    public class DeleteApplicationUsersResponse : ResultResponse
    {

    }
}
