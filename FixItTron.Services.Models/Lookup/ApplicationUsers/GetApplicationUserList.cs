﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.ApplicationUsers
{
    public class GetApplicationUserListRequest : SortedAndPagedListRequestBase
    {
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public UnboundedRange<int> AvailablePrints { get; set; }
        public UnboundedRange<int> UsedPrints { get; set; }
        public UnboundedPeriod CreateDate { get; set; }
        public UserType? UserType { get; set; }
    }
    public class GetApplicationUserListResponse : PagedListResponseBase
    {
        public IEnumerable<ApplicationUserListItem> Users { get; set; }
    }

    public class ApplicationUserListItem
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public int AvailablePrints { get; set; }
        public int UsedPrints { get; set; }
        public DateTime? CreateDate { get; set; }
        public UserType UserType { get; set; }
    }
}
