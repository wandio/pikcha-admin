﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.ProductClass
{
    public class GetProductClassListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class GetProductClassListResponse : PagedListResponseBase
    {
        public IEnumerable<ProductClassListItem> ProductClasses { get; set; }
    }
}
