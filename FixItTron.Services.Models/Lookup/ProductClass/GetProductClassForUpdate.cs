﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ProductClass
{
    public class GetProductClassForUpdateRequest
    {
        public int ProductClassId { get; set; }
    }

    public class GetProductClassForUpdateResponse
    {
        public ProductClassListItem ProductClass { get; set; }
    }
}
