﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ProductClass
{
    public class DeleteProductClassRequest
    {
        public int ProductClassId { get; set; }
    }

    public class DeleteProductClassResponse : ResultResponse
    {

    }
}
