﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ProductClass
{
    public class ProductClassListItem
    {
        public int ProductClassId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
