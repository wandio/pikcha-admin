﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ProductClass
{
    public class CreateProductClassRequest
    {
        public int ProductClassId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class CreateProductClassResponse : ResultResponse
    {
        public int ProductClassId { get; set; }
    }
}
