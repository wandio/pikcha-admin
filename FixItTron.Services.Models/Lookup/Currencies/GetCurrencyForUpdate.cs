﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Currencies
{
    public class GetCurrencyForUpdateRequest
    {
        public int CurrencyId { get; set; }
    }

    public class GetCurrencyForUpdateResponse
    {
        public AddEditCurrencyItem Currency { get; set; }
    }
}
