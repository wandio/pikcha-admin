﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Currencies
{
    public class CreateCurrencyRequest
    {
        public AddEditCurrencyItem Currency { get; set; }
    }

    public class CreateCurrencyResponse : ResultResponse
    {
        public int CurrencyId { get; set; }
    }
}
