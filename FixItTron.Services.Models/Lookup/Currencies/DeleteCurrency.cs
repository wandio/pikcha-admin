﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Currencies
{
    public class DeleteCurrencyRequest
    {
        public int CurrencyId { get; set; }
    }

    public class DeleteCurrencyResponse : ResultResponse
    {

    }
}
