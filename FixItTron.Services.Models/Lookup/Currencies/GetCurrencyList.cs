﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.Currencies
{
    public class GetCurrencyListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class GetCurrencyListResponse : PagedListResponseBase
    {
        public IEnumerable<CurrencyListItem> Currencies { get; set; }
    }
}
