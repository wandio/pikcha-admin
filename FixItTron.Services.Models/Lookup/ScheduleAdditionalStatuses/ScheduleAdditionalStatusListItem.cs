﻿using Common.Enums;
using FixItTron.Services.Models.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ScheduleAdditionalStatuses
{
    public class ScheduleAdditionalStatusListItem
    {
        public int ScheduleAdditionalStatusId { get; set; }
        public string Name { get; set; }
        public SalesOrderScheduleStatus Status { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
    }
}
