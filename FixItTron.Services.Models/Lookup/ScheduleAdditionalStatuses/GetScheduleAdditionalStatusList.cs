﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.ScheduleAdditionalStatuses
{
    public class GetScheduleAdditionalStatusListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public SalesOrderScheduleStatus? Status { get; set; }
    }

    public class GetScheduleAdditionalStatusListResponse : PagedListResponseBase
    {
        public IEnumerable<ScheduleAdditionalStatusListItem> Items { get; set; }
    }
}
