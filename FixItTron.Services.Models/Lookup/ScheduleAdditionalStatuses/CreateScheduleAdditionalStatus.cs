﻿using Common.Enums;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ScheduleAdditionalStatuses
{
    public class CreateScheduleAdditionalStatusRequest
    {
        public ScheduleAdditionalStatusListItem Status { get; set; }
    }
    public class CreateScheduleAdditionalStatusResponse : ResultResponse
    {
        public int ScheduleAdditionalStatusId { get; set; }
    }
}
