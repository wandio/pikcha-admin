﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ScheduleAdditionalStatuses
{
    public class DeleteScheduleAdditionalStatusRequest
    {
        public int ScheduleAdditionalStatusId { get; set; }
    }

    public class DeleteScheduleAdditionalStatusResponse : ResultResponse
    {

    }
}
