﻿using FixItTron.Services.Models.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ScheduleAdditionalStatuses
{
    public class GetScheduleAdditionalStatusImageRequest
    {
        public int Id { get; set; }
    }
    public class GetScheduleAdditionalStatusImageResponse
    {
        public DocumentListItem Image { get; set; }
    }
}
