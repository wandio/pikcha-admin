﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ScheduleAdditionalStatuses
{
    public class GetScheduleAdditionalStatusForUpdateRequest
    {
        public int ScheduleAdditionalStatusId { get; set; }
    }

    public class GetScheduleAdditionalStatusForUpdateResponse
    {
        public ScheduleAdditionalStatusListItem Status { get; set; }
    }
}
