﻿using Common.Enums;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Reason
{
    public class CreateReasonRequest
    {
        //public int CancelReasonId { get; set; }
        public string Name { get; set; }
        public ReasonType Type { get; set; }
    }

    public class CreateReasonResponse : ResultResponse
    {
        public int ReasonId { get; set; }
    }
}
