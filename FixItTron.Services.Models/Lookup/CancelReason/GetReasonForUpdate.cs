﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Reason
{
    public class GetReasonForUpdateRequest
    {
        public int ReasonId { get; set; }
    }

    public class GetReasonForUpdateResponse
    {
        public string Name { get; set; }
        public ReasonType Type{ get; set; }
    }
}
