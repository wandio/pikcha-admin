﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Reason
{
    public class ChangeReasonStatusRequest
    {
        public int ReasonId { get; set; }
    }
    public class ChangeReasonStatusResponse : ResultResponse
    {

    }
}
