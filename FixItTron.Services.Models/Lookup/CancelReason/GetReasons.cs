﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Reason
{
    public class GetReasonsRequest
    {
        public ReasonType? Type { get; set; }
        public bool? IsActive { get; set; }
    }
    public class GetReasonsResponse
    {
        public IEnumerable<ReasonListItem> Reasons { get; set; }
    }
}
