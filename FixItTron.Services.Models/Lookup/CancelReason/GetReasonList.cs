﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.Reason
{
    public class GetReasonListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public ReasonType? Type { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsSystem { get; set; }
    }

    public class GetReasonListResponse : PagedListResponseBase
    {
        public IEnumerable<ReasonListItem> Reasons { get; set; }
    }
}
