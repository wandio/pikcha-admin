﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.AbsenseTypes
{
    public class GetAbsenseTypeListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public bool? IsActive { get; set; }
    }

    public class GetAbsenseTypeListResponse : PagedListResponseBase
    {
        public IEnumerable<AbsenseTypeListItem> AbsenseTypes { get; set; }
    }
}
