﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseTypes
{
    public class UpdateAbsenseTypeRequest
    {
        public AddEditAbsenseTypeItem AbsenseType { get; set; }
    }

    public class UpdateAbsenseTypeResponse : ResultResponse
    {
    }
}
