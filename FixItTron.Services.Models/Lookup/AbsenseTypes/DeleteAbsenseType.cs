﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseTypes
{
    public class DeleteAbsenseTypeRequest
    {
        public int AbsenseTypeId { get; set; }
    }

    public class DeleteAbsenseTypeResponse : ResultResponse
    {

    }
}
