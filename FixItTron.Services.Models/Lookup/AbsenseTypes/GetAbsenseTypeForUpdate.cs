﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseTypes
{
    public class GetAbsenseTypeForUpdateRequest
    {
        public int AbsenseTypeId { get; set; }
    }

    public class GetAbsenseTypeForUpdateResponse
    {
        public AddEditAbsenseTypeItem AbsenseType { get; set; }
    }
}
