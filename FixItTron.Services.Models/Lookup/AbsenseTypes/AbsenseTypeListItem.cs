﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseTypes
{
    public class AbsenseTypeListItem
    {
        public int AbsenseTypeId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
