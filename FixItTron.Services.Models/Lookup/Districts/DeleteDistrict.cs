﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Services.Models.Lookup
{
    public class DeleteDistrictRequest
    {
        public int DistrictId { get; set; }
    }

    public class DeleteDistrictResponse : ResultResponse
    {

    }
}
