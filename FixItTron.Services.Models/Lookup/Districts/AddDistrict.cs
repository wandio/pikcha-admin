﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Services.Models.Lookup
{
    public class AddDistrictRequest
    {
        public int CityId { get; set; }
        public string Name { get; set; }
    }

    public class AddDistrictResponse : ResultResponse
    {

    }
}
