﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.Districts
{
    public class GetDistrictListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public string City { get; set; }
    }

    public class GetDistrictListResponse : PagedListResponseBase
    {
        public IEnumerable<DistrictModel> Districts { get; set; }
    }
}
