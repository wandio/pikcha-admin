﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Districts
{
    public class GetDistrictForEditRequest
    {
        public int DistrictId { get; set; }
        public int CityId { get; set; }
    }

    public class GetDistrictForEditResponse
    {
        public DistrictModel District { get; set; }
    }
}
