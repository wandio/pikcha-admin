﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Services.Models.Lookup
{
    public class EditDistrictRequest
    {
        public int CityId { get; set; }
        public int DistrictId { get; set; }
        public string Name { get; set; }
    }

    public class EditDistrictResponse : ResultResponse
    {

    }
}
