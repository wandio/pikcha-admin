﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup
{
    public class GetContractorCategoryListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class GetContractorCategoryListResponse : PagedListResponseBase
    {
        public IEnumerable<ContractorCategoryModel> ContractorCategories { get; set; }
    }
}
