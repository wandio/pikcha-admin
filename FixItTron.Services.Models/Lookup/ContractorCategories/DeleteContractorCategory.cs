﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Services.Models.Lookup
{
    public class DeleteContractorCategoryRequest
    {
        public int ContractorCategoryId { get; set; }
    }

    public class DeleteContractorCategoryResponse : ResultResponse
    {

    }
}
