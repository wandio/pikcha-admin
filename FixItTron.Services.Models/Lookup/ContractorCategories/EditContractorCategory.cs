﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Services.Models.Lookup
{
    public class EditContractorCategoryRequest
    {
        public int ContractorCategoryId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }

    public class EditContractorCategoryResponse : ResultResponse
    {

    }
}
