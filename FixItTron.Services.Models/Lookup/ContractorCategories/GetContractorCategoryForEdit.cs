﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup
{
    public class GetContractorCategoryForEditRequest
    {
        public int ContractorCategoryId { get; set; }
    }

    public class GetContractorCategoryForEditResponse
    {
        public ContractorCategoryModel ContractorCategory { get; set; }
    }
}
