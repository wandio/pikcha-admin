﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseEmployee
{
    public class GetAbsencesByEmployeeRequest
    {
        public int EmployeeId { get; set; }
    }

    public class GetAbsencesByEmployeeResponse
    {
        public IEnumerable<GetAbsencesByEmployeeResponseItem> Items { get; set; }
    }

    public class GetAbsencesByEmployeeResponseItem
    {
        public int EmployeeAbsenseId { get; set; }
        public int AbsenseTypeId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Comment { get; set; }
        public Guid ExternalId { get; set; }
        public AbsenseStatus Status { get; set; }
    }
}
