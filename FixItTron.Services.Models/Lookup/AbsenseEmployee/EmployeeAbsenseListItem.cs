﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseEmployee
{
    public class EmployeeAbsenseListItem
    {
        public int EmployeeAbsenseId { get; set; }
        public string Employee { get; set; }
        public string AbsenseType { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public AbsenseStatus AbsenseStatus { get; set; }
        public string Comment { get; set; }
    }
}
