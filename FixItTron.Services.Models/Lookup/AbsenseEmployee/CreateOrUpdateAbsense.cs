﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseEmployee
{
    public class CreateOrUpdateAbsenceRequest
    {
        public CreateOrUpdateAbsenceItem EmployeeAbsence { get; set; }
    }
    public class CreateOrUpdateAbsenceResponse: Shared.ResultResponse
    {
        public Guid EmployeeAbsenseExtId { get; set; }
    }

    public class CreateOrUpdateAbsenceItem
    {
        public int EmployeeAbsenseId { get; set; }
        public int EmployeeId { get; set; }
        public int AbsenseTypeId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Comment { get; set; }
        public bool IsDeleted { get; set; }
        public Guid EmployeeAbsenseExtId { get; set; }
    }
}
