﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseEmployee
{
    public class GetEmployeeAbsenseForUpdateRequest
    {
        public int EmployeeAbsenseId { get; set; }
    }

    public class GetEmployeeAbsenseForUpdateResponse
    {
        public AddEditEmployeeAbsenseItem EmployeeAbsense { get; set; }
    }
}
