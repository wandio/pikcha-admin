﻿using Common.Enums;
using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseEmployee
{
    public class ChangeEmployeeAbsenseStatusRequest
    {
        public int EmployeeAbsenseId { get; set; }
        public AbsenseStatus AbsenseStatus { get; set; }
    }
    public class ChangeEmployeeAbsenseStatusResponse : ResultResponse
    {

    }
}

