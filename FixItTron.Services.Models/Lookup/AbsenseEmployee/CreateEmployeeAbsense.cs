﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseEmployee
{
    public class CreateEmployeeAbsenseRequest
    {
        public AddEditEmployeeAbsenseItem EmployeeAbsense { get; set; }
    }

    public class CreateEmployeeAbsenseResponse : ResultResponse
    {
        public int EmployeeAbsenseId { get; set; }

        public Guid EmployeeAbsenseExtId { get; set; }
    }
}
