﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.AbsenseEmployee
{
    public class GetEmployeeAbsenseListRequest : SortedAndPagedListRequestBase
    {
        public int? EmployeeId { get; set; }
        public int? AbsenseTypeId { get; set; }
        public UnboundedPeriod StartTime { get; set; }
        public UnboundedPeriod EndTime { get; set; }
        public AbsenseStatus? AbsenseStatus { get; set; }
        public string Comment { get; set; }
    }

    public class GetEmployeeAbsenseListResponse : PagedListResponseBase
    {
        public IEnumerable<EmployeeAbsenseListItem> EmployeeAbsenses { get; set; }
    }
}
