﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.AbsenseEmployee
{
    public class UpdateEmployeeAbsenseRequest
    {
        public AddEditEmployeeAbsenseItem EmployeeAbsense { get; set; }
    }

    public class UpdateEmployeeAbsenseResponse : ResultResponse
    {
    }
}
