﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.WarehouseTypes
{
    public class CreateWarehouseTypeRequest
    {
        public int WarehouseTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class CreateWarehouseTypeResponse : ResultResponse
    {
        public int WarehouseTypeId { get; set; }
    }
}
