﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.WarehouseTypes
{
    public class GetWarehouseTypeForUpdateRequest
    {
        public int WarehouseTypeId { get; set; }
    }

    public class GetWarehouseTypeForUpdateResponse
    {
        public WarehouseTypeListItem WarehouseType { get; set; }
    }
}
