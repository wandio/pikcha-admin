﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.WarehouseTypes
{
    public class WarehouseTypeListItem
    {
        public int WarehouseTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
