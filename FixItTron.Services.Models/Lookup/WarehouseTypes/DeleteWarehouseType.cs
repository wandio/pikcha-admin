﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.WarehouseTypes
{
    public class DeleteWarehouseTypeRequest
    {
        public int WarehouseTypeId { get; set; }
    }

    public class DeleteWarehouseTypeResponse : ResultResponse
    {

    }
}
