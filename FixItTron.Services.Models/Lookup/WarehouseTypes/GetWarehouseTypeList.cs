﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.WarehouseTypes
{
    public class GetWarehouseTypeListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
    }

    public class GetWarehouseTypeListResponse : PagedListResponseBase
    {
        public IEnumerable<WarehouseTypeListItem> WarehouseTypes { get; set; }
    }
}
