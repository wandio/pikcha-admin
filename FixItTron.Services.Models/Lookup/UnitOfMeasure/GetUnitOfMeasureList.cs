using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.UnitOfMeasure
{
    public class GetUnitOfMeasureListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
    }

    public class GetUnitOfMeasureListResponse : PagedListResponseBase
    {
        public IEnumerable<UnitOfMeasureListItem> UnitOfMeasures { get; set; }
    }
}
