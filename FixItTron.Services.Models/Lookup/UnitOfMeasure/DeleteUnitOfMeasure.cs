using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.UnitOfMeasure
{
    public class DeleteUnitOfMeasureRequest
    {
        public int UnitOfMeasureId { get; set; }
    }

    public class DeleteUnitOfMeasureResponse : ResultResponse
    {

    }
}
