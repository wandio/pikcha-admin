using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.UnitOfMeasure
{
    public class CreateUnitOfMeasureRequest
    {
        public int UnitOfMeasureId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class CreateUnitOfMeasureResponse : ResultResponse
    {
        public int UnitOfMeasureId { get; set; }
    }
}
