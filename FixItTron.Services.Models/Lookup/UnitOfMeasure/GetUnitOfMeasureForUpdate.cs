using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.UnitOfMeasure
{
    public class GetUnitOfMeasureForUpdateRequest
    {
        public int UnitOfMeasureId { get; set; }
    }

    public class GetUnitOfMeasureForUpdateResponse
    {
        public UnitOfMeasureListItem UnitOfMeasure { get; set; }
    }
}
