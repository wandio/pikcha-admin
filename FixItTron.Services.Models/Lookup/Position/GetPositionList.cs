﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.Position
{
    public class GetPositionListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public bool? IsActive { get; set; }
    }

    public class GetPositionListResponse : PagedListResponseBase
    {
        public IEnumerable<PositionListItem> Positions { get; set; }
    }
}
