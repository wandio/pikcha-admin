﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Position
{
    public class UpdatePositionRequest
    {
        public AddEditPositionItem Position { get; set; }
    }

    public class UpdatePositionResponse : ResultResponse
    {
    }
}
