﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Position
{
    public class CreatePositionRequest
    {
        public AddEditPositionItem Position { get; set; }
    }

    public class CreatePositionResponse : ResultResponse
    {
        public int PositionId { get; set; }
    }
}
