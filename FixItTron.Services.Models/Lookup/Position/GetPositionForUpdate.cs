﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Position
{
    public class GetPositionForUpdateRequest
    {
        public int PositionId { get; set; }
    }

    public class GetPositionForUpdateResponse
    {
        public AddEditPositionItem Position { get; set; }
    }
}
