﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Position
{
    public class PositionListItem
    {
        public int PositionId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
