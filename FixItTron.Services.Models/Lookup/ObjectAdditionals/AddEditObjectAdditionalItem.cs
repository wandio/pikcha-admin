﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ObjectAdditionals
{
    public class AddEditObjectAdditionalItem
    {
        public int ContractorObjectAdditionalItemId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
