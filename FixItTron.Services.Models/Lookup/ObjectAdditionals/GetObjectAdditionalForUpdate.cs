﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ObjectAdditionals
{
    public class GetObjectAdditionalForUpdateRequest
    {
        public int ContractorObjectAdditionalItemId { get; set; }
    }

    public class GetObjectAdditionalForUpdateResponse
    {
        public AddEditObjectAdditionalItem ObjectAdditional { get; set; }
    }
}
