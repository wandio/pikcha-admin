﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.ObjectAdditionals
{
    public class GetObjectAdditionalListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public bool? IsActive { get; set; }
    }

    public class GetObjectAdditionalListResponse : PagedListResponseBase
    {
        public IEnumerable<ObjectAdditionalListItem> ObjectAdditionals { get; set; }
    }
}
