﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ObjectAdditionals
{
    public class UpdateObjectAdditionalRequest
    {
        public AddEditObjectAdditionalItem ObjectAdditional { get; set; }
    }

    public class UpdateObjectAdditionalResponse : ResultResponse
    {
    }
}
