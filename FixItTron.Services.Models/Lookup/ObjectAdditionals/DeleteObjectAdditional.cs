﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ObjectAdditionals
{
    public class DeleteObjectAdditionalRequest
    {
        public int ContractorObjectAdditionalItemId { get; set; }
    }

    public class DeleteObjectAdditionalResponse : ResultResponse
    {

    }
}
