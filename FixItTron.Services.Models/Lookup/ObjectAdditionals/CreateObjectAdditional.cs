﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.ObjectAdditionals
{
    public class CreateObjectAdditionalRequest
    {
        public AddEditObjectAdditionalItem ObjectAdditional { get; set; }
    }

    public class CreateObjectAdditionalResponse : ResultResponse
    {
        public int ContractorObjectAdditionalItemId { get; set; }
    }
}
