﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup
{
    public class GetStreetListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public int? CityId { get; set; }
        public int? DistictId { get; set; }
    }

    public class GetStreetListResponse : PagedListResponseBase
    {
        public IEnumerable<StreetModel> Streets { get; set; }
    }
}
