﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Services.Models.Lookup
{
    public class EditStreetRequest
    {
        public int StreetId { get; set; }
        public string Name { get; set; }
        public int CityId { get; set; }
        public int? DistrictId { get; set; }

    }

    public class EditStreetResponse : ResultResponse
    {

    }
}
