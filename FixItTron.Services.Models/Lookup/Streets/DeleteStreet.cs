﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Services.Models.Lookup
{
    public class DeleteStreetRequest
    {
        public int StreetId { get; set; }
    }

    public class DeleteStreetResponse : ResultResponse
    {

    }
}
