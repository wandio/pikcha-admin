﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FixItTron.Services.Models.Lookup
{
    public class AddStreetRequest
    {
        public string Name { get; set; }
        public int CityId { get; set; }
        public int? DistrictId { get; set; }
    }

    public class AddStreetResponse : ResultResponse
    {

    }
}
