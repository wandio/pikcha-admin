﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.ApplicationUsers
{
    public class GetSurveyListRequest : SortedAndPagedListRequestBase
    {
        public string EventName { get; set; }
        public string Username { get; set; }
        public UserType? UserType { get; set; }
        public SourceType? SourceType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Gender? Gender { get; set; }
        public SurveyQuestion1Answer? Question1 { get; set; }
        public SurveyQuestion2Answer? Question2 { get; set; }
        public UnboundedPeriod CreateDate { get; set; }
    }
    public class GetSurveyListResponse : PagedListResponseBase
    {
        public IEnumerable<SurveyListItem> Surveys { get; set; }
    }

    public class SurveyListItem
    {
        public int SurveyId { get; set; }
        public string EventName { get; set; }
        public string Username { get; set; }
        public UserType UserType { get; set; }
        public SourceType SourceType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Common.Enums.Gender? Gender { get; set; }
        public Common.Enums.SurveyQuestion1Answer Question1 { get; set; }
        public Common.Enums.SurveyQuestion2Answer Question2 { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
