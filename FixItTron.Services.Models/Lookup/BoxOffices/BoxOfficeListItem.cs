﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.BoxOffices
{
    public class BoxOfficeListItem
    {
        public int BoxOfficeId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string CashRegisterNumber { get; set; }
        public decimal Balance { get; set; }
        public bool IsActive { get; set; }
    }
}
