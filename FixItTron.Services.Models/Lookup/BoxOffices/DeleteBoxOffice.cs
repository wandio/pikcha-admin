﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.BoxOffices
{
    public class DeleteBoxOfficeRequest
    {
        public int BoxOfficeId { get; set; }
    }

    public class DeleteBoxOfficeResponse : ResultResponse
    {

    }
}
