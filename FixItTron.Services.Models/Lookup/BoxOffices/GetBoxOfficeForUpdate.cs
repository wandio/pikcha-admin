﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.BoxOffices
{
    public class GetBoxOfficeForUpdateRequest
    {
        public int BoxOfficeId { get; set; }
    }

    public class GetBoxOfficeForUpdateResponse
    {
        public AddEditBoxOfficeItem BoxOffice { get; set; }
    }
}
