﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.BoxOffices
{
    public class GetBoxOfficeListRequest : SortedAndPagedListRequestBase
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string CashRegisterNumber { get; set; }
        public UnboundedRange<decimal> Balance { get; set; }
        public bool? IsActive { get; set; }
    }

    public class GetBoxOfficeListResponse : PagedListResponseBase
    {
        public IEnumerable<BoxOfficeListItem> BoxOffices { get; set; }
    }
}
