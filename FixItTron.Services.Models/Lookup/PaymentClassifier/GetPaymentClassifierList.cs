﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.PaymentClassifier
{
    public class GetPaymentClassifierListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public PaymentType? Type { get; set; }
        public bool? IsSystem { get; set; }
    }

    public class GetPaymentClassifierListResponse : PagedListResponseBase
    {
        public IEnumerable<PaymentClassifierListItem> PaymentClassifiers { get; set; }
    }
}
