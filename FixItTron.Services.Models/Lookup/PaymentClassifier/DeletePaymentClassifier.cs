﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.PaymentClassifier
{
    public class DeletePaymentClassifierRequest
    {
        public int PaymentClassifierId { get; set; }
    }

    public class DeletePaymentClassifierResponse : ResultResponse
    {

    }
}
