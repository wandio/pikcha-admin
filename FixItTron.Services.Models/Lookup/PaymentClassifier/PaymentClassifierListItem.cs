﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.PaymentClassifier
{
    public class PaymentClassifierListItem
    {
        public int PaymentClassifierId { get; set; }
        public string Name { get; set; }
        public bool IsSystem { get; set; }
        public PaymentType Type { get; set; }
    }
}
