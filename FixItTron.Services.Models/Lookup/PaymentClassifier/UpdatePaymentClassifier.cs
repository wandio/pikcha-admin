﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.PaymentClassifier
{
    public class UpdatePaymentClassifierRequest
    {
        public AddEditPaymentClassifierItem PaymentClassifier { get; set; }
    }

    public class UpdatePaymentClassifierResponse : ResultResponse
    {
    }
}
