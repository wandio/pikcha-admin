﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.PaymentClassifier
{
    public class GetPaymentClassifierForUpdateRequest
    {
        public int PaymentClassifierId { get; set; }
    }

    public class GetPaymentClassifierForUpdateResponse
    {
        public AddEditPaymentClassifierItem PaymentClassifier { get; set; }
    }
}
