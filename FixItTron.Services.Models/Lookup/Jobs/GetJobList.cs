﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup
{
    public class GetJobListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class GetJobListResponse : PagedListResponseBase
    {
        public IEnumerable<JobModel> Jobs { get; set; }
    }
}
