﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Lookup.Countries
{
    public class GetCountryListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
    }

    public class GetCountryListResponse : PagedListResponseBase
    {
        public IEnumerable<CountryListItem> Countries { get; set; }
    }
}
