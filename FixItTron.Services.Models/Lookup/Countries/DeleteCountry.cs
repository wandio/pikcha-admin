﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Countries
{
    public class DeleteCountryRequest
    {
        public int CountryId { get; set; }
    }

    public class DeleteCountryResponse : ResultResponse
    {

    }
}
