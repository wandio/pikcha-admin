﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Countries
{
    public class IsCountryGeorgiaRequest
    {
        public int? CountryId { get; set; }
    }
    public class IsCountryGeorgiaResponse : ResultResponse
    {

    }
}
