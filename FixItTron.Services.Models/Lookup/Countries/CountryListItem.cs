﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Countries
{
    public class CountryListItem
    {
        public int CountryId { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
    }
}
