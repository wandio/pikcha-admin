﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Countries
{
    public class CreateCountryRequest
    {
        public int CountryId { get; set; }
        public string Name { get; set; }
        public bool IsDefault { get; set; }
    }

    public class CreateCountryResponse : ResultResponse
    {
        public int CountryId { get; set; }
    }
}
