﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Lookup.Countries
{
    public class GetCountryForEditRequest
    {
        public int CountryId { get; set; }
    }

    public class GetCountryForEditResponse
    {
        public CountryListItem Country { get; set; }
    }
}
