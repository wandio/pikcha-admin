﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class UploadIsAvailableRequest
    {
        public string UserName { get; set; }
        public UserType UserType { get; set; }
    }
    public class UploadIsAvailableResponse
    {
        public bool Available { get; set; }
    }
}
