﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class ValidateEventCodeRequest
    {
        public string EventCode { get; set; }
        public string UserName { get; set; }
        public UserType UserType { get; set; }
    }
    public class ValidateEventCodeResponse
    {
        public bool Success { get; set; }
        public bool IsValid { get; set; }
        public bool HasSurvey { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public bool SurveySubmitted { get; set; }
        public int EventId { get; set; }
    }
}
