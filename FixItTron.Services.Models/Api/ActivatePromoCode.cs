﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class ActivatePromoCodeRequest
    {
        public string PromoCode { get; set; }
        public string UserName { get; set; }

        public UserType UserType { get; set; }
    }
    public class ActivatePromoCodeResponse
    {
        public bool Success { get; set; }
        public bool PromoCodeExists { get; set; }
        public bool PromoCodeIsAlreadyUsed { get; set; }
    }
}
