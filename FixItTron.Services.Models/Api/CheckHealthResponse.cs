﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class CheckHealthResponse
    {
        public bool IsHealty { get; set; }
    }
}
