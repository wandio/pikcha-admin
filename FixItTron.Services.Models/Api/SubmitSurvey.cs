﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class SubmitSurveyRequest
    {
        public int EventId { get; set; }
        public string UserName { get; set; }
        public UserType UserType { get; set; }
        public SourceType SourceType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Common.Enums.SurveyQuestion1Answer Question1Answer { get; set; }
        public Common.Enums.SurveyQuestion2Answer Question2Answer { get; set; }
    }

    public class SubmitSurveyResponse
    {
        public bool Success { get; set; }
        public int SurveyId { get; set; }
        public string ErrorMessage { get; set; }
    }
}
