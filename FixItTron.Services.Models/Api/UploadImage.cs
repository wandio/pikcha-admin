﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class UploadImageRequest
    {
        public string ImageBase64 { get; set; }
        public SourceType Source { get; set; }
    }

    public class UploadImageResponse
    {
        public int PhotoId { get; set; }
        public bool Success { get; set; }
    }

    /* public enum SourceType
    {
        Web = 1,
        IOS = 2,
        Android = 3
    }*/
}
