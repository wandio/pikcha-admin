﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class CreateSurveyRequest
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Common.Enums.Gender Gender { get; set; }
        public int EventId { get; set; }
    }

    public class CreateSurveyResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
