﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public enum UserType : int
    {
        Device = 1,
        Facebook = 2
    }
}
