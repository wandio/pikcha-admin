﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class GetFacebookShareLogoResponse
    {
        public string LogoUrl { get; set; }
    }
    public class GetInstagramShareLogoResponse
    {
        public string LogoUrl { get; set; }
    }
    public class GetPreviewLogoResponse
    {
        public string LogoUrl { get; set; }
    }
}
