﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class GetEventSurveyRequest
    {
        public string PageId { get; set; }
        public string UserId { get; set; }
    }

    public class GetEventSurveyResponse
    {
        public int EventId { get; set; }
        public bool ShowSurvey { get; set; }
        public string FacebookAppSecret { get; set; }
        public string FacebookPageId { get; set; }
        public string FacebookPageAccessToken { get; set; }
        public bool SurveyExists { get; set; }
        public bool SurveyFilled { get; set; }
        public bool PartiallyFilled { get; set; }
    }
}
