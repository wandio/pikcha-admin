﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class CreateUserRequest
    {
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public UserType UserType { get; set; }
    }

    public class CreateUserResponse
    {
        public bool Success { get; set; }
        public bool UserExists { get; set; }
        public int UserId { get; set; }
    }
}
