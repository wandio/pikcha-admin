﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class GetUserInfoRequest
    {
        public string UserName { get; set; }
        public UserType UserType { get; set; }
    }
    public class GetUserInfoResponse
    {
        public int AvailablePrints { get; set; }
        public int UsedPrints { get; set; }
    }
}
