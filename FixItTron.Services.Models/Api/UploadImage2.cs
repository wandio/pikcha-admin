﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class UploadImage2Request
    {
        public string UserName { get; set; }
        public string ImageBase64 { get; set; }
        public SourceType Source { get; set; }
        public UserType UserType { get; set; }
    }
    public class UploadImage2Response
    {
        public int PhotoId { get; set; }
        public bool Success { get; set; }
    }
}
