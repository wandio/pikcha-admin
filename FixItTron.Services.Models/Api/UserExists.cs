﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class UserExistsRequest
    {
        public string UserName { get; set; }
        public UserType UserType { get; set; }
    }
    public class UserExistsResponse
    {
        public bool Exists { get; set; }
        public int UserId { get; set; }
    }
}
