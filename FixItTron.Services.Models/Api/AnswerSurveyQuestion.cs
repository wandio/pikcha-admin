﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Api
{
    public class AnswerSurveyQuestion1Request
    {
        public int UserId { get; set; }
        public Common.Enums.SurveyQuestion1Answer Question1Answer { get; set; }
    }

    public class AnswerSurveyQuestion1Response
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class AnswerSurveyQuestion2Request
    {
        public int UserId { get; set; }
        public Common.Enums.SurveyQuestion2Answer Question2Answer { get; set; }
    }

    public class AnswerSurveyQuestion2Response
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
