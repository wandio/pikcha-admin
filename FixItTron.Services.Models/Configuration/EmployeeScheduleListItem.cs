﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Configuration
{
    public class EmployeeScheduleListItem
    {
        public int EmployeeScheduleId { get; set; }
        public int Employeeid { get; set; }
        public DateTime Date { get; set; }
        public bool? NightShift { get; set; }
        public bool? DayShift { get; set; }
    }
}
