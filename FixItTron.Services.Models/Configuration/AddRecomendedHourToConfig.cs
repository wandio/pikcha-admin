﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Configuration
{
    public class AddRecomendedHourToConfigRequest
    {
        public int ConfigId { get; set; }
        public float Hour { get; set; }
    }
    public class AddRecomendedHourToConfigResponse:ResultResponse
    {

    }
}
