﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Configuration
{

    public class EditGeneralScheduleConfigRequest
    {
        public int GeneralScheduleConfigId { get; set; }
        public float? WorkingHourStart { get; set; }
        public float? WorkingHourEnd { get; set; }
        public float? DurationBetweenSchedules { get; set; }
    }

    public class EditGeneralScheduleConfigResponse : ResultResponse
    {

    }
}
