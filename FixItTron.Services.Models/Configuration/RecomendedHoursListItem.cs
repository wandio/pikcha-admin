﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Configuration
{
    public class RecomendedHoursListItem
    {
        public int RecomendedHourId { get; set; }
        public int GeneralScheduleConfigId { get; set; }
        public float Hour { get; set; }
    }
}
