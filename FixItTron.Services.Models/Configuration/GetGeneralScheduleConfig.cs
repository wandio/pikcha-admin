﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Configuration
{
    public class GetGeneralScheduleConfigRequest
    {
        public int Id { get; set; }
    }
    public class GetGeneralScheduleConfigResponse
    {
        public GeneralScheduleConfigItem GeneralScheduleConfig { get; set; }
    }

}