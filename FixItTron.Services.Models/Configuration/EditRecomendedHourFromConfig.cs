﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Configuration
{
    public class EditRecomendedHourFromConfigRequest
    {
        public int hourId { get; set; }
        public float? newHour { get; set; }
    }

    public class EditRecomendedHourFromConfigResponse:ResultResponse
    {

    }
}
