﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class CreateOrderFromAgreementRequest
    {
        public int ContractorId { get; set; }
        public int AgreementId { get; set; }
    }

    public class CreateOrderFromAgreementResponse : ResultResponse
    {

    }

}
