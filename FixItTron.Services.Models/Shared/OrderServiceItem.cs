﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class OrderServiceItem
    {
        public int ServiceId { get; set; }

        public string ServiceNumber { get; set; }

        public string ServiceName { get; set; }

        public decimal ItemPrice { get; set; }

        public string Category { get; set; }

        public string UnitOfMeasure { get; set; }

        public int UnitOfMeasureId { get; set; }

        public decimal SelectedQuantity { get; set; }

        public bool IsHourlyMeasuring { get; set; }

        public int? ParentServiceId { get; set; }
    }
}
