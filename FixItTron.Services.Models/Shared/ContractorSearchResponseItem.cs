﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class ContractorSearchResponseItem
    {
        public int ContractorId { get; set; }
        public string Name { get; set; }
        public string IdentificationCode { get; set; }
        public bool BlackList { get; set; }
        public LegalForm LegalForm { get; set; }
        public string Category { get; set; }
        public bool HasAgreement { get; set; }
        public decimal? Balance { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public ContractorStatus Status { get; set; }
    }
}
