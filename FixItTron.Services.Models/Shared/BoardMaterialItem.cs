﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class BoardMaterialItem
    {
        public int MaterialId { get; set; }

        public string BoardNumber { get; set; }

        public string Code { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string ProductType { get; set; }

        public decimal ItemPrice { get; set; }

        public decimal AvailableQuantity { get; set; }

        public decimal MinimalQuantity { get; set; }

        public string Category { get; set; }

        public string UnitOfMeasure { get; set; }
    }
}
