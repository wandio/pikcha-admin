﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class SalesQuotationLineModel
    {
        public int SalesQuotationLineId { get; set; }
        public string ProductCategory { get; set; }
        public string ProductName { get; set; }
        public string BaseUnitOfMeasure { get; set; }
        public string UnitOfMeasure { get; set; }
        public string TaxGroup { get; set; }
        public string Comment { get; set; }

        public int? ParentLineId { get; set; }
        public bool HourlyMeasuring { get; set; }

        public int SalesQuotationId { get; set; }
        public int ProductId { get; set; }
        public int BaseUnitOfMeasureId { get; set; }
        public int UnitOfMeasureId { get; set; }
        public decimal BaseQuantity { get; set; }
        public decimal Quantity { get; set; }
        public bool IsServiceLineItem { get; set; }
        /// <summary>
        /// Sales price including everything, including tax
        /// </summary>
        public decimal SalesPrice { get; set; }
        public int? TaxGroupId { get; set; }
        public decimal? PriceWithoutTax { get; set; }
        public decimal? TaxAmount { get; set; }
        public decimal? TaxPercent { get; set; }
        public decimal DiscountAmount { get; set; }

        public List<int> AllowedMeasures { get; set; }
    }
}
