﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class SalesQuotationScheduleLineModel
    {
        public int? SupportScheduleId { get; set; }
        public int? SupportId { get; set; }
        public bool IsSupport { get; set; }
        public int EmployeeId { get; set; }
        public int SalesQuotationScheduleId { get; set; }
        public DateTimeItem DatePicker { get; set; }
        public string Comment { get; set; }
        public IEnumerable<ScheduleAdditionalStatusItem> AdditionalStatuses { get; set; }
        public SalesOrderScheduleStatus OrderScheduleStatus { get; set; }
        public SalesOrderScheduleDoneStatus? OrderScheduleDoneStatus { get; set; }
        public string CancelReason { get; set; }
        public int SalesQuotationId { get; set; }
        public List<SalesOrderScheduleServiceItem> ServiceIds { get; set; }
        public bool EndHourIsInNextDay { get; set; }
        //for edit container
        public string EmployeeName { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string SupportName { get; set; }
    }
}
