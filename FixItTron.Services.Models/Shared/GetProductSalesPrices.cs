﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class GetProductSalesPricesRequest
    {
        public int ProductId { get; set; }
        public int? CustomerId { get; set; }

        public GetProductSalesPricesRequest(int productId, int? customerId = null, int? priceListId = null)
        {
            ProductId = productId;
            CustomerId = customerId;
            PriceListId = priceListId;
        }

        public int? PriceListId { get; set; }
    }

    public class GetProductSalesPricesResponse
    {
        public int ProductId { get; set; }
        public int? CustomerId { get; set; }
        public int? PriceListId { get; set; }
        /// <summary>
        /// All prices, including base price. Key is measureId, value is sales price
        /// </summary>
        public IDictionary<int, decimal> Prices { get; set; }
        public GetProductSalesPricesResponse()
        {
            Prices = new Dictionary<int, decimal>();
        }
    }
}
