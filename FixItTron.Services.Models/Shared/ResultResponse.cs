﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class ResultResponse
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}
