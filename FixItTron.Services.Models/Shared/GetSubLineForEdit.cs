﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class GetSubLineForEditRequest
    {
        public int LineId { get; set; }
        public int OrderId { get; set; }
    }

    public class GetSubLineForEditResponse
    {
        public int OrderId { get; set; }
        public int ParentLineId { get; set; }
        public int LineId { get; set; }
        public int ProductId { get; set; }
        public int UnitOfMeasureId { get; set; }
        public decimal Quantity { get; set; }
        public string Comment { get; set; }

        public object ProductName { get; set; }
    }
}
