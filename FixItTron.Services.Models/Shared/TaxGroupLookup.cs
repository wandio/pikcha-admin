﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class TaxGroupLookup
    {
        public int TaxGroupId { get; set; }
        public string Name { get; set; }
        public decimal Percent { get; set; }
    }
}
