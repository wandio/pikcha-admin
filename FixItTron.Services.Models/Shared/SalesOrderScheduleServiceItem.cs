﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class SalesOrderScheduleServiceItem
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
    }
}
