﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class ContractorAgreementLookup : LookupModel
    {
        public Common.Enums.PaymentTerm PaymentTerm { get; set; }
    }
}
