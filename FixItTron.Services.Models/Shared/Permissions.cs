﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    [Flags]
    public enum Permissions
    {
        None = 0,
        Create = 1 << 0,
        Read = 1 << 1,
        Update = 1 << 2, //4
        Delete = 1 << 3, //8
        PowerPermission = 1 << 4,// 16
        Book = 1 << 5,//32
        Approve = 1 << 6,//64
        Post = 1 << 7,//128
        Cancel = 1 << 8,
        FullCrud = Permissions.Create | Permissions.Read | Permissions.Update | Permissions.Delete,
        PostCrud = Permissions.Book | Permissions.Approve | Permissions.Post | Permissions.Cancel,
        All = FullCrud | PostCrud | PowerPermission
    }
}
