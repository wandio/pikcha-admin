﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class ContractorSearchRequest
    {
        public int? ContractorId { get; set; }
        public string Term { get; set; }
        public bool IsCustomer { get; set; }
        public bool IsVendor { get; set; }
        public bool DisableNewCustomer { get; set; }
    }

    public class ContractorSearchResponse
    {
        public IEnumerable<ContractorSearchResponseItem> Items { get; set; }
    }
}
