﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public enum LegalFormModel
    {
        LegalEntity = 1,
        Individuals = 2
    }
}
