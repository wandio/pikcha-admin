﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class WarehouseProductRequest
    {
        public int ProductId { get; set; }
        public decimal Quantity { get; set; }
    }
}
