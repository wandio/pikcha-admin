﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class JobModel
    {
        public int JobId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
