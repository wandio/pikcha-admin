﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class DistrictModel
    {
        public int DistrictId { get; set; }
        public int CityId { get; set; }
        public string City { get; set; }
        public string Name { get; set; }
    }
}
