﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class ContractorObjectLookup
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CountryName { get; set; }
    }
}
