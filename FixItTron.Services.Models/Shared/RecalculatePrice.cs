﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class RecalculatePriceRequest
    {
        public int OrderId { get; set; }

        public int? PriceRuleId { get; set; }

        public DiscountType? DiscountType { get; set; }

        public decimal? DiscountVal { get; set; }

        public SalesType? SalesType { get; set; }

        public int? AgreementId { get; set; }
    }

    public class RecalculatePriceResponse : ResultResponse
    {

        public decimal? TotalDiscountAmount { get; set; }

        public decimal TotalPrice { get; set; }

        public decimal? TotalTaxAmount { get; set; }

        public decimal? TotalAmountWithoutTax { get; set; }
    }
}
