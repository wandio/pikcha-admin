﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class ScheduleAdditionalStatusItem
    {
        public int OrderId { get; set; }
        public int ScheduleStatusId { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
        public string Icon { get; set; }
        public SalesOrderScheduleStatus Status { get; set; }
    }
}
