﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public enum Securable
    {
        None = 0,
        Roles = 1,
        Employees = 2,
        Customers = 3,
        SalesOrders = 4,
        PriceRules = 5,
        Reasons = 6,
        Invoices = 7,
        InboundPayments = 8,
        CustomerFinancials = 9,
        SalesQuotations = 10,
        Schedules = 11,
        ObjectAdditionals = 12,
        ContractorCategories = 13,
        PriceLists = 14,
        Jobs = 15,
        TaxGroups = 16,
        Countries = 17,
        Cities = 18,
        Streets = 19,
        Districts = 20,
        ScheduleAdditionalStatuses = 21,
        Services = 22,
        Materials = 23,
        ServiceCategories = 24,
        MaterialCategories = 25,
        Warehouses = 26,
        WarehouseTypes = 27,
        ServiceClasses = 28,
        MaterialTypes = 29,
        UnitOfMeasures = 30,
        BankOperations = 31,
        BoxOfficeOperations = 32,
        BankAccounts = 33,
        BoxOffices = 34,
        PaymentClassifiers = 35,
        Banks = 36,
        Currencies = 37,
        SystemConfiguration = 38,
        Positions = 39,
        ScheduleGeneralConfiguration = 40,
        WorkingSchedules = 41,
        EmployeeSchedule = 42,
        AbsenceTypes = 43,
        EmployeeAbsences = 44,
        CodeGenerations = 45,
        Questionnaires = 46,
        NotificationTemplates = 47
    }
}
