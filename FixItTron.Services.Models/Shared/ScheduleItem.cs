﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class ScheduleItem
    {
        public int OrderId { get; set; }
        public int ScheduleId { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public int EmployeeId { get; set; }
        public int? SupportId { get; set; }
        public int? SupportScheduleId { get; set; }
        public bool IsSupport { get; set; }
        public string Comment { get; set; }
        public bool? EmployeeArrived { get; set; }
        public int OrderScheduleStatus { get; set; }
        public int? OrderScheduleDoneStatus { get; set; }
        public int  CancelReasonId { get; set; }
        public string CancelComment { get; set; }
        public bool? IsApproved { get; set; }

        public IEnumerable<ScheduleServiceItem> Services { get; set; }
        public IEnumerable<AdditionalStatus> AdditionalStatuses { get; set; }
    }
    public class AdditionalStatus
    {
        public string Status { get; set; }
        public string Icon { get; set; }
    }
    public class ScheduleServiceItem
    {
        public int ScheduleId { get; set; }

        public int ServiceId { get; set; }
    }
}
