﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public enum AddressTypeModel
    {
        Legal = 1,
        Physical = 2
    }
}
