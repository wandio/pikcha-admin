﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class DateTimeItem
    {
        public DateTime? Date { get; set; }
        public float? EndHour { get; set; }
        public float? StartHour { get; set; }
        public bool EndHourIsInNextDay { get; set; }
    }
}
