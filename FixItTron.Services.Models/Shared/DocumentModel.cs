﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Shared
{
    public class DocumentModel
    {
        public int DocumentId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public UploadedFileModel File { get; set; }
    }
}
