﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Company
{
    public class DeleteCompanyRequest
    {
        public int CompanyId { get; set; }
    }

    public class DeleteCompanyResponse : ResultResponse
    {

    }
}
