﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Company
{
    public class CreateCompanyRequest
    {
        public AddEditCompanyItem Company { get; set; }
    }

    public class CreateCompanyResponse : ResultResponse
    {
        public int CompanyId { get; set; }
    }
}
