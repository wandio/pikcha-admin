﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Company
{
    public class GetCompanyListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
    }

    public class GetCompanyListResponse : PagedListResponseBase
    {
        public IEnumerable<CompanyListItem> Companies { get; set; }
    }
}
