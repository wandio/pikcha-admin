﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Company
{
    public class GetCompanyForUpdateRequest
    {
        public int CompanyId { get; set; }
    }

    public class GetCompanyForUpdateResponse
    {
        public AddEditCompanyItem Company { get; set; }
    }
}
