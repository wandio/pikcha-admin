﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.ImageUpload
{
    public class AddImageItem
    {
        public string Username { get; set; }
        public byte[] Data { get; set; }
    }
}
