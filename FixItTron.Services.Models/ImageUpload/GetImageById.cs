﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.ImageUpload
{
    public class GetImageByIdRequest
    {
        public int Id { get; set; }
        public int EventId { get; set; }
    }

    public class GetImageByIdResponse : ResultResponse
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }  
    }
}
