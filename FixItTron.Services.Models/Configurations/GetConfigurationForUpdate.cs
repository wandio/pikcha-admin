﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Configurations
{
    public class GetConfigurationForUpdateRequest
    {
        public int ConfigurationId { get; set; }
    }

    public class GetConfigurationForUpdateResponse
    {
        public ConfigurationItem Configuration { get; set; }
    }
}
