﻿using FixItTron.Services.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.Services.Models.Configurations
{
    public class UpdateConfigurationRequest
    {
        public int ConfigurationId { get; set; }
        public string Value { get; set; }
    }

    public class UpdateConfigurationResponse : ResultResponse
    {
    }
}
