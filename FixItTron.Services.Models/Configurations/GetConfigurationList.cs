﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wandio.Domain.Services.Models;

namespace FixItTron.Services.Models.Configurations
{
    public class GetConfigurationListRequest : SortedAndPagedListRequestBase
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }

    public class GetConfigurationListResponse : PagedListResponseBase
    {
        public IEnumerable<ConfigurationItem> Configurations { get; set; }
    }
}
