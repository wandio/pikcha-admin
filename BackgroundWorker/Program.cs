﻿using FixItTron.Domain.Db;
using FixItTron.Domain.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.BackgroundWorker
{
    public class Program
    {
        public const string LogSourceName = "FixItTronWorker";
        public const string LogName = "FixItTronConsole";
        public const string GenerateInvoice = "-I";
        public const string GenerateSalesOrder = "-S";

        static void Main(string[] args)
        {
            if (!EventLog.SourceExists(LogSourceName))
            {
                EventLog.CreateEventSource(LogSourceName, LogName);
            }

            var _log = new EventLog(LogName);
            _log.Source = LogSourceName;

            using (var db = new FixItTronDbContext())
            {
                try
                {
                    var processor = new SchedulerProcessor(db);

                    if (args.Any())
                    {
                        foreach (var arg in args)
                        {
                            switch (arg)
                            {
                                case GenerateInvoice:
                                    processor.ProcessInvoices(_log);
                                    break;
                                case GenerateSalesOrder:
                                    processor.ProcessOrders(_log);
                                    break;
                            }
                        }
                    }
                    else
                    {
                        processor.ProcessOrders(_log);
                        processor.ProcessInvoices(_log);
                    }

                    _log.WriteEntry(string.Format("Process finished: {0}", DateTime.Now), EventLogEntryType.Information);
                }

                catch (Exception ex)
                {
                    Extensions.LogError(_log, ex);
                }
            }
        }
    }
}
