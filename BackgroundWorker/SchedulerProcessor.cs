﻿using FixItTron.Domain.Db;
using FixItTron.Domain.Model;
using FixItTron.Services;
using FixItTron.Services.Helpers;
using FixItTron.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.BackgroundWorker
{
    public class SchedulerProcessor
    {
        FixItTronDbContext Db;
        DateTime today;
        DateTime tomorrow;

        public SchedulerProcessor(FixItTronDbContext db)
        {
            Db = db;

            today = DateTime.Today;
            tomorrow = DateTime.Today.AddDays(1);
        }

        public void ProcessOrders(EventLog log)
        {
            ISalesOrderService service = new SalesOrderService(Db);

            var response = service.ProcessScheduledOrders(new Services.Models.SalesOrder.ProcessScheduledOrdersRequest());
            if (!response.Success)
            {
                foreach (var message in response.ErrorMessages)
                {
                    Extensions.LogError(log, message);
                }
            }
        }

        public void ProcessInvoices(EventLog log)
        {
            IInvoiceService service = new InvoiceService(Db);

            var response = service.ProcessScheduledInvoices(new FixItTron.Services.Models.Invoice.ProcessScheduledInvoicesRequest());
            if (!response.Success)
            {
                foreach (var message in response.ErrorMessages)
                {
                    Extensions.LogError(log, message);
                }
            }
        }
    }
}
