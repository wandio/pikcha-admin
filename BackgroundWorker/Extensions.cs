﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FixItTron.BackgroundWorker
{
    public static class Extensions
    {
        public static void LogSuccess(EventLog log, string message)
        {
            log.WriteEntry(string.Format("Processed {0}", message), EventLogEntryType.SuccessAudit);
        }

        public static void LogError(EventLog log, Exception ex)
        {
            log.WriteEntry(string.Format("Failed: {0}", ex.ToString()), EventLogEntryType.Error);
        }

        public static void LogError(EventLog log, string message)
        {
            log.WriteEntry(string.Format("Failed: {0}", message), EventLogEntryType.Error);
        }
    }
}
